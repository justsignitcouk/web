(function($)
{
    /**
     * Auto-growing textareas; technique ripped from Facebook
     *
     * https://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function(options)
    {
        return this.filter('textarea').each(function()
        {
            var self         = this;
            var $self        = $(self);
            var minHeight    = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;

            var shadow = $('<div></div>').css({
                position:    'absolute',
                top:         -10000,
                left:        -10000,
                width:       $self.width(),
                fontSize:    $self.css('fontSize'),
                fontFamily:  $self.css('fontFamily'),
                fontWeight:  $self.css('fontWeight'),
                lineHeight:  $self.css('lineHeight'),
                resize:      'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function(event)
            {
                var times = function(string, number)
                {
                    for (var i=0, r=''; i<number; i++) r += string;
                    return r;
                };

                var val = self.value.replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/&/g, '&amp;')
                    .replace(/\n$/, '<br/>&nbsp;')
                    .replace(/\n/g, '<br/>')
                    .replace(/ {2,}/g, function(space){ return times('&nbsp;', space.length - 1) + ' ' });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.
                $self.height(Math.max(shadow.height() + noFlickerPad, minHeight));
            }

            $self.change(update).keyup(update).keydown({event:'keydown'},update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);


var noteTemp =  '<div class="noteeee">'
    +	'<a href="javascript:;" class="button remove">X</a>'
    + 	'<div class="note_cnt">'
    +		'<textarea class="title" placeholder="Enter note title"></textarea>'
    + 		'<textarea class="cnt" placeholder="Enter note description here"></textarea>'
    +	'</div> '
    +'</div>';

var noteZindex = 1;
function deleteNote(){
    $(this).parent('.note2').hide("puff",{ percent: 133}, 250);
};



function newNote(id,content,full_name,created_at,account_id,positions) {
    var noteTemp =  '<div class=" note2 note_'+ id +'" id="note_' + id + '" style="'+ positions +'" data-id="'+ id +'">'
        + 	'<div class="note_cnt">'
        +		'<div class="title">'+ full_name +'</div>'
        +		'<div class="title">'+ created_at +'</div>'
        + 		'<div>'+ content +'</div>'
        +	'</div> '
        +'</div>';
    setTimeout(function(){
        $('.sticky-note').append(noteTemp).show("fade", 300);
        // $('#note_' + id).draggable({ containment: "parent" }).on('dragstart',
        //     function(){
        //         $(this).zIndex(++noteZindex);
        //     }).on('dragstop',function(){
        //     var top = $(this).css('top');
        //     var bottom = $(this).css('bottom');
        //     var left = $(this).css('left');
        //     var right = $(this).css('right');
        //     var positions = 'top:' +  top + ';bottom:' + bottom + ';left:' + left + ';right:' + right + ';';
        //     var id = $(this).data('id');
        //
        //     $.post('/document/notes/savePosition',{'positions':positions,'note_id':id},function(data){
        //         console.log(data);
        //     },'json');
        // });
    },1000);



    $('.remove').click(deleteNote);

    $('.note')
    return false;
};


$(document).ready(function() {


    //$("#board").height($(document).height());

    $("#add_new").click(newNote);

    $('.remove').click(deleteNote);


    return false;
});