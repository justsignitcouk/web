/*document.oncontextmenu = function () {
    return false
}
if (document.layers) {
    window.captureEvents(Event.MOUSEDOWN);
    window.onmousedown = function (e) {
        if (e.target == document) return false;
    }
}
else {
    document.onmousedown = function () {
        return false
    }
}*/


/*(function () {
    var headTag = document.getElementsByTagName('head')[0];
    var headContent = headTag.innerHTML;
    headContent = headContent.replace("<!-- This is Squarespace. -->", "");
    headTag.innerHTML = headContent;
})();*/

var aTo = [] ;
var aSigner = [] ;
var aCC = [] ;

$(document).ready(function () {

    //user is "finished typing," do something








    $.fn.document_block($('.sheet'));


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    function drawReceipts(type, data_type, data) {

        if (data_type == 'account') {
            $.each(data, function (k, v) {
                var description = v.account.account_name;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }
                var recipient_type = 'To' ;
                if(type=='to'){
                    recipient_type = 'To' ;
                }

                if(type=='cc'){
                    recipient_type = 'CC' ;
                }

                if(type=='signer'){
                    recipient_type = 'Signer' ;
                }


                $.fn.drawRecipient({'first_name' : v.account.user.first_name,'last_name' : v.account.user.last_name ,'data_type' : recipient_type ,'id' : v.account.id },true);

                //$('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
            });
        }

        if (data_type == 'contact') {
            $.each(data, function (k, v) {
                var description = v.contact.display_name;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }
                var recipient_type = 'To' ;
                if(type=='to'){
                    recipient_type = 'To' ;
                }

                if(type=='cc'){
                    recipient_type = 'CC' ;
                }

                if(type=='signer'){
                    recipient_type = 'Signer' ;
                }


                $.fn.drawRecipient({'first_name' : v.contact.first_name,'last_name' : v.contact.last_name ,'data_type' : recipient_type ,'id' : v.contact.id },true);

            });
        }

        // if (data_type == 'department') {
        //
        //     $.each(data, function (k, v) {
        //         var description = v.department.name;
        //         var orign_description = description;
        //         if (v.description !== null) {
        //             description = v.description;
        //         }
        //         $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
        //     });
        //
        // }

        // if (data_type == 'role') {
        //     $.each(data, function (k, v) {
        //         var description = v.role.name;
        //         var orign_description = description;
        //         if (v.description !== null) {
        //             description = v.description;
        //         }
        //         $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
        //     });
        // }

        // if (data_type == 'invitations') {
        //     $.each(data, function (k, v) {
        //         var description = v.description;
        //         var orign_description = description;
        //         if (v.description !== null) {
        //             description = v.description;
        //         }
        //         $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
        //     });
        // }
    }




    function drawWorkFlow(document_workflow, oDocument) {
        var current_stage = document_workflow.wf_current_stage_id;
        var work_flow = document_workflow.work_flow;
        var stages = document_workflow.work_flow.wf_stage;

        $.each(stages, function (stage_id, stage) {

            if (current_stage !== stage.id) {
                return true;
            }

            var states = stage.wf_state;
            drawSigners(oDocument, null);

            $.each(states, function (state_id, state) {
                var state_actions = state.wf_state_action;

                $.each(state_actions, function (state_action_id, action) {
                    var action = action.wf_action;

                    if (action.name == 'Sign') {
                        //drawSigners(oDocument,state_id);
                    }
                });

            });

        });

    }

    function drawSigners(data, state_id) {

        var signer_accounts = data.signer_account;
        $.each(signer_accounts, function (account_id, signer_account) {
            var account = signer_account.account;
            var html = '<div class="sign_item">';

            if (signer_account.desciption == undefined) {
                html += signer_account.account.user.first_name + ' ' + signer_account.account.user.last_name;
            } else {
                html += signer_account.desciption;
            }

/*            if (signer_account.is_sign == 1) {
                html += '<img src="' + signer_account.document_sign_data.sign_image + '" class="sign_image" width="100%"/>';
            } else if (signer_account.is_sign == 0 && signer_account.account_id == globals.account_id && data.status == 'inprogress') {
                html += '<div class="input_signer"' +
                    'style="display: inline-block;">' +
                    '<img src="/assets/images/sign_here.png"' +
                    'class="sign"' +
                    'id="sign_' + signer_account.account_id + '"' +
                    'data-id="' + signer_account.account_id + '"' +
                    'data-state_id="' + state_id + '"' +
                    'style="width: 30px;cursor: pointer"/>' +
                    '</div>';
            }*/

            html += '</div>';
            //$('.workflow_action').append(html);
            $('.signer_document_editor').append(html);


        });


        var signer_accounts = data.signer_invitations;
        $.each(signer_accounts, function (account_id, signer_account) {
            var account = signer_account.account;
            var html = '<div class="sign_item">';

            if (signer_account.desciption == undefined) {
                html += signer_account.invitations.email;
            } else {
                html += signer_account.desciption;
            }
/*
            if (signer_account.is_sign == 1) {
                html += '<img src="' + signer_account.document_sign_data.sign_image + '" class="sign_image" width="100%"/>';
            } else if (signer_account.is_sign == 0 && signer_account.account_id == globals.account_id && data.status == 'inprogress') {
                html += '<div class="input_signer"' +
                    'style="display: inline-block;">' +
                    '<img src="/assets/images/sign_here.png"' +
                    'class="sign"' +
                    'id="sign_' + signer_account.account_id + '"' +
                    'data-id="' + signer_account.account_id + '"' +
                    'data-state_id="' + state_id + '"' +
                    'style="width: 30px;cursor: pointer"/>' +
                    '</div>';
            }*/

            html += '</div>';
            //$('.workflow_action').append(html);
            $('.signer_document_editor').append(html);


        });


    }





    $.ajaxSetup({async: false});
    $.fn.getDocument = function () {

        var id = $('#document').data('id');

        $.post(viewAjax, {}, function (data) {
            $.fn.storeDocument(data);
            if (!$.isEmptyObject(data.document_layout)) {

                if (!$.isEmptyObject(data.document_layout.layout_components)) {
                    $.each(data.document_layout.layout_components, function (k, v) {

                        if (k == 'header') {
                            $('.header_container').html(v);
                        }

                        if (k == 'footer') {
                            $('.footer_container').html(v);
                        }

                        if(k =='content'){

                            $('.a4_page_content').html(v) ;
                            $('.subject_page').html(data.document.title);
                            $('#document_number_text').html(data.document.outbox_number);
                            $('#document_date').val(data.document.document_date);
                            $('#document_date').attr('disabed','disabled');
                            $('.content_page').html(data.document.content);

                            if(editor){
                                $.fn.load_editor();
                            }






                            //drawReceipts('to', 'account', data.document.to_account);
                            //drawReceipts('to', 'contact', data.document.to_contact);
                            $.fn.drawDocumentReceipts('user', data.document.recipients);
                            //drawReceipts('to', 'user', data.document.to_user);
                            //drawReceipts('to', 'department', data.document.to_department);
                            //drawReceipts('to', 'role', data.document.to_role);
                            //drawReceipts('to', 'invitations', data.document.to_invitations);

                            // drawReceipts('cc', 'account', data.document.cc_account);
                            // drawReceipts('cc', 'contact', data.document.cc_contact);
                            // drawReceipts('cc', 'user', data.document.cc_user);
                            // drawReceipts('cc', 'department', data.document.cc_department);
                            // drawReceipts('cc', 'role', data.document.cc_role);
                            // drawReceipts('cc', 'invitations', data.document.cc_invitations);
                            //
                            //
                            // drawReceipts('signer', 'account', data.document.signer_account);
                            // drawReceipts('signer', 'contact', data.document.signer_contact);


                            $.fn.drawAttachment(data.document.files);


                            if(displayWorkflow){
                                if(data.document.work_flow !== null) {
                                    if(('work_flow' in data.document.work_flow)) {
                                        $.fn.drawWorkFlow({
                                            'item': data.document.work_flow.work_flow,
                                            'current': data.document.work_flow.current_stage_id
                                        });
                                    }
                                    // $.fn.drawActualWorkFlow({'item' : data.document.work_flow.work_flow ,'current': data.document.work_flow.current_stage_id });
                                }



                            }

                        }

                    });
                    if(editor) {
                        $.fn.fillInput(data);
                    }
                }
            }

            if( ('qr_code' in data.document)  ) {
                if(data.document.qr_code !==null ) {
                    $('#qr_code').html('<img src="' + data.document.qr_code.data + '" />');
                }

            }
            console.log('==========================');
            console.log(data.document);
            console.log('==========================');
            $.fn.document_unblock($('.sheet'));
        }, 'json');
    };



    $.fn.getDocument();



    $(document).on("click", ".sign", function (e) {
        var signById = $(this);
        var sign_id = $(this).data('id');
        var state_id = $(this).data('state_id');
        $.post(getSign, {
        }, function (data) {

            var img = '<img src="' + data.sign.sign_data + '" class="sign_image" width="100%"  />';
            signById.closest('.input_signer').html(img);
            $('.saveSign').removeAttr('disabled');
            $('.saveSign').attr('data-id',sign_id);
            $('.saveSign').attr('data-state_id',state_id);

        }, 'json');

        e.preventDefault();

    });

    $(document).on("click", ".saveSign", function (e) {
        $(this).attr('disabed','disabled');
     var sign_id = $(this).data('id');
     var signById = $(this);
     var state_id = $(this).data('state_id');
        $.ajaxSetup({async: true});
     $.post(signDoc, {
     'document_id': document_id ,
     'state_id': state_id
     }, function (data) {
         $(this).removeAttr('disabed') ;
     location.reload();
     }, 'json');

     e.preventDefault();
     });

    /*
     function copyToClipboard() {
     // Create a "hidden" input
     var aux = document.createElement("input");
     // Assign it the value of the specified element
     aux.setAttribute("value", ".");
     // Append it to the body
     document.body.appendChild(aux);
     // Highlight its content
     aux.select();
     // Copy the highlighted text
     document.execCommand("copy");
     // Remove it from the body
     document.body.removeChild(aux);
     alert("Print screen disable.");
     }

     $(window).keyup(function (e) {
     if (e.keyCode == 44) {
     copyToClipboard();
     }
     });

     document.onkeydown = function (e) {
     if (e.ctrlKey &&
     (e.keyCode === 67 ||
     e.keyCode === 86 ||
     e.keyCode === 85 ||
     e.keyCode === 117)) {
     return false;
     } else {
     return true;
     }
     };

     document.onkeydown = function (e) {
     if (e.keyCode == 123) {
     return false;
     }
     if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
     }
     if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
     }
     if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
     }

     if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
     }
     }


     $(document).on("click", "#reject", function (e) {
     var c = confirm("Are you sure you want reject?");
     if (c) {
     $.post(statusDoc, {
     'document_id': document_id,
     'status': 'rejected'
     }, function (data) {
     $('#actions').addClass('hide');
     window.location.reload();
     });
     }

     e.preventDefault();
     });

     $(document).on("click", "#reply", function (e) {

     window.location.replace("{{route('document.replyDoc',[$oDocument->id])}}");

     });

     $(document).on("click", ".to_next_stage", function (e) {

     var state_id = $(this).data('state_id');
     var action_id = $(this).data('action_id');
     var message = $(this).data('message');
     var c = confirm(message);
     if (c) {
     $.post('{{ route("document.saveWfAction") }}', {
     'action_id': action_id,
     'state_id': state_id,
     'document_id': '{{$oDocument->id}}'
     }, function (data) {
     //location.reload();
     });
     }

     });


     */



    /********************* render pdf ***********************/
    $('#print').on('click', function (e) {
        printJS(pdf_url);

        e.preventDefault();
    });



    if ( $( ".js-switch" ).length ) {

        var elem = document.querySelector('.js-switch');
        var init = new Switchery(elem);

        elem.onchange = function() {
            if(elem.checked){
                $('.note').fadeIn();
                return true;
            }
            $('.note').fadeOut();
            return false;
        };
    }






});
