<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LookupsDatum
 */
class LookupsDatum extends Model
{
    protected $table = 'lookups_data';

    public $timestamps = false;

    protected $fillable = [
        'lookup_id',
        'description'
    ];

    protected $guarded = [];

        
}