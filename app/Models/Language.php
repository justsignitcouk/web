<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 */
class Language extends Model
{
    protected $table = 'languages';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'code',
        'direction',
        'flag',
        'active'
    ];

    protected $guarded = [];

        
}