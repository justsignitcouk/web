@extends('layouts.admin')

@push('scripts')

    {{--Select--}}
    <script type="text/javascript"
            src="{{ asset('/theme/default/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/form_select2.js') }}"></script>


    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/style.css')}}">
    {{--    <script src="{{asset('/assets/components/jquery-ui/external/jquery/jquery.js')}}"></script>--}}
    <script src="{{asset('/assets/components/jquery-ui/jquery-ui.js')}}"></script>
    <script>
        $(function () {
//                $( "#accordion" ).accordion();
            $("#accordion-styled").sortable({
                // cancel: ".ui-state-disabled",
                items: ".panel:not(:first-child,:last-child)",
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');

                    // POST to server using $.post or $.ajax
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: '/stage/sortable'
                    });
                }
            });
            $("#accordion-styled").disableSelection();


            $('#delete_workflow_submit').on('click', function () {
                $(this).prop('disabled', true);
                $.post("{{ route('admin.workflow.delete_wf') }}", {'id': '{{$id}}'}, function (data) {
                    window.location = "{{ url('workflow') }}";
                });
            });
            $('#delete_workflow_model').on('hide.bs.modal', function () {
                $('#delete_workflow_submit').prop('disabled', false);
            });
        });
    </script>
@endpush


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __('workflow.workflow.manage_workflow') }}</h6>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_stage">
                        <i class="icon-plus3 left"></i> {{ __('workflow.stage.add_stage') }}
                    </button>

                    <button type="button" class="btn  btn-danger" data-toggle="modal"
                            data-target="#delete_workflow_model" id="delete_workflow">
                        <i class="icon-plus3 left"></i> {{ __('workflow.stage.delete_workflow') }}
                    </button>

                </div>
            </div>
        </div>
        <div class="panel-body">
            @if(count($oStages) > 0)
                <div class="panel-group" id="accordion-styled">

                    @foreach($oStages as $k => $oStage)
                        <div id="stages-{{$oStage->id}}" class="panel  @if ($loop->first or $loop->last) 1ui-state-disabled1 @endif ">
                            <div class="panel-heading {{$action_types[$oStage->action_type]}}">
                                <h6 class="panel-title">
                                    <div data-toggle="collapse" data-parent="#accordion-styled"
                                         href="#accordion-styled-group{{$k}}">
                                        <div class="row">
                                            <div class="col-md-3">{{ $oStage->name }}</div>
                                            <div class="col-md-4">{{ $oStage->description }}</div>
                                            <div class="col-md-2">{{ $oStage->action_type }}</div>
                                            <div class="col-md-3 pull-right">
                                                {{--<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#edit_stage">--}}
                                                {{--<i class="fa fa-edit"></i> Edit--}}
                                                {{--</button>--}}
                                                <button type="button" class="btn btn-default btn-sm"
                                                        id="add_state_button" data-stage-id="{{ $oStage->id }}"
                                                        data-toggle="modal" data-target="#add_state">
                                                    <i class="fa fa-edit"></i> {{__('workflow.state.add_state')}}
                                                </button>
                                                <button type="button" class="btn btn-default btn-sm"
                                                        id="delete_stage_button" data-stage-id="{{ $oStage->id }}"
                                                        data-toggle="modal" data-target="#modal-delete_stage">
                                                    <i class="glyphicon glyphicon-trash left"></i> {{__('common.delete')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </h6>
                            </div>
                            <div id="accordion-styled-group{{$k}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-body">

                                        @if( $oStage->count() > 0)

                                            @foreach($oStage->StageState as $oStageState)
                                                <div class="row">
                                                    <div class="col-md-3">{{$oStageState->State->name}}</div>
                                                    <div class="col-md-4">{{ isset($aStagesInfo[$oStageState->next_stage_id]) ? $aStagesInfo[$oStageState->next_stage_id] : $oStageState->next_stage_id  }}</div>
                                                    <div class="col-md-1">&nbsp;</div>
                                                    <div class="col-md-4 pull-right">
                                                        <button type="button" class="btn btn-default btn-sm delete_state_button"
                                                                id="delete_state_button"
                                                                data-state-id="{{ $oStageState->id }}" data-toggle="modal"
                                                                data-target="#modal-delete-state-stage">
                                                            <i class="glyphicon glyphicon-trash left"></i> {{__('common.delete')}}
                                                        </button>
                                                    </div>
                                                    </div>
                                            <div class="row">
                                                @foreach($oStageState->State->StateAction as $oStateAction)
                                                    @if($oStateAction->Action) <button class="btn btn-primary"> {{ $oStateAction->Action->name }} </button> @endif
                                                @endforeach
                                            </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="well well-sm m-a-0">
                    <h3 class="m-a-0 text-center">{{__('common.there_is_no').' '.__('workflow.stage.stages')}}</h3>
                </div>
            @endif
        </div>
        {{--{{ model to add stage workflow }}--}}
        <div class="modal fade" id="add_stage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.stage.add_stage') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.workflow.stage.store'), 'method'=>'POST', 'id'=>'add_stage_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <fieldset title="2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.stage.stage') .' '. __('common.name') }}: <span
                                                    class="text-danger">*</span></label><br>
                                        <input name="stage_name" value="{{old('stage_name')}}" class="required form-control">

                                        <br/><font color="red" class="validation" id="stage_name_validation"></font>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.stage.stage').' '.__('common.description')}}
                                            : </label><br>
                                        <textarea name="description" class="form-control"></textarea>
                                        <input type="hidden" name="workflow_id" value="{{$oWorkflow->id}}"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                        <button type="button" id="save_stage" class="btn btn-primary">{{__('common.save')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--{{ model to add stage workflow }}--}}

        <div class="modal fade" id="add_state" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.state.add_state') }}</h4>
                    </div>
                    {!! Form::open(['url'=> url('/admin/stage/add_state'), 'method'=>'POST', 'id'=>'add_state_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <fieldset title="2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.state.state') .' '. __('common.name') }}: <span
                                                            class="text-danger">*</span></label><br>
                                                {!! Form::select('state_id', ['' => __('common.please_choose') ] + $oStates, '', ['class' => 'form-control m-bot15']) !!}



                                                <br/><font color="red" class="validation"
                                                           id="name_validation"></font>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.state.next_stage') }}: <span
                                                            class="text-danger">*</span></label><br>
                                                <select name="next_stage" class="select-fixed-single">
                                                    <option value="">__select__</option>
                                                    @foreach($oStages as $oStage)
                                                        <option value="{{$oStage->id}}">{{$oStage->name}}</option>
                                                    @endforeach
                                                </select>
                                                <br/><font color="red" class="validation"
                                                           id="next_stage_validation"></font>
                                                <input type="hidden" name="stage_id" id="stage_id"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                        <button type="button" id="save_state" class="btn btn-primary">{{__('common.save')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade scrollbox" id="add_actions" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.actions.add_actions') }}</h4>
                    </div>
                    {!! Form::open(['url'=> url('stateActions'), 'method'=>'POST', 'id'=>'add_action_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <fieldset title="2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.actions.select_actions') }}: <span
                                                            class="text-danger">*</span></label><br>
                                                <select required="required"
                                                        class="select-multiple-tags select2-validation"
                                                        id="actions" name="actions">
                                                    <option value="">__select__</option>
                                                    @foreach ($aActions as $aAction)
                                                        <option value="{{ $aAction->id }}">
                                                            {{ $aAction->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <br/><font color="red" class="validation"
                                                           id="action_validation"></font>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.action.assignee') }}: <span
                                                            class="text-danger">*</span></label><br>
                                                <select required="required"
                                                        class="select-multiple-tags select2-validation"
                                                        id="assignee[]"
                                                        name="assignee[]" multiple="multiple">
                                                    <optgroup label="{{ __('common.author') }}">
                                                        <option value="AUTHOR">{{ __('common.author') }}</option>
                                                    </optgroup>
                                                    <optgroup label="{{ __('common.users') }}">
                                                        @foreach ($aAccounts as $aAccount)
                                                            <option value="{{ $aAccount->id }}">
                                                                {{ $aAccount->account_name }}
                                                            </option>
                                                        @endforeach
                                                    </optgroup>
                                                    {{--<optgroup label="Departments">--}}
                                                    {{--@foreach ($aDepartments as $aDepartment)--}}
                                                    {{--<option value="d_{{ $aDepartment['id'] }}">{{ $aDepartment['name'] }}</option>--}}
                                                    {{--@endforeach--}}
                                                    {{--</optgroup>--}}
                                                </select>
                                                <input type="hidden" name="state_id" id="state_id" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                        <button type="button" id="save_state_action"
                                class="btn btn-primary">{{__('common.save')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        {{--{{ model to delete workflow }}--}}
        <div class="modal fade" id="delete_workflow_model" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.delete') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.workflow.delete_wf'), 'method'=>'POST', 'id'=>'delete_workflow_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ __('workflow.do_you_want_delete_workflow') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.no')}}</button>
                        <button type="button" id="delete_workflow_submit"
                                class="btn btn-primary">{{__('common.yes')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--{{ model to delete workflow }}--}}

        {{--{{ model to delete state }}--}}
        <div class="modal fade" id="modal-delete-state-stage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.delete') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.workflow.delete_state'), 'method'=>'POST', 'id'=>'delete_state_stage_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ __('workflow.do_you_want_delete_state_stage') }}
                        <input type="hidden" name="state_id" value="" id="state_id_modal" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.no')}}</button>
                        <button type="button" id="delete_state_stage_submit"
                                class="btn btn-primary">{{__('common.yes')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--{{ model to delete state }}--}}


        {{--{{ model to delete stage }}--}}
        <div class="modal fade" id="modal-delete_stage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.delete_stage') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.workflow.delete_stage'), 'method'=>'POST', 'id'=>'delete_stage_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ __('workflow.do_you_want_delete_stage') }}
                        <input type="hidden" name="stage_id" value="" id="stage_id_modal" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.no')}}</button>
                        <button type="button" id="delete_stage_submit"
                                class="btn btn-primary">{{__('common.yes')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--{{ model to delete state }}--}}


        @endsection

        @push('scripts')
            <script>


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                // Add Stage
                $(document).on("click", "#save_stage", function (e) {
                    var data = $("#add_stage_form").serializeArray(),
                        url = $("#add_stage_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add Stage
                $(document).on('click','.delete_state_button',function(){
                    var state_id  = $(this).data('state-id');
                    $('#state_id_modal').val(state_id);
                });
                // Add State
                $(document).on("click", "#save_state", function (e) {
                    var data = $("#add_state_form").serializeArray(),
                        url = $("#add_state_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add State

                // Add State
                $(document).on("click", "#save_state_action", function (e) {
                    var data = $("#add_action_form").serializeArray(),
                        url = $("#add_action_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add State


                $(document).on("click", "#add_state_button", function (e) {
                    $('#stage_id').val($(this).attr('data-stage-id'));
                });

                $(document).on('click','#delete_stage_button',function(data){
                    var id = $(this).data('stage-id');
                    $('#stage_id_modal').val(id);

                });

                $(document).on("click", "#delete_stage_submit", function (e) {
                    var delete_stage_button = $(this);
                    var id = $('#stage_id_modal').val();

                    $.post( '{{  route('admin.workflow.delete_stage') }}' ,{'id':id},function(data){
                        if (data.status == false) {
                            new PNotify({
                                title: 'Alert',
                                text: data.message,
                                icon: 'icon-warning',
                                addclass: 'bg-danger'
                            });
                        } else {
                            delete_stage_button.closest('.panel').remove();
                            new PNotify({
                                title: 'Success',
                                text: data.message,
                                icon: 'icon-checkmark3',
                                addclass: 'bg-success'
                            });

                            location.reload();
                        }
                    },'json');

                    e.preventDefault();
                    {{--$.ajax({--}}
                        {{--url: "{{url('stage')}}/" + id,--}}
                        {{--type: 'DELETE',--}}
                        {{--datatype: 'json',--}}
                    {{--}).done(function (data) {--}}
                        {{--if (data.status == false) {--}}
                            {{--new PNotify({--}}
                                {{--title: 'Alert',--}}
                                {{--text: data.message,--}}
                                {{--icon: 'icon-warning',--}}
                                {{--addclass: 'bg-danger'--}}
                            {{--});--}}
                        {{--} else {--}}
                            {{--delete_stage_button.closest('.panel').remove();--}}
                            {{--new PNotify({--}}
                                {{--title: 'Success',--}}
                                {{--text: data.message,--}}
                                {{--icon: 'icon-checkmark3',--}}
                                {{--addclass: 'bg-success'--}}
                            {{--});--}}
                        {{--}--}}
                    {{--});--}}

                });

                $(document).on("click", "#delete_state_stage_submit", function (e) {
                    var delete_stage_button = $(this);
                    //var id = delete_stage_button.attr('data-stage-id');
                    var id = $('#state_id_modal').val();

                    $.post("/admin/stage/delete_state",{'id' : id },function(data){
                        if (data.status == false) {
                            new PNotify({
                                title: 'Alert',
                                text: data.message,
                                icon: 'icon-warning',
                                addclass: 'bg-danger'
                            });
                        } else {
                            delete_stage_button.closest('.panel').remove();
                            new PNotify({
                                title: 'Success',
                                text: data.message,
                                icon: 'icon-checkmark3',
                                addclass: 'bg-success'
                            });

                            location.reload();
                        }
                    },'json') ;

                    e.preventDefault();
                });


                $(document).on("click", "#actions_state_button", function (e) {
                    $('#state_id').val($(this).attr('data-state-id'));

                });

                function validation_messages(data) {
                    $('.validation').html('');
                    $.each(data.message, function (index, data2) {

                        var field_name = index + '_validation',
                            messages = data2;
                        var html = '';
                        $.each(messages, function (index3, data3) {
                            if (html != '') {
                                html = html + '<br/>';
                            }
                            html += data3;
                        });
                        $('#' + field_name).html(html);
                    });
                }


            </script>
    @endpush