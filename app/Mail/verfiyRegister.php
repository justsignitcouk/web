<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class verfiyRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $oUser;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($oUser)
    {
        if(isset($oUser) && is_array($oUser))
            $this->oUser = $oUser;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('MyCompose Account verification')->view('mails.verfiyRegister');
    }
}
