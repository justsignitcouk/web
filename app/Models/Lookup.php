<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lookup
 */
class Lookup extends Model
{
    protected $table = 'lookups';

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

        
}