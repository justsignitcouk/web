<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LanguagesA
 */
class LanguagesA extends Model
{
    protected $table = 'languages_a';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'app_name',
        'flag',
        'abbr',
        'script',
        'native',
        'active',
        'default'
    ];

    protected $guarded = [];

        
}