<?php

namespace App\Providers;

use App\Services\Services;
use Illuminate\Support\ServiceProvider;

class ClientAPIServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ClientAPI', function () {
            return new Services(); // Name of your class, be sure to include the namespace if you are using one.
        });
    }

}
