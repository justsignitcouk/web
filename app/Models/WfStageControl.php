<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WfStageControl
 */
class WfStageControl extends Model
{
    protected $table = 'wf_stage_control';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'stage_role_id'
    ];

    protected $guarded = [];

        
}