<?php

return [

    'sent' => 'Sent',
    'inbox' => 'Inbox',
    'draft' => 'Draft',
    'pending_action' => 'Pending Action',
    'completed' => 'Completed',
    'my_inbox' => 'My Inbox',
    'rejected' => 'Rejected',
    'templates' => 'Templates',


];