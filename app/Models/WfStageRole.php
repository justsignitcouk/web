<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WfStageRole
 */
class WfStageRole extends Model
{
    protected $table = 'wf_stage_role';

    public $timestamps = false;

    protected $fillable = [
        'stage_id',
        'role_id'
    ];

    protected $guarded = [];

        
}