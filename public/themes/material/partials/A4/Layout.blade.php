
<div class="paper A4 editor" id="document" data-id="{{isset($id) ? $id : '' }}"
     style="direction:ltr;padding: 0;zoom:1;margin: 0 auto;">
    <section class="sheet padding-10mm sticky-note" id="sheet">

        <div class="header_and_footer">
            <div class="header_and_footer header_page">
                <div class="header_container">
                    {!! $aDocument['layout']['header']  !!}
                </div>
            </div>
        </div>

        @partial("A4.Content",['aDocument'=>$aDocument,'id'=>$id,'bContentEditable'=>isset($bContentEditable) ? $bContentEditable : false ])

        <div class="header_and_footer footer_page">
            <div class="footer_container">
                {!! $aDocument['layout']['footer']  !!}
            </div>
        </div>

        @if(isset($oDocumentNotes))
            @if(sizeof($oDocumentNotes)>0)
                    <div class=" note2 note_{{ $oDocumentNotes['id'] }}" id="note_{{ $oDocumentNotes['id'] }}" style="{{ $oDocumentNotes['note_position'] }}"
                         data-id="31">
                        <div class="note_cnt">
                            <div class="title">{{ $oDocumentNotes['user']['name'] }}</div>
                            <div class="title">{{ $oDocumentNotes['created_at'] }}</div>
                            <div>{{ $oDocumentNotes['note'] }}</div>
                        </div>
                    </div>

                @if(sizeof($oDocumentNotes['parents'])>0)
                        @foreach($oDocumentNotes['parents'] as $oDocumentNote)
                            <div class=" note2 note_{{ $oDocumentNote['id'] }}" id="note_{{ $oDocumentNote['id'] }}" style="{{ $oDocumentNote['note_position'] }}"
                                 data-id="31">
                                <div class="note_cnt">
                                    <div class="title">{{ $oDocumentNote['user']['name'] }}</div>
                                    <div class="title">{{ $oDocumentNote['created_at'] }}</div>
                                    <div>{{ $oDocumentNote['note'] }}</div>
                                </div>
                            </div>
                        @endforeach
                @endif
                @endif

        @endif
    </section>
</div>

@if(!isset($bFirstPage) || $bFirstPage==false)

    @if(isset($aDocument['files']))
        @foreach($aDocument['files'] as $file)

            <?php
            $ext = pathinfo($file['file']['name'], PATHINFO_EXTENSION);

            $type = $ext;
            if (in_array(mb_strtolower($ext), ['png', 'jpg', 'jpeg', 'gif', ''])) {
                $type = 'image';
            }
            ?>

            <br/><br/>

            <div class="paper A4 editor" id="document" data-id="{{isset($id) ? $id : '' }}"
                 style="direction:ltr;padding: 0;zoom:1;margin: 0 auto;">
                <section class="sheet padding-10mm sticky-note">
                    <p style="text-align:center">{{$file['file']['name']}}</p>
                    @if($type=='image')
                        <img src="{{ $file['file']['url'] . '/files/' . $file['file']['path'] . '/' . $file['file']['hash_name']  }}"
                             style="width:100%"/>
                    @endif
                    <a href="{{ $file['file']['url'] . '/files/' . $file['file']['path'] . '/' . $file['file']['hash_name']  }}"
                       target="blank">Click here to view</a>
                </section>
            </div>
        @endforeach
    @endif
@endif

<script>
    var viewAjax = '{{route('document.viewAjax',[$id])}}';
    {{--//var editor = '{{$editor}}' ;--}}
</script>

