{!! Theme::asset()->add([ ['signature_pad', 'assets/components/signature_pad/dist/signature_pad.js', ['editable']]]) !!}
{!! Theme::asset()->add([ ['signature_pad_app', 'assets/components/signature_pad/example/js/app.js', ['signature_pad']]]) !!}

{{--{!! Theme::asset()->add([ ['app', 'js/app.js', ['signature_pad']]]) !!}--}}
{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js', ['app']]]) !!}
{!! Theme::asset()->themePath()->add([ ['switchery', 'js/plugins/forms/styling/switchery.min.js', ['plupload_full']]]) !!}
{!! Theme::asset()->themePath()->add([ ['switch', 'js/plugins/forms/styling/switch.min.js', ['switchery']]]) !!}
{!! Theme::asset()->themePath()->add([ ['spectrum', 'js/plugins/pickers/color/spectrum.js', ['switch']]]) !!}
{!! Theme::asset()->themePath()->add([ ['editable', 'js/plugins/forms/editable/editable.min.js', ['anytime']]]) !!}
{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js', ['editable']]]) !!}
{!! Theme::asset()->themePath()->add([ ['fancybox', 'js/plugins/media/fancybox.min.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js', ['fancybox']]]) !!}
{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js', ['uniform']]]) !!}
{!! Theme::asset()->themePath()->add([ ['gallery_library', 'js/pages/gallery_library.js', ['datatables']]]) !!}

{!! Theme::asset()->themePath()->add([ ['gallery_library', 'pages/profile/layouts.js', ['datatables']]]) !!}

{!! Theme::asset()->add([ ['tinymce', 'assets/components/tinymce/js/tinymce/tinymce.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['interactions', 'js/core/libraries/jquery_ui/interactions.min.js',['tinymce']]]) !!}


<link href="{{  asset('assets/components/signature_pad/example/css/signature-pad.css') }}" rel="stylesheet type="
      text/css">
<link href="{{  asset('theme/default/assets/css/colors.css') }}" rel="stylesheet type=" text/css">

<style>
    .fp {
        opacity: 0.65;
        margin-top: 50px;
        margin-left: auto;
        margin-right: auto;
        width: 100px;
        height: 150px;
        background-color: #C51D31;
        border-radius: 60%/ 80%;
        background-image: repeating-radial-gradient(circle 4px at 25px 40px, rgba(165, 114, 87, 0.8), transparent),
        radial-gradient(circle at top, #FFDFC4, #F0D5BE, #FFDCB2);

    }

</style>
<script>
    var route_upload_profile = "{{route('profile.uploadProfilePicture',[getUniqueName()])}}"
    var route_delete_profile = "{{route('profile.deleteProfilePicture',[getUniqueName()])}}"
</script>
{!! Theme::asset()->themePath()->add([ ['profile_index', 'pages/profile/index.js',['app']]]) !!}

@partial('common.modal',['disable_button'=>'true','modal_id'=>'change_avatar','title'=>'Change Avatar','buttons'=>['main_button'=>['title'=>'Save']],'content'=> ['type'=>'partial','partial_name'=>'modals.avatar'] ])

<!-- Main content -->
<div class="content-wrapper">

    <!-- Cover area -->
    <div class="profile-cover">
        <div class="profile-cover-img"
             style="background-image: url(/themes/material/assets/images/backgrounds/slider_1.jpg)"></div>
        <div class="media">
            <div class="media-left" style="padding-right: 0">
                <div class="profile_pic">
                    <a href="{{route('profile.index',[getUniqueName()])}}" class="profile-thumb">
                        <img src="{{user_profile()}}"
                             class=" " alt="" style="width: 100px!important;height: 100px!important;display: block">

                    </a>
                    <div class="overlay">
                        <a href="" data-toggle="modal" data-target="#change_avatar">Change</a>
                    </div>
                </div>
            </div>

            <div class="media-body" style="padding-left: 20px">
                <h1> {{ oUser()->full_name }}
                    <small class="display-block"></small>
                </h1>
            </div>

            <div class="media-right media-middle">
                <ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
                    {{--<li><a href="#" class="btn btn-default"><i class="icon-file-picture position-left"></i> <span data-i18n="profile.btn.cover" ></span></a>
                    </li>
--}}

                </ul>
            </div>
        </div>
    </div>
    <!-- /cover area -->

    <!-- Toolbar -->
    <div class="navbar navbar-default navbar-xs content-group">
        <ul class="nav navbar-nav visible-xs-block">
            <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i
                            class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-filter">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#info" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile_nav.info"></span></a></li>
                {{--<li><a href="#accounts" data-toggle="tab"><i--}}
                {{--class="icon-menu7 position-left"></i> <span data-i18n="profile_nav.accounts">{{__('profile.accounts')}}</span></a></li>--}}
                <li><a href="#signatures" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile_nav.signatures"></span></a></li>
                {{--<li><a href="#fingerprint" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span data-i18n="profile_nav.fingerprint">{{__('profile.fingerprint')}}</span></a></li>--}}
                {{--<li><a href="#attachments" data-toggle="tab"><i--}}
                {{--class="icon-menu7 position-left"></i> <span data-i18n="profile_nav.my_library">{{__('profile.my_library')}}</span></a></li>--}}
                <li><a href="#activity" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile_nav.activity"></span></a></li>
                {{-- <li><a href="#activities" data-toggle="tab"><i
                                 class="icon-menu7 position-left"></i> <span data-i18n="profile_nav.activity">{{__('profile.activities')}}</span></a></li>--}}
                <li><a href="#notifications" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile_nav.notifications"></span></a></li>
                <li><a href="#layouts" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile.layouts"></span></a></li>
                <li><a href="#io_reference" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span
                                data-i18n="profile.io_reference"></span></a></li>

            </ul>

            {{--     <div class="navbar-right">
                     <ul class="nav navbar-nav">
                         <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top" title=""
                                data-content="after 1/11/2017" data-original-title="under construction"><i
                                         class="icon-stack-text position-left"></i> <span data-i18n="profile_nav.notes"></span></a></li>
                         <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top" title=""
                                data-content="after 1/11/2017" data-original-title="under construction"><i
                                         class="icon-collaboration position-left"></i> <span data-i18n="profile_nav.Friends"></span></a></li>
                         <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top" title=""
                                data-content="after 1/11/2017" data-original-title="under construction"><i
                                         class="icon-images3 position-left"></i> <span data-i18n="profile_nav.Photos"></span></a></li>
                         <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear"></i> <span
                                         class="visible-xs-inline-block position-right"> Options</span> <span
                                         class="caret"></span></a>
                             <ul class="dropdown-menu dropdown-menu-right">
                                 <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top"
                                        title="" data-content="after 1/11/2017" data-original-title="under construction"><i
                                                 class="icon-image2"></i>  <span data-i18n="profile_nav.Update_cover"></span></a></li>
                                 <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top"
                                        title="" data-content="after 1/11/2017" data-original-title="under construction"><i
                                                 class="icon-clippy"></i> <span data-i18n="profile_nav.Update_info"></span></a></li>
                                 <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top"
                                        title="" data-content="after 1/11/2017" data-original-title="under construction"><i
                                                 class="icon-make-group"></i> <span data-i18n="profile_nav.Manage_sections"></span></a></li>
                                 <li class="divider"></li>
                                 <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top"
                                        title="" data-content="after 1/11/2017" data-original-title="under construction"><i
                                                 class="icon-three-bars"></i>  <span data-i18n="profile_nav.Activity_log"></span></a></li>
                                 <li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top"
                                        title="" data-content="after 1/11/2017" data-original-title="under construction"><i
                                                 class="icon-cog5"></i>  <span data-i18n="profile_nav.Profile_settings"></span></a></li>
                             </ul>
                         </li>
                     </ul>
                 </div>--}}
        </div>
    </div>
    <!-- /toolbar -->


    <!-- Content area -->
    <div class="content">

        <!-- User profile -->
        <div class="row">
            <div class="col-lg-12">
                <div class="tabbable">
                    <div class="tab-content">


                        <!-- Basic modal -->
                        <div id="modal_default" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title"><span
                                                    data-i18n="Signatures.main">{{__('profile.signatures')}}</span></h5>
                                    </div>

                                    <div class="modal-body">

                                        <div class="checkbox checkbox-switch">
                                            <label>
                                                <input type="checkbox" data-on-color="danger" data-off-color="primary"
                                                       data-on-text="{{__('profile.signature')}}"
                                                       data-off-text="{{__('profile.initials')}}" class="switch"
                                                       checked="checked">

                                            </label>
                                        </div>

                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                                <li class="active"><a href="#signatures_draw" id="signatures_draw_href"
                                                                      data-toggle="tab"><span
                                                                data-i18n="Signatures.draw"></span></a>
                                                </li>
                                                <li><a href="#signatures_upload_image"
                                                       data-toggle="tab"><span
                                                                data-i18n="Signatures.upload"></span></a>
                                                </li>
                                                <li><a href="#signatures_phone"
                                                       data-toggle="tab"><span
                                                                data-i18n="Signatures.mobile"></span></a>
                                                </li>
                                                <li><a href="#signatures_type" data-toggle="tab"
                                                       class="hide"><span
                                                                data-i18n="type"></span></a>
                                                </li>

                                            </ul>

                                            <div class="tab-content">

                                                <div class="tab-pane active" id="signatures_draw"
                                                     style="position: relative;">

                                                    <div id="signature-pad" class="m-signature-pad">
                                                        <div class="m-signature-pad--body">
                                                            <canvas width="698" height="366"
                                                                    style="touch-action: none;"></canvas>


                                                        </div>

                                                        <div class="col-md-4">
                                                            {{--<input type="text"--}}
                                                            {{--class="form-control colorpicker-show-input"--}}
                                                            {{--data-preferred-format="rgb" value="#000"--}}
                                                            {{--value="#E63E3E">--}}
                                                            <div style="cursor: pointer;width:40px;height:40px;display:inline-block;margin:0 5px;background:#000"
                                                                 class="color_sign" data-value="#000"></div>
                                                            <div style="cursor: pointer;width:40px;height:40px;display:inline-block;margin:0 5px;background:blue"
                                                                 class="color_sign" data-value="blue"></div>


                                                        </div>
                                                        <div class="m-signature-pad--footer">
                                                            <div class="description"><span
                                                                        data-i18n="sign_above"></span></div>
                                                            <div class="left">
                                                                <button type="button"
                                                                        class="button clear btn btn-primary"
                                                                        data-action="clear"><span
                                                                            data-i18n="clear"></span>
                                                                </button>
                                                            </div>
                                                            <div class="right">
                                                                <button type="button"
                                                                        class="button save btn btn-primary"
                                                                        id="save_draw_sign" data-action="save-png"><span
                                                                            data-i18n="save"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                {{--ask--}}
                                                <div class="tab-pane" id="signatures_upload_image">

                                                    {!! Plupload::make('my_uploader_id', action('Individual\UserController@uploadSign',[getUniqueName()]))
                                                        ->setOptions([
                                                            'filters' => [
                                                                'max_file_size' => '1mb',
                                                                'mime_types' => [
                                                                    ['title' => "Image files", 'extensions' => "jpg,gif,png"],
                                                                ],
                                                            ],
                                                        ])
                                                        ->render() !!}

                                                </div>{{--ask--}}
                                                <div class="tab-pane" id="signatures_phone">
                                                    <div class="row">
                                                        <h4 class="col-xs-4">
                                                            <img src="{{asset('/assets/images/sign-mobile.png')}}"
                                                                 style="width: 100%;">
                                                        </h4>
                                                        <div class="col-xs-8">
                                                            <form class="form-inline">
                                                                <div class="form-group">
                                                                    <label for="phone"
                                                                           data-i18n="Signatures.phone"></label>
                                                                    <input type="phone" class="form-control" id="phone">
                                                                </div>
                                                                <button type="submit" class="btn btn-default"
                                                                        data-i18n="submit">
                                                                </button>
                                                            </form>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="tab-pane hide" id="signatures_type">

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /basic modal -->


                        <div class="tab-pane fade in active profile_info" id="info">

                            <div class="col-md-6">

                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-body panel-accounts ">
                                            <form action="{{route('profile.change_username',[getUniqueName()])}}"
                                                  class="form-validate-jquery" role="form" method="POST"
                                                  id="change_username">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label data-i18n="profile.username"></label>
                                                            <input type="text" name="username" id="username_input"
                                                                   class="form-control" value="{{ oUser()->username }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-right">
                                                    <button type="submit"
                                                            class="btn btn-primary legitRipple change_username_submit">
                                                        <span data-i18n="save"></span> <i class="icon-arrow-right14 position-right"></i></button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                @partial('profile.table_info',[])
                            </div>

                        </div>

                        @if(false)
                            <div class="tab-pane fade" id="accounts">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h6 class="panel-title"><span
                                                    data-i18n="accounts.account.main"></span><a
                                                    class="heading-elements-toggle"><i
                                                        class="icon-more"></i></a></h6>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body panel-accounts">

                                        @partial('profile.link_account',[])

                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="tab-pane fade" id="signatures">
                            <div class="panel panel-flat">
                                {{-- <div class="panel-heading">
                                     <h6 class="panel-title"><span data-i18n="Signatures.main">{{__('profile.signatures')}}</span><a
                                                 class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                     <div class="heading-elements">
                                         <ul class="icons-list">
                                             <li><a data-action="collapse"></a></li>
                                             <li><a data-action="reload"></a></li>
                                         </ul>
                                     </div>
                                 </div>--}}
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-default btn-sm sig_btn"
                                                data-toggle="modal"
                                                data-target="#modal_default"><span
                                                    data-i18n="Signatures.creat_new"></span><i
                                                    class="icon-play3 position-right"></i></button>
                                        <br/><br/>

                                    </div>
                                    <div class="col-md-12 profile_signature">
                                        <div class="panel panel-info panel-bordered signature">
                                            <div class="panel-heading">
                                                <h6 class="panel-title"><span class="title"
                                                                              data-i18n="Signatures.main"></span>
                                                </h6>
                                                {{--<div class="heading-elements">
                                                    <ul class="icons-list">
                                                        <li><a data-action="collapse"></a></li>
                                                        <li><a data-action="reload"></a></li>

                                                    </ul>
                                                </div>--}}
                                            </div>

                                            <div class="panel-body panel-signatures">

                                            </div>
                                        </div>

                                        <div class="panel panel-info panel-bordered">
                                            <div class="panel-heading">
                                                <h6 class="panel-title"><span class="title"
                                                                              data-i18n="Signatures.Initials"></span>
                                                </h6>
                                                {{--<div class="heading-elements">
                                                    <ul class="icons-list">
                                                        <li><a data-action="collapse"></a></li>
                                                        <li><a data-action="reload"></a></li>

                                                    </ul>
                                                </div>--}}
                                            </div>

                                            <div class="panel-body panel-initials">

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="activity">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><span data-i18n="activity.main"></span><a
                                                class="heading-elements-toggle"> <i class="icon-more"></i></a>
                                    </h6>
                                    <div class="heading-elements hide">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">


                                        @partial('profile.devices')

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="activities">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><span
                                                data-i18n="profile.notifications"></span><a
                                                class="heading-elements-toggle"><i class="icon-more"></i></a>
                                    </h6>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body panel-accounts">

                                    @partial('profile.activities',['aActivities' => $aActivities ])

                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="notifications">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><span
                                                data-i18n="notifications.notifications"></span><a
                                                class="heading-elements-toggle"><i class="icon-more"></i></a>
                                    </h6>

                                    <div class="notification">

                                    </div>
                                    {{-- <div class="heading-elements">
                                         <ul class="icons-list">
                                             <li><a data-action="collapse"></a></li>

                                         </ul>
                                     </div>--}}
                                </div>

                                <div class="panel-body panel-accounts">
                                    @partial('profile.notifications',['aNotifications' => $aNotifications ])
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="layouts">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><span
                                                data-i18n="profile.layouts"></span><a
                                                class="heading-elements-toggle"><i class="icon-more"></i></a>
                                    </h6>

                                    <div class="layouts_table_container">


                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#addLayout" data-i18n="profile.add_layout">
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="addLayout" tabindex="-1" role="dialog"
                                             aria-labelledby="addLayoutLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="comment">Name:</label>
                                                            <input type="text" class="form-control" id="layout_name"
                                                                   required="required">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="comment">Header:</label>
                                                            <textarea class="form-control" rows="5"
                                                                      id="header_content"></textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="comment">Footer:</label>
                                                            <textarea class="form-control" rows="5"
                                                                      id="footer_content"></textarea>
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button type="button" id="add_layout" class="btn btn-primary">
                                                            Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <table class="table datatable-ajax table-inbox responsive layouts table-lay"
                                               id="layouts_table"
                                               style="width:100%;table-layout: auto !important;">
                                            <thead>
                                            <tr>
                                                <th data-i18n="name"></th>
                                                <th data-i18n="details"></th>
                                            </tr>
                                            </thead>
                                            <tbody data-link="row" class="rowlink">

                                            </tbody>
                                        </table>

                                    </div>


                                </div>

                                <div class="panel-body panel-accounts">
                                    @partial('profile.notifications',['aNotifications' => $aNotifications ])
                                </div>
                            </div>
                        </div>



                        <div class="tab-pane fade" id="io_reference">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><span
                                                data-i18n="profile.io_reference"></span><a
                                                class="heading-elements-toggle"><i class="icon-more"></i></a>
                                    </h6>

                                </div>

                                <div class="panel-body panel-accounts">
                                    @partial('profile.io_reference',['aSequences' => $aSequences ])
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>

        </div>
        <!-- /user profile -->

    </div>
    <!-- /content area -->

</div>
<script>
    var profile_no_signature = '{{__("profile.no_signature")}}';
    var profile_no_initials = '{{__("profile.no_initials")}}';
    var profile_deleteSign = '{{route('profile.deleteSign',[getUniqueName()])}}';
    var profile_updateEditable = '{{route( 'profile.updateEditable',[getUniqueName()]) }}';
    var currentUserID = '{{nUserID()}}';
    var account_make_default = '{{route("account.make_default")}}';
    var deactivate = '{{route("profile.deactivate",[getUniqueName()])}}';
    var layouts_user = '{{route("profile.getLayouts",[getUniqueName()])}}';
    var layouts_user_save = '{{route("profile.saveLayout",[getUniqueName()])}}';
    var layouts_user_delete = '{{route("profile.deleteLayout",[getUniqueName()])}}';

</script>
<!-- /main content -->

