@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

   <div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.edit") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

   <div class="panel-body">
	   <div class="tabbable">

		<ul class="breadcrumb">
			<li><a href="{{route('users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
			<li><a href="{{route('users.profile'  , $account->user_id)}}"><i class="position-left"></i>{{ $account->account_name }}</a></li>
			<li><a href="{{route('users.accounts' , $account->user_id)}}"><i class="position-left"></i>{{ __("common.accounts") }}</a></li>
			<li class="active">{{ __("common.edit") }}</li>
		</ul>

        @include('layouts/partials/tabs/account_tabes')

        {!! Form::model($account , ['method' => 'POST', 'url' => route('accounts.update',$account->id), 'class' => '', 'files' => true]) !!}

            @include('accounts.form')
            
        {!! Form::close() !!}

        </div>
      </div>
    </div>
</div>

@endsection