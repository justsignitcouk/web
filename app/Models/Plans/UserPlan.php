<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    protected $table = 'user_plan';

    public function Plan(){
        return $this->belongsTo('App\Models\Plans\Plan');
    }

}
