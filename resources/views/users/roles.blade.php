@extends('layouts.admin')

@section('content')

<div class="col-md-12">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="tabbable">

                <ul class="breadcrumb">
                    <li><a href="{{route('admin.users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
                    <li><a href="{{route('admin.users.profile' , $user['id'])}}"><i class="position-left"></i>{{ $user['username'] }}</a></li>
                    <li class="active">{{ __("common.roles") }}</li>
                </ul>
                
                @include('layouts/partials/tabs')

                <div class="tab-content">

                 <div class="panel panel-flat">
                    <div class="panel-heading">
                       
                    </div>

                    <div class="panel-body">
                        <div class="row"> 
                          

                                    {!! Form::open(['route' => ['admin.users.updateroles', $user->id],'method'=>'POST']) !!}

                                        <div class="panel panel-primary">

                                            <div class="panel-heading">

                                                <label class="form-check-label">
                                                <input id="all" type="checkbox" value="">
                                                    {{ __("common.select_all") }} :
                                                </label>

                                            </div>

                                            <div class="panel-body">
                                                @foreach($user_roles as $key => $role)
                                                <div class="form-check">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name ="{{'roles-'.$key}}" value="{{$role->id}}"
                                                    @foreach($checked_user_roles as $check)

                                                    @if($check['role_id'] == $role->id)
                                                    checked
                                                    @endif
                                                 
                                                    @endforeach
                                                    >
                                                    {{$role->name}}
                                                  </label>
                                                </div>
                                                @endforeach
                                            </div>

                                        </div>   
                                        

                                        {{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

                          
                                    {!! Form::close() !!}
                   
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('#all').change(function () {
 
        checkAll() ;
        
        $state = $("#all").is(':checked');
        if($state){
           $('.form-check-input').prop('checked', true); 
        }else{
           $('.form-check-input').prop('checked', false); 
        }
        
    });

    function checkAll(){

    }
});
</script>
@endpush
@endsection
