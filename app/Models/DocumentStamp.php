<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentStamp
 */
class DocumentStamp extends Model
{
    protected $table = 'document_stamps';

    public $timestamps = true;

    protected $fillable = [
        'document_id',
        'stamp_id',
        'is_stamp',
        'stamp_by'
    ];

    protected $guarded = [];

        
}