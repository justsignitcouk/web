{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['form_select2', 'js/pages/form_select2.js',['select2']]]) !!}
{!! Theme::asset()->add([ ['tinymce', 'assets/components/tinymce/js/tinymce/tinymce.min.js',['form_select2']]]) !!}
{!! Theme::asset()->themePath()->add([ ['interactions', 'js/core/libraries/jquery_ui/interactions.min.js',['tinymce']]]) !!}
{{--
{!! Theme::asset()->themePath()->add([ ['mail_list_write', 'js/pages/mail_list_write.js',['interactions']]]) !!}
--}}
{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}

{!! Theme::asset()->themePath()->add([ ['daterangepicker', 'js/plugins/pickers/daterangepicker.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['compose', 'pages/document/compose.js',['daterangepicker']]]) !!}

{{--jquery.tinymce.min.js--}}

<link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">



<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold" data-i18n="page_header.Compose" ></span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                </div>
            </div>
        </div>

        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> <span  data-i18n="page_header.Compose" ></span></a></li>
                <li><a href="sidebar_dual_left.html"><span  data-i18n="page_header.breadcumb.document" ></span></a></li>
                <li class="active"><span  data-i18n="page_header.Compose" ></span></li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="{{route('needHelp')}}" class="legitRipple"><i class="icon-comment-discussion position-left"></i> <span data-i18n="Support"></span></a></li>
                <li class="dropdown hide">
                    <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Settings
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content ">
        <div class="modal fade layout_modal" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input id="search" type="text" class="form-control input-lg"
                                   placeholder="Search"/>
                            <span class="input-group-btn">
                                                                <button class="btn btn-info btn-lg" type="button">
                                                                    <i class="glyphicon glyphicon-search"></i>
                                                                </button>
                                                            </span>
                        </div>
                        <ul class="list-group">

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        {{--                -moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;"
                        unselectable="on"
                        onselectstart="return false;"
                        onmousedown="return false;"--}}
        <div class="row">

            <div class="a4_paper_container" style="width: auto;width: 210mm;margin: 0 auto;">
                <div class="paper_container">

                    <div class="paper_tools" style="display:none">
                        <div class="paper_tools_elements">
                        </div>
                    </div>
@partial('document.compose.a4',[])
</div>
</div>

</div>
</div>
<!-- /content area -->

</div>


@partial('compose_secondary_sidebar',[])

<script>
    var globals_route;
    var globals_routeSendDocument = '{{ route('document.sendDoc') }}';
    var globals_routeSendDraft = '{{ route('document.sendDraft') }}';
    var globals_route_getLayout = '{{ route('document.getLayout',['id' => 1]) }}';
</script>