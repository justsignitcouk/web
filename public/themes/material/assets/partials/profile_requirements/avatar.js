$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('container'), // ... or DOM Element itself
        url : route_upload_profile,
        flash_swf_url : '../js/Moxie.swf',
        silverlight_xap_url : '../js/Moxie.xap',
        unique_names:true,
        multi_selection :false,
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"},
                {title : "Zip files", extensions : "zip"}
            ]
        },
        multipart_params : {

        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: {
            PostInit: function() {
                document.getElementById('filelist').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },

            FilesAdded: function(up, files) {
                up.start();
                plupload.each(files, function(file) {
                    //document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                });
            },

            UploadProgress: function(up, file) {
                //document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            FileUploaded : function(up,file,result){
                var myData;
                try {
                    myData = eval(result.response);
                } catch(err) {
                    myData = eval('(' + result.response + ')');
                }
                var url = myData.result.oAvatar.avatar.file.url ;
                var path = myData.result.oAvatar.avatar.file.path ;
                var hash_name = myData.result.oAvatar.avatar.file.hash_name ;
                var file_id = myData.result.oAvatar.avatar.file.id ;
                $('#profile_pic').attr('src',url + '/files/' + path + '/' + hash_name ) ;
                $('#pickfiles').closest('.col-md-6').remove();
                $('#profile_pic').closest(".col-md-6").append('<a id="delete_profile_pic" data-file_id="'+  +'" href="#">Delete Picture</a>');
                $('#profile_pic').closest(".col-md-6").addClass('col-md-12').removeClass('col-md-6').addClass("animated pulse").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {

                });

            },
            UploadComplete: function (up,files) {

            },
            Error: function(up, err) {
                document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
        }
    });

    uploader.init();


    $(document).on('click','#delete_profile_pic',function(e){

        $.post(route_delete_profile,{},function(data){

        });
        e.preventDefault();
    });

    $(document).on('click','.skip_requirement',function(e){
        $.post('/profile_requirements/skip')
        e.preventDefault();
    });
});