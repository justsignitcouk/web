@foreach($roles_permissions as $key => $role)
    <div class="form-check">
      <label class="form-check-label">
        <input class="role-check-input" type="checkbox" name ="{{'roles-'.$key}}"  id="{{$role->key}}" value="{{$role->id}}"

        @if(!empty($checked_user_permissions))
            @foreach($checked_user_permissions as $check)

                @if($check['permission_id'] == $role->id)
                checked
                @endif
         
            @endforeach
        @endif
       
        >
        {{$role->name}}
      </label>
    </div>
@endforeach