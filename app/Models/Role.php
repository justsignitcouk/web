<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PermissionRole;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','display_name','description'
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    //create new role
    public function storeRole($roleData){


    	//fill in 'roles' table
        $role  = new Role ;
        $role->name          = $roleData->name ;
        $role->display_name  = $roleData->display_name ;
        $role->description   = $roleData->description ;
        $role->type_id       = $roleData->role_type ;
        $role->save();

        //store user new permissios
        foreach ($roleData->except(['name','description','display_name','type','_token','role_type']) as $key => $permission) {

       
            $permissionRole = new PermissionRole;
            $permissionRole->role_id = $role->id  ;
            $permissionRole->permission_id = $permission ;

            $permissionRole->save();

        }

    
        return true ;

    }

    //update the role
    public function updateRole($roleData,$id){

        //fill in 'roles' table
        $role  =  Role::find($id) ;
        $role->name  = $roleData->name ;
        $role->display_name  = $roleData->display_name ;
        $role->description   = $roleData->description ;
        $role->type_id          = $roleData->role_type ;
        $role->save();

        //delete user old permissios
        $rolePermissions =  PermissionRole::where('role_id',$id)->get();
        foreach ($rolePermissions as $rolePermission) {
            $rolePermission->delete();  
        }

        //store user new permissios
        foreach ($roleData->except(['name','description','display_name','type','_token','PUT','role_type']) as $key => $permission) {

                if($permission == 'PUT'){
                    continue;
                }

                $permissionRole = new PermissionRole;
                $permissionRole->role_id = $role->id  ;
                $permissionRole->permission_id = $permission ;
                $permissionRole->save();

        }
        
        return true ;

    }

}
