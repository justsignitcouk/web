<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class Actions extends Model
{
    protected $table = 'wf_action';

    public $timestamps = true;

    private static $requirementTypes = [
        'SYSTEM'     => 'SYSTEM',
        'CUSTOM'    => 'CUSTOM',
    ];

    public static function getActionTypes() {
        return self::$actionTypes;
    }

    public static function getSystemAction(){
        return self::where(['company_id' => 1, 'source_type' => 'SYSTEM'])->get();
    }

}
