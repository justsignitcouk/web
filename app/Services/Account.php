<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Account extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function get($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = currentUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserId ."/accounts")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }

    public static function updateDefaultAccount(){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("user/default_account")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);
        $oDefaultAccount = $response_array['data']['account'] ;
        session()->put("oDefaultAccount", serialize($oDefaultAccount));
        return true ;
    }

    public static function updateCurrentAccount($account_id){
        $oServices = Parent::getInstance();
        $nUserId = currentUserID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/setCurrentAccount")
                ->setBody(['account_id' => $account_id])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);
        $oCurrentAccount = $response_array['data']['account'] ;
        session()->put("oCurrentAccount", serialize($oCurrentAccount));
        session()->put("oDefaultAccount", serialize($oCurrentAccount));

        session()->put("oCurrentCompany", serialize($oCurrentAccount['department']['branch']['company']));

        return true ;
    }

    public static function make_default($nAccountID){
        $oServices = Parent::getInstance();
        $nUserId = currentUserID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/setDefaultAccount")
                ->withToken()
                ->setBody(['account_id'=>$nAccountID])
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);
        $oCurrentAccount = $response_array['data']['account'] ;
        session()->put("oCurrentAccount", serialize($oCurrentAccount));
        session()->put("oDefaultAccount", serialize($oCurrentAccount));

        return (object) $response_array['data'] ;
    }

    public  static function inbox($aParams){
        $oServices = Parent::getInstance();
        $nAccountID = currentAccountID();

        try{
            $oClient =  $oServices
                                    ->setMethod('GET')
                                    ->setUrl("account/" . $nAccountID ."/inbox")
                                    ->setBody( $aParams )
                                    ->withToken()
                                    ->request()
                                    ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[]],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }

    public  static function pending($aParams){
        $oServices = Parent::getInstance();
        $nAccountID = currentAccountID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $nAccountID ."/pending")
                ->setBody( $aParams )
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[]],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }

    public static function sent($aParams){
        $oServices = Parent::getInstance();
        $nAccountID = currentAccountID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $nAccountID ."/sent")
                ->setBody( $aParams )
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[]],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }

    public static function draft($aParams){
        $oServices = Parent::getInstance();
        $nAccountID = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("account/" . $nAccountID ."/draft")
                ->setBody(['account_id'=>$aParams['account_id']])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;



            return $response_array['data'] ;
        }


        $response_array = json_decode($oClient,true);


        return $response_array['data'] ;
    }

    public static function templates($aParams){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("templateDocuments")
                ->setBody(['account_id'=>$aParams['account_id']])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;


            return $response_array['data'] ;
        }


        $response_array = json_decode($oClient,true);

        foreach($response_array['data']['template_documents'] as $k=>$data){
            $response_array['data']['template_documents'][$k]['content'] = html_entity_decode($data['content']) ;
        }

        return $response_array['data'] ;
    }

    public static function all(){
        $oServices = Parent::getInstance();
        $company_id = currentCompanyID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("account/to_whom_addresses")
                ->setBody(['company_id'=>$company_id])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','rejected'=>[],'error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);


        return $response_array['data'] ;


    }

    public static function rejected(){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("accounts/rejected")
                ->setBody(['account_id'=>defaultAccountID()])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','rejected'=>[]],
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        foreach($response_array['data']['rejected'] as $k=>$data){
            $response_array['data']['rejected'][$k]['content'] = html_entity_decode($data['content']) ;
        }

        return $response_array['data'] ;
    }

    public static function badges(){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("accounts/badges")
                ->setBody(['account_id'=>currentAccountID()])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','rejected'=>[]],
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function findBy($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("account/findBy")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function findFirstById($id){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id )
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['account'] ;
    }

    public static function getContacts(){
        $oServices = Parent::getInstance();
        $id = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id . "/contacts" )
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['aContacts'] ;
    }

    public static function getContactsForRecipient($aParams){
        $oServices = Parent::getInstance();
        $id = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id . "/contacts_for_recipient" )
                ->setQuery($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['aContacts'] ;
    }

    public static function saveContacts($aContacts){
        $oServices = Parent::getInstance();
        $id = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("account/" . $id . "/contacts" )
                ->setBody(['aContacts'=>$aContacts])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }



}
