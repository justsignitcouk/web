$(document).ready(function(){




    var new_email = 1;
$.fn.without_content = 1;
    var aTo = [] ;
    var aSigner = [] ;
    var aCC = [] ;

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    function is_numeric(str){
        return /^\d+$/.test(str);
    }



    var contacts = new Bloodhound({
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.identifier);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url : '/ajax/user/get_contact_for_recipient',
            prepare: function (settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                return settings;
            },
            cache: false}
    });

    var contacts_by_name = new Bloodhound({
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.name);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url : '/ajax/user/get_contact_for_recipient',
            prepare: function (settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                return settings;
            },
            cache: false}
    });


    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('#recipient_input').keypress(function (e) {
        if (e.which == 13) {
            if($(".tt-suggestion:first-child").length>0){
            $(".tt-suggestion:first-child").trigger('click');
            return true;
            }else{
                var value = $(this).val() ;
                var error = 0 ;

                if(!validateEmail(value)){
                    var mobile=value;
                    if( value.charAt( 0 ) === '+' )
                        mobile = value.slice( 1 );
                    if(!$.isNumeric( mobile )){
                        error = 1 ;
                    }
                    if( mobile.length <8 ){
                        error = 1;
                    }

                }

                if(error==1){
                    return false;
                }
                datum = {identifier:value,name:'New Contact'} ;

                if ($.inArray(datum.identifier, aTo) == -1 || $.inArray(datum.identifier.toString() , aTo) == -1 ) {
                    aTo.push(datum.identifier);
                    console.log(aTo);
                    data_type = 'To';
                } else if ($.inArray(datum.identifier, aSigner) == -1 || $.inArray(datum.identifier.toString() , aSigner) == -1 ) {
                    aSigner.push(datum.identifier);
                    data_type = 'Signer';
                } else if ($.inArray(datum.identifier, aCC) == -1 || $.inArray(datum.identifier.toString() , aCC) == -1 ){
                    aCC.push(datum.identifier);
                    data_type = 'CC';
                } else{
                    return '';
                }
                if($.fn.without_content) {
                    datum['data_type'] = data_type;
                }else{
                    datum['data_type'] = 'To';
                }
                datum['display_name'] = datum.name;
                if( datum.name == 'New Contact') {
                    datum['display_name'] = datum.identifier;
                }
                datum['display_name'] = datum['data_type'] + ' ' +  datum['display_name'];
                datum['full_name'] = datum.name;
                datum['first_name'] = '' ;//datum.name;
                datum['last_name'] =  datum.identifier;
                var id_css = datum.identifier.replace("@", "___");
                id_css = id_css.split('.').join('---');
                id_css = id_css.split(' ').join('');
                id_css = id_css.split('+').join('');
                datum['id'] = id_css;
                $('#recipient_input').val('');
                $.fn.drawRecipient(datum,$.fn.without_content,true) ;
                $('#recipient_input').typeahead('val','');

                return '' ;
            }
        }
    });


    $('#recipient_input').typeahead({
            highlight: true,
            hint: false,
            minLength: 1
        },
        {
            name: 'identifier',
            displayKey: 'identifier',
            display: 'identifier',
            source: contacts ,
            limit: 10,
            engine: Handlebars,
            templates: {
                suggestion: Handlebars.compile('<div>{{name}} - {{identifier}}</div>')
            }
        },
        {
            name: 'name',
            displayKey: 'name',
            display: 'name',
            source: contacts_by_name ,
            limit: 10,
            engine: Handlebars,
            templates: {
                suggestion: Handlebars.compile('<div>{{name}} - {{identifier}}</div>')
            },
        }).bind('typeahead:autocompleted', function(obj, datum) {

        $('#recipient_input').val('');
    }).bind('typeahead:selected', function(obj, datum, name) {
        var data_type = '';

        if ($.inArray(datum.identifier, aTo) == -1 || $.inArray(datum.identifier.toString() , aTo) == -1 ) {
            aTo.push(datum.identifier);
            console.log(aTo);
            data_type = 'To';
        } else if ($.inArray(datum.identifier, aSigner) == -1 || $.inArray(datum.identifier.toString() , aSigner) == -1 ) {
            aSigner.push(datum.identifier);
            data_type = 'Signer';
        } else if ($.inArray(datum.identifier, aCC) == -1 || $.inArray(datum.identifier.toString() , aCC) == -1 ){
            aCC.push(datum.identifier);
            data_type = 'CC';
        } else{
            return '';
        }
        if($.fn.without_content) {
            datum['data_type'] = data_type;
        }else{
            datum['data_type'] = 'To';
        }
        datum['display_name'] = datum.name;
        if( datum.name == 'New Contact') {
            datum['display_name'] = datum.identifier;
        }
        datum['display_name'] = datum['data_type'] + ' ' +  datum['display_name'];
        datum['full_name'] = datum.name;
        datum['first_name'] = '' ;//datum.name;
        datum['last_name'] =  datum.identifier;
        var id_css = datum.identifier.replace("@", "___");
        id_css = id_css.split('.').join('---');
        id_css = id_css.split(' ').join('');
        id_css = id_css.split('+').join('');
        datum['id'] = id_css;
        $('#recipient_input').val('');
        $.fn.drawRecipient(datum,$.fn.without_content,true) ;
        $('#recipient_input').typeahead('val','');

        return '' ;


    });




    $('#recipient-list').on('click','.remove-recipient-user',function(e){
        $(this).closest('li').remove();
        var id = $(this).closest('li').attr('data-id');
        var type = $(this).closest('li').attr('data-type');

        if(type=='To'){
            aTo.pop(id);
        }
        if(type=='Signer'){
           aSigner.pop(id);

        }
        if(type=='CC'){
            aCC.pop(id);

        }

        $('.' + type.toLowerCase() + '_user_item_' + id).each(function(){
            $(this).remove();
        });

        e.preventDefault();
    });

    $('#recipient-list').on('click','.recipient-user-switch-to',function(e){
        var type = $(this).attr('data-type');
        var id = $(this).closest('.recipient-user').data('id');
        var error = 0 ;

        var elementType = $(this).closest('.recipient-user').data('elementtype');
        var elementIDN = $(this).closest('.recipient-user').data('elementid');
        var data_origin = $(this).closest('.recipient-user').data('origin');
        var old_type = $(this).closest('.recipient-user').attr('data-type');

        if(type=='To') {
            if ($.inArray(id, aTo) != -1 || $.inArray(id , aTo) != -1 ) {
                return false;

            }
        }

        if(type=='Signer') {
            if ($.inArray(id, aSigner) != -1 || $.inArray(id, aSigner) != -1) {
                return false;

            }
        }

        if(type=='CC') {
            if ($.inArray(id, aCC) != -1 || $.inArray(id, aCC) != -1 ) {
                return false;

            }
        }



        var text = $(this).closest('.recipient-user').data('text');

        $('.recipient-user-' + id).each(function(){

            if($(this).attr('data-type') == type){
                error = 1 ;
                return '';
            }
        });

        if(!error) {
            elementID = elementIDN;

            $('#' + old_type.toLowerCase() + '_user_item_' + id).remove();
            $(this).closest('.recipient-user').find('.recipient-user-type').text(type);
            $(this).closest('.recipient-user').attr('data-type', type);
            $(this).closest('.recipient-user').data('data', type);



            if(old_type=='To'){
                aTo.pop(id);
            }
            if(old_type=='Signer'){
                aSigner.pop(id);
            }
            if(old_type=='CC'){
                aCC.pop(id);
            }

            if(type=='To'){
                aTo.push(id);
            }
            if(type=='Signer'){
                aSigner.push(id);
            }
            if(type=='CC'){
                aCC.push(id);
            }


            if(type=='CC'){
                var editor_content = '<li class="' + type.toLowerCase() + '_user_item ' + type.toLowerCase() + '_user_item_' + id + '" id="' + type.toLowerCase() + '_user_item_' + id + '" contenteditable="true" data-id="' + id + '" data-type="to"   data-elementType="'+ elementType +'"  data-elementid="'+ id +'"  data-origin="' + data_origin +'">' + text + '</li>';

                $('.' + type.toLowerCase() + '_document_editor').find('ul').append(editor_content);
            }else{
                var editor_content = '<div class="' + type.toLowerCase() + '_user_item ' + type.toLowerCase() + '_user_item_' + id + '" id="' + type.toLowerCase() + '_user_item_' + id + '" contenteditable="true" data-id="' + id + '" data-type="to"   data-elementType="'+ elementType +'"  data-elementid="'+ id +'"  data-origin="' + data_origin +'">' + text + '</div>';

                $('.' + type.toLowerCase() + '_document_editor').append(editor_content);
            }

        }


        e.preventDefault();
    });



});