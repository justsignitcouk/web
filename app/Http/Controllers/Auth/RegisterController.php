<?php

namespace App\Http\Controllers\Auth;

use App\Mail\resetPassword;
use App\Http\Requests\setPasswordRequest;
use App\Http\Requests\UserRegisterRequest;
use App\LaravelModels\User;
use App\Http\Controllers\Controller;
use App\Services\Plans;
use App\Services\Register;
use Facuz\Theme\Facades\Theme;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use GuzzleHttp\Client;
use Illuminate\Http\Request ;
use App\Mail\verfiyRegister;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'verfiyRegister']);
    }

    public function showRegisterForm(){
        Theme::layout('auth');
        return Theme::view('auth.register');
    }


    public function showRegister(){
        return redirect(route('prices.prices'));
        Theme::layout('auth');
        return Theme::view('auth.signup');
    }

    public function showRegisterWithPlan($plan_id){
        $oPlan = Plans::getPlanById($plan_id);
        Theme::layout('auth');
        return Theme::view('auth.signup_with_plan',['oPlan' => $oPlan['plan'] ]);
    }

    //RegisterUserRequest
    public function register(UserRegisterRequest $request){

        $data = $request->all();
        $client = new \App\Services\Register();
        $response = $client->register($data);

        $response = ['status'=>'true'];
        return \GuzzleHttp\json_encode($response) ;

    }

    public function resendVerify(){
        session()->flash('resend_code','resend_code');
        $verifyRegister = Register::resendVerify();
        return redirect()->to(route('inbox.all')) ;
    }

    public function verfiyRegister(Request $request){

        $data = $request->all();
        $verifyRegister = Register::verifyEmail($data);
        if ($verifyRegister['status']) {
                $active = Register::activateUser($verifyRegister['oUser']);
                if ($active['status'])
                    $request->session()->push('register_verify_message', 'Activated successfully! You can login now');
                else
                    $request->session()->push('register_verify_message', 'Sorry! Something went wring.<br/>Please try again');

        }else
            $request->session()->push('register_verify_message', 'Sorry! Something went wring.<br/>Please try again');
        return redirect(route('logout'));

    }


    public function verifyByMobile(Request $request){
        Theme::layout('auth');
        return Theme::view('auth.verify_mobile');

    }
    public function confirmMobile(Request $request){
        $code = $request->get('code');
        $sIdentifier =  getIdentifier();

        $aParams = ['code' => $code, 'sIdentifier' => $sIdentifier ,'type' => getIdentifierType() ] ;

        $response = \App\Services\Auth::confirm($aParams);

        if($response['status']=='success'){
            session()->flash('user_activated', 'Your account activated!');
            session()->put('registerCode',$code) ;
            return redirect()->route("user.login");
        }else{
            session()->flash('status', 'error!');
        }

        Theme::layout('auth');
        return Theme::view('auth.verify_mobile');

    }

    public function setPasswordForm(){
        if(!session()->has('registerMobile')) {
            return redirect()->route("login") ;
        }

        $mobile = session()->get('registerMobile');
        Theme::layout('auth');
        return Theme::view('auth.set_password');
    }

    public function setPassword(setPasswordRequest $request){
        $data = $request->all();
        $data['mobile'] = session()->get('registerMobile') ;
        $data['code'] = session()->get('registerCode') ;
        $response = \App\Services\Auth::confirm($data);

    }

    public function prices(){
        $oPlans = Plans::get();
        Theme::layout('auth');
        return Theme::view('prices.index',['oPlans'=>$oPlans]) ;
    }



}
