<?php
Route::group(['prefix' => 'ajax','as'=>'ajax.'], function() {
Route::post('/user/get_contact_for_recipient', 'Individual\UserController@getContactForRecipient', [])->name('getContactForRecipient');
    Route::post('/user/check_if_have_sign', 'Individual\UserController@checkIfHaveSignture', [])->name('checkIfHaveSignture');

    Route::post('/user/get_avatars', 'Individual\UserController@getAvatars', [])->name('getAvatars');
    Route::post('/user/set_avatar', 'Individual\UserController@setAvatar', [])->name('setAvatar');
    Route::post('/set_language', 'Individual\UserController@setLanguage', [])->name('setLanguage');
});