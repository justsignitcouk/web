@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        {!! Form::open(['route'=>'roles.store','files'=>'true','enctype'=>'multipart/form-data' , 'class'=>'submit-roles']) !!}

            @include('roles.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
