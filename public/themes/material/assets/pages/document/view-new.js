$(document).ready(function(){

    $(document).on('click','.zoom_for_page',function(e){
            var op = $(this).data('operation') ;
        if(op=='+') {
            var value  =  $('.A4').css('zoom');
            var new_value =  parseFloat(value) + 0.1;
            if(new_value>=1){
                new_value = 1 ;
            }

            $('.A4').css('zoom',new_value);
        }
        if(op=='-') {
            var value  =  $('.A4').css('zoom');
            var new_value =  parseFloat(value) - 0.1;
            if(new_value<=0.4){
                new_value = 0.4 ;
            }

            $('.A4').css('zoom',new_value);
        }
            e.preventDefault();
    });

    $(document).on('click','.print_a4',function(e){
        var document_id = $('.A4').data('id');
        $('#iframe').html("<iframe src='/document/print/" + document_id + "'></iframe>" );
        e.preventDefault();
    }) ;


    $(document).on("click", ".sign", function (e) {
        var signById = $(this);
        var sign_id = $(this).data('id');
        var state_id = $(this).data('state_id');
        $.post(getSign, {
        }, function (data) {

            var img = '<img src="' + data.sign.sign_data + '" class="sign_image" width="100%"  />';
            signById.closest('.input_signer').html(img);
            $('.saveSign').removeAttr('disabled');
            $('.saveSign').attr('data-id',sign_id);
            $('.saveSign').attr('data-state_id',state_id);

        }, 'json');

        e.preventDefault();

    });

    $(document).on("click", ".saveSign", function (e) {
        $(this).attr('disabed','disabled');
        var sign_id = $(this).data('id');
        var signById = $(this);
        var state_id = $(this).data('state_id');
        $.ajaxSetup({async: true});
        $.post(signDoc, {
            'document_id': document_id ,
            'state_id': state_id
        }, function (data) {
            $(this).removeAttr('disabed') ;
            location.reload();
        }, 'json');

        e.preventDefault();
    });

    $('.pages_frame').slimScroll({
        height: '580px',
        width: '1020px',
    });

});