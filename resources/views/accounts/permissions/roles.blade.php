@foreach($roles_permissions as $key => $role)
    <div class="form-check">
      <label class="form-check-label">
        <input class="role-check-input" type="checkbox" name ="{{'roles-'.$key}}" value="{{$role->id}}"

        @foreach($checked_user_permissions as $check)

            @if($check['permission_id'] == $role->id)
            checked
            @endif
     
        @endforeach
       
        >
        {{$role->name}}
      </label>
    </div>
@endforeach