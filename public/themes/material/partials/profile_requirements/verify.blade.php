{!! Theme::asset()->container('auth')->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['script', 'partials/profile_requirements/avatar.js',['plupload_queue']]]) !!}

{!! Theme::asset()->container('auth')->themePath()->add([ ['animatecss.css','css/extras/animate.min.css',['bootstrap']]]) !!}

<style>
    #uploadfiles{
        display:none;
    }
</style>


    <div class=" fade in" tabindex="-1" role="dialog" style="display:block">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Confirm Your Email</h4>
                </div>
                <div class="modal-body">
                    <p>To continue using My Compose, you'll need to confirm your email. Since you signed up with <b>{{oUser()->email}}</b>,
                        you can do that automatically through mail.<br/><br/>
                        Your Email	<b>{{ oUser()->email }}</b></p>
                </div>
                <div class="modal-footer">
                    <a  href="{{route("logout")}}" class="btn btn-default" data-dismiss="modal">Logout</a>
                    <a  href="{{route("user.resendVerify")}}" class="btn btn-default" >Resend Code</a>

                    @if(strpos(oUser()->email,'@gmail.com')>0)
                        <a href="{{url('https://gmail.com')}}" target="blank" class="btn btn-primary">Go to Gmail</a>
                    @endif
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<script>
    var route_upload_profile = "{{route('profile.uploadProfilePicture',[getUniqueName()])}}"
    var route_delete_profile = "{{route('profile.deleteProfilePicture',[getUniqueName()])}}"
</script>