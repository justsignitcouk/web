<?php
namespace App\Services;

use Illuminate\Support\Facades\Facade;

class ServicesFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'ClientAPI'; }
}