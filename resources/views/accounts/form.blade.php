
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($branch['id']))
            <h5 class="panel-title">{{ __("accounts.edit_account") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 

            {{ csrf_field() }}

            <meta name="csrf-token" content="{{ csrf_token() }}">

            @if(!empty($oUser->id))
            <input type="hidden" name="user_id" value="{{$oUser->id}}">
            @elseif(!empty($account->id))
            <input type="hidden" name="user_id" value="{{$account->user_id}}">
            @endif 

           
            <!-- First column -->
		    <div class="col-md-6">

                <div class="form-group">
		        	<label>{{ __("accounts.account_name") }}
		        
		        	 <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> 
		       
		        	 :</label>
		            <input type="text"
		                   name="account_name"
		                   class="form-control"
		                   placeholder='{{ __("accounts.account_name") }}'
		                   @if(!empty($account['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('account_name') }}"
			                    @else
			                    	value="{{ $account['account_name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('account_name') }}"
		                   @endif >
		            
		            @if ($errors->has('account_name'))
		                <font color="red">{{ $errors->first('account_name') }}</font>
		        	@endif

		        </div>

                @if(empty($account['id']))
                <div class="form-group">
		            <label>
			            {{ __("common.branches") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>
			        <div class="form-group">
	                	<select id="branches" name="branch_id">
	                		<option disabled selected value>
	                         	{{ __("common.select") }}
	                    	</option>
	                	</select>
			        </div>
			        @if ($errors->has('branch_id'))
			                <font color="red">{{ $errors->first('branch_id') }}</font>
			        @endif

            	</div>
            	@endif

      
			    <div class="form-group">
	            	<label>{{ __("common.verified") }} :</label><br/>
			            <input type="checkbox"
			                   name="verify"
			                   class="js-switch2"
			                   @if(!empty($account['verify']) && $account['verify'] == 1)
			                    checked
			                   @else
			                    
			                   @endif > 
			    </div> 

		    </div>

		    <!-- Second column -->
	        <div class="col-md-6">
                @if(empty($account['id']))
                <div class="form-group">
			        <label>
			            {{ __("common.companies") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>
	                <div class="form-group">
	                   <select id="select_company" name="company_id">
		                    <option disabled selected value>
		                         	{{ __("common.select") }}
		                    </option>
		                    <div id='company_options'>
		                        @if(!empty($oCompanies))
				                   	@foreach($oCompanies as $company)
				                          <option value="{{$company->id}}">{{$company->name}}
				                          </option>
				                   	@endforeach
				                @endif
			                </div>   	
	                   </select>
	                </div>
	                @if ($errors->has('company_id'))
			                <font color="red">{{ $errors->first('company_id') }}</font>
			        @endif
				</div>
				@endif
				@if(!empty($account['id']))
				<input type="hidden" name="department_id" value="{{$account->department_id}}" >
				@endif
                @if(empty($account['id']))
				<div class="form-group">
			        <label>
			            {{ __("common.departments") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>
	                <div class="form-group">

	                      @if(!empty($oDepartments))

                            <select name="department_id" id="departments">

	                            <option disabled selected value>
		                         	{{ __("common.select") }}
		                    	</option>
		                      	@foreach($oDepartments as $department)

                                    @if(count($errors) > 0)

                                    	@if(old('department_id') == $department->id)

	                                    	<option value="{{$department->id}}" selected>
					                               	{{$department->name}}
					                        </option>

                                    	
                                    	@endif()

                                    @else

		                                @if(!empty($account->department_id) && $account->department_id == $department->id)

				                               <option value="{{$department->id}}" selected>
				                               	{{$department->name}}
				                               </option>

			                            @else

			                                   <option value="{{$department->id}}" >
				                               	{{$department->name}}
				                               </option>

			                            @endif

		                            @endif


		                      	@endforeach
	                      	</select>

	                      @else
	                      	<select id="departments" name="department_id">
			                	<option disabled selected >
		                         	{{ __("common.select") }}
		                    	</option>
		                    </select>
	                      @endif
		                
			        </div>
			        @if ($errors->has('department_id'))
			                <font color="red">
			                {{ $errors->first('department_id') }}
			                </font>
			        @endif
		        </div>
		        @endif

			    <div class="form-group">
	            	<label>{{ __("common.is_default") }}  :</label><br/>
			            <input type="checkbox"
			                   name="is_default"
			                   class="js-switch1"
			                   @if(!empty($account['is_default']) && $account['is_default'] == 1)
			                    checked
			                   @else
			                    
			                   @endif > 
			    </div> 

			    <div class="form-group">
	            	
	            	<label>{{ __("common.profile_image") }} 
	            	
	            	:</label>
			        <br/>
	                    <input type="file" name="profile_image" class="pull-left"><br/>
	                     @if ($errors->has('profile_image'))
			                <font color="red">{{ $errors->first('profile_image') }}</font>
			            @endif
			    </div> 
			    @if(!empty($account['id']))
							<img src="{{cdn_url($sImageName,$sImagePath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	            @endif  

	    	</div>

		    <div class="row col-md-12">

	         	{{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

	            @if(!empty($account->id))
		            @can('allowed-permission', ['accounts-deactive','accounts-deactive'])

	                     @if($account->active == 1)
	                     	<div class="btn btn-danger pull-right"    data-toggle="modal" data-target="#modal_theme_danger">{{__("common.deactive")}}
	                     	</div> 
	                     @elseif($account->active == 0)
	                        <div class="btn btn-success pull-right" id="deactive-account"  data-toggle="modal" >{{__("common.active")}}
	                     	</div> 
	                     @endif
		             
		            @endcan 
	            @endif
	          

		    </div>    
    </div>
</div>

@if(!empty($account->id))
<div id="modal_theme_danger" class="modal fade delete-caution" style="display: none;">
 <input id='account-data' type="hidden" data-status="{{$account->active }}" data-id='{{$account->id}}'  data-user_id='{{$account->user_id}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.deactive")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("accounts.deactive_account")}}
				</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">
					{{__("common.cancel")}}
					</button>
					<button type="button" class="btn btn-danger" id='deactive-account'>
					{{__("common.deactive")}}
					</button>
				</div>
		    </div>
	    </div>
    </div>
</div>
@endif
<div id="modal_theme_danger" class="modal fade cant-delete" style="display: none;">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("branches.cant_delete_branch")}}
				</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">
					{{__("common.cancel")}}
					</button>
					<button type="button" class="btn btn-danger" id='deactive-account'>
					{{__("common.delete")}}
					</button>
				</div>
		    </div>
	    </div>
    </div>
</div>

<script>


var elem1= document.querySelector('.js-switch1');
var init = new Switchery(elem1);
var elem2= document.querySelector('.js-switch2');
var init = new Switchery(elem2);
var _token = $('input[name="_token"]').val();


    $('#select_company').on('change', function() {
	   var company_id = this.value ;

	   $.ajax({
 
	        method: 'POST',
            url: "{!! url('branches' ) !!}" + "/" + company_id +'/list',
            data: { _token : _token },
	        success: function( branches ) {

	        	$('#branches').html('<option disabled selected value>{{ __("common.select") }} </option>');
	        	$('#departments').html('<option disabled selected value>{{ __("common.select") }} </option>');
	        	

				$.each(branches, function(key, value) {
					
				   $('#branches').append('<option value="'+value.id+'">'+value.name+'</option>');
		
				});

             
	        },
	        error: function( data ) {

	        }
	    });

	});

    $('#branches').on('change', function() {

    	var branch_id = this.value ;

    	$.ajax({
 
	        method: 'POST',
            url: "{!! url('departments' ) !!}" + "/" + branch_id +'/list',
            data: { _token : _token },
	        success: function( departments ) {

	        	$('#departments').html('<option disabled selected value>{{ __("common.select") }} </option>');
	        	
				$.each(departments, function(key, value) {
				
				   $('#departments').append('<option value="'+value.id+'">'+value.name+'</option>');
		
				});
	        },
	        error: function( data ) {

	        }
	    });


    });

    /*delete*/
	$('#deactive-account').click(function() {

        var id = $('#account-data').data("id");
        var user_id = $('#account-data').data("user_id");
        var status  = $('#account-data').data("status");

        if(status == 1){
           status = 0 ;
        }else if(status == 0){
           status = 1 ;
        }

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('accounts' ) !!}" + "/" + id + "/" + status + '/delete',
            data: { _token : _token },
	        success: function( msg ) {

	        	location.replace("/users/"+user_id+"/accounts");

	        },
	        error: function( data ) {

	        }
	    });
     });

</script>

