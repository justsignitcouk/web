{!! Theme::asset()->themePath()->add([ ['io_reference', 'partials/profile/io_reference.js',['bootstrap']]]) !!}

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">



<div class="col-md-12">
    <div class="panel panel-flat">
        <div class="panel-body panel-accounts ">

            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8" data-i18n="profile.inbox_sequence">Inbox Sequence</div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-info add-inbox-new"><i class="fa fa-plus"></i> <span data-i18n="profile.add_new">Add New</span></button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered io_sequence_table inbox_sequence_table">
                    <thead>
                    <tr>
                        <th data-i18n="profile.year">Year</th>
                        <th data-i18n="profile.start_sequence">Start Sequence</th>
                        <th data-i18n="profile.end_sequence">End Sequence</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($aSequences as $aSequence)
                        @if($aSequence['type'] != 'inbox' )
                            @continue
                        @endif
                        <tr data-id="{{ $aSequence['id'] }}">
                            <td>{{ $aSequence['year'] }}</td>
                            <td>{{ $aSequence['start_sequence'] }}</td>
                            <td>{{ $aSequence['end_sequence'] }}</td>
                            <td>
                                <a class="no-smoothState add add_inbox_sequence" style="display:none" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                                <a class="no-smoothState edit edit_inbox_sequence" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                <a class="no-smoothState delete delete_inbox_sequence" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                            </td>
                        </tr>
                    @endforeach
                    <tr data-id="" style="display:none;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="io_sequence_actions">
                            <a class="no-smoothState add add_inbox_sequence" title="Add" style="" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                            <a class="no-smoothState edit edit_inbox_sequence" title="Edit" style="display:none" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                            <a class="no-smoothState delete delete_inbox_sequence" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-8">Outbox Sequence</div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-info add-outbox-new"><i class="fa fa-plus"></i> <span data-i18n="profile.add_new">Add New</span></button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered io_sequence_table outbox_sequence_table">
                    <thead>
                    <tr>
                        <th data-i18n="profile.year">Year</th>
                        <th data-i18n="profile.start_sequence">Start Sequence</th>
                        <th data-i18n="profile.end_sequence">End Sequence</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($aSequences as $aSequence)
                        @if($aSequence['type'] != 'outbox' )
                            @continue
                        @endif
                        <tr data-id="{{ $aSequence['id'] }}">
                            <td>{{ $aSequence['year'] }}</td>
                            <td>{{ $aSequence['start_sequence'] }}</td>
                            <td>{{ $aSequence['end_sequence'] }}</td>
                            <td>
                                <a class="no-smoothState add add_outbox_sequence" style="display:none" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                                <a class="no-smoothState edit edit_outbox_sequence" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                <a class="no-smoothState delete delete_outbox_sequence" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                            </td>
                        </tr>
                    @endforeach
                    <tr data-id="" style="display:none;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="io_sequence_actions">
                            <a class="no-smoothState add add_outbox_sequence" title="Add" style="" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                            <a class="no-smoothState edit edit_outbox_sequence" title="Edit" style="display:none" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                            <a class="no-smoothState delete delete_outbox_sequence" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>



            <form action="{{route('profile.reference_document',[getUniqueName()])}}"
                  class="form-validate-jquery" role="form" method="POST"
                  id="change_reference">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label data-i18n="profile.reference">Reference</label>
                                <input type="text" name="reference_input" value="{{ oUser()->sIONumber }}" class="reference_input" id="reference_input" >
                                <div class="items"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit"
                            class="btn btn-primary legitRipple set_reference_submit">
                        <span data-i18n="save">Save</span> <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>

    </div>
</div>
