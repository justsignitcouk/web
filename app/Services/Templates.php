<?php

namespace App\Services;

class Templates extends \App\Services\Services
{

    public static function get($aParams=[])
    {
        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("templateDocuments")
                ->setBody(['user_id' => nUserID() ])
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];

            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }


    public static function delete($aParams=[])
    {
        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("templateDocuments/delete")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];

            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

}