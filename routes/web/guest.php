<?php
Route::group(['middleware' => ['web']], function() {
    Route::get('/documents/validation/{qr_id}', 'Individual\DocumentController@getDocumentByQRID')->name('sign_validation');

    Route::get('/', 'Auth\LoginController@showLoginForm');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login')->name('user.login');
    Route::post('/validateEmailOrPhone', 'Auth\LoginController@validateEmailOrPhone')->name('validateEmailOrPhone');
    Route::post('/validatePassword', 'Auth\ResetPasswordController@validatePassword')->name('validatePassword');


    /*********************user registration*************************/
    Route::get('/register', 'Auth\RegisterController@showRegister')->name('registerForm');
    Route::get('/register/{plan_id}', 'Auth\RegisterController@showRegisterWithPlan')->name('showRegisterWithPlan');
    Route::post('/register', 'Auth\RegisterController@register')->name('register');
    Route::get('/verifyRegister', 'Auth\RegisterController@verfiyRegister')->name('user.verifyRegister');
    Route::get('/verifyByMobile', 'Auth\RegisterController@verifyByMobile')->name('user.verifyByMobile');
    Route::post('/confirmMobile', 'Auth\RegisterController@confirmMobile')->name('user.confirmMobile');
    Route::get('/setPassword', 'Auth\RegisterController@setPasswordForm')->name('user.setPasswordForm');
    Route::post('/setPassword', 'Auth\RegisterController@setPassword')->name('user.setPassword');
    Route::get('/resendVerify', 'Auth\RegisterController@resendVerify')->name('user.resendVerify');

    /*********************authentication  routes*************************/

    /*********************social media routes*************************/
    Route::get('login/github', 'Auth\SocialMediaController@redirectToGitHubProvider')->name('login.github');
    Route::get('login/facebook', 'Auth\SocialMediaController@redirectToFaceBookProvider')->name('login.facebook');
    Route::get('login/twitter', 'Auth\SocialMediaController@redirectToTwitterProvider')->name('login.twitter');
    Route::get('login/linkedin', 'Auth\SocialMediaController@redirectToLinkedinProvider')->name('login.linkedin');
    Route::get('login/google', 'Auth\SocialMediaController@redirectToGoogleProvider')->name('login.google');


    /*********************password  routes*************************/
    Route::get('/forgotPassword', 'Auth\ForgotPasswordController@showForgotForm')->name('forgotPassword');
    Route::post('/forgotPassword', 'Auth\ForgotPasswordController@getToken')->name('getToken');
    Route::get('/resetPassword', 'Auth\ResetPasswordController@showResetForm')->name('view.resetPassword');
    Route::post('/resetPassword', 'Auth\ResetPasswordController@resetPassword')->name('resetPassword');

    /*********************need help routes*************************/
    Route::get('/needhelp', 'Auth\LoginController@showNeedHelp')->name('needHelp');
    Route::post('/storeneedhelp', 'Auth\LoginController@storeNeedHelp');

    /*********************logout  route*************************/
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::post("/saveUserData",'Auth\LoginController@saveUserData')->name('saveUserData') ;
    Route::post('/register/validation/{field_db}/{field_label}', 'Auth\RegisterController@remoteValidation')->name('register.validation');
    Route::post('/login/validation/{field_db}/{field_label}', 'Auth\LoginController@remoteValidation')->name('login.validation');


    /********************* price routes*************************/
    Route::group(['prefix' => 'prices','as'=>'prices.'], function (){
        Route::get('/', 'Auth\RegisterController@prices', [])->name('prices');
    });
    /********************* price routes*************************/


});