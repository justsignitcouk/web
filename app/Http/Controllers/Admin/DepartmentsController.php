<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreDepartment;
use App\Http\Requests\Admin\UpdateDepartment;
use App\Http\Requests\Admin\UpdateDepartmentRoles;
use App\Http\Requests\Admin\UpdateDepartmentPermissions;
use App\Services\DepartmentService;
use App\Models\Department;
use App\Models\Branch;
use App\Models\Role;
use App\Models\Company;
use App\Models\DepartmentRole;
use Yajra\Datatables\Datatables;
use DB ;
use Gate;

class DepartmentsController extends Controller
{
    /**
     * @var department
     */
    protected $department;
    /**
     * @var usersService
     */
    protected $departmentService;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Department $department , DepartmentService $departmentService)
    {
        //$this->middleware('auth');
        $this->department = $department;
        $this->departmentService = $departmentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

            $active_departments = 1 ;
            $branch = Branch::find($id) ;
            $company = Company::select('id','name')->where('id',$branch->company_id )->get();

            return view('departments.index',compact('active_departments','branch' , 'company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

            $branch_id = $id ;
            $parent_departments = DB::table('departments')
                                   ->get() ;
            return view('departments.create',compact('branch_id','parent_departments'));

    }

    public function store(StoreDepartment $storeDepartment){

            if($this->department->storeDepartment($storeDepartment)){
            return redirect('/branches/'.$storeDepartment->branch_id.'/departments');  
             }

    }

    public function show($id){

            $oDepartment = Department::find($id);
            $aDepartment = $oDepartment->toArray();

            $branch = Branch::find($oDepartment->branch_id) ;
            $company = Company::select('id','name')->where('id',$branch->company_id )->get();
            
            $aDepartment['parent_department_name'] = $oDepartment->Parent ? $oDepartment->Parent->name : '';
            $active_show = 1 ;
            $sLogoName   = $oDepartment->Logo ? $oDepartment->Logo->hash_name : '';
            $sLogoPath   = $oDepartment->Logo ? $oDepartment->Logo->path : '';
       
            return view('departments.show',compact('oDepartment','active_show','aDepartment','sLogoName','sLogoPath' , 'company' , 'branch'));

    }

    public function edit($id){

            $department = Department::find($id);
            $oDepartment= Department::find($id);
            $parent_departments = DB::table('departments')
                                       ->get() ;
            $branch = Branch::find($oDepartment->branch_id) ;
            $company = Company::select('id','name')->where('id',$branch->company_id )->get();

            $sLogoName = $department->Logo ? $department->Logo->hash_name : '';
            $sLogoPath = $department->Logo ? $department->Logo->path : '';
            $active_edit = 1 ;

            return view('departments.edit',compact('department','parent_departments','sLogoName','sLogoPath','oDepartment','active_edit','branch','company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartment $updateDepartment, $id){

            if($this->department->updateDepartment($updateDepartment,$id)){
            return redirect('/branches/'.$updateDepartment->branch_id.'/departments');  
            }

    }

    public function destroy($id){
            $department = Department::find($id);
            $department->delete();

    }
   
    public function datatables($branch_id){

            $oDepartments = Department::where('branch_id',$branch_id)->get() ;
            $aDepartments = $oDepartments->toArray() ;
            foreach ($oDepartments as $key => $value) {
                    $aDepartments[$key]['parent_department_name'] = $value->Parent ? $value->Parent->name : '';
                }
            return Datatables::of($aDepartments)->make(true);

    }

    /*list of branch by company ID*/
    public function list($branch_id){

        return $departments = Department::where('branch_id',$branch_id)->get();

    }

    /**
     * Department roles dislay and edit
     */
    public function editRoles($id){

            $oDepartment     = Department::find($id);
            $department      = Department::find($id);
            $branch          = $department->Branch ;
            $company         = $department->Branch->Company ;


            $checked_department_roles = DepartmentRole::select('role_id')->where('department_id',$id)->get()->toArray();
            $department_roles= Role::where('type_id',2)->get();
            $active_roles    = 1 ;

            return view('departments.roles',compact('oDepartment',
                                           'active_roles',
                                           'roles',
                                           'department_roles',
                                           'checked_department_roles',
                                           'department',
                                           'branch',
                                           'company'
                                           )); 


    }

    /**
     * Department roles update
     */
    public function updateRoles(UpdateDepartmentRoles $updateDepartmentRoles , $id){

            if($this->department->updateRoles($updateDepartmentRoles,$id)){
            return redirect('/branchdeps/'.$id.'/roles');  
            }

    }

    /**
     * Account permissions dislay and edit
     */
    public function editPermissions($id){

        $department = Department::find($id);
        $oDepartment = Department::find($id);
        $active_permissions = 1 ;

        $checked_user_permissions =  $this->departmentService
                                    ->checkedDepartmentPermissions($id);

        //users module permissions
        $users_permissions  = $this->departmentService
                                    ->Permissions('users');
        
        //companies module permissions
        $companies_permissions  = $this->departmentService
                                    ->Permissions('companies');
       
        //accounts module permissions
        $accounts_permissions   = $this->departmentService
                                    ->Permissions('accounts');

        //permissions module permissions
        $permissions_permissions   = $this->departmentService
                                    ->Permissions('permissions');
        //branches module permissions
        $branches_permissions   = $this->departmentService
                                    ->Permissions('branches'); 

        //roles module permissions
        $roles_permissions   = $this->departmentService
                                    ->Permissions('roles'); 

        //roles module permissions
        $departments_permissions   = $this->departmentService
                                    ->Permissions('departments');    

        //workflow module permissions
        $workflow_permissions   = $this->departmentService
                                    ->Permissions('workflow'); 
        //requirements module permissions
        $requirements_permissions   = $this->departmentService
                                    ->Permissions('requirements');

        $branch          = $oDepartment->Branch ;
        $company         = $oDepartment->Branch->Company ;
        
        return view('departments.permissions',compact('department',
                                          'oDepartment',
                                          'users_permissions',
                                          'active_permissions',
                                          'checked_user_permissions',
                                          'companies_permissions',
                                          'accounts_permissions',
                                          'permissions_permissions',
                                          'branches_permissions',
                                          'roles_permissions',
                                          'departments_permissions',
                                          'workflow_permissions',
                                          'requirements_permissions',
                                          'branch',
                                          'company'
                                           )); 
        

    }

    /**
     * User permissions update
     */
    public function updatePermissions(UpdateDepartmentPermissions $updateDepartmentPermissions , $id){

             if($this->department->updatePermissions($updateDepartmentPermissions,$id)){
            return redirect('/branchdeps/'.$id.'/permissions');  
            }
    }
}
