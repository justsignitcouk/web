<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Branch;
use App\Models\Country;
use App\Models\BranchType;
use App\Http\Requests\Admin\StoreCompany;
use App\Http\Requests\Admin\UpdateCompany;
use Yajra\Datatables\Datatables;
use Gate;

class CompaniesController extends Controller
{

    /**
     * @var user
     */
    protected $company;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Company $company)
    {

        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $branchTypes = BranchType::all();
        return view('companies.create', compact('countries', 'branchTypes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompany $storeCompany)
    {
        if ($this->company->storeCompany($storeCompany)) {
            return redirect('/companies');
        }
    }

    public function datatables()
    {

        $aCompanies = Company::all()->toArray();
        return Datatables::of($aCompanies)->make(true);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $company = Company::find($id)->toArray();
        $oCompany = Company::find($id);
        $active_show = 1;

        $sLogoName = $oCompany->Logo ? $oCompany->Logo->hash_name : '';
        $sLogoPath = $oCompany->Logo ? $oCompany->Logo->path : '';

        return view('companies.show', compact('company', 'active_show', 'sLogoName', 'sLogoPath'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
            $company = Company::find($id)->toArray();
            $oCompany = Company::find($id);
            $active_edit = 1;

            $sLogoName = $oCompany->Logo ? $oCompany->Logo->hash_name : '';
            $sLogoPath = $oCompany->Logo ? $oCompany->Logo->path : '';

            return view('companies.edit', compact('company', 'active_edit', 'sLogoName', 'sLogoPath'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompany $updateCompany, $id)
    {
            if ($this->company->updateCompany($updateCompany, $id)) {
                return redirect('/companies');
            }

    }

    public function destroy($id)
    {
            $check_branches = Branch::where('company_id', '=', $id)->get()
                ->count();

            if ($check_branches == 0) {
                $company = Company::find($id);
                $company->delete();
                return $msg = 'can';
            } else {
                return $msg = 'cant';
            }

    }
}
