<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreAccount;
use App\Http\Requests\Admin\UpdateAccount;
use App\Http\Requests\Admin\UpdateAccountRoles;
use App\Http\Requests\Admin\UpdateUserPermissions;
use App\Services\AccountsService;
use App\Models\User;
use App\Models\Company;
use App\Models\Account;
use App\Models\AccountRole;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Role;
use Yajra\Datatables\Datatables;
use Gate ;

class AccountsController extends Controller
{


    /**
     * @var account
     */
    protected $account;
    /**
     * @var accountsService
     */
    protected $accountsService;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Account $account , AccountsService $accountsService)
    {
        $this->middleware('auth');
        $this->account   = $account;
        $this->accountsService   = $accountsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
           if (Gate::allows('allowed-permission', ['accounts-index','accounts-index'] )) {
                 $deps = Department::all()->count();
                 $user = User::find($id)->toArray();
                 $active_account = 1 ;
                 return view('accounts.index',compact('active_account','user','deps'));

           }else{
                return view('401'); 
           }
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user_id)
    {
        $deps = Department::all()->count();

        if (Gate::allows('allowed-permission', ['accounts-create','accounts-create'] ) && $deps > 0) {

            $oUser = User::find($user_id);  
            $oCompanies = Company::all();

            return view('accounts.create',compact('oUser','oCompanies'));

        }else{
            return view('401'); 
        }
        
          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAccount $storeAccount)
    {
        if (Gate::allows('allowed-permission', ['accounts-create','accounts-create'] )) {

            if($this->account->storeAccount($storeAccount)){ 
            return redirect('/users/'.$storeAccount->user_id.'/accounts');  
             }

        }else{

            return view('401'); 

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('allowed-permission', ['accounts-details','accounts-details'] )) {

            $account = Account::find($id);
            $active_show = 1 ;

            $sImageName = $account->ProfileImage ? $account->ProfileImage->hash_name : '';
            $sImagePath = $account->ProfileImage ? $account->ProfileImage->path : '';

            return view('accounts.show',compact('account','active_show','sImageName','sImagePath'));

        }else{

            return view('401'); 

        }
       
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (Gate::allows('allowed-permission', ['accounts-edit','accounts-edit'] )) {

            $account = Account::find($id);
        $active_edit = 1 ;

        $sImageName = $account->ProfileImage ? $account->ProfileImage->hash_name : '';
        $sImagePath = $account->ProfileImage ? $account->ProfileImage->path : '';

        $oDepartments = Department::all();
        $oCompanies =  Company::all();


        return view('accounts.edit',compact('account','active_edit','sImageName','sImagePath','oCompanies','oDepartments'));

        }else{
            return view('401'); 
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccount $updateAccount, $id)
    {

        if (Gate::allows('allowed-permission', ['accounts-edit','accounts-edit'] )) {

            if($this->account->updateAccount($updateAccount , $id)){ 
            return redirect('/users/'.$updateAccount->user_id.'/accounts');  
        }

        }else{
            return view('401'); 
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , $status){

         if (Gate::allows('allowed-permission', ['accounts-deactive','accounts-deactive'] )) {

            $account = Account::find($id);
            $account->active = $status ;
            $account->save();

         }else{
            return view('401'); 
         }


    }

    public function datatables($user_id){

        if (Gate::allows('allowed-permission', ['accounts-index','accounts-index'] )) {

        $oAccounts = Account::where('user_id',$user_id)
                            ->get();

        $aAccounts = $oAccounts->toArray() ;

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['branch_name'] = $value->Department ? ( $value->Department->Branch ? $value->Department->Branch->name : '') : '' ;

               
            }  

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['company_name'] = 
                $value->Department ? ($value->Department->Branch->Company ? $value->Department->Branch->Company->name : '') : '' ;
                ;
            } 

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['department_name'] = 

                $value->Department ? ($value->Department->name ? $value->Department->name : '') : '' ;
                
            }     
  
        return Datatables::of($aAccounts)->make(true);


        }else{
             return view('401'); 
        }
        
    }

    

    /**
     * Account roles dislay and edit
     */
    public function editRoles($id){

        if (Gate::allows('allowed-permission', ['accounts-roles','accounts-roles'] )) {

            $account = Account::find($id);
          $checked_account_roles = AccountRole::select('role_id')->where('account_id',$id)->get()->toArray();

          $account_roles= Role::where('type_id',3)->get();
          $active_roles = 1 ;

          return view('accounts.roles',compact('account',
                                           'active_roles',
                                           'roles',
                                           'account_roles',
                                           'checked_account_roles'
                                           )); 

        }else{
            return view('401'); 
        }
          
          
    }

    /**
     * Account roles update
     */
    public function updateRoles(UpdateAccountRoles $updateAccountRoles , $id){

        if (Gate::allows('allowed-permission', ['accounts-roles','accounts-roles'] )) {

            if($this->account->updateRoles($updateAccountRoles,$id)){
            return redirect('/accounts/'.$id.'/roles');  
        }


        }else{
            return view('401'); 
        }

        
    }

    /**
     * Account permissions dislay and edit
     */
    public function editPermissions($id){

        if (Gate::allows('allowed-permission', ['accounts-permissions','accounts-permissions'] )) {

        $account = Account::find($id);
        $active_permissions = 1 ;

        $checked_user_permissions =  $this->accountsService
                                    ->checkedAccountPermissions($id);

        //users module permissions
        $users_permissions  = $this->accountsService
                                    ->Permissions('users');
        
        //companies module permissions
        $companies_permissions  = $this->accountsService
                                    ->Permissions('companies');
       
        //accounts module permissions
        $accounts_permissions   = $this->accountsService
                                    ->Permissions('accounts');

        //permissions module permissions
        $permissions_permissions   = $this->accountsService
                                    ->Permissions('permissions');
        //branches module permissions
        $branches_permissions   = $this->accountsService
                                    ->Permissions('branches'); 

        //roles module permissions
        $roles_permissions   = $this->accountsService
                                    ->Permissions('roles'); 

        //roles module permissions
        $departments_permissions   = $this->accountsService
                                    ->Permissions('departments'); 

        //workflow module permissions
        $workflow_permissions   = $this->accountsService
                                    ->Permissions('workflow'); 
        //requirements module permissions
        $requirements_permissions   = $this->accountsService
                                    ->Permissions('requirements');                                                                             
        
        return view('accounts.permissions',compact('account',
                                          'users_permissions',
                                          'active_permissions',
                                          'checked_user_permissions',
                                          'companies_permissions',
                                          'accounts_permissions',
                                          'permissions_permissions',
                                          'branches_permissions',
                                          'roles_permissions',
                                          'departments_permissions',
                                          'workflow_permissions',
                                          'requirements_permissions'
                                           )); 

        }else{
            return view('401'); 
        }

        
        

    }

    /**
     * Account permissions update
     */
    public function updatePermissions(UpdateUserPermissions $updateAccountPermissions , $id){


        if (Gate::allows('allowed-permission', ['accounts-permissions','accounts-permissions'] )) {

            if($this->account->updatePermissions($updateAccountPermissions,$id)){
            return redirect('/accounts/'.$id.'/permissions');  
        }

        }else{
            return view('401'); 
        }

        

    }

    public function accounts(){

        if (Gate::allows('allowed-permission', ['accounts-all','accounts-all'] )) {

            return view('accounts.accounts');

        }else{

            return view('401'); 

        }

        
    }

    public function accountsDatatables(){
        
        $oAccounts = Account::all();

        $aAccounts = $oAccounts->toArray() ;

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['branch_name'] = $value->Department->Branch ? $value->Department->Branch->name : '';
            }  

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['company_name'] = $value->Department->Branch->Company ? $value->Department->Branch->Company->name : '';
            } 

        foreach ($oAccounts as $key =>$value) {
                $aAccounts [$key] ['department_name'] = $value->Department->name ? $value->Department->name : '';
            }     
  
        return Datatables::of($aAccounts)->make(true);

    }
}
