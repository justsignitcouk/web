<?php
/********************* price routes*************************/
Route::group(['prefix' => 'settings','as'=>'settings.'], function (){
Route::get('/', 'Premium\DashboardController@index', [])->name('index');
Route::get('/companies', 'Premium\CompaniesController@getCompanies', [])->name('companies');
Route::post('/companies', 'Premium\CompaniesController@CompaniesAjax', [])->name('CompaniesAjax');
Route::get('/create-company', 'Premium\CompaniesController@createCompanyForm', [])->name('create-company');

Route::post('/create-company', 'Premium\CompaniesController@store', [])->name('save_company_details');
Route::post('/create-company/check-slug', 'Premium\CompaniesController@checkAvailability', [])->name('check-slug');

Route::get('/companies/{slug}', 'Premium\CompaniesController@getCompaniesDetailsBySlug', [])->name('companies_details');
Route::get('/companies/{slug}/edit', 'Premium\CompaniesController@createCompanyForm', [])->name('edit_company');

Route::get('/companies/{slug}/add-branch', 'Premium\CompaniesController@createBranchForm', [])->name('add-branch');
Route::post('/companies/{slug}/add-branch', 'Premium\CompaniesController@storeBranch', [])->name('save_branch_details');



Route::post('/companies/{slug}', 'Premium\CompaniesController@getBranches', [])->name('getBranches');
Route::post('/companies/{slug}/employees', 'Premium\CompaniesController@getEmployees', [])->name('getEmployees');
Route::post('/companies/{slug}/roles', 'Premium\CompaniesController@getRoles', [])->name('getRoles');



Route::get('/companies/{slug}/{branch_id}', 'Premium\CompaniesController@branchDetails', [])->name('branch_details');

Route::get('/workflows', 'Premium\CompaniesController@getCompanies', [])->name('workflows');
Route::get('/employees', 'Premium\CompaniesController@getCompanies', [])->name('employees');



Route::get('/accounts', 'Premium\AccountsController@index', [])->name('accounts');
Route::get('/accounts/{id}', 'Premium\AccountsController@details', [])->name('accounts.details');

});
/********************* price routes*************************/


Route::group(['prefix' => '/c/{company_name}','as'=>'company.'], function (){
Route::group(['prefix' => 'inbox','as'=>'inbox.'], function() {
Route::resource('/',  'Company\InboxController', []);

Route::get('/', 'Company\InboxController@index', [])->name('all');
Route::post('/', 'Company\InboxController@getInbox', []);
Route::get('/sent', 'Company\InboxController@sent', []);
Route::post('/sent', 'Company\InboxController@getSent', []);

Route::get('/draft', 'Company\InboxController@draft', [])->name('Draft');
Route::post('/draft', 'Company\InboxController@getDraft', []);

Route::get('/pending', 'Company\InboxController@pending', []);
Route::post('/pending', 'Company\InboxController@getPendingAction', []);

Route::get('/rejected', 'Company\InboxController@rejected', []);
Route::post('/rejected', 'Company\InboxController@getRejected', []);

Route::get('/incomplete', 'Company\InboxController@incomplete', []);
Route::get('/approved', 'Company\InboxController@approved', []);
Route::get('/rejected', 'Company\InboxController@rejected', []);
Route::get('/archived', 'Company\InboxController@archived', []);

});


Route::get('/', function ($company_name) {
return $company_name;
});

});