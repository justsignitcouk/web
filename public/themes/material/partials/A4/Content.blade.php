@php $sInboxNumber = '' ; @endphp

<div class="a4_page_content ">
    <div class="" style="position: absolute;top: 22px; right: 40px;">
        @if(isset($aDocument['qr_code']) && isset($aDocument['qr_code']['data']) && !empty($aDocument['qr_code']['data'])  )
            <img src="{{  $aDocument['qr_code']['data']  }}"/>
        @endif
    </div>

    <div class="padd_20">
        <div class="left_side">
            <div class="document_date">
                @if(!empty($aDocument['document_date']))
                        <span style="display: inline-block;float:left">Date :</span> <span class="document_date_text"> {{$aDocument['document_date']}} </span>
                    @endif

            </div>
            <div class="document_number">
                                @if(!empty($aDocument['outbox_number']))
                        <span style="display: inline-block;float:left">Document Reference :</span>   <span class="document_number_text">{{$aDocument['outbox_number']}}</span>
                    @endif

            </div>
        </div>
        <div class="to_page">
            <div class="to_document_editor" id="to_document_editor" style="display:float:right !important;">
                @foreach($aDocument['recipients'] as $aRecipients )
                    @if($aRecipients['type'] == 'To')

                         @if($aRecipients['user_id'] == nUserID() )
                                @php $sInboxNumber = $aRecipients['inbox_number']; @endphp
                        @endif

                        <?php
                        $id_identifier = str_replace("@","___",$aRecipients['identifier']);
                        $id_identifier = str_replace(".","---",$id_identifier);
                        $id_identifier = str_replace(" ","",$id_identifier);
                        $id_identifier = str_replace("+","",$id_identifier);
                        ?>
                        <div class="popover_identifier to_user_item to_user_item_{{ $id_identifier }}" id="to_user_item_{{ $id_identifier }}" @if($bContentEditable) contenteditable="true" @endif  data-id="{{ $id_identifier }}" data-elementtype="user" data-type="to">
                            {!!  $aRecipients['display_name'] !!}
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
        <div style="position:absolute;top:40px;right:10px;color:darkblue;font-weight: bold;font-family: arial;">{{ $sInboxNumber }}</div>
        <br/>

        <div class="subject_page"   @if($bContentEditable) contenteditable="true" @endif > @if(!empty($aDocument['title'])){{ $aDocument['title'] }} @endif</div>
        <br/>
        <br/>
        <div class="editable content_page" id="page_0" @if($bContentEditable) contenteditable="true" @endif
        style=";min-height: 200px">
            {!!  $aDocument['content']  !!}
        </div>

    </div>

    <div style="margin-bottom: 100px;margin-top:5px;width:100%;text-align: center;">

        <div class="signs_page">
            <div class="signer_document_editor" id="signer_document_editor" style="position: relative;">
                @foreach($aDocument['recipients'] as $aRecipients )
                    @if($aRecipients['type'] == 'Signer')
                        <?php
                        $id_identifier = str_replace("@","___",$aRecipients['identifier']);
                        $id_identifier = str_replace(".","---",$id_identifier);
                        ?>
                        <div class="signer_container popover_identifier signer_user_item signer_user_item_{{ $id_identifier }}" id="signer_user_item_{{ $id_identifier }}" @if($bContentEditable) contenteditable="true" @endif  data-id="{{ $id_identifier }}" data-elementtype="user" data-type="signer">

                            {!!  $aRecipients['display_name'] !!}
                            @if($aRecipients['is_sign'] == 1)
                                <img src="{{  $aRecipients['sign']['sign_image']  }}" class="signer_sign_img" />
                            @endif
                        </div>
                    @endif

                @endforeach
            </div>
        </div>

        <div class="cc_page" style="padding: 10px;">

            <div class="cc_document_editor" id="cc_document_editor">
                <ul>
                    @foreach($aDocument['recipients'] as $aRecipients )
                        @if($aRecipients['type'] == 'CC')
                            <?php
                            $id_identifier = str_replace("@","___",$aRecipients['identifier']);
                            $id_identifier = str_replace(".","---",$id_identifier);
                            ?>
                            <li style="text-align: left;" class="popover_identifier cc_user_item cc_user_item_{{ $id_identifier }}" id="cc_user_item_{{ $id_identifier }}" @if($bContentEditable) contenteditable="true" @endif  data-id="{{ $id_identifier }}" data-elementtype="user" data-type="CC">
                                {!!  $aRecipients['display_name'] !!}
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>




    </div>
<div style="clear: both;"></div>
</div>
