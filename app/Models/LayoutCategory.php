<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LayoutCategory
 */
class LayoutCategory extends Model
{
    protected $table = 'layout_categories';

    public $timestamps = true;

    protected $fillable = [
        'category_id',
        'layout_id'
    ];

    protected $guarded = [];

        
}