<?php

return [
    'member_login' => 'Member Login',
    'email' => 'Email',
    'password' => 'Password',
    'login' => 'Login',
    'next'=>'Next',
    'need_help'=>'Need Help',
    'contact_us'=>'Contact Us',
    'require'=>'All Field Are Required',
    'title'=>'Title',
    'your_name'=>'Your Name',
    'your_email'=>'Your Email',
    'your_ms'=>'Your Message',
    'submit'=>'Submit',
    'no_account'=>"Don't have an account",
    'sign_up'=>'Sign Up',
    'sign_with'=>'OR Sign In With',
    'forgot_password'=>'Forgot Your Password',
    'cancel'=>'Cancel',
    'reset_password'=>'Reset My Password',
    'my_compose'=>'My compose',
    'email_or_phone'=>'Email or Mobile'
];