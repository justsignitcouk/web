<?php


namespace App\Services;
use App ;

class Notifications extends \App\Services\Services
{


    public static function getUserNotifications($lang){


        $oServices = Parent::getInstance();

        $url   = 'notifications' ;
        try{

            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->withToken()
                ->setQuery(['lang'=>$lang])
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            return $exception->getMessage() ;
        }


        return json_decode($oClient,true);


    }

    public static function changeReadStatus($notification_id,$document_id){

        $oServices = Parent::getInstance();

        $url   = 'notifications/change_read_status/' ;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->withToken()
                ->setBody(['notification_id'=>$notification_id, 'document_id'=>$document_id])
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            return $exception->getMessage()  ;
        }

        return json_decode($oClient,true);


    }

}