$(document).ready(function(){

    var aTo = [] ;
    var aSigner = [] ;
    var aCC = [] ;

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    function is_numeric(str){
        return /^\d+$/.test(str);
    }



    var contacts = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url : '/ajax/user/get_contact_for_recipient',
            prepare: function (settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                return settings;
            }},
        remote: {
            url: '/ajax/user/get_contact_for_recipient',
            prepare: function (query, settings) {
                settings.type = "POST";
                settings.url  = '/ajax/user/get_contact_for_recipient';
                settings.data = {q: query, foo: 'bar'}; // you can pass some data if you need to
                return settings;
            }
        }
    });


    $('#recipient_input').typeahead(null,
        {
            name: 'recipient111',
            displayKey: 'display_name',
            display: 'a',
            source: contacts ,
            templates: {
                suggestion: Handlebars.compile('<div>{{display_name}}</div>')
            }
        }).bind('typeahead:selected', function(obj, datum, name) {
        var data_type = '';

        if ($.inArray(datum.id, aTo) == -1 || $.inArray(datum.id.toString() , aTo) == -1 ) {
            aTo.push(datum.id);
            data_type = 'To';
        } else if ($.inArray(datum.id, aSigner) == -1 || $.inArray(datum.id.toString() , aSigner) == -1 ) {
            aSigner.push(datum.id);
            data_type = 'Signer';
        } else if ($.inArray(datum.id, aCC) == -1 || $.inArray(datum.id.toString() , aCC) == -1 ){
            aCC.push(datum.id);
            data_type = 'CC';
        } else{
            return '';
        }

        datum['data_type'] = data_type;
        $.fn.drawRecipient(datum) ;
        return '' ;
        ;
    }).bind('typeahead:autocompleted', function(obj, datum) {
        $('#recipient_input').val('');
    });




    $('#recipient-list').on('click','.remove-recipient-contact',function(e){
        $(this).closest('li').remove();
        var id = $(this).closest('li').attr('data-id');
        var type = $(this).closest('li').attr('data-type');

        if(type=='To'){
            aTo.pop(id);
        }
        if(type=='Signer'){
           aSigner.pop(id);

        }
        if(type=='CC'){
            aCC.pop(id);

        }

        $('.' + type.toLowerCase() + '_contact_item_' + id).each(function(){
            $(this).remove();
        });
        e.preventDefault();
    });

    $('#recipient-list').on('click','.recipient-contact-switch-to',function(e){
        var type = $(this).attr('data-type');
        var id = $(this).closest('.recipient-contact').data('id');
        var error = 0 ;

        var elementType = $(this).closest('.recipient-contact').data('elementtype');
        var elementIDN = $(this).closest('.recipient-contact').data('elementid');
        var data_origin = $(this).closest('.recipient-contact').data('origin');
        var old_type = $(this).closest('.recipient-contact').attr('data-type');

        if(type=='To') {
            if ($.inArray(id, aTo) != -1 || $.inArray(id.toString() , aTo) != -1 ) {
                return false;

            }
        }

        if(type=='Signer') {
            if ($.inArray(id, aSigner) != -1 || $.inArray(id.toString(), aSigner) != -1) {
                return false;

            }
        }

        if(type=='CC') {
            if ($.inArray(id, aCC) != -1 || $.inArray(id.toString(), aCC) != -1 ) {
                return false;

            }
        }



        var text = $(this).closest('.recipient-contact').data('text');

        $('.recipient-contact-' + id).each(function(){

            if($(this).attr('data-type') == type){
                error = 1 ;
                return '';
            }
        });

        if(!error) {
            elementID = elementIDN;

            $('#' + old_type.toLowerCase() + '_contact_item_' + id).remove();
            $(this).closest('.recipient-contact').find('.recipient-contact-type').text(type);
            $(this).closest('.recipient-contact').attr('data-type', type);
            $(this).closest('.recipient-contact').data('data', type);



            if(old_type=='To'){
                aTo.pop(id);
            }
            if(old_type=='Signer'){
                aSigner.pop(id);
            }
            if(old_type=='CC'){
                aCC.pop(id);
            }

            if(type=='To'){
                aTo.push(id);
            }
            if(type=='Signer'){
                aSigner.push(id);
            }
            if(type=='CC'){
                aCC.push(id);
            }






            var editor_content = '<div class="' + type.toLowerCase() + '_contact_item ' + type.toLowerCase() + '_contact_item_' + id + '" id="' + type.toLowerCase() + '_contact_item_' + id + '" contenteditable="true" data-id="' + id + '" data-type="to"   data-elementType="'+ elementType +'"  data-elementid="'+ id +'"  data-origin="' + data_origin +'">' + text + '</div>';

            $('.' + type.toLowerCase() + '_document_editor').append(editor_content);
        }


        e.preventDefault();
    });



});