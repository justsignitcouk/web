{!! Theme::asset()->themePath()->add([ ['link_account', 'partials/profile/link_account.js', ['bootstrap']]]) !!}

<button type="button" class="btn btn-default btn-sm" data-toggle="modal"
        data-target="#link_account"><span  data-i18n="link_account" ></span><i
            class="icon-play3 position-right"></i></button>

<br/><br/>
{{--{{ __('profile.link_account') }}--}}
<!-- Modal -->
<div id="link_account" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span  data-i18n="link_account" ></span></h4>
            </div>
            <div class="modal-body">

                <form class="link_account" action="" method="post">

                    <div class="form-group">{{--Email address:--}}
                        <label for="email" data-i18n="email_add"></label>
                        <input type="email" name="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd" data-i18n="password"></label>
                        <input type="password" name="password" class="form-control" id="pwd">
                    </div>

                    {{--<input type="submit" class="btn btn-default" class="link_account" data-i18n="password" />--}}
                    <button  type="submit" class="btn btn-default" class="link_account" data-i18n="link"></button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="deactivate_account" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span  data-i18n="deactivate_account" ></span></h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="form-group row">
                        <label for="pwd" class="col-sm-2 col-form-label text-right" data-i18n="password"></label>
                        <div class="col-sm-10">
                            <input type="password" name="password" id="password" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reason" class="col-sm-2 col-form-label text-right" data-i18n="reason"></label>
                        <div class="col-sm-10">
                            <textarea name="reason" class="form-control" id="reason" rows="3"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="accountId" name="accountId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
                <button type="button" class="btn btn-default deactivate_account" data-dismiss="modal" data-i18n="save"></button>
            </div>
        </div>

    </div>
</div>

<script>
    var current_account = '' ;
</script>
<div class="all_accounts">



</div>

