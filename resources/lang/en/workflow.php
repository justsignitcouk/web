<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CompaniesController Language Lines
    |--------------------------------------------------------------------------
    */


    'workflow' =>[
        'workflow'          => 'Workflow',
        'add_workflow'      => 'Add Workflow',
        'active'            => 'Active',
        'manage_workflow'   => 'Manage Workflow',
        'workflow_management'   =>'Workflow Management',
        'titles'            =>[
                                    'manage_workflows' => 'Manage Workflows'
                              ],
    ],

    'stage' => [
        'stage'             => 'Stage',
        'stages'             => 'Stages',
        'add_stage'         => 'Add Stage',
        'action_type'       => 'Action Type',
        'previous_stage'    => 'Previous Stage',
        'due_date'          => 'Due Date In Hours',
        'active'            => 'Active',
        'titles'            =>[
                                    'manage_stages' => 'Manage Stages'
                              ],
    ],

    'state' => [
        'state'             => 'State',
        'add_state'         => 'Add State',
        'next_stage'         => 'Next Stage',
        'delete_state' => 'Delete State'
    ],

    'action' => [
        'actions_management'   =>'Actions Management',
        'actions'              => 'Actions',
        'action'               => 'Action',
        'add_action'           => 'Add Action',
        'manage_actions'       => 'Manage Actions',
        'action_type'          => 'Action Type',
        'select_action'        => 'Select Action Type',
        'assignee'                  => 'Assignee',
    ],

];
