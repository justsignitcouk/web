<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentNote
 */
class DocumentNote extends Model
{
    protected $table = 'document_notes';

    public $timestamps = true;

    protected $fillable = [
        'content',
        'positions',
        'action_id',
        'account_id',
        'deleted'
    ];

    protected $guarded = [];

        
}