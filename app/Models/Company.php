<?php

namespace App\Models;
use App\Services\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Branch;
/**
 * Class Company
 */
class Company extends Model
{

    use SoftDeletes;
    
    /*variables*/
    protected $table = 'companies';
    public $timestamps = true;
    protected $guarded = [];
    protected $fillable = [
        'name',
        'slug',
        'license_no',
        'logo',
        'deleted',
        'deleted_by'
    ];

    
    /*relations*/
    public function branches(){
        return $this->hasMany('App\Models\Branch');
    }
    public function main_branch(){
            return $this->hasOne('App\Models\Branch')->where('is_main','=', 1);
    }
    public function Logo(){
    
        return $this->hasOne('App\Models\File', 'id', 'logo');
    }

    /*new company*/
    public function storeCompany($companyData){
         
        $company  = new Company ;

        //store company data
        $company->name = $companyData->name ;
        $company->slug      = $companyData->slug ;
        $company->license_no= $companyData->license_no ;

        if(!empty($companyData->logo)){
            $nLogo = File::put($companyData->logo);
            $company->logo = $nLogo;
        }
        

        $company->save();

        //create new account 
        $branch = new Branch;
        $BranchData =  (object) [
        'name'=> $companyData->branch_name ,
        'email'=> $companyData->branch_email,
        'company_id'=> $company->id,
        'license_no'=> $companyData->branch_license_no,
        'branch_type'=> $companyData->branch_type,
        'address'=> $companyData->branch_address,
        'country_id'=> $companyData->branch_country_id,
        'pobox'=> $companyData->branch_pobox,
        'phone'=> $companyData->branch_phone,
        'fax'=> $companyData->branch_fax,
        'is_main'=> $companyData->branch_is_main,
        'logo'=> $companyData->branch_logo,
         ];
        
        $branch->storeBranch($BranchData);

        return true ;

    }

    /*update company*/
    public function updateCompany($companyData , $id){

        $company = Company::find($id);

        $company->name = $companyData->name ;
        $company->slug = $companyData->slug ;
        $company->license_no = $companyData->license_no ;

        if(!empty($companyData->logo)){
            $nLogo = File::put($companyData->logo);
            $company->logo = $nLogo;
        }
        $company->save();

        return true ;

    }
        
}