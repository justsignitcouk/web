<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class StateAction extends Model
{
    protected $table = 'wf_state_action';

    public $timestamps = true;

    public function StateActionAssignee(){
        return $this->hasMany('\App\Models\Workflow\StateActionAssignee', 'action_state_id', 'id');
    }

    public function State()
    {
        return $this->belongsTo('App\Models\Workflow\State','state_id','id');
    }

    public function Action()
    {
        return $this->belongsTo('App\Models\Workflow\Actions','action_id','id');
    }

}
