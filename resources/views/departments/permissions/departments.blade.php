@foreach($departments_permissions as $key => $department)
    <div class="form-check">
      <label class="form-check-label">
        <input class="department-check-input" type="checkbox" name ="{{'departments-'.$key}}" value="{{$department->id}}"

        @foreach($checked_user_permissions as $check)

            @if($check['permission_id'] == $department->id)
            checked
            @endif
     
        @endforeach
       
        >
        {{$department->name}}
      </label>
    </div>
@endforeach