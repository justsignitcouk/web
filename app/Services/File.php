<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class File extends \App\Services\Services
{

    public function get($name){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("accounts/pending")
                ->setBody(['account_id'=>defaultAccountID()])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[] ],
            ] ;

            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return $response_array['data'] ;
    }

    public static function put($file,$path='common'){
        $oServices = Parent::getInstance();

        try{
            $file->move( storage_path() . '/tmp/'  , $file->getClientOriginalName() ) ;

            $aFile = ['name'     => 'file',
                'contents' => fopen( storage_path() . '/tmp/'  . $file->getClientOriginalName(),'r'),
                'filename' => $file->getClientOriginalName() ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("files/upload")
                ->setBody(['file' =>$aFile])
                ->setQuery(['path_type' =>$path ])
                ->withToken()
                ->setMultiPart(true)
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        Storage::delete('/tmp/file.jpg');
        return ['file_id' => $response_array['data']['file_id'],'oFile'=> $response_array['data']['oFile'] ];

    }


    public static function putDocument($file,$path='common'){
        $oServices = Parent::getInstance();

        try{
            $file->move( storage_path() . '/tmp/'  , $file->getClientOriginalName() ) ;

            $aFile = ['name'     => 'file',
                'contents' => fopen( storage_path() . '/tmp/'  . $file->getClientOriginalName(),'r'),
                'filename' => $file->getClientOriginalName() ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("document/import")
                ->setBody(['file' =>$aFile])
                ->setQuery(['path_type' =>$path ])
                ->withToken()
                ->setMultiPart(true)
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        Storage::delete('/tmp/file.jpg');
        return ['file_id' => $response_array['data']['file_id'],'document_id' => $response_array['data']['document_id'],'oFile'=> $response_array['data']['oFile'] ];

    }

}