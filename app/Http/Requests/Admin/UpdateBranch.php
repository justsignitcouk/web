<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:branches,email,'.$this->id,
            'license_no'=>'required|numeric',
            'address'=>'required',
            'pobox'=>'required|numeric',
            'phone'=>'required|numeric',
            'fax'=>'numeric',
            'branch_type'=>'required',
            'country_id'=>'required',
            //'is_main'=>'required',
            //'logo'=>'required',
            'company_id'=>'required',
        ];
    }
}
