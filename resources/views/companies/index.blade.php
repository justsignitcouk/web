@extends('layouts.admin')

@section('content')

    <!-- Ajax sourced data -->
    <div class="panel panel-flat">

        <div class="panel-heading">
            <h5 class="panel-title">{{ __("companies.companies") }}</h5>
        </div>

        <div class="panel-body">

            <ul class="breadcrumb">
                <li><a href="{{route('companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
            </ul>

            @can('allowed-permission', ['companies-create','companies-create'])
                <a href="{{ url('companies/create') }}"
                   class="btn btn-primary"
                   id="bootbox_form">
                    {{ __("companies.add_company") }}
                </a>
            @endcan    
          
            <table id="companies-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __("common.name") }}</th>
                        <th>{{ __("common.slug") }}</th>
                        <th>{{ __("common.license_no") }}</th>
                        <th>{{ __("common.created_at") }}</th>

                    </tr>
                </thead>
            </table>
        </div>
        <!-- /ajax sourced data -->

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });

     
        var table = $('#companies-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.companies.data') !!}',
            columns: [
                { data: 'name'},
                { data: 'slug'},
                { data: 'license_no'},
                { data: 'created_at',   render: function(data){
                    return data ;
                } },
            ]
        });
     


        $('.datatable-ajax').on('click', 'tbody td', function() {

            var data = table.row(this).data();
            var id = data.id ;
            window.location = '/admin/companies/' + id ;
            
        });


    });
</script>

@endpush