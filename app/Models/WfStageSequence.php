<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WfStageSequence
 */
class WfStageSequence extends Model
{
    protected $table = 'wf_stage_sequence';

    public $timestamps = false;

    protected $fillable = [
        'stage_id',
        'sequence'
    ];

    protected $guarded = [];

        
}