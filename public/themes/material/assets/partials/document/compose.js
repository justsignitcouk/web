$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    setTimeout(function() {

        tinymce.get("page_0").remove();

        tinymce.init({
            selector: 'div.editable',
            inline: true,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.outbox_number + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
            ]
        });
    },1000);
});