$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    var actions = $(".inbox_sequence_table .io_sequence_actions").html();
    // Append table with add row form on add new button click
    $(".add-inbox-new").click(function(){
        $(this).attr("disabled", "disabled");
        var index = $(".inbox_sequence_table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><select name="year_inbox" class="form-control year_inbox"><option>2018</option><option>2019</option><option>2020</option></select></td>' +
            '<td><input type="text" class="form-control start_sequence_inbox" name="department" id="start_sequence_inbox"></td>' +
            '<td><input type="text" class="form-control end_sequence_inbox" name="phone" id="end_sequence_inbox"></td>' +
            '<td>' + actions + '</td>' +
            '</tr>';

        $(".inbox_sequence_table").append(row);
        //$(".inbox_sequence_table tbody tr").eq(index + 1).find(".add_inbox_sequence, .edit_inbox_sequence").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
    // Add row on add button click
    $(document).on("click", ".add_inbox_sequence", function(){
        var empty = false;
        var input = $(this).parents("tr").find('input[type="text"],select');
        input.each(function(){
            if(!$(this).val()){
                $(this).addClass("error");
                empty = true;
            } else{
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if(!empty){
            var start_sequence = '';
            var end_sequence = '';
            var year = '';
            input.each(function(){
                if($(this).hasClass('start_sequence_inbox')){
                    start_sequence = $(this).val();
                }
                if($(this).hasClass('end_sequence_inbox')){
                    end_sequence = $(this).val();
                }
                if($(this).hasClass('year_inbox')){
                    year = $(this).val();
                }


                $(this).parent("td").html($(this).val());
            });
            var id = $(this).closest('tr').data('id');
            var tr = $(this).closest('tr');
            $.post(globals.unique_name + "/profile/save_sequence",{
                'id':id,
                'start_sequence':start_sequence,
                'end_sequence' : end_sequence,
                'year':year,
                'type':'inbox' },function(data){
                console.log(data);
                tr.data('id',data.id);
                tr.attr('id',data.id);
            },'json');
            $(this).parents("tr").find(".add_inbox_sequence, .edit_inbox_sequence").toggle();
            $(".add-inbox-new").removeAttr("disabled");
        }
    });
    // Edit row on edit button click
    $(document).on("click", ".edit_inbox_sequence", function(){
        var i = 0 ;

        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if(i==0){

                $(this).html('<select name="year_inbox" class="form-control year_inbox"><option>2018</option><option>2019</option><option>2020</option></select>');
                i++;
            }else if(i==1){
                $(this).html('<input type="text" class="form-control start_sequence_inbox" value="' + $(this).text() + '">');
                i++;
            }else {
                $(this).html('<input type="text" class="form-control end_sequence_inbox" value="' + $(this).text() + '">');
            }


        });
        $(this).parents("tr").find(".add_inbox_sequence, .edit_inbox_sequence").toggle();
        $(".add-inbox-new").attr("disabled", "disabled");
    });
    // Delete row on delete button click
    $(document).on("click", ".delete_inbox_sequence", function(){
        var id = $(this).closest('tr').data('id');
        $.post(globals.unique_name + "/profile/delete_sequence",{'id':id},function(data){
            console.log(data);
        },'json');
        $(this).parents("tr").remove();
        $(".add-inbox-new").removeAttr("disabled");
    });


    /************************** outbox *************************************/

    var actions = $(".outbox_sequence_table .io_sequence_actions").html();
    // Append table with add row form on add new button click
    $(".add-outbox-new").click(function(){
        $(this).attr("disabled", "disabled");
        var index = $(".outbox_sequence_table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><select name="year_outbox" class="form-control year_outbox"><option>2018</option><option>2019</option><option>2020</option></select></td>' +
            '<td><input type="text" class="form-control start_sequence_outbox" name="department" id="start_sequence_outbox"></td>' +
            '<td><input type="text" class="form-control end_sequence_outbox" name="phone" id="end_sequence_outbox"></td>' +
            '<td>' + actions + '</td>' +
            '</tr>';

        $(".outbox_sequence_table").append(row);
        //$(".outbox_sequence_table tbody tr").eq(index + 1).find(".add_outbox_sequence, .edit_outbox_sequence").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
    // Add row on add button click
    $(document).on("click", ".add_outbox_sequence", function(){
        var empty = false;
        var input = $(this).parents("tr").find('input[type="text"],select');
        input.each(function(){
            if(!$(this).val()){
                $(this).addClass("error");
                empty = true;
            } else{
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if(!empty){
            var start_sequence = '';
            var end_sequence = '';
            var year = '';
            input.each(function(){
                if($(this).hasClass('start_sequence_outbox')){
                    start_sequence = $(this).val();
                }
                if($(this).hasClass('end_sequence_outbox')){
                    end_sequence = $(this).val();
                }
                if($(this).hasClass('year_outbox')){
                    year = $(this).val();
                }


                $(this).parent("td").html($(this).val());
            });
            var id = $(this).closest('tr').data('id');
            var tr = $(this).closest('tr');
            $.post(globals.unique_name + "/profile/save_sequence",{'id':id,
                'start_sequence':start_sequence,
                'end_sequence' : end_sequence,
                'year':year,
                'type':'outbox'},function(data){
                console.log(data);
                tr.data('id',data.id);
                tr.attr('id',data.id);
            },'json');
            $(this).parents("tr").find(".add_outbox_sequence, .edit_outbox_sequence").toggle();
            $(".add-outbox-new").removeAttr("disabled");
        }
    });
    // Edit row on edit button click
    $(document).on("click", ".edit_outbox_sequence", function(){
        var i = 0 ;

        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if(i==0){

                $(this).html('<select name="year_outbox" class="form-control year_outbox"><option>2018</option><option>2019</option><option>2020</option></select>');
                i++;
            }else if(i==1){
                $(this).html('<input type="text" class="form-control start_sequence_outbox" value="' + $(this).text() + '">');
                i++;
            }else {
                $(this).html('<input type="text" class="form-control end_sequence_outbox" value="' + $(this).text() + '">');
            }


        });
        $(this).parents("tr").find(".add_outbox_sequence, .edit_outbox_sequence").toggle();
        $(".add-outbox-new").attr("disabled", "disabled");
    });
    // Delete row on delete button click
    $(document).on("click", ".delete_outbox_sequence", function(){
        var id = $(this).closest('tr').data('id');
        $.post(globals.unique_name + "/profile/delete_sequence",{'id':id},function(data){
            console.log(data);
        },'json');
        $(this).parents("tr").remove();
        $(".add-outbox-new").removeAttr("disabled");
    });






    /************************** outbox *************************************/


});