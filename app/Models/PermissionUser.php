<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleUser
 */
class PermissionUser extends Model
{
    protected $table = 'permission_user';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'permission_id'
    ];

    protected $guarded = [];

}