<?php

namespace App\Http\Controllers\Premium;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyDetailsRequest;
use App\Services\Account;
use App\Services\Company;
use App\Services\Lookup;
use App\Services\Plans;
use App\Services\Services;
use App\Services\User;
use DeepCopy\f008\A;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AccountsController extends Controller
{

    public function index(){
        $aAccounts = Account::findBy(['operation'=>'=','key'=>'company_id','value'=>currentCompanyID()]);
        return Theme::view('premium.settings.accounts.index', ['aAccounts'=>$aAccounts]);
    }

    public function details($id) {
        $aAccount = Account::findFirstById($id) ;
        return Theme::view('premium.settings.accounts.details', ['aAccount'=>$aAccount]);

    }

}
