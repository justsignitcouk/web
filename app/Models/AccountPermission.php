<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountPermission
 */
class AccountPermission extends Model
{
    protected $table = 'account_permission';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'permission_id',
        'is_granted'
    ];

    protected $guarded = [];

        
}