$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    $('.delete_template').on('click',function(e){
        var id = $(this).data('id') ;
        $('#template_id').val(id);
        $('#global_modal').modal('show');
        e.preventDefault();
    })

    $('#delete_template_submit').on('click',function(e){
        var id = $('#template_id').val();
        $.post('/templates/delete',{'id':id},function(data){
           location.reload();
        });
        e.preventDefault();
    });

});