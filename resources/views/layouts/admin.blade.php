<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" {{}}>

    @include('layouts.partials.htmlheader')




<body class="">

@include('layouts.partials.mainheader')
@include('layouts.partials.sidebar')
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
             <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                @yield('content')
            </div>
            <!-- Content area -->
        </div>
    </div>
</div>
</body>
</html>







