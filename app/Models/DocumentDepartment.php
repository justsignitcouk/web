<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentDepartment
 */
class DocumentDepartment extends Model
{
    protected $table = 'document_department';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'department_id'
    ];

    protected $guarded = [];

        
}