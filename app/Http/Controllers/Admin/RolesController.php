<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\RoleRequest;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\RoleUser;
use App\Models\RoleType;
use Yajra\Datatables\Datatables;
use App\Services\RolesService;
use DB;
use Gate;

class RolesController extends Controller
{

    /**
     * @var role
     */
    protected $role;

    /**
     * @var rolesService
     */
    protected $rolesService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Role $role, RolesService $rolesService)
    {
        $this->middleware('auth');
        $this->role = $role;
        $this->rolesService = $rolesService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $all_permissions = Permission::all();
            $role_types = RoleType::all();


            //users module permissions
            $users_permissions = $this->rolesService
                ->Permissions('users');

            //companies module permissions
            $companies_permissions = $this->rolesService
                ->Permissions('companies');

            //accounts module permissions
            $accounts_permissions = $this->rolesService
                ->Permissions('accounts');

            //permissions module permissions
            $permissions_permissions = $this->rolesService
                ->Permissions('permissions');
            //branches module permissions
            $branches_permissions = $this->rolesService
                ->Permissions('branches');

            //roles module permissions
            $roles_permissions = $this->rolesService
                ->Permissions('roles');

            //roles module permissions
            $departments_permissions = $this->rolesService
                ->Permissions('departments');
            //workflow module permissions
            $workflow_permissions = $this->rolesService
                ->Permissions('workflow');
            //requirements module permissions
            $requirements_permissions = $this->rolesService
                ->Permissions('requirements');
            return view('roles.create',
                compact(
                    'all_permissions',
                    'users_permissions',
                    'companies_permissions',
                    'accounts_permissions',
                    'permissions_permissions',
                    'branches_permissions',
                    'roles_permissions',
                    'departments_permissions',
                    'workflow_permissions',
                    'requirements_permissions',
                    'role_types'
                ));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $roleRequest)
    {
            if ($this->role->storeRole($roleRequest)) {
                return redirect('/roles');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
            $role = Role::find($id)->toArray();

            $role_types = RoleType::all();

            $active_edit = 1;

            //check role permissions
            $checked_user_permissions = $this->rolesService
                ->checkedRolePermissions($id);


            //all permissions in database
            $all_permissions = Permission::all();

            //users module permissions
            $users_permissions = $this->rolesService
                ->Permissions('users');

            //companies module permissions
            $companies_permissions = $this->rolesService
                ->Permissions('companies');

            //accounts module permissions
            $accounts_permissions = $this->rolesService
                ->Permissions('accounts');

            //permissions module permissions
            $permissions_permissions = $this->rolesService
                ->Permissions('permissions');
            //branches module permissions
            $branches_permissions = $this->rolesService
                ->Permissions('branches');

            //roles module permissions
            $roles_permissions = $this->rolesService
                ->Permissions('roles');

            //roles module permissions
            $departments_permissions = $this->rolesService
                ->Permissions('departments');
            //workflow module permissions
            $workflow_permissions = $this->rolesService
                ->Permissions('workflow');
            //requirements module permissions
            $requirements_permissions = $this->rolesService
                ->Permissions('requirements');

            return view('roles.edit', compact(
                'role',
                'users_permissions',
                'companies_permissions',
                'accounts_permissions',
                'permissions_permissions',
                'branches_permissions',
                'roles_permissions',
                'departments_permissions',
                'workflow_permissions',
                'requirements_permissions',
                'checked_user_permissions',
                'active_edit',
                'role_types'
            ));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $roleRequest, $id)
    {
            if ($this->role->updateRole($roleRequest, $id)) {
                return redirect('/roles');
            }

    }

    //view role data 
    public function show($id)
    {

            $role = Role::find($id)->toArray();

            $role_permissions =
                DB::table('permission_role')
                    ->join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
                    ->select('permissions.name')
                    ->where('permission_role.role_id', $id)
                    ->get()
                    ->toArray();

            $active_show = 1;

            return view('roles.show', compact('role', 'active_show', 'role_permissions'));

    }

    //delete role by ajax request
    public function destroy($id)
    {
            $role = Role::find($id);
            $check_users = RoleUser::select('user_id')->where('role_id', '=', $role->id)->get()->toArray();
            if (count($check_users) == 0) {
                $role->delete();
            } else {
                return $msg = 'faild';
            }

    }


    //list of content for datatable in index
    public function datatables()
    {

        $aRole = Role::all()->toArray();
        return Datatables::of($aRole)->make(true);
    }

}
