@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

				<ul class="breadcrumb">
					<li><a href="{{route('admin.users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
					<li><a href="{{route('admin.users.profile' , $user['id'])}}"><i class="position-left"></i>{{ $user['username'] }}</a></li>
					<li class="active">{{ __("common.show") }}</li>
				</ul>

				@include('layouts/partials/tabs')

				<div class="tab-content">
                 <div class="panel panel-flat">
                    <div class="panel-body">

							<table class="table">

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("users.username") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$user['username']}}</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("users.first_name") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$user['first_name']}}</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("users.last_name") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$user['last_name']}}</span>
										</td>
									</tr>

									<tr class="form-group mt-5">
										<td>
											<label class="text-semibold">{{ __("users.email") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$user['email']}}</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("common.activated") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">
												@if($user['activated'] == 1)
													{{__("common.yes")}}
												@elseif($user['activated'] == 0)
													{{__("common.no")}}
												@endif
											</span>

										</td>
									</tr>

									<tr class="form-group mt-5">
										<td>
											<label class="text-semibold">{{ __("common.created_at") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">
                                            <?php

                                            $date = new DateTime($user['created_at']) ;
                                            echo $date->format('g:ia \o\n l jS F Y');
                                            ?>
											</span>
										</td>
									</tr>

									<tr class="form-group mt-5">
										<td>
											<label class="text-semibold">{{ __("users.profile_image") }}:</label>
										</td>
										<td>
											<div class="pull-right-sm">
												<img src="{{cdn_url($sImageProfileName,$sImageProfilePath)}}" alt="" style="max-width: 115px;max-height: 90px;">
											</div>
										</td>
									</tr>

							    </table>

		             		</div>
		            </div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
