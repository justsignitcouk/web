<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Folder
 */
class Folder extends Model
{
    protected $table = 'folders';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'account_id',
        'parent_folder_id',
        'deleted',
        'sort'
    ];

    protected $guarded = [];

        
}