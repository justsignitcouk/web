<?php

return [

    /*
    |--------------------------------------------------------------------------
    | UsersController Language Lines
    |--------------------------------------------------------------------------
    */

    'branches' => 'Branches' ,
    'add_branche' => 'Add Branche' ,
    'branche_type'=> 'Branche Type' ,
    'add_branch'=>'Add Branch',
    'delete_branch'=>'Are you sure you want to delete this branch',
    'cant_delete_branch' =>'Sorry you can not delete branch which contains departments',
    'branch_name'=>'Branch Name',
    'license_no'=>'License No',
    'address'=>'Address',
    'pobox'=>'Pobox',
    'phone'=>'Phone',
    'fax'=>'Fax',
    'email'=>'Email',
    'branch_type'=>'Branch Type',

];