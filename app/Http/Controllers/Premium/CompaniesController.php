<?php

namespace App\Http\Controllers\Premium;

use App\Http\Controllers\Controller;
use App\Http\Requests\BranchDetailsRequest;
use App\Http\Requests\CompanyDetailsRequest;
use App\Services\Account;
use App\Services\Company;
use App\Services\Lookup;
use App\Services\Plans;
use App\Services\Quota;
use App\Services\Services;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CompaniesController extends Controller
{

    #create new company from business account

    public function getCompanies(){
        $oCompaniesQuota = getQuota('number_of_companies');
        return Theme::view('premium.settings.companies', ['oCompaniesQuota'=>$oCompaniesQuota]);
    }

    public function CompaniesAjax(){
        $oCompanies = Company::getCompanyByCreatorID();
        return DataTables::of($oCompanies['companies'])->make(true);
    }

    public function createCompanyForm($sSlug=false){
        if($sSlug){
            $oCompany = Company::getCompanyBySlug($sSlug);
            view()->share('oCompany', $oCompany['company']);
        }

        $oCountriesLookup = Lookup::getCountries()['data'] ;
        $oCountries = array_column($oCountriesLookup, 'name', 'id');

        $oBranchTypesLookup = Lookup::getBranchType()['data'] ;
        $oBranchTypes = array_column($oBranchTypesLookup, 'name', 'id');

        return Theme::view('premium.settings.create_company', [ 'oCountries' => $oCountries,'oBranchTypes' => $oBranchTypes, ]);
     }

    public function getCompaniesDetailsBySlug($sSlug){
        $oCompany = Company::getCompanyBySlug($sSlug);
        return Theme::view('premium.settings.companies.details', [ 'oCompany' => $oCompany,'sSlug'=>$sSlug]);
    }

    public function getBranches($sSlug){
        $oCompany = Company::getCompanyBySlug($sSlug);
        $oBranch = $oCompany['company']['branches'];

        return DataTables::of($oBranch)->make(true);
    }

    public function branchDetails($sSlug,$branch_id){
        $oCompany = Company::getCompanyBySlug($sSlug);
        $oBranch = Company::getBrachById($sSlug,$branch_id);
        return Theme::view('premium.settings.companies.branch_details', [ 'oCompany'=>$oCompany,'oBranch' => $oBranch,'sSlug'=>$sSlug,'branch_id'=>$branch_id]);
    }

     public function store(CompanyDetailsRequest $request) {

        $data = $request->all() ;
        $saveCompany = Company::save($data);

        if(!$saveCompany['response_code']) {
            session()->flash('alert-danger', $saveCompany['response_message']);
            return redirect()->to(route('settings.create-company')) ;
        }

         return redirect()->to(route('settings.companies')) ;
     }

    public function createBranchForm($sSlug,$nBranchID=false){

            $oCompany = Company::getCompanyBySlug($sSlug);

            view()->share('oCompany', $oCompany['company']);

        $oCountriesLookup = Lookup::getCountries()['data'] ;
        $oCountries = array_column($oCountriesLookup, 'name', 'id');

        $oBranchTypesLookup = Lookup::getBranchType()['data'] ;
        $oBranchTypes = array_column($oBranchTypesLookup, 'name', 'id');

        return Theme::view('premium.settings.create_branch', ['sSlug'=>$sSlug, 'oCountries' => $oCountries,'oBranchTypes' => $oBranchTypes, ]);
    }


    public function storeBranch(BranchDetailsRequest $request,$sSlug) {

        $data = $request->all() ;
        $data['slug'] = $sSlug ;
        $saveCompany = Company::saveBranch($data);

        if(!$saveCompany['response_code']) {
            session()->flash('alert-danger', $saveCompany['response_message']);
            return redirect()->to(route('settings.companies_details',[$sSlug])) ;
        }

        return redirect()->to(route('settings.companies_details',[$sSlug])) ;
    }


    public function getEmployees($sSlug){
        $oEmployees = Company::getEmployeesByCompanySlug($sSlug);
        if(isset($oEmployees['employees'])) {
            return DataTables::of($oEmployees['employees'])->make(true);
        }
        return DataTables::of( [] )->make(true);

    }

    public function getRoles($sSlug){
        $oRoles = Company::getRolesByCompanySlug($sSlug);

        if(isset($oRoles['roles'])) {
            return DataTables::of($oRoles['roles'])->make(true);
        }
        return DataTables::of( [] )->make(true);
    }


    public function checkAvailability(Request $request) {
         $data = $request->all() ;
         $data['value'] = $request->get('slug');
         $slug = Company::checkAvailability($data);
         if($request->has('slug')){
             if($slug['status'] == 'false'){
                 return 'false';
             }else{
                 return 'true' ;
             }
         }
         return \GuzzleHttp\json_encode($slug) ;
     }

}
