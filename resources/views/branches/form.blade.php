
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($branch['id']))
            <h5 class="panel-title">{{ __("branches.add_branch") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 

            <!-- First column -->
	        <div class="col-md-6">

                {{ csrf_field() }}

                <meta name="csrf-token" content="{{ csrf_token() }}">

                @if(!empty($branch['id']))
                <input type="hidden" name="company_id" value="{{$branch['company_id']}}">
                @else
                <input type="hidden" name="company_id" value="{{$company_id}}">
                @endif 
  
		        <div class="form-group">
		            
		            <label>
			            {{ __("common.name") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>  :
		            </label>

                    <input type="text"
		                   name="name"
		                   class="form-control"
		                   placeholder='{{ __("common.name") }}'
		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('name') }}"
			                    @else
			                    	value="{{ $branch['name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('name') }}"
		                   @endif >

	                @if ($errors->has('name'))
		                <font color="red">{{ $errors->first('name') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.license_no") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>  :
		            </label>

                    <input type="text"
		                   name="license_no"
		                   class="form-control"
		                   placeholder='{{ __("common.license_no") }}'
		                   @if(!empty($branch['id']))
					                    @if(count($errors) > 0)
					                        value="{{ old('license_no') }}"
					                    @else
					                    	value="{{ $branch['license_no'] }}"
					                    @endif  
				                   @else
				                    value="{{ old('license_no') }}"
				                   @endif >

			                @if ($errors->has('license_no'))
				                <font color="red">{{ $errors->first('license_no') }}</font>
				        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.address") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <input type="text"
		                   name="address"
		                   class="form-control"
		                   placeholder='{{ __("common.address") }}'

		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('address') }}"
			                    @else
			                    	value="{{ $branch['address'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('address') }}"
		                   @endif >

	                @if ($errors->has('address'))
		                <font color="red">{{ $errors->first('address') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.pobox") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <input type="text"
		                   name="pobox"
		                   class="form-control"
		                   placeholder='{{ __("common.pobox") }}'
		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('pobox') }}"
			                    @else
			                    	value="{{ $branch['pobox'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('pobox') }}"
		                   @endif >

	                @if ($errors->has('pobox'))
		                <font color="red">{{ $errors->first('pobox') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.phone") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <input type="text"
		                   name="phone"
		                   class="form-control"
		                   placeholder='{{ __("common.phone") }}'
		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('phone') }}"
			                    @else
			                    	value="{{ $branch['phone'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('phone') }}"
		                   @endif >

	                @if ($errors->has('phone'))
		                <font color="red">{{ $errors->first('phone') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.fax") }}
			              :
		            </label>

                    <input type="text"
		                   name="fax"
		                   class="form-control"
		                   placeholder='{{ __("common.fax") }}'
		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('fax') }}"
			                    @else
			                    	value="{{ $branch['fax'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('fax') }}"
		                   @endif >

	                @if ($errors->has('fax'))
		                <font color="red">{{ $errors->first('fax') }}</font>
		        	@endif

		        </div>
		        
		    </div>
            <!-- Second column -->
		    <div class="col-md-6">
            	<div class="form-group">
		            
		            <label>
			            {{ __("common.email") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>  :
		            </label>

                    <input type="text"
		                   name="email"
		                   class="form-control"
		                   placeholder='{{ __("common.email") }}'
		                   @if(!empty($branch['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('email') }}"
			                    @else
			                    	value="{{ $branch['email'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('email') }}"
		                   @endif >

	                @if ($errors->has('email'))
		                <font color="red">{{ $errors->first('email') }}</font>
		        	@endif

		        </div>

		        
		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.branch_type") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <select  class="col-md-12"  name="branch_type">  
                         <option disabled selected value>
                         	{{ __("common.select") }}
                         </option>
			        	@foreach($branchTypes as $branchType)
                            
                        
                            @if(count($errors) > 0)

                            <option 
                            @if(old('branch_type')==$branchType['id'])
								selected 
                            @endif
		                    value="{{$branchType['id']}}" 
		                    >
		                    {{$branchType['name']}}
		                    </option>

		                    @elseif(!empty($branch['branch_type_id']) && $branch['branch_type_id'] == $branchType['id'])

		                    <option selected
		                    value="{{$branch['branch_type_id']}}" >
		                    {{$branchType['name']}}
		                    </option>

                            @else

	                        <option 
		                    value="{{$branchType['id']}}" >
		                    {{$branchType['name']}}
		                    </option>

		                    @endif

                        @endforeach
		        	</select>
                    <br/>
	                @if ($errors->has('branch_type'))
		                <font color="red">{{ $errors->first('branch_type') }}</font>
		        	@endif

		        </div>
                <br/><br/>
		        <div class="form-group">
		            
		            <label>
			            {{ __("common.country") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <select  class="col-md-12"  name="country_id"> 
                        <option disabled selected value>
                         	{{ __("common.select") }}
                        </option> 
			        	@foreach($countries as $countriy)
                           
                            @if(count($errors) > 0)
                            <option 
                            @if(old('country_id')==$countriy->id)
								selected 
                            @endif
		                    value="{{$countriy->id}}" 
		                    >
		                    {{$countriy->name}}
		                    </option>
		                    @elseif(!empty($branch['country_id']) && $branch['country_id'] == $countriy->id)

		                    <option selected
		                    value="{{$countriy->id}}" >
		                    {{$countriy->name}}
		                    </option>

                            @else
	                        <option 
		                    value="{{$countriy->id}}" >
		                    {{$countriy->name}}
		                    </option>
		                    @endif
                        @endforeach
		        	</select>
                    <br/>
	                @if ($errors->has('country_id'))
		                <font color="red">{{ $errors->first('country_id') }}</font>
		        	@endif

		        </div>
                <br/><br/><br/>
		        
		        <div class="form-group">
		            
		            <label>
			            {{ __("common.main") }} :    
		            </label>

                    <input type="checkbox"
		                   name="is_main"
		                   class="js-switch"
		                   @if(!empty($branch['is_main']) && $branch['is_main'] == 1)
		                    checked
		                   @else
		                    
		                   @endif >

	                @if ($errors->has('is_main'))
		                <font color="red">{{ $errors->first('is_main') }}</font>
		        	@endif

		        </div>
                <br/>
		        <div class="form-group">

		            <label>
			            {{ __("common.logo") }}
			             @if(!empty($brancch['id']))
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>  
			             @endif
			             :
		            </label>

		        	<div>
	                    <input type="file" name="logo" class="pull-left">
	                    
	                    @if(!empty($branch['id']))
							<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	                    @endif
                    </div>

					@if ($errors->has('logo'))
		                <font color="red">{{ $errors->first('logo') }}</font>
		        	@endif
		        </div>

		    </div>
	    </div>

	    <div class="row col-md-12">

         	{{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

            @if(!empty($branch["id"]))
            @can('allowed-permission', ['branches-delete','branches-delete'])
            <div class="btn btn-danger pull-right"   class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_theme_danger">{{__("common.delete")}}</div>
            @endif
            @endcan

	    </div>    
    </div>
</div>

@if(!empty($branch["id"]))
<div id="modal_theme_danger" class="modal fade delete-caution" style="display: none;">
 <input id='branch-data' type="hidden" data-company_id='{{$branch["company_id"]}}' data-id='{{$branch["id"]}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("branches.delete_branch")}}
				</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">
					{{__("common.cancel")}}
					</button>
					<button type="button" class="btn btn-danger" id='delete-branch'>
					{{__("common.delete")}}
					</button>
				</div>
		    </div>
	    </div>
    </div>
</div>
@endif
<div id="modal_theme_danger" class="modal fade cant-delete" style="display: none;">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("branches.cant_delete_branch")}}
				</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">
					{{__("common.cancel")}}
					</button>
					<button type="button" class="btn btn-danger" id='delete-branch'>
					{{__("common.delete")}}
					</button>
				</div>
		    </div>
	    </div>
    </div>
</div>

<script type="text/javascript">

    var elem = document.querySelector('.js-switch');
	var init = new Switchery(elem);
	var _token = $('input[name="_token"]').val();

	$('#delete-branch').click(function() {
        var id = $('#branch-data').data("id");
        var company_id = $('#branch-data').data("company_id");

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('branches' ) !!}" + "/" + id +'/delete',
            data: { _token : _token },
	        success: function( msg ) {

	        	if(msg == 'cant'){

                    $('.delete-caution').modal('hide');
                    $('.cant-delete').modal('show');

	        	}else if(msg == 'can'){

                location.replace("/companies/"+company_id+"/branches");
	        	}

	        },
	        error: function( data ) {

	        }
	    });
     });

</script>
