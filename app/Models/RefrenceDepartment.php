<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RefrenceDepartment
 */
class RefrenceDepartment extends Model
{
    protected $table = 'refrence_department';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'department_id'
    ];

    protected $guarded = [];

        
}