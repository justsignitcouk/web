<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{

    use SoftDeletes;
    protected $table = 'prices_plans';

    protected $fillable = [
        'title', 'description', 'price', 'most_popular', 'amount', 'activated'
    ];


}
