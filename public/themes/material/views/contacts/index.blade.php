{!! Theme::asset()->themePath()->add([ ['d3', 'js/plugins/visualization/d3/d3.min.js', ['blockui']]]) !!}
{!! Theme::asset()->themePath()->add([ ['d3_tooltip', 'js/plugins/visualization/d3/d3_tooltip.js', ['d3']]]) !!}
{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['datatables_responsive', 'assets/components/datatables-responsive/js/dataTables.responsive.js',['datatables']]]) !!}
{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js',['bootstrap']]]) !!}

{!! Theme::asset()->add([ ['validate', 'assets/components/validation/dist/jquery.validate.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['validate_additonal', 'assets/components/validation/dist/additional-methods.js',['validate']]]) !!}

{!! Theme::asset()->themePath()->add([ ['tagsinput', 'js/plugins/forms/tags/tagsinput.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['tokenfield', 'js/plugins/forms/tags/tokenfield.min.js',['tagsinput']]]) !!}
{!! Theme::asset()->themePath()->add([ ['prism', 'js/plugins/ui/prism.min.js',['tagsinput']]]) !!}
{!! Theme::asset()->themePath()->add([ ['typeahead', 'js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',['tagsinput']]]) !!}

{!! Theme::asset()->themePath()->add([ ['compose', 'pages/contacts/index.js',['datatables']]]) !!}


@partial('common.modal',['disable_button'=>'true','modal_id'=>'import_contact','title'=>'Import','buttons'=>['main_button'=>['title'=>'Save']],'content'=> ['type'=>'partial','partial_name'=>'modals.import_contact'] ])


<!-- Main content -->
<div class="content-wrapper">


    <!-- Content area -->
    <div class="content">
        <!-- Horizontal icons -->

        {{--<h6 class="content-group text-semibold">
            <span data-i18n="contacts">{{__('contacts.title')}}</span>
            <small class="display-block"></small>
        </h6>--}}


        {{--add contact button--}}
        @if(session()->has('success'))
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">×</a>
                <span data-i18n="conn.success">{!! session()->get('success') !!}</span>
            </div>

        @endif
        <div class="text-right add_conntact">


            <a href="" class="no-smoothState btn btn-default btn-sm bg-indigo import_contact" data-toggle="modal"
               data-target="#import_contact"><span data-i18n="conn.import_contacts">Import Contacts</span> <i
                        class="icon-play3 position-right"></i></a>


        </div>
        <div id="modal_default" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><span class='add_title' data-i18n="conn.add">Add Contact</span></h5>
                    </div>
                    <form action="" class="form-validate-jquery" role="form" method="POST" id="add_contact_form">
                        <div class="modal-body">

                            <div class="form-group has-feedback">
                                <label for="name" data-i18n="conn.label.name"></label>
                                <input type="name" name="name_modal" class="form-control" id="name_modal">
                                <span class="help-block"></span>
                            </div>


                            <div class="form-group has-feedback">
                                <label for="email" data-i18n="conn.label.email"></label>
                                <input type="email" name="email_modal" class="form-control" id="email_modal">
                                <span class="help-block"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="mobile" data-i18n="conn.label.mobile"></label>
                                <input type="mobile" name="mobile_modal" class="form-control" id="mobile_modal">
                                <span class="help-block"></span>
                            </div>

                            <!-- Typeahead support -->
                            <div class="form-group">
                                <label>Groups</label>
                                <input type="text" value="" data-role="tagsinput" class="tagsinput-typeahead" id="group_input_add">
                            </div>
                            <!-- /typeahead support -->


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal"
                                    data-i18n="conn.close"></button>
                            <button type="button" class="btn btn-primary" id="add_contact" data-i18n="save"></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Add Contact Modal section-->

        <!-- Edit Contact Modal section-->
        <div id="modal_edit" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><span data-i18n="conn.edit">Edit Contact</span></h5>
                    </div>

                    <div class="modal-body">


                            <div class="form-group">
                                <label for="name" data-i18n="conn.label.name"></label>
                                <input type="name" class="form-control" id="name_edit_modal">
                            </div>

                        <div class="form-group">
                            <label for="email" data-i18n="conn.label.email"></label>
                            <input type="email" class="form-control" id="email_edit_modal">
                        </div>


                        <div class="form-group">
                            <label for="mobile" data-i18n="conn.label.mobile"></label>
                            <input type="mobile" class="form-control" id="mobile_edit_modal">
                        </div>


                        <!-- Typeahead support -->
                        <div class="form-group">
                            <label>Groups</label>
                            <input type="text" value=""  id="group_input_edit">
                        </div>

                    </div>


                    <div class="modal-footer">
                        <input type="hidden" id="id_edit_modal">
                        <button type="button" class="btn btn-link" data-dismiss="modal" data-i18n="conn.close"></button>
                        <button type="button" class="btn btn-primary" id="edit_contact" data-i18n="save"></button>
                        <button type="button" class="btn btn-danger delete-contact" id="delete_contact" data-id=""
                                data-i18n="delete">delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Contact Modal section-->

        <!-- Add Group Modal section-->
        <div id="modal_group_add" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><span data-i18n="group.add"></span></h5>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" data-i18n="group.label.name">Group Name</label>
                            <input type="name" class="form-control" id="group_name_modal">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"
                                data-i18n="group.close"></button>
                        <button type="button" class="btn btn-primary" id="add_group" data-i18n="save"></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add group Modal section end-->
        <!-- edit Group Modal section-->
        <div id="modal_group_edit" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><span data-i18n="group.edit"></span></h5>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" data-i18n="group.label.name">Group Name</label>
                            <input type="name" class="form-control" id="group_name_edit_modal">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" id="group_id_edit_modal">
                        <button type="button" class="btn btn-link" data-dismiss="modal"
                                data-i18n="group.close"></button>
                        <button type="button" class="btn btn-primary" id="edit_group" data-i18n="save"></button>
                        <button type="button" class="btn btn-danger delete-group" data_id="" id="delete_group"
                                data-i18n="delete">delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- edit group Modal section end-->

        {{--add contact button--}}

        <div class="col-md-12">
            <div class="panel panel-flat contacts_page">
                <div class="panel-body">
                    <div class="tabbable ">


                        <div class="navbar navbar-inverse bg-white navbar-component "
                             style="position: relative; z-index: 30">
                            <div class="navbar-header">

                                <ul class="nav navbar-nav pull-right visible-xs-block">
                                    <li><a data-toggle="collapse" data-target="#navbar-mixed" style="color:#313131"><i
                                                    class="icon-menu"></i></a></li>
                                </ul>
                            </div>


                            <div class="navbar-collapse collapse" id="navbar-mixed">

                                <ul class="nav nav-tabs nav-tabs-highlight .nav-justified" style="margin-bottom: 0">
                                    <li class="active"><a href="#contacts " data-toggle="tab" data-type="contact" data-i18n="conn.contacts">
                                            Contacts</a></li>
                                    <li><a href="#groups" data-toggle="tab" data-type="group" data-i18n="conn.groups">Groups </a></li>
                                </ul>


                            </div>

                        </div>


                        <div class="tab-content">
                            <div class="tab-pane active" id="contacts">
                                <button type="button" class="btn btn-default btn-sm bg-indigo add_contact_button_open_dialog" data-toggle="modal"
                                        data-target="#modal_default"><span data-i18n="conn.add">Add Contact</span> <i
                                            class="icon-play3 position-right"></i></button>
                                <div class="content-table">
                                    <table class="table datatable-ajax {{--table-inbox--}} table-contact responsive  table-lay"
                                           id="contact_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th data-i18n="name">Name</th>
                                            <th data-i18n="email">Email</th>
                                            <th data-i18n="mobile">Mobile</th>
                                            <th data-i18n="group">Group</th>
                                            <th></th>

                                        </tr>
                                        </thead>

                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane" id="groups">
                                <button type="button" class="btn btn-default btn-sm bg-indigo" data-toggle="modal"
                                        data-target="#modal_group_add"><span data-i18n="group.add">Add Group</span> <i
                                            class="icon-play3 position-right"></i></button>
                                <div class="content-table">
                                    <table class="table datatable-ajax {{--table-inbox--}} responsive  table-lay"
                                           id="group_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>Group Name</th>
                                            <th>Contacts</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var contact_url = 'contacts';
    var sGroups = [ {!! $sGroups  !!} ];
</script>
