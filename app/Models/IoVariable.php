<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class IoVariable
 */
class IoVariable extends Model
{
    protected $table = 'io_variables';

    public $timestamps = false;

    protected $fillable = [
        'type',
        'type_id',
        'io_numbering_id'
    ];

    protected $guarded = [];

        
}