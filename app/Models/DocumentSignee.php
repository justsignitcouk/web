<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentSignee
 */
class DocumentSignee extends Model
{
    protected $table = 'document_signee';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'document_id',
        'is_sign',
        'acting_id',
        'description'
    ];

    protected $guarded = [];

        
}