<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentHistory
 */
class DocumentHistory extends Model
{
    protected $table = 'document_history';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'content',
        'account_id'
    ];

    protected $guarded = [];

        
}