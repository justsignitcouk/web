@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("branches.branches") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

                <ul class="breadcrumb">
                    <li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
                    <li><a href="{{route('admin.companies.show' , $company['id'])}}"><i class="position-left"></i>{{ $company['name'] }}</a></li>
                    <li class="active">{{ __("common.branches") }}</li>
                </ul>
				
				@include('layouts/partials/tabs/companies_tabs')

                <input type="hidden" id="company_data"data-company_id="{{$company['id']}}">

				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">

						  
                            @can('allowed-permission', ['branches-create','branches-create'])
			                <a href="{{ url('companies/'.$company['id'].'/createbranche') }}"
			                   class="btn btn-primary"
			                   id="bootbox_form">
			                    {{ __("branches.add_branch") }}
			                </a>
                            @endcan
			        

				            <table id="branches-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
				                <thead>
				                    <tr>
				                        <th>{{ __("common.name") }}</th>
                                        <th>{{ __("common.country") }}</th>
				                        <th>{{ __("common.address") }}</th>
				                        <th>{{ __("common.email") }}</th>
                                        <th>{{ __("common.phone") }}</th>
                                        <th>{{ __("common.is_main") }}</th>
                                        <th>{{ __("common.created_at") }}</th>
				                    </tr>
				                </thead>
				            </table>

		             	</div>
				    </div>
			    </div>
			</div>
		</div>
</div>

</div>

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });

       
        var company_id = $('#company_data').data('company_id');

        var table = $('#branches-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.company.branch.datatables',[$id] ) !!}' ,
            columns: [
                { data: 'name',         name: 'name' },
                { data: 'country.name', name: 'country' },
                { data: 'address',      name: 'address' },
                { data: 'email',        name: 'email' },
                { data: 'phone',        name: 'phone' },
                { data: 'is_main',      render:function (data){

                    if(data == 1){return 'Yes'}
                    else{return 'No'}

                }},

                { data: 'created_at',   name: 'created_at' },

            ]
        });


        $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/admin/combranches/' + id ;

        });


       });    
</script>

@endpush
