/* ------------------------------------------------------------------------------
 *
 *  # Template JS core
 *
 *  Core JS file with default functionality configuration
 *
 *  Version: 1.3
 *  Latest update: Aug 10, 2016
 *
 * ---------------------------------------------------------------------------- */


// Allow CSS transitions when page is loaded
$(window).on('load', function() {
    $('body').removeClass('no-transitions');
});


$(function() {

    // Disable CSS transitions on page load
    $('body').addClass('no-transitions');



    // ========================================
    //
    // Content area height
    //
    // ========================================


    // Calculate min height
    // function containerHeight() {
    //     var availableHeight = $(window).height() - $('.page-container').offset().top - $('.navbar-fixed-bottom').outerHeight();
    //
    //     $('.page-container').attr('style', 'min-height:' + availableHeight + 'px');
    // }
    //
    // // Initialize
    // containerHeight();




    // ========================================
    //
    // Heading elements
    //
    // ========================================


    // Heading elements toggler
    // -------------------------

    // Add control button toggler to page and panel headers if have heading elements
    $('.panel-footer').has('> .heading-elements:not(.not-collapsible)').prepend('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');
    $('.page-title, .panel-title').parent().has('> .heading-elements:not(.not-collapsible)').children('.page-title, .panel-title').append('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');


    // Toggle visible state of heading elements
    $('.page-title .heading-elements-toggle, .panel-title .heading-elements-toggle').on('click', function() {
        $(this).parent().parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
    });
    $('.panel-footer .heading-elements-toggle').on('click', function() {
        $(this).parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
    });



    // Breadcrumb elements toggler
    // -------------------------

    // Add control button toggler to breadcrumbs if has elements
    $('.breadcrumb-line').has('.breadcrumb-elements').prepend('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>');


    // Toggle visible state of breadcrumb elements
    $('.breadcrumb-elements-toggle').on('click', function() {
        $(this).parent().children('.breadcrumb-elements').toggleClass('visible-elements');
    });




    // ========================================
    //
    // Navbar
    //
    // ========================================


    // Navbar navigation
    // -------------------------

    // Prevent dropdown from closing on click
    $(document).on('click', '.dropdown-content', function (e) {
        e.stopPropagation();
    });

    // Disabled links
    $('.navbar-nav .disabled a').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    // Show tabs inside dropdowns
    $('.dropdown-content a[data-toggle="tab"]').on('click', function (e) {
        $(this).tab('show');
    });




    // ========================================
    //
    // Element controls
    //
    // ========================================


    // Reload elements
    // -------------------------

    // Panels
    $('.panel [data-action=reload]').click(function (e) {
        e.preventDefault();
        var block = $(this).parent().parent().parent().parent().parent();
        $(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

        // For demo purposes
        window.setTimeout(function () {
            $(block).unblock();
        }, 2000);
    });


    // Sidebar categories
    $('.category-title [data-action=reload]').click(function (e) {
        e.preventDefault();
        var block = $(this).parent().parent().parent().parent();
        $(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.5,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #000'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none',
                color: '#fff'
            }
        });

        // For demo purposes
        window.setTimeout(function () {
            $(block).unblock();
        }, 2000);
    });


    // Light sidebar categories
    $('.sidebar-default .category-title [data-action=reload]').click(function (e) {
        e.preventDefault();
        var block = $(this).parent().parent().parent().parent();
        $(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

        // For demo purposes
        window.setTimeout(function () {
            $(block).unblock();
        }, 2000);
    });



    // Collapse elements
    // -------------------------

    //
    // Sidebar categories
    //

    // Hide if collapsed by default1
    $('.category-collapsed').children('.category-content').hide();


    // Rotate icon if collapsed by default1
    $('.category-collapsed').find('[data-action=collapse]').addClass('rotate-180');


    // Collapse on click
    $('.category-title [data-action=collapse]').click(function (e) {
        e.preventDefault();
        var $categoryCollapse = $(this).parent().parent().parent().nextAll();
        $(this).parents('.category-title').toggleClass('category-collapsed');
        $(this).toggleClass('rotate-180');

        containerHeight(); // adjust page height

        $categoryCollapse.slideToggle(150);
    });


    //
    // Panels
    //

    // Hide if collapsed by default1
    $('.panel-collapsed').children('.panel-heading').nextAll().hide();


    // Rotate icon if collapsed by default1
    $('.panel-collapsed').find('[data-action=collapse]').addClass('rotate-180');


    // Collapse on click
    $('.panel [data-action=collapse]').click(function (e) {
        e.preventDefault();
        var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();
        $(this).parents('.panel').toggleClass('panel-collapsed');
        $(this).toggleClass('rotate-180');

        containerHeight(); // recalculate page height

        $panelCollapse.slideToggle(150);
    });



    // Remove elements
    // -------------------------

    // Panels
    $('.panel [data-action=close]').click(function (e) {
        e.preventDefault();
        var $panelClose = $(this).parent().parent().parent().parent().parent();

        containerHeight(); // recalculate page height

        $panelClose.slideUp(150, function() {
            $(this).remove();
        });
    });


    // Sidebar categories
    $('.category-title [data-action=close]').click(function (e) {
        e.preventDefault();
        var $categoryClose = $(this).parent().parent().parent().parent();

        containerHeight(); // recalculate page height

        $categoryClose.slideUp(150, function() {
            $(this).remove();
        });
    });




    // ========================================
    //
    // Main navigation
    //
    // ========================================


    // Main navigation
    // -------------------------

    // Add 'active' class to parent list item in all levels
    $('.navigation').find('li.active').parents('li').addClass('active');

    // Hide all nested lists
    $('.navigation').find('li').not('.active, .category-title').has('ul').children('ul').addClass('hidden-ul');

    // Highlight children links
    $('.navigation').find('li').has('ul').children('a').addClass('has-ul');

    // Add active state to all dropdown parent levels
    $('.dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu').has('li.active').addClass('active').parents('.navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)').addClass('active');



    // Main navigation tooltips positioning
    // -------------------------

    // Left sidebar
    $('.navigation-main > .navigation-header > i').tooltip({
        placement: 'right',
        container: 'body'
    });



    // Collapsible functionality
    // -------------------------

    // Main navigation
    $('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
        e.preventDefault();

        // Collapsible
        $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active').children('ul').slideToggle(250);

        // Accordion
        if ($('.navigation-main').hasClass('navigation-accordion')) {
            $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(250);
        }
    });


    // Alternate navigation
    $('.navigation-alt').find('li').has('ul').children('a').on('click', function (e) {
        e.preventDefault();

        // Collapsible
        $(this).parent('li').not('.disabled').toggleClass('active').children('ul').slideToggle(200);

        // Accordion
        if ($('.navigation-alt').hasClass('navigation-accordion')) {
            $(this).parent('li').not('.disabled').siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(200);
        }
    });




    // ========================================
    //
    // Sidebars
    //
    // ========================================


    // Mini sidebar
    // -------------------------

    // Toggle mini sidebar
    $('.sidebar-main-toggle').on('click', function (e) {
        e.preventDefault();

        // Toggle min sidebar class
        $('body').toggleClass('sidebar-xs');
    });



    // Sidebar controls
    // -------------------------

    // Disable click in disabled navigation items
    $(document).on('click', '.navigation .disabled a', function (e) {
        e.preventDefault();
    });


    // Adjust page height on sidebar control button click
    $(document).on('click', '.sidebar-control', function (e) {
        containerHeight();
    });


    // Hide main sidebar in Dual Sidebar
    $(document).on('click', '.sidebar-main-hide', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-main-hidden');
    });


    // Toggle second sidebar in Dual Sidebar
    $(document).on('click', '.sidebar-secondary-hide', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-secondary-hidden');
    });


    // Hide detached sidebar
    $(document).on('click', '.sidebar-detached-hide', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-detached-hidden');
    });


    // Hide all sidebars
    $(document).on('click', '.sidebar-all-hide', function (e) {
        e.preventDefault();

        $('body').toggleClass('sidebar-all-hidden');
    });



    //
    // Opposite sidebar
    //

    // Collapse main sidebar if opposite sidebar is visible
    $(document).on('click', '.sidebar-opposite-toggle', function (e) {
        e.preventDefault();

        // Opposite sidebar visibility
        $('body').toggleClass('sidebar-opposite-visible');

        // If visible
        if ($('body').hasClass('sidebar-opposite-visible')) {

            // Make main sidebar mini
            $('body').addClass('sidebar-xs');

            // Hide children lists
            $('.navigation-main').children('li').children('ul').css('display', '');
        }
        else {

            // Make main sidebar default1
            $('body').removeClass('sidebar-xs');
        }
    });


    // Hide main sidebar if opposite sidebar is shown
    $(document).on('click', '.sidebar-opposite-main-hide', function (e) {
        e.preventDefault();

        // Opposite sidebar visibility
        $('body').toggleClass('sidebar-opposite-visible');

        // If visible
        if ($('body').hasClass('sidebar-opposite-visible')) {

            // Hide main sidebar
            $('body').addClass('sidebar-main-hidden');
        }
        else {

            // Show main sidebar
            $('body').removeClass('sidebar-main-hidden');
        }
    });


    // Hide secondary sidebar if opposite sidebar is shown
    $(document).on('click', '.sidebar-opposite-secondary-hide', function (e) {
        e.preventDefault();

        // Opposite sidebar visibility
        $('body').toggleClass('sidebar-opposite-visible');

        // If visible
        if ($('body').hasClass('sidebar-opposite-visible')) {

            // Hide secondary
            $('body').addClass('sidebar-secondary-hidden');

        }
        else {

            // Show secondary
            $('body').removeClass('sidebar-secondary-hidden');
        }
    });


    // Hide all sidebars if opposite sidebar is shown
    $(document).on('click', '.sidebar-opposite-hide', function (e) {
        e.preventDefault();

        // Toggle sidebars visibility
        $('body').toggleClass('sidebar-all-hidden');

        // If hidden
        if ($('body').hasClass('sidebar-all-hidden')) {

            // Show opposite
            $('body').addClass('sidebar-opposite-visible');

            // Hide children lists
            $('.navigation-main').children('li').children('ul').css('display', '');
        }
        else {

            // Hide opposite
            $('body').removeClass('sidebar-opposite-visible');
        }
    });


    // Keep the width of the main sidebar if opposite sidebar is visible
    $(document).on('click', '.sidebar-opposite-fix', function (e) {
        e.preventDefault();

        // Toggle opposite sidebar visibility
        $('body').toggleClass('sidebar-opposite-visible');
    });



    // Mobile sidebar controls
    // -------------------------

    // Toggle main sidebar
    $('.sidebar-mobile-main-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-mobile-main').removeClass('sidebar-mobile-secondary sidebar-mobile-opposite sidebar-mobile-detached');
    });


    // Toggle secondary sidebar
    $('.sidebar-mobile-secondary-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-mobile-secondary').removeClass('sidebar-mobile-main sidebar-mobile-opposite sidebar-mobile-detached');
    });


    // Toggle opposite sidebar
    $('.sidebar-mobile-opposite-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-mobile-opposite').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached');
    });


    // Toggle detached sidebar
    $('.sidebar-mobile-detached-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-mobile-detached').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-opposite');
    });


    //
    // // Mobile sidebar setup
    // // -------------------------
    //
    // $(window).on('resize', function() {
    //     setTimeout(function() {
    //         containerHeight();
    //
    //         if($(window).width() <= 768) {
    //
    //             // Add mini sidebar indicator
    //             $('body').addClass('sidebar-xs-indicator');
    //
    //             // Place right sidebar before content
    //             $('.sidebar-opposite').insertBefore('.content-wrapper');
    //
    //             // Place detached sidebar before content
    //             $('.sidebar-detached').insertBefore('.content-wrapper');
    //
    //             // Add mouse events for dropdown submenus
    //             $('.dropdown-submenu').on('mouseenter', function() {
    //                 $(this).children('.dropdown-menu').addClass('show');
    //             }).on('mouseleave', function() {
    //                 $(this).children('.dropdown-menu').removeClass('show');
    //             });
    //         }
    //         else {
    //
    //             // Remove mini sidebar indicator
    //             $('body').removeClass('sidebar-xs-indicator');
    //
    //             // Revert back right sidebar
    //             $('.sidebar-opposite').insertAfter('.content-wrapper');
    //
    //             // Remove all mobile sidebar classes
    //             $('body').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached sidebar-mobile-opposite');
    //
    //             // Revert left detached position
    //             if($('body').hasClass('has-detached-left')) {
    //                 $('.sidebar-detached').insertBefore('.container-detached');
    //             }
    //
    //             // Revert right detached position
    //             else if($('body').hasClass('has-detached-right')) {
    //                 $('.sidebar-detached').insertAfter('.container-detached');
    //             }
    //
    //             // Remove visibility of heading elements on desktop
    //             $('.page-header-content, .panel-heading, .panel-footer').removeClass('has-visible-elements');
    //             $('.heading-elements').removeClass('visible-elements');
    //
    //             // Disable appearance of dropdown submenus
    //             $('.dropdown-submenu').children('.dropdown-menu').removeClass('show');
    //         }
    //     }, 100);
    // }).resize();




    // ========================================
    //
    // Other code
    //
    // ========================================


    // Plugins
    // -------------------------

    // Popover
    $('[data-popup="popover"]').popover();


    // Tooltip
    $('[data-popup="tooltip"]').tooltip();



    /**************************** custom knowmx *****************************/

    $.fn.block_panel = function(panel_name){
        $(panel_name).closest('.panel').block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

        // For demo purposes


    }

    $.fn.unblock_panel = function(panel_name){
        setTimeout(function(){
            $(panel_name).closest('.panel').unblock();
        },1000) ;

    }

    /**************************** custom mycompose *****************************/

/*************************** workflow **************************/


$.fn.drawWorkFlow = function (data) {

    var current = data.current;
    $('#diagram').html('');
    var flowchart_parse = '';//'st=>start: Start:>#modal \n' ;
    var flowchart_parse2 = ''; //'st->' ;
    var color = 'past';
    var i = 0 ;
    $.each(data.item.stage, function (key, value) {
        if(value.id == current || current==null){
            color = 'current';
            i = 1 ;
        }
        if(value.action_type=='END'){
            color = 'past';
        }
        var type = value.action_type == 'START' ? 'start' : (value.action_type == 'END' ? 'end' : 'operation');
        flowchart_parse += 'sub' + value.id + '=>' + type + ': ' + value.name + ' |' + color + ' :>#stage_' + value.id + ' \n';
        flowchart_parse2 += 'sub' + value.id + '->';

        if(i==1){
            color = 'future';
        }
    });

    var diagram = flowchart.parse(flowchart_parse + '\n' + flowchart_parse2);
    diagram.drawSVG('diagram');

    // you can also try to pass options:

    diagram.drawSVG('diagram', {
        'x': 0,
        'y': 0,
        'line-width': 3,
        'line-length': 50,
        'text-margin': 10,
        'font-size': 14,
        'font-color': 'black',
        'line-color': 'black',
        'element-color': 'black',
        'fill': 'white',
        'yes-text': 'yes',
        'no-text': 'no',
        'arrow-end': 'block',
        'scale': 1,
        // style symbol types
        'symbols': {
            'start': {
                'font-color': 'red',
                'element-color': 'green',
                'fill': 'yellow'
            },
            'end': {
                'class': 'end-element'
            }
        },
        // even flowstate support ;-)
        'flowstate': {
            'past': {'fill': 'green', 'font-size': 12, 'font-color': 'black', 'font-weight': 'bold'},
            'current': {'fill': 'yellow', 'font-color': 'red', 'font-weight': 'bold'},
            'future': {'fill': '#CCCCCC'},
            'request': {'fill': 'blue'},
            'invalid': {'fill': '#444444'},
            'approved': {'fill': '#58C4A3', 'font-size': 12, 'yes-text': 'APPROVED', 'no-text': 'n/a'},
            'rejected': {'fill': '#C45879', 'font-size': 12, 'yes-text': 'n/a', 'no-text': 'REJECTED'}
        }
    });

}

    $.fn.drawActualWorkFlow = function (data) {
        var current = data.current;
        $('#diagram_actual').html('');
        var flowchart_parse = '';//'st=>start: Start:>#modal \n' ;
        var flowchart_parse2 = ''; //'st->' ;
        $.each(data.item.stage, function (key, value) {
            var type = value.action_type == 'START' ? 'start' : (value.action_type == 'END' ? 'end' : 'operation');
            flowchart_parse += 'sub' + value.id + '=>' + type + ': ' + value.name + ' |future :>#stage_' + value.id + ' \n';
            flowchart_parse2 += 'sub' + value.id + '->';
            //$('#show_workflow').append("<div>" + value.name + '</div>')
        if(value.id == current || current==null){
            return false;
        }
        });

        var diagram = flowchart.parse(flowchart_parse + '\n' + flowchart_parse2);
        diagram.drawSVG('diagram_actual');

        // you can also try to pass options:

        diagram.drawSVG('diagram_actual', {
            'x': 0,
            'y': 0,
            'line-width': 3,
            'line-length': 50,
            'text-margin': 10,
            'font-size': 14,
            'font-color': 'black',
            'line-color': 'black',
            'element-color': 'black',
            'fill': 'white',
            'yes-text': 'yes',
            'no-text': 'no',
            'arrow-end': 'block',
            'scale': 1,
            // style symbol types
            'symbols': {
                'start': {
                    'font-color': 'red',
                    'element-color': 'green',
                    'fill': 'green'
                },
                'end': {
                    'class': 'end-element'
                }
            },
            // even flowstate support ;-)
            'flowstate': {
                'past': {'fill': '#CCCCCC', 'font-size': 12},
                'current': {'fill': 'yellow', 'font-color': 'red', 'font-weight': 'bold'},
                'future': {'fill': '#FFFF99'},
                'request': {'fill': 'blue'},
                'invalid': {'fill': '#444444'},
                'approved': {'fill': '#58C4A3', 'font-size': 12, 'yes-text': 'APPROVED', 'no-text': 'n/a'},
                'rejected': {'fill': '#C45879', 'font-size': 12, 'yes-text': 'n/a', 'no-text': 'REJECTED'}
            }
        });

    }

    $('#diagram').on('click', 'a', function (e) {

        var link = $(this).attr("href");
        var slink = link.split('_');
        var id = slink[1];
        var workflow_modal = $('#workflow_stage_modal');
        workflow_modal.find('.modal-body').html('');
        $.post(route_get_stage_details, {'id': id}, function (data) {
            console.log(data);
            workflow_modal.find('.modal-body').append("<h1>" + data.stage.name + "</h1>");
            var html = '<div class="content col-md-12">';
            var permits_container = '<div class="state_action_permits_account">';

            $.each(data.stage.stage_state, function (k, v) {
                var permits_text = '';
                $.each(v.state.state_action, function (k2, v2) {
                    $.each(v2.state_action_permits_account, function (k3, v3) {
                        permits_container += '<span class="label label-flat border-default text-primary-600">' + v3.account.account_name + '</span>';
                        permits_text += v3.account.account_name + ',';
                    });

                    $.each(v2.state_action_permits_role, function (k3, v3) {
                        permits_container += '<span class="label label-flat border-primary text-primary-600">' + v3.role.name + '</span>';
                        permits_text += v3.role.name + ',';
                    });

                    html += '<button class="btn btn-primary" data-placement="top" data-trigger="hover" data-popup="popover-solid" title="" data-content="' + permits_text + '" data-original-title="Permissions" aria-describedby="popover136822">' + v2.action.name + '</button> ';

                });
                html += '<hr/>';
            });
            html += '</div>';
            permits_container += '</div>';
            workflow_modal.find('.modal-body').append(html);
            workflow_modal.find('.modal-body').append(permits_container);
            $('[data-popup=popover-solid]').popover({
                // template: '<div class="popover bg-primary"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
            });
            workflow_modal.modal('show');
        }, 'json');

        e.preventDefault();
    }) ;
    /*************************** workflow **************************/




});
