<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountSign
 */
class AccountSign extends Model
{
    protected $table = 'account_sign';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'sign_id',
        'is_default'
    ];

    protected $guarded = [];

        
}