<?php

namespace App\Services;

use ClientAPI;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Config;


class Register extends \App\Services\Services
{

    public function register($aParams)
    {
        $oServices = Parent::getInstance();


        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/register")
                ->setBody($aParams)
                ->request()
                ->getContent();

        } catch (BadResponseException $exception) {
dd($exception->getMessage());
            return ['status' => 'failed'];
        }

        $response_array = json_decode($oClient, true);
        $sToken = $response_array['data']['token'];

        $oUser = $response_array['data']['aUser'];

        $sLoginType = 'user';
        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nQuotaSize' => $oUser['nQuotaSize'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
        ];

        Auth::setSessions($aParams);

        return $response_array['data'];
    }

    public static function verifyEmail($aParams)
    {
        $sVerifyToken = $aParams['code'];
        try {
            $oClient = ClientAPI::
                setMethod('POST')
                ->setUrl("user/verifyEmailAndToken")
                ->setBody([ 'verify_token' => $sVerifyToken])
                ->request()
                ->getContent();

        } catch (BadResponseException $exception) {
            return ['status' => 'failed'];
        }


        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function verfiyResetCode($aParams) {
        $oServices = Parent::getInstance();


        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/verfiyResetCode")
                ->setBody($aParams)
                ->request()
                ->getContent();

        } catch (BadResponseException $exception) {
            return ['status' => 'failed'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function activateUser($aUser)
    {
        if (!is_array($aUser) || !isset($aUser['id']))
            return false;

        $nUserID = $aUser['id'];
        try {
            $oClient = ClientAPI::
            setMethod('POST')
                ->setUrl("user/activateUser")
                ->setBody([ 'id' => $nUserID])
                ->request()
                ->getContent();

        } catch (BadResponseException $exception) {
            return ['status' => 'failed'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function resendVerify()
    {

        $nUserID = nUserID();
        try {
            $oClient = ClientAPI::
            setMethod('POST')
                ->setUrl("user/resendVerify")
                ->setBody([ 'id' => $nUserID])
                ->withToken()
                ->request()
                ->getContent();

        } catch (BadResponseException $exception) {
            dd($exception->getMessage());
            return ['status' => 'failed'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }


}