
<!-- Footer -->
<div class="footer text-muted">
    &copy; 2017.
    Made with Love <img draggable="false" class="emoji" style="width: 15px;" alt="❤" src="https://s.w.org/images/core/emoji/2/svg/2764.svg"> <a href="http://mycompose.com">MyCompose</a>

</div>
@stack('scripts')
<!-- /footer -->
