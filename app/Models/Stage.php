<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Stage
 */
class Stage extends Model
{
    protected $table = 'wf_stage';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'created_by',
        'updated_by'
    ];

    public function States(){
        return $this->hasMany('\App\Models\Workflow\State', 'stage_id', 'id');
    }

    public function wfWorkflow()
    {
        return $this->belongsTo('App\Models\Workflow\Workflow');
    }


    protected $guarded = [];

        
}