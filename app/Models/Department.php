<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\File;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DepartmentRole;
use App\Models\DepartmentPermission;



/**
 * Class Department
 */
class Department extends Model
{

    use SoftDeletes;
    
    /*variables*/
    protected $table = 'departments';
    public $timestamps = true;
    protected $guarded = [];
    protected $fillable = [
        'name',
        'description',
        'parent_department_id',
        'branch_id',
        'logo'
    ];

    /*relationships*/
    public function Logo(){
    
        return $this->hasOne('App\Models\File','id','logo');
    }
    public function Branch(){

        return $this->belongsTo('App\Models\Branch');
    }
   
    public function Parent(){

        return $this->belongsTo('App\Models\Department','parent_department_id','id');
    }

    /*new department*/
    public function storeDepartment($departmentData){

        $department = new Department;

        $department->name = $departmentData->name ;
        $department->description = $departmentData->description ;
        $department->branch_id = $departmentData->branch_id ;
        $department->parent_department_id = $departmentData->parent_department ;
        if(!empty($departmentData->logo)){
            $nLogo = File::put($departmentData->logo);
            $department->logo  = $nLogo;
        }
        $department->save();
        return true ;

    }

    public function updateDepartment($departmentData,$id){

        $department =  Department::find($id);

        $department->name = $departmentData->name ;
        $department->description = $departmentData->description ;
        $department->branch_id = $departmentData->branch_id ;
        $department->parent_department_id = $departmentData->parent_department ;

        if(!empty($departmentData->logo)){
            $nLogo = File::put($departmentData->logo);
            $department->logo  = $nLogo;
        }
        
        $department->save();
        return true ;
            
    }

    /**
     * Update department roles 
     */
    public function updateRoles($roleData ,$id){

        //delete department old roles
        $departmentRoles =  DepartmentRole::where('department_id',$id)->delete();

        //store department new roles
        foreach ($roleData->all() as $key => $role) {

            if($key == '_token'){
                continue ;
            }

            $departmentRole = new DepartmentRole;
            $departmentRole->department_id = $id ;
            $departmentRole->role_id = $role ;

            $departmentRole->save();

        }

        return true ;
       
    }

    /**
     * Update account permissions 
     */
    public function updatePermissions($permissionsData , $id){
        

        //delete account old permissios
        $departmentPermissions =  DepartmentPermission::where('department_id',$id)->delete();

     

        //store account new permissios
        foreach ($permissionsData->all() as $key => $permission) {

            if($key == '_token'){
                continue ;
            }

            $departmentPermission = new DepartmentPermission;
            $departmentPermission->department_id = $id ;
            $departmentPermission->permission_id = $permission ;

            $departmentPermission->save();

        }

        return true ;        


    }


   
    
        
}