<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Document
 */
class Document extends Model
{
    protected $table = 'documents';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'content',
        'status',
        'category_id',
        'privacy',
        'expiry_date',
        'account_id',
        'parent_document_id',
        'layout_id',
        'outbox_number'
    ];

    protected $guarded = [];

        
}