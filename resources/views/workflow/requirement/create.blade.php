@extends('layouts.admin')

@push('scripts')

{{--Select--}}
<script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/form_select2.js') }}"></script>
@endpush
@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __('workflow.requirement.add_requirement') }}</h6>
        </div>
        <div class="panel-body">

                {!! Form::open(['url'=> url('requirement'), 'method'=>'POST', 'id' => 'requirement', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <fieldset class="content-group">
                        <legend class=""></legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.requirement.requirement') .' '. __('common.name') }}: <span class="text-danger">*</span></label><br>
                                        <input name="name" id="name" value="{{ old('name') }}" class="required">
                                        @if ($errors->has('name'))
                                            <br/><font color="red">{{ $errors->first('name') }}</font>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.requirement.requirement_type') }}: <span class="text-danger">*</span></label><br>
                                        @php
                                            $selected = "";
                                        @endphp
                                        <select name="type" class="select-fixed-single">
                                            <option value="">__select__</option>
                                            @foreach($requirementTypes as $type => $requirementType)
                                                @php
                                                    $selected = ($type == old('type')) ? 'selected' : '';
                                                @endphp
                                                <option value="{{$type}}" {{$selected}} >{{$requirementType}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <br/><font color="red">{{ $errors->first('type') }}</font>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.requirement.requirement') .' '. __('common.description') }}:</label>
                                        <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary stepy-finish pull-right">Submit <i class="icon-check position-right"></i></button>
                    </fieldset>
            </div>
    </div>

        {!! Form::close() !!}


@endsection
