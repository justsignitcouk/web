<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'              =>'required|regex:/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/|confirmed',
            'password_confirmation' =>'required|regex:/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/',
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'Password should contains Numbers, Upper-case, Lower-case, Special characters (!@#$%^&*)',
            'password_confirmation.regex' => 'Password should contains Numbers, Upper-case, Lower-case, Special characters (!@#$%^&*)',
        ];
    }

}
