<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Prerequisite
 */
class Prerequisite extends Model
{
    protected $table = 'prerequisite';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'type'
    ];

    protected $guarded = [];

        
}