<form class="form-horizontal form-validate-jquery" role="form" method="POST" action="{{ route('resetPassword') }}">
    {{ csrf_field() }}
    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-green text-green"><i class="icon-lock"></i></div>
            <h5 class="content-group">Create New Password</h5>
        </div>

        <div class="form-group has-feedback has-feedback-left">
            <input type="password" class="form-control" name="password" id="password" placeholder="Create New Password">
            <div class="form-control-feedback">
                <i class="icon-user-lock text-muted"></i>
            </div>
            @if ($errors->has('password'))
                <font color="red">{{ $errors->first('password') }}</font>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left">
            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm New Password">
            <div class="form-control-feedback">
                <i class="icon-user-lock text-muted"></i>
            </div>
            @if ($errors->has('password_confirmation'))
                <font color="red">{{ $errors->first('password_confirmation') }}</font>
            @endif
        </div>
        {{--<input type="hidden" name="id" value="{{$oUser['id']}}">--}}
        <input type="hidden" name="verify_token" value="{{$verify_token}}">

        <button type="submit" class="btn bg-blue btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
    </div>
</form>
