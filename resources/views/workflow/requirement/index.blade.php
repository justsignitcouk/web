@extends('layouts.admin')

@section('content')

    <!-- Ajax sourced data -->
    <div class="panel panel-flat">

        @include('layouts/partials/message')

        <div class="panel-heading">
            <h5 class="panel-title">{{ __("workflow.requirement.manage_requirements") }}</h5>
            <div class="heading-elements hidden">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="{{ url('requirement/create') }}" class="btn btn-primary pull-right" id="bootbox_form">{{ __("workflow.requirement.add_requirement") }}</a>

            <table id="requirement-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                <tr>
                    <th>{{ __("common.name") }}</th>
                    <th>{{ __("common.description") }}</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /ajax sourced data -->

        @endsection
        @push('scripts')
        <script>

            $(function() {

                $.extend( $.fn.dataTable.defaults, {
                    autoWidth: true,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                        "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                        "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                        "sInfo":         "{{ __('components/datatables.sInfo') }}",
                        "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                        "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                        "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                        "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                        "sUrl":          "{{ __('components/datatables.sUrl') }}",
                        "oPaginate": {
                            "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                            "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                            "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                            "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                        }
                    }
                });

                var table = $('#requirement-table').DataTable({

                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('requirement.data') !!}',
                    columns: [
                        { data: 'name', name: 'name' },
                        { data: 'description', name: 'description' }
                    ]
                });

                $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/requirement/' + id;
                });
            });
        </script>

    @endpush