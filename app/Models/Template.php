<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 */
class Template extends Model
{
    protected $table = 'templates';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'content',
        'design_id'
    ];

    protected $guarded = [];

        
}