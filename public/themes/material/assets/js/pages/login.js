/* ------------------------------------------------------------------------------
*
*  # Login page
*
*  Specific JS code additions for login and registration pages
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */


$(document).ready(function(){
    $('input').on('focus',function(){
        $(this).attr('autocomplete', 'off');
        $(this).prop('autocomplete',false);
    });

});