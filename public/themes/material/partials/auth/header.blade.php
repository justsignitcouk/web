<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header" style="background:#fff;">


        <a href="/" class="navbar-brand" >
            <img src="/assets/images/mycompose_logo.png" alt="mycompose" style="margin: -8px 0 0 0;height: 36px">
        </a>

    </div>

    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown language-switch">
            <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false"><img src="/themes/material/assets/images/flags/gb.png" alt="" class="position-left"> English <i class="caret"></i></a>
            <ul class="dropdown-menu">
                <li><a class="english">English</a></li>
                <li><a class="arabic">Arabic</a></li>
            </ul>
        </li>
    </ul>

</div>