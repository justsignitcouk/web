<?php

use Illuminate\Database\Seeder;

class TranslatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translator_languages')->insert([
            'id' => "1",
            'locale' => "en",
            'name' => "English",
            'direction' => "ltr"
        ]);

        DB::table('translator_languages')->insert([
            'id' => "2",
            'locale' => "ar",
            'name' => "Arabic",
            'direction' => "rtl"
        ]);

    }
}
