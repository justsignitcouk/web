<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountActing
 */
class AccountActing extends Model
{
    protected $table = 'account_acting';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'acting_id',
        'from_date',
        'to_date',
        'active'
    ];

    protected $guarded = [];

        
}