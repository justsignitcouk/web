@extends('layouts.admin')

@section('content')

    <!-- Ajax sourced data -->
    <div class="panel panel-flat">

        <div class="panel-heading">
            <h5 class="panel-title">{{ __("permissions.permissions") }}</h5>
            <div class="heading-elements hidden">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <table id="users-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __("permissions.permission") }}</th>
                        <th>{{ __("permissions.status") }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /ajax sourced data -->

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });
        
        
        var table = $('#users-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! route('permissions.data') !!}',
            columns: [
                { data: 'name'},
                { data: 'status'},
            ]
        });
      
    });
</script>

@endpush