<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Stamp
 */
class Stamp extends Model
{
    protected $table = 'stamps';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'file_id'
    ];

    protected $guarded = [];

        
}