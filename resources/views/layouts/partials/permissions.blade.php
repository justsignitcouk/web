<div class="row">

<div class="row">
        <!-- users module permissions -->
        <div class="col-md-4  hide_per users_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">
                      <label class="form-check-label">
                          <input id="all" type="checkbox" value="">
                              {{ __("users.users_permissions") }} :
                      </label>
                  </div>
                  <div class="panel-body ">
                    @include('layouts/partials/permissions/users')
                  </div>
            </div>
        </div>

        <!-- roles module permissions -->
        <div class="col-md-4 hide_per roles_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-roles" type="checkbox" value="">
                            {{ __("users.roles_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                        @include('layouts/partials/permissions/roles') 
                  </div>
            </div>
        </div>

        <!-- permissions module permissions -->
        <div class="col-md-4 hide_per permissions_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-permissions" type="checkbox" value="">
                            {{ __("users.permissions_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                  @include('layouts/partials/permissions/permissions')
                  </div>
            </div>
        </div>
</div>

<div class="row">

        <!-- accounts module permissions -->
        <div class="col-md-4 hide_per accounts_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-accounts" type="checkbox" value="">
                            {{ __("users.accounts_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                       @include('layouts/partials/permissions/accounts') 
                  </div>
            </div>
        </div>

        <!-- companies module permissions -->
        <div class="col-md-4 hide_per companies_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                        <input id="all-companies" type="checkbox" value="">
                            {{ __("users.companies_permissions") }} :
                        </label>

                  </div>
                  <div class="panel-body ">
                     @include('layouts/partials/permissions/companies')
                  </div>
            </div>
        </div>
        <!-- branches module permissions -->
        <div class="col-md-4 hide_per branches_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-branches" type="checkbox" value="">
                            {{ __("users.branches_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                        @include('layouts/partials/permissions/branches')
                  </div>
            </div>
        </div>
</div>
  <div class="row">
      <!-- departments module permissions -->
        <div class="col-md-4 hide_per departments_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-departments" type="checkbox" value="">
                            {{ __("users.departments_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                        @include('layouts/partials/permissions/departments') 
                  </div>
            </div>
        </div>

        <!-- workflow module permissions -->
        <div class="col-md-4 hide_per workflow_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-workflow" type="checkbox" value="">
                            {{ __("users.workflow_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                        @include('layouts/partials/permissions/workflow') 
                  </div>
            </div>
        </div>

        <!-- requirements module permissions -->
        <div class="col-md-4 hide_per requirements_permissions">
            <div class="panel panel-primary">
                  <div class="panel-heading ">

                        <label class="form-check-label">
                            <input id="all-requirements" type="checkbox" value="">
                            {{ __("users.requirements_permissions") }} :
                        </label>
                  </div>
                  <div class="panel-body ">
                        @include('layouts/partials/permissions/requirements') 
                  </div>
            </div>
        </div>
       
    </div>                            
</div>


@push('scripts')
 <script src="{{ asset('js/permissions/permissions.js') }}"></script>
 <script src="{{ asset('js/permissions/roles-permissions.js') }}"></script>
 <script src="{{ asset('js/permissions/permissions-permissions.js') }}"></script>
 <script src="{{ asset('js/permissions/accounts-permissions.js') }}"></script>
<script src="{{ asset('js/permissions/companies-permissions.js') }}"></script>
<script src="{{ asset('js/permissions/branches-permissions.js') }}"></script>
<script src="{{ asset('js/permissions/departments-permissions.js') }}"></script>
<script src="{{ asset('js/permissions/top-checks.js') }}"></script>
<script src="{{ asset('js/permissions/workflow-permissions.js') }}"></script>
<script src="{{ asset('js/permissions/requirements-permissions.js') }}"></script>


@endpush