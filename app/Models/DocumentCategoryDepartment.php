<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentCategoryDepartment
 */
class DocumentCategoryDepartment extends Model
{
    protected $table = 'document_category_department';

    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'department_id',
        'active'
    ];

    protected $guarded = [];

        
}