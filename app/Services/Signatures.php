<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Signatures extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function get($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("user/" . $nUserId ."/signatures")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

    public static function getDefault($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("user/getDefaultSignature")
                ->setBody(['user_id'=>$nUserId])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

    public static function save($aParams){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        $aParams['user_id'] =  $nUserId;
        try{
            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("user/signatures")
                ->withToken()
                ->setBody($aParams)
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;

    }

    public static function delete($nSignID){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        try{
            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("user/delete_signature")
                ->setBody(['id'=>$nSignID,'user_id'=>$nUserId])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;

            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

}
