
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });


var $uploader;
var uploader;



    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('uploader-my_uploader_id'), // ... or DOM Element itself
        url : route_upload_profile,
        flash_swf_url : '../js/Moxie.swf',
        silverlight_xap_url : '../js/Moxie.xap',
        unique_names:true,
        multi_selection :true,
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png,jpeg"},
                {title : "Document files", extensions : "doc,pdf,docx"}
                ]
        },
        multipart_params : {

        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: {
            PostInit: function() {
                document.getElementById('filelist').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },

            FilesAdded: function(up, files) {
                up.start();
                plupload.each(files, function(file) {
                    //document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                });
            },

            UploadProgress: function(up, file) {
                //document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },
            FileUploaded : function(up,file,result){
                var myData;
                try {
                    myData = eval(result.response);
                } catch(err) {
                    myData = eval('(' + result.response + ')');
                }
                 console.log(file);
                 console.log(myData.result.id);
                $('#attachment_contaner').append("<div id='file_attachment_"+ myData.result.id +"' class=\"\" style=\"display:inline-block;padding:0 10px;\"> " +
                    "<a href=\"#\" class=\"remove_attachment\" data-file_id=\"" + myData.result.id + "\" data-id=\""+ myData.result.id  +"\">X</a> <a href='"+ globals.CDN_HOST +"/files/document_attachment/" + myData.result.name + "' target='blank'>" + file.name  + " </a></div>")

                $('#files_hidden_input').append("'<input type=\"hidden\" name=\"files[]\" class=\"input_file_hidden\" value=\"" + myData.result.id + "\" id=\"input_file_hidden_" + myData.result.id + "\" />");


            },
            UploadComplete: function (up,files) {

            },
            Error: function(up, err) {
                //document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
                alert(err.message);
            }
        }
    });

    uploader.init();





$(document).on('click','.remove_attachment',function(e){
    var file_id = $(this).data('id');
    var file_selector_id = $(this).data('file_id');
    $('#file_attachment_' + file_selector_id).remove();
    $('#input_file_hidden_' + file_selector_id).remove();
    e.preventDefault();
})

});