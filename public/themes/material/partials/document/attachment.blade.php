{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}
{!! Theme::asset()->themePath()->add([ ['document_attachment', 'partials/document/document_attachment.js',['plupload_queue']]]) !!}
<style>
    .filelist {
        margin: 0 0 20px 0px;
    }

    .sidebar .thumbnail {
        margin: 10px 0;
    }

    .thumbnail > img, .thumbnail a > img {
        max-width: 100%;
        height: 120px;
    }
</style>


<!-- Block buttons -->

        <div class="col-md-12">
            <div id="console"></div>
            <div id="filelist" style="display:none">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>

            <div id="uploader-my_uploader_id">
                <a id="uploadfiles" style="display:none" href="javascript:;">[Upload files]</a>
            </div>
            <div class="files_hidden_input" id="files_hidden_input" style="display: none;">
                @foreach($aDocument['files'] as $file)
                    <input type="hidden" name="files[]" class="input_file_hidden" value="{{$file['file']['id']}}" id="input_file_hidden_{{$file['file']['id']}}">
                @endforeach
            </div>
        </div>
        <div class="col-md-12" id="attachment_contaner" style="margin-bottom: 5px;">

            @foreach($aDocument['files'] as $file)

                <div id="file_attachment_{{$file['file']['id']}}" class="" style="display:inline-block;padding:0 10px;">
                    <a href="#" class="remove_attachment"  data-file_id="{{$file['file']['id']}}" data-id="{{$file['file']['id']}}">X</a>
                    <a href="{{cdn_url($file['file']['hash_name'],'document_attachment')}}" target="blank">{{ $file['file']['name'] }}</a>
                </div>

            @endforeach
        </div>



<script>
    var route_upload_profile = "{{ route('upload_file') }}";
</script>
<!-- Block buttons -->