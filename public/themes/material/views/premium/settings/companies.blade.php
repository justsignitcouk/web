{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['datatables_responsive', 'assets/components/datatables-responsive/js/dataTables.responsive.js',['datatables']]]) !!}
{!! Theme::asset()->themePath()->add([ ['settings_companies', 'pages/premium/settings/js/companies.js',['datatables_responsive']]]) !!}


<div class="content-wrapper">
    <div class="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('alert-' . $msg) }}
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-12">
            <div class="panel panel-flat inbox_content" data-i18n="filter" data-i18n-target="dataTables_info">
                <div class="tabbable tab-content-bordered">
                    <div class="panel-body">
                        <div class="tab-content ">
                            <div class="tab-pane active" id="justified-right-icon-inbox">
                                <div class="content-table">
                                    @if($oCompaniesQuota->total > $oCompaniesQuota->usage)
                                    <a href="{{ route('settings.create-company') }}" class="btn btn-primary">add new
                                        companies ( {{ $oCompaniesQuota->usage }} / {{ $oCompaniesQuota->total }} )
                                    </a>
                                    @endif

                                    <table id="companies"
                                           class="table datatable-ajax table-inbox responsive inbox table-lay"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>name</th>
                                            <th>Short Name</th>
                                            <th>license number</th>
                                            <th>Created Date</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>name</th>
                                            <th>Short Name</th>
                                            <th>Licence number</th>
                                            <th>Created Date</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>