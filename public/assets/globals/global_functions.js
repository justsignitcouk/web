$(document).ready(function(){

    var options = {
            // anchors: 'a.smState',
            // forms: 'form.smState',
            blacklist: '.no-smoothState,.sf-dump-ref',
            prefetch: true,
            cacheLength: 2,
            onStart: {
                duration: 250, // Duration of our animation
                render: function ($container) {
                    // Add your CSS animation reversing class
                    $container.addClass('is-exiting');

                    // Restart your animation
                    smoothState.restartCSSAnimations();
                }
            },
            onReady: {
                duration: 0,
                render: function ($container, $newContent) {
                    // Remove your CSS animation reversing class
                    $container.removeClass('is-exiting');

                    // Inject the new content
                    $container.html($newContent);

                    $.fn.load_inbox_page();
                }
            }
        },
        smoothState = $('#body').smoothState(options).data('smoothState');


    $.ajaxSetup({async: false});

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    $(document).on('click','.details_button',function(e){
        var document_id  = $(this).data('id') ;
        $('#details_table').find('.modal-title').html('Details') ;
        $.get('/document/details/' + document_id,{'document_id':document_id},function(data){
            $('.details_modal_body').html(data) ;


            var wf_object = $(".workflow_steps_details").steps({
                headerTag: "h6",
                bodyTag: "fieldset",
                saveState: false,
                enableAllSteps:false,
                enableKeyNavigation:false,
                startIndex: current_stage,
                enableFinishButton:false,
                titleTemplate: '<span class="number">#index#</span> #title#',
                autoFocus: true,
                onFinished: function (event, currentIndex) {
                    alert("Form submitted.");
                },
                onInit: function(event, currentIndex){
                    $(".workflow_steps_details").show();
                    if(currentIndex!=0){
                        //$('#compose-content').hide();
                        //$('.compose-components').hide();
                    }
                    $(".workflow_steps_details").find('.current').show();
                    $.fn.disabled_done_steps();
                },
                onStepChanging:function (event, currentIndex, newIndex) {
                    $.fn.document_block('body');


                    return true; },
                onStepChanged:function (event, currentIndex, priorIndex) {

                    if(currentIndex!=0){
                        //$('#compose-content').hide();
                        //$('.compose-components').hide();

                    }else{
                        $('#compose-content').show();
                        $('.compose-components').show();
                    }

                    $('.pages_frame').slimScroll({
                        height: '380px',
                        width: '1020px',
                    });

                    $.fn.document_unblock('body');

                    $.fn.disabled_done_steps();
                }
            });


        });
        e.preventDefault();
    });

    // $(document).on('click','.details_button',function(e){
    //     var document_id  = $(this).data('id') ;
    //     var html = '' ;
    //     $.post('/document/details',{'document_id':document_id},function(data){
    //
    //         var document = data.aDetails.document;
    //         var rec = data.aDetails.recipients ;
    //         var document_forwarded = data.aDetails.oDocumentForwarded ;
    //         html+= '<div class="col-md-12">';
    //         html+= '<div class="col-md-6">';
    //         html+='<label>Subject:</label> : ' + document.title ;
    //         html+= '</div>';
    //         html+= '<div class="col-md-6">';
    //         html+='<label>Subject:</label> : ' + document.title ;
    //         html+= '</div>';
    //         html+= '</div>';
    //         html += '<table class="table">' ;
    //         html+="<tr><th>Name</th><th>Read At</th></tr>" ;
    //         $.each(rec,function(k,v){
    //             html+="<tr><td>"+ v.identifier +" <small>("+ v.type +")</small> </td><td>"+  (v.read_at!=null ? v.read_at : '')  +"</td></tr>" ;
    //
    //         }) ;
    //         html += "</table>" ;
    //         html += "<br/> <h2>Document Forwarded</h2>" +
    //             "<table class='table'>" +
    //             "<tr><th>Refernce number</th><th>Note</th></tr>";
    //         $.each(document_forwarded,function(k,v){
    //             html +=    "<tr><td><a href='/document/forward/" + v.id + "'>" +  v.id + "</a></td><td>" +  v.note + "</td></tr>" ;
    //         });
    //
    //         html += "</table>"
    //     },'json');
    //     $('#details_table').find('.modal-title').html('Details') ;
    //     $('.details_modal_body').html(html) ;
    //
    //     e.preventDefault();
    // });





    $("#anytime-both").AnyTime_picker({
        format: "%M %D %H:%i",
    });

$.fn.getElements = function() {

    var to_users = {};
    var cc_users = {};
    var signer_users = {};




    var documents_reference = {} ;

    var workflow_id;

    $('.to_document_editor').children('div').each(function () {
        var type = $(this).data('elementtype'); //'user' 'external';
        var recipient_type = $(this).data('type'); //'To';

        if(type=='user') {
            var user_id = $(this).data('id');
            var origin = $(this).data('origin');

            var value = $(this).html();
            to_users[user_id] = value;
        }
    });

    $('.document_reference').each(function() {
        var document_id = $(this).data('id');
        documents_reference[document_id] = document_id;
    });

    $('.signer_document_editor').children('div').each(function () {
        var type = $(this).data('elementtype');
        var recipient_type = 'Signer';


        if(type=='user') {
            var user_id = $(this).data('id');
            var origin = $(this).data('origin');
            var value = $(this).html();
            signer_users[user_id] = value;
        }


    });


    $('.cc_document_editor').find('li').each(function () {
        var type = $(this).data('elementtype');
        var recipient_type = 'CC';

        if(type=='user') {
            var user_id = $(this).data('id');
            var origin = $(this).data('origin');
            var value = $(this).html();
            cc_users[user_id] = value;
        }


    });


    var files = {};
    var files_count = 0;
    $('input[name^=files]').each(function (e) {
        var value = $(this).val();
        files[files_count] = value;
        files_count++;
    });


    var document_id = $('#document_id').val();
    var title = $('#title_input').val();
    var document_number_input = $('#document_number_input').val();
    var document_layout_input = $('#document_layout_input').val();
    var document_date_input = $('#document_date_input').val();
    document_date_input = moment(document_date_input, 'YYYY/MM/DD').format('YYYY-MM-DD');

    var document_content = '' ;
    var to_document_editor = '' ;
    var signer_document_editor = '' ;
    var cc_document_editor = '' ;


    document_content = $('div.editable').html();
    // for (i=0; i < tinyMCE.editors.length; i++){
    //     document_content = tinyMCE.activeEditor.getContent();
    //
    //     // var content = tinyMCE.editors[i].getContent();
    //     // if(tinyMCE.editors[i].id == 'page_0'){
    //     //     document_content = content;
    //     //     break;
    //     // }
    //     //
    //     // if(tinyMCE.editors[i].id == 'to_document_editor'){
    //     //     to_document_editor = content;
    //     // }
    //     //
    //     // if(tinyMCE.editors[i].id == 'signer_document_editor'){
    //     //     signer_document_editor = content;
    //     // }
    //     //
    //     // if(tinyMCE.editors[i].id == 'cc_document_editor'){
    //     //     cc_document_editor = content;
    //     // }
    //
    // }

    var workflow_id = $('.workflow_select').val();
    if(workflow_id === undefined){
        workflow_id = false;
    }

    var elements = {
        'to_users': to_users,
        'signers_users': signer_users ,
        'cc_users': cc_users,
        'content': document_content,
        'title': title,
        'files': files,
        'document_date': document_date_input,
        'document_number': document_number_input,
        'layout_id': document_layout_input,
        'workflow_id': workflow_id,
        'document_id': document_id,
        'documents_reference':documents_reference
    };


    return elements;


}

var aTo = [] ;
var aSigner = [] ;
var aCC = [] ;


$.fn.drawRecipient = function(data,without_content,compose_mode){
    without_content = without_content || false;
    compose_mode = compose_mode || false;

    var data_type = '' ;
    var elementID = data.id ;
    var elementType = data.element_type ;

    if(elementType === undefined){
        elementType = 'user' ;
    }

        data_type = data.data_type ;
        if(data_type =='To'){
            aTo.push(elementID)
        }
        if(data_type =='Signer'){
            aSigner.push(elementID)
        }
        if(data_type =='CC'){
            aCC.push(elementID)
        }


var elementIDN = elementID;
    var content = '<li class="media recipient-user recipient-' + elementType + '-' + elementID +'" id="recipient-' + elementType + '-'+ elementID +'" data-id="'+ data.id +'" data-type="'+ data_type +'" data-text="'+ data.first_name + ( data.last_name=='' ? '' : ' ') + data.last_name + '" data-elementType="'+ elementType +'" data-elementid="'+ elementIDN +'"  data-origin="' + data.first_name + ( data.last_name=='' ? '' : ' ') + data.last_name +'">' +
        '<div class="media-left">'  +
        '<a href="#">' +
        '<a href="" class="remove-recipient-' + elementType + '">X</a>' +
        '</a>' +
        '</div>' +
        '<div class="media-body media-middle"><span class="recipient-value">' +
        data.first_name + ( data.last_name=='' ? '' : ' ') + data.last_name + '</span><i class="recipient-' + elementType + '-type pull-right">'+ data_type +'</i>' +
        '</div>';
    if(without_content) {
        content +=
            '<div class="media-right media-middle">' +
            '<ul class="icons-list text-nowrap">' +
            '<li>' +
            '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-arrow-down5"></i></a>' +
            '<ul class="dropdown-menu dropdown-menu-right">' +
            '<li><a href="#" class="recipient-' + elementType + '-switch-to" data-type="To"><i class="icon-mail5 pull-right"></i> To</a></li>' +
            '<li><a href="#" class="recipient-' + elementType + '-switch-to" data-type="Signer"><i class="icon-quill4 pull-right"></i> Signer</a></li>' +
            '<li><a href="#" class="recipient-' + elementType + '-switch-to" data-type="CC"><i class="icon-cc pull-right"></i> CC</a></li>' +
            '<li class="divider"></li>' +
            '<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>' +
            '</ul>' +
            '</li>' +
            '</ul>' +
            '</div>';
    }
    content += '</li>' ;
if(without_content){

    if(data_type=='Signer'){

        //data-placement="top" data-toggle="popover" data-trigger="hover" title="Identifier" data-content="' + data.identifier +'"

        var editor_content = '<div ' ;
        if(!compose_mode){
            editor_content += ' data-placement="top" data-toggle="popover" data-trigger="hover" title="Identifier" data-content="' + data.identifier +'" ' ;
        }
        editor_content += 'class="popover_identifier ' + data_type.toLowerCase() + '_' + elementType + '_item ' + data_type.toLowerCase() + '_' + elementType + '_item_'+ data.id +'" id="' + data_type.toLowerCase() + '_' + elementType + '_item_'+  data.id +'" contenteditable="true" data-id="'+ data.id +'" data-type="' + data_type + '"  data-elementType="'+ elementType +'"  data-elementid="'+ elementIDN +'" data-origin="' + name + '">' + data.name;
        if(data.contact !== undefined){
            if(data.contact.is_sign=='1'){
                editor_content += '<img src="' + data.contact.sign.sign_image + '" class="sign_img" />';
            }
        }
        editor_content += '</div>' ;
        $('.' + data_type.toLowerCase() + '_document_editor').append(editor_content);

    }else if(data_type=='CC'){
        var editor_content = '<li  ' ;
        if(!compose_mode){
            editor_content += ' data-placement="top" data-toggle="popover" data-trigger="hover" title="Identifier" data-content="' + data.identifier +'" ' ;
        }
        editor_content += ' class="popover_identifier ' + data_type.toLowerCase() + '_' + elementType + '_item ' + data_type.toLowerCase() + '_' + elementType + '_item_'+ data.id +'" id="' + data_type.toLowerCase() + '_' + elementType + '_item_'+  data.id +'" contenteditable="true" data-id="'+ data.id +'" data-type="' + data_type + '"  data-elementType="'+ elementType +'"  data-elementid="'+ elementIDN +'" data-origin="'   + data.name + '">' + data.name +'</li>' ;
        $('.' + data_type.toLowerCase() + '_document_editor').find('ul').append(editor_content);

    } else{

        var editor_content = '<div  ' ;
        if(!compose_mode){
            editor_content += ' data-placement="top" data-toggle="popover" data-trigger="hover" title="Identifier" data-content="' + data.identifier +'" ' ;
        }
        editor_content += ' class="popover_identifier ' + data_type.toLowerCase() + '_' + elementType + '_item ' + data_type.toLowerCase() + '_' + elementType + '_item_'+ data.id +'" id="' + data_type.toLowerCase() + '_' + elementType + '_item_'+  data.id +'" contenteditable="true" data-id="'+ data.id +'" data-type="' + data_type + '"  data-elementType="'+ elementType +'"  data-elementid="'+ elementIDN +'" data-origin="' + data.name +  '">' + data.name +'</div>' ;
        $('.' + data_type.toLowerCase() + '_document_editor').append(editor_content);

    }
    if(!compose_mode) {
        $('.popover_identifier').popover();
    }

}
    //$('#recipient-list').append(content);

    //$.fn.saveDraft();
   // $('#recipient_input').val('');

};

    $.fn.load_editor = function() {
        $('editable').attr('contenteditable','true') ;
        tinymce.remove();
        tinymce.init({
            selector: 'div.editable',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
        tinymce.init({
            selector: 'div.to_document_editor',
            inline: true,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
        tinymce.init({
            selector: 'div.signer_document_editor',
            inline: true,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
        tinymce.init({
            selector: 'div.cc_document_editor',
            inline: true,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
        tinymce.init({
            selector: 'div.subject_page',
            inline: true,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste, mention',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            mentions: {
                queryBy:'title',
                delimiter: ['#'],
                source: function (query, process, delimiter) {
                    // Do your ajax call
                    // When using multiple delimiters you can alter the query depending on the delimiter used
                    if (delimiter == '#') {
                        $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                            //call process to show the result
                            process(data)
                        },'json');
                    }
                },
                highlighter: function(text) {
                    return text;
                    //make matched block italic
                    return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                        return '<i>' + match + '</i>';
                    });
                },
                insert: function(item) {
                    return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                }
            },
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
    };

$.fn.storeDocument = function(document) {

    globals['doument'] = document ;
    console.log('=================================================');
    console.log(document);
    console.log('=================================================');
    $.fn.drawWorkFlowAction();
}

$.fn.getStoreDocument = function() {
     return globals['doument'];
}

$.fn.getDocumentById = function(document_id) {

    $.post('/document/viewAjax/' + document_id ,{'json':true},function(data){
        return data;
    });
}


    $.fn.loadDraft = function() {

        $.post('/document/load_draft' ,{'json':true},function(data){
            if (!$.isEmptyObject(data.document_layout)) {
                if (!$.isEmptyObject(data.document_layout.layout_components)) {
                    $.each(data.document_layout.layout_components, function (k, v) {

                        if (k == 'header') {
                            $('.header_container').html(v);
                        }

                        if (k == 'footer') {
                            $('.footer_container').html(v);
                        }

                        if(k =='content'){

                            $('.a4_page_content').html(v) ;
                            $('.subject_page').html(data.document.title);
                            $('#document_number_text').html(data.document.outbox_number);
                            $('#document_date').val(data.document.document_date);
                            $('#document_date').attr('disabed','disabled');
                            $('.content_page').html(data.document.content);

                            if(editor){
                                $.fn.load_editor();

                            }






                            //drawReceipts('to', 'account', data.document.to_account);
                            //drawReceipts('to', 'contact', data.document.to_contact);
                            $.fn.drawDocumentReceipts('user', data.document.recipients);



                            $.fn.drawAttachment(data.document.files);



                                if(data.document.work_flow !== null) {
                                    if(('work_flow' in data.document.work_flow)) {
                                        $.fn.drawWorkFlow({
                                            'item': data.document.work_flow.work_flow,
                                            'current': data.document.work_flow.current_stage_id
                                        });
                                    }
                                    $.fn.drawActualWorkFlow({'item' : data.document.work_flow.work_flow ,'current': data.document.work_flow.current_stage_id });
                                }





                        }

                    });
                    if(editor) {
                        $.fn.fillInput(data);
                    }
                }
            }

            if( ('qr_code' in data.document)  ) {
                if(data.document.qr_code !==null ) {
                    $('#qr_code').html('<img src="' + data.document.qr_code.data + '" />');
                }

            }
            console.log('==========================');
            console.log(data.document);
            console.log('==========================');
            $.fn.document_unblock($('.sheet'));
        },'json');
    }


    $.fn.saveDraft = function () {

        var elements = $.fn.getElements();

        if (!elements) {
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return;
        }

        if (sessionStorage.randNumber) {
        } else {
            var rand_number = Math.floor((Math.random() * 1000000) + 1);
            elements['rand_number'] = rand_number  ;
            sessionStorage.randNumber = rand_number;
        }

elements['tab_number'] = sessionStorage.randNumber;
$.post(globals_routeSendDraft,elements,function(data){

        },'json');

    }



$.fn.getTemplate = function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajaxSetup({async: false});
    $.post( globals_routes.templates.getTemplates ,{},function(data){
        aTemplates =  data.templates;
    },'json');
}








    $.fn.drawDocumentReceipts = function(data_type, data) {


        if (data_type == 'user') {
            $.each(data, function (k, v) {

                var display_name = v.user.full_name;
                var first_name = v.user.first_name;
                var last_name = v.user.last_name;

                var orign_display_name = display_name;

                if (v.display_name !== null ) {
                    display_name = v.display_name;
                }

                var recipient_type = v.type;

                $.fn.drawRecipient({'user':v,'display_name' : display_name,'first_name' : first_name,'last_name' : last_name ,'data_type' : recipient_type ,'id' : v.user_id,'identifier' : v.identifier },true);



            });
        }

    }

    $.fn.drawAttachment = function(files) {

        var html = '<div>';
        $.each(files, function (key, file) {
            // html += '<div class="paper page A4">' ;
            var file_name = file.file.hash_name;
            var file_name_split = file_name.split(".");

            var ex = file_name_split[1];
            switch (ex){


                case "png":
                case "jpg":
                case "jpeg":
                case "gif":
                case "pcx":
                case "pic":
                    html += '<div class="paper page A4">' ;
                    html +='<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>';
                    html += /*'<span class="mail-attachments-preview">' +
                        '<i class="icon-file-picture icon-2x"></i>' +
                        '</span>' +*/
                        '<div class="mail-attachments-content">' +
                        '<span class="text-semibold">' + file.file.name + '</span>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +
                        '<li class="att_link"><a id="img_attached" target="blank"' +
                        'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<span class="text-semibold">' + file.file.name +  '</span>'+'<i class="icon-file-picture icon-2x">'+'</i>' +'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';
                    break;

                case"txt":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-file-text-o">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';

                    break;

                case "mp4":
                case "flv":
                case "mkv":
                case "avi":
                case "mov":
                case "mpeg":
                case "mpa":
                case "wm":
                case "wma":
                case "ram":
                case "rm":

                    html += '<div class="paper page A4">' ;
                    html +='<iframe class="vedio_attached" control ="1" autoplay="false"  src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>';
                    html += /*'<span class="mail-attachments-preview">' +
                        '<i class="icon-file-picture icon-2x"></i>' +
                        '</span>' +*/
                        '<div class="mail-attachments-content">' +

                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +
                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-file-video-o">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';


                    break;

                case"xl":
                case"xlt":
                case"xls":
                case"xla":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-file-excel-o">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';

                    break;

                case"ppt":
                case"pps":
                    html += '<div class="paper page A4">' ;

                    html +=
                        '<div class="mail-attachments-content">' +

                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +
                        '<li><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<span class="text-semibold">' + file.file.name + '</span>' +'<i class="fa fa-file-powerpoint-o">'+'</i>' + /*' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';

                case"doc":
                case"dot":
                case"rtf":
                case"wri":
                case"mcw":
                case"wpd":
                    html += '<div class="paper page A4">' ;

                    html +=
                        '<div class="mail-attachments-content">' +

                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +
                        '<li><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<span class="text-semibold">' + file.file.name + '</span>' +'<i class="fa fa-file-word-o">'+'</i>' + /*' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';

                case"wav":
                case"mid":
                case"mda":
                case"mp3":
                case"snd":
                case"au":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-file-sound-o">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';

                    break;
                /*'mdb','adp','mda','mde','ade','db','dbf'*/
                case"pdf":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="icon-file-pdf">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';

                    break;
                /*case"mdb":*/
                case"adp":
                case"mde":
                case"ade":
                case"db":
                case"dbf":
                case"laccdb":
                case"accdt":
                case"accdb":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-database">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';

                    break;

                case"ZIP":
                case"RAR":
                case"ARJ":
                case"ACE":
                case"TAR":
                case"JAR":
                case"IOS":
                case"UUE":
                case"BZ2":
                    html += '<div class="paper page A4">' ;

                    html +=

                        '<iframe class=""   src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>' +
                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +

                        '<li class="att_link"><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+'<span class="text-semibold">' + file.file.name + '</span>' + '<i class="fa fa-file-archive-o">'+'</i>' +/* ' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +

                        '</div>';
                    break;
                default:
                    html += '<div class="paper page A4">' ;

                    html +=
                        '<div class="mail-attachments-content">' +

                        '<ul class="list-inline list-inline-condensed no-margin">' +
                        '<li class="text-muted">174 KB</li>' +
                        '<li><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<span class="text-semibold">' + file.file.name + '</span>' +'<i class="icon-file-pdf icon-2x">'+'</i>' + /*' ' +'.'+ ex +*/'</a>' +
                        '</li>' +
                        '<li><a href="#" class="hide">Download</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';
            }



        });
        html += '<div>';
        $('.attachments').html(html);


    }

    $.fn.fillInput = function(data){

        // console.log(data.document.work_flow.work_flow);
        // $('#document_id').val(data.document.id);
        // $('.workflow_select').val(data.document.work_flow.work_flow.id);
        //
        //
        //
        // $('.workflow_select').select2({
        //     initSelection: function (element, callback) {
        //         var id = $(element).val();
        //         if (id !== "") {
        //             $('.workflow_select').val(data.document.work_flow.work_flow.id);
        //             $('.workflow_select').html('<option value="'+ data.document.work_flow.work_flow.id +'" selected="selected">'+data.document.work_flow.work_flow.name +'</option>');
        //             callback({"text":data.document.work_flow.work_flow.name, "id":data.document.work_flow.work_flow.id,'item':data.document.work_flow.work_flow});
        //         }
        //     },
        //     ajax: {
        //         url: workflow_findByCompany,
        //         dataType: 'json',
        //         processResults: function (data) {
        //             return {
        //                 results: $.map(data, function (item) {
        //                     return {
        //                         text: item.name,
        //                         id: item.id,
        //                         item: item
        //                     }
        //                 })
        //             };
        //         },
        //         results: function (data) {
        //             return {
        //                 results: $.map(data, function (item) {
        //                     return {
        //                         text: item.name,
        //                         slug: item.name,
        //                         id: item.id
        //                     }
        //                 })
        //             };
        //         }
        //         // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        //     }
        // });
        //
        // $('.workflow_select').on('select2:select', function (e) {
        //     var data = e.params.data;
        //     $.fn.drawWorkFlow(data);
        // });


        $('#title_input').val(data.document.title);
        $('#document_number_input').val(data.document.outbox_number);

        //var html_files = '' ;
        // $.each(data.document.files,function(k,file){
        //     $('.filelist').append('<img src="' + globals.CDN_HOST + '/files/common/' + file.file.hash_name + '" class="img-responsive img-thumbnail" />');
        //     $('.filelist').append('<input type="hidden" name="my_uploader_id_files[]" value="' + file.file.id + '" id="' + file.file.id + '-hidden">');
        //
        // });
        //$('.filelist').html(html_files);

    }

    $.fn.document_block = function (selector) {

        var block = selector;
        $(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });


    }

    $.fn.document_unblock = function (selector) {
        var block = selector;
        $(block).unblock();
    }




$('.storage_full').on('click',function(){
    swal({
        title: "Oops...",
        text: i18n.t("Your storage is full,Please upgrade your plan! However you will keep recieving doeucments in your inbox with no limit."),
        confirmButtonColor: "#EF5350",
        type: "error",
        showCancelButton: true,
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
    },function(isConfirm){

        if (isConfirm){
            //swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
            window.location= '/plans/upgrade' ;
        } else {
            //swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });
})



    //$.fn.getTemplate();

});