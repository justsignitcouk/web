@if( request()->ajax() )


    {!! Theme::asset()->themePath()->add([ ['compose-ajax', 'partials/document/compose.js',['bootstrap']]]) !!}

    @partial('document.recipient',[ 'aDocument'=> $oDocument['document'] ])
    <div class="btn-group navbar-btn">
        <button type="button" id="saveDraftButton" class="btn btn-default legitRipple"><i class="icon-plus2"></i> <span
                    class="hidden-xs position-right">Save</span></button>
        <button type="button" id="saveTemplate" class="btn btn-default legitRipple"><i class="icon-plus2"></i> <span
                    class="hidden-xs position-right">Save Template</span></button>
        <button type="button" id="choose_layout" class="btn btn-default legitRipple choose_layout"><i
                    class="icon-plus2"></i> <span class="hidden-xs position-right">Choose layout</span></button>

        <a href="{{route('inbox.all') }}" class="btn btn-default legitRipple"><i class="icon-cross2"></i> <span
                    class="hidden-xs position-right">Cancel</span></a>

        <div class="btn-group">
            {{--<button type="button" class="btn btn-default dropdown-toggle legitRipple" data-toggle="dropdown">--}}
            {{--<i class="icon-menu7"></i>--}}
            {{--<span class="caret"></span>--}}
            {{--</button>--}}

            {{--<ul class="dropdown-menu dropdown-menu-right">--}}
            {{--<li><a href="#">Action</a></li>--}}
            {{--<li><a href="#">Another action</a></li>--}}
            {{--<li><a href="#">Something else here</a></li>--}}
            {{--<li><a href="#">One more line</a></li>--}}
            {{--</ul>--}}
        </div>
    </div>
    @partial('A4.Layout',['aDocument' => $oDocument['document'],'id'=>$id ,'bContentEditable'=> true ] )

@else

    {!! Theme::asset()->themePath()->add([ ['daterangepicker', 'js/plugins/pickers/daterangepicker.js',['bootstrap']]]) !!}
    {!! Theme::asset()->themePath()->add([ ['compose', 'pages/document/compose-new.js',['interactions']]]) !!}
    {!! Theme::asset()->themePath()->add([ ['paperjs', 'pages/document/paper.js',['jquery']]]) !!}

    {!! Theme::asset()->themePath()->add([ ['fab', 'js/plugins/ui/fab.min.js',['jquery']]]) !!}

    {!! Theme::asset()->add([ ['papercss', 'assets/components/paper-css/paper.css',['jquery']]]) !!}
    {!! Theme::asset()->add([ ['paper_tools', 'assets/components/paper-css/paper_tools.css',['papercss']]]) !!}

    {!! Theme::asset()->add([ ['tinymce', 'assets/components/tinymce/js/tinymce/tinymce.min.js',['form_select2']]]) !!}
    {!! Theme::asset()->themePath()->add([ ['interactions', 'js/core/libraries/jquery_ui/interactions.min.js',['tinymce']]]) !!}


    <div id="global_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="global_modal_form">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save-action">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!---- global modal ---->

    <div class="content-wrapper">


        <!-- Content area -->
        <div class="content ">
            <div class="modal fade layout_modal" data-route="{{route('document.getLayouts')}}" tabindex="-1"
                 role="dialog"
                 aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                    </div>
                </div>
            </div>

            <div class="panel panel-flat " style="padding-top: 0;">

                <div class="panel-body">
                    @partial('document.wizard.dynamic',[ 'aWorkFlow'=>$aWorkFlow , 'aDocument'=> $oDocument['document']
                    ])

                    <div class="compose-components">
                        @partial('document.recipient',[ 'aDocument'=> $oDocument['document'] ])
                        @partial('document.elements',[ 'aDocument'=> $oDocument['document'] , 'id'=> isset($id) ? $id :
                        false ,'route'=>''])
                        @partial('document.attachment',[ 'aDocument'=>$oDocument['document'] , 'id'=> isset($id) ? $id :
                        false ])
                        <div class="col-md-12 text-center">
                            <a href="#" id="pickfiles" class="link-icon details-document details_button tooltip-tools"
                               data-popup="tooltip" title="Add Attachment" data-placement="bottom"
                               data-original-title="Add Attachment"><i class="icon-attachment"></i></a>

                            <a {!! $oDocument['document']['work_flow']['current_stage_id'] != 1 ? 'disabled' : 'href="#" id="sendDocument"'  !!}
                               class="link-icon details-document details_button tooltip-tools {{ $oDocument['document']['work_flow']['current_stage_id'] != 1 ? 'disabled' : '' }}" data-popup="tooltip"
                               title="Send Document" data-placement="bottom" data-original-title="Send Document"><i
                                        class="fa fa-send"></i></a>

                            <a href="#" id="saveDraftButton"
                               class="link-icon details-document details_button tooltip-tools" data-popup="tooltip"
                               title="Save Draft" data-placement="bottom" data-original-title="Save Draft"><i
                                        class="fa fa-save"></i></a>

                            <a href="#" id="saveTemplate"
                               class="link-icon details-document details_button tooltip-tools" data-popup="tooltip"
                               title="Save Template" data-placement="bottom" data-original-title="Save Template"><i
                                        class="fa fa-puzzle-piece"></i></a>
                            <a href="#" id="choose_layout"
                               class="link-icon details-document details_button tooltip-tools choose_layout"
                               data-popup="tooltip" title="Choose Layout" data-placement="bottom"
                               data-original-title="Choose Layout"><i class="fa fa-paint-brush"></i></a>
                            <a href="{{route('inbox.all') }}" id="cancel"
                               class="link-icon details-document details_button tooltip-tools" data-popup="tooltip"
                               title="Cancel" data-placement="bottom" data-original-title="Cancel"><i
                                        class="fa fa-remove"></i></a>
                        </div>
                    </div>

                    <div class="" id="compose-content">

                        <div class="pages_frame" id="">

                            <div class="a4_paper_container" style="width: auto;width: 210mm;margin: 0 auto;">
                                <div class="paper_container">

                                    <div class="">
                                        @partial('A4.Layout',['aDocument' => $oDocument['document'],'id'=>$id
                                        ,'bContentEditable'=> true ] )
                                        {{--@partial('document.compose.a4',['id'=> isset($id) ? $id : false , 'editor' => $editor,'displayWorkflow' => $displayWorkflow])--}}

                                        <img src="" id="canvas_data"/>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /content area -->

            </div>

        </div>
    </div>

    {{--    @partial('compose_secondary_sidebar',[
        'id'=> isset($id) ? $id : false ,
        'route'=> isset($route) ? $route : '' ,
        'edit' => isset($edit) ? $edit : false ,
        'displayWorkflow' => $displayWorkflow,
        'aDocument' => $oDocument['document']
        ])--}}

    <script>
        var globals_route;
        var globals_routeSendDocument = '{{ route('document.sendDoc') }}';
        var globals_routeSendDraft = '{{ route('document.sendDraft') }}';
        var globals_route_getLayout = '{{ route('document.getLayout',['id' => 1]) }}';
        var globals_route_get_workflows = '{{ route('document.getWorkFlowsForCurrentAccount') }}';
        var document_id = {{ isset($id) ? $id : 'null' }} ;
        var displayWorkflow = {{ $displayWorkflow ? 1 : 0 }} ;
    </script>
@endif