@extends('layouts.admin')

@section('content')

<div class="col-md-12">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="tabbable">

                <ul class="breadcrumb">
                    <li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
                    <li><a href="{{route('admin.companies.show' , $company->id)}}"><i class="position-left"></i>{{ $company->name }}</a></li>
                    <li><a href="{{route('admin.branches',$company->id)}}"><i class="position-left"></i>{{ __("common.branches") }}</a></li>
                    <li><a href="{{route('admin.combranches.show',$company->id)}}"><i class="position-left"></i>{{ $branch->name }}</a></li>
                    <li><a href="{{route('admin.departments',$branch->id )}}"><i class="position-left"></i>{{ __("common.departments") }}</a></li>
                    <li><a href="{{route('admin.branchdeps.show' ,$oDepartment->id)}}"><i class="position-left"></i>{{ $oDepartment->name }}</a></li>
                    <li class="active">{{ __("common.permissions") }}</li>
                </ul>
                
            @include('layouts/partials/tabs/department_tabes')

            <div class="tab-content">
                 <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row"> 

                        {!! Form::open(['route' => ['admin.branchdeps.updatepermissions', $department->id],'method'=>'POST']) !!}

                          @include('layouts/partials/permissions') 


                        {{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

                          
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $('#all').change(function () {

        $state = $("#all").is(':checked');

        if($state){
           $('.form-check-input').prop('checked', true); 
       }else{
           $('.form-check-input').prop('checked', false); 
       }
        
    });

    $('#all-companies').change(function () {

        $state = $("#all-companies").is(':checked');

        if($state){
           $('.company-check-input').prop('checked', true); 
       }else{
           $('.company-check-input').prop('checked', false); 
       }
        
    });

    $('#all-accounts').change(function () {

        $state = $("#all-accounts").is(':checked');

        if($state){
           $('.account-check-input').prop('checked', true); 
       }else{
           $('.account-check-input').prop('checked', false); 
       }
        
    });

    $('#all-permissions').change(function () {

        $state = $("#all-permissions").is(':checked');

        if($state){
           $('.permission-check-input').prop('checked', true); 
       }else{
           $('.permission-check-input').prop('checked', false); 
       }
        
    });

    $('#all-branches').change(function () {

        $state = $("#all-branches").is(':checked');

        if($state){
           $('.branch-check-input').prop('checked', true); 
       }else{
           $('.branch-check-input').prop('checked', false); 
       }
        
    });

    $('#all-roles').change(function () {

        $state = $("#all-roles").is(':checked');

        if($state){
           $('.role-check-input').prop('checked', true); 
       }else{
           $('.role-check-input').prop('checked', false); 
       }
        
    });

    $('#all-departments').change(function () {

        $state = $("#all-departments").is(':checked');

        if($state){
           $('.department-check-input').prop('checked', true); 
       }else{
           $('.department-check-input').prop('checked', false); 
       }
        
    });

   
});
</script>
@endpush
@endsection
