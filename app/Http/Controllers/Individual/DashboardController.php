<?php


namespace App\Http\Controllers\Individual;

use App\Services\Dashboard;

class DashboardController extends Controller
{


    public function needsMySignatureAjax(){

        return Dashboard::getNeedsUserSignature();

    }

    public function waitingFromOthersSignatureAjax(){

        return Dashboard::getOthersSignature();

    }

}