<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Workflow extends Model
{
    //
    protected $table = 'wf_workflow';

    //public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'active',
        'created_by',
        'updated_by'
    ];

    protected $guarded = [];

    public function Stages(){
        return $this->hasMany('\App\Models\Workflow\Stage', 'workflow_id', 'id');
    }


}
