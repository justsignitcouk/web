<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentFile
 */
class DocumentFile extends Model
{
    protected $table = 'document_file';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'file_id',
        'valid'
    ];

    protected $guarded = [];

        
}