<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepartmentRole
 */
class DepartmentRole extends Model
{
    protected $table = 'department_role';

    public $timestamps = true;

    protected $fillable = [
        'department_id',
        'role_id',
        'active'
    ];

    protected $guarded = [];

        
}