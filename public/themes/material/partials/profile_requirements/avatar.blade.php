{!! Theme::asset()->container('auth')->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['script', 'partials/profile_requirements/avatar.js',['plupload_queue']]]) !!}

{!! Theme::asset()->container('auth')->themePath()->add([ ['animatecss.css','css/extras/animate.min.css',['bootstrap']]]) !!}

<style>
    #uploadfiles {
        display: none;
    }
</style>

<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel">
            <div class="panel-body">
                <h3>Upload a profile picture</h3>

                <div class="row">
                    <div class="col-md-6" style="text-align: center;">
                        <div style="min-height: 200px; text-align: center;">
                            <div class=""
                                 style="background: white;border: 1px solid #888;border-color: rgba(0,0,0,.5);border-radius: 3px;box-shadow: 0 1px 3px rgba(0,0,0,.3);display: inline-block;min-height: 160px;min-width: 160px;padding: 4px;width: auto;">
                                <div>
                                    <img id="profile_pic" src="/assets/images/default_profile.png"
                                         style="display: block;max-height: 160px;"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="text-align: center;">


                        <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                        <br/>

                        <div id="container">
                            <button id="pickfiles" class="btn btn-success">Add Picture</button>
                            <a id="uploadfiles" href="javascript:;">[Upload files]</a>
                        </div>

                        <hr/>
                        {{--<a class="">Take a Photo <br/> <small>With your webcam</small></a>--}}
                        @if($aProfileRequirement['should_fill']==0)
                            <a href="#">Skip</a>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    var route_upload_profile = "{{route('profile.uploadProfilePicture',[getUniqueName()])}}"
    var route_delete_profile = "{{route('profile.deleteProfilePicture',[getUniqueName()])}}"
</script>