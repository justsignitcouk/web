<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
                'username'=>'required|unique:users',
                'first_name'=>'required',           
                'last_name'=>'required',
                'email'=>'required|email|unique:users',
                'password'=>'required|
                confirmed|
                min: 6 |
                regex:/[a-z]/|
                regex:/[A-Z]/|
                regex:/[0-9]/|
                ',
                'password_confirmation' => 'required',
                'profile_image' => 'mimes:jpeg,jpg,png',
                ];

    }
}
