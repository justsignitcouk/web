<?php

namespace App\Http\Controllers\Admin\Workflow;

use App\Models\Workflow\StateActionPermitsAccount;
use App\Models\Workflow\StateActionPermitsRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workflow\StateAction;
use App\Models\Workflow\StateActionAssignee;
use Validator;

class StateActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'actions'  => "required",
            'permits'  => 'required|array',
        ]);
        if($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ]);
        }

        $data = $request->all();

        $oStateAction = new StateAction();
        $oStateAction->state_id = $data['state_id'];
        $oStateAction->action_id = $data['actions'];
        $oStateAction->created_by = 1;
        if ($oStateAction->save()){
            $aPermits = $data['permits'];
            foreach ($aPermits as $permits){
                $explode_permits = explode('_',$permits) ;
                $type = $explode_permits[0] ;
                $id = $explode_permits[1] ;
                if($type=='role'){
                    $oStateActionPermitsRole = new StateActionPermitsRole();
                    $oStateActionPermitsRole->role_id = $id ;
                    $oStateActionPermitsRole->state_action_id = $oStateAction->id;
                    $oStateActionPermitsRole->is_inherit = '0';
                    $oStateActionPermitsRole->created_by = '1' ;
                    $oStateActionPermitsRole->created_at = date('Y-m-d H:i:s') ;
                    $oStateActionPermitsRole->save();
                }
                if($type=='account'){
                    $oStateActionPermitsAccount = new StateActionPermitsAccount();
                    $oStateActionPermitsAccount->account_id = $id ;
                    $oStateActionPermitsAccount->state_action_id = $oStateAction->id;
                    $oStateActionPermitsAccount->is_inherit = '0';
                    $oStateActionPermitsAccount->created_by = '1' ;
                    $oStateActionPermitsAccount->created_at = date('Y-m-d H:i:s') ;
                    if(!$oStateActionPermitsAccount->save()){
                        return response()->json(['status' => false, 'message' => $oStateActionPermitsAccount->getMessage() ]);
                    }
                }

            }
            return response()->json(['status' => true, 'message' => 'Added Successfully']);
        }

        return response()->json(['status' => false, 'message' => $oStateAction->getMessage() ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
