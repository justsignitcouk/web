<?php

namespace App\Http\Controllers\Individual;
use Illuminate\Http\Request;
use Facuz\Theme\Facades\Theme;
use App ;
use App\Services\WorkFlow;

class WorkflowController extends Controller
{
    public function __construct()
    {

        parent::__construct();
        view()->share('bShowMainSidebar', false);
    }

    public function findByCompany(Request $request){
        $data = $request->all();
        $oWorkflow = WorkFlow::getByCompany($data);
        return json_encode($oWorkflow) ;

    }

    public function getStageDetails(Request $request){
        $data = $request->all();
        $oStage = WorkFlow::getStageDetails($data);
        return json_encode($oStage) ;

    }

    public function getActions(Request $request){

        $data = $request->all();
        $oStage = WorkFlow::getActions($data);
        return json_encode($oStage) ;

    }

    public function saveAction(Request $request){

        $data = $request->all();
        $oStage = WorkFlow::saveAction($data);
        return json_encode($oStage) ;
    }

}

