@if(request()->ajax())
    {!! Theme::asset()->themePath()->add([ ['view-new', 'pages/document/view-new.js',['bootstrap']]]) !!}
    {!! Theme::asset()->themePath()->add([ ['workflow_actions', 'partials/workflow/actions-new.js',['bootstrap']]]) !!}
    {!! Theme::asset()->container('inline')->add([['custom-workflow', '
    <script type="text/javascript">
        var workflow_get_actions = "' .   route('workflow.getActions')  .'" ;
        var workflow_save_action = "' .   route('workflow.saveAction')  .'" ;
        var document_id = "' . $id . '" ;
        var current_user_id = "' . nUserID() . '" ;
        var account_roles_ids = ["' . implode([],'","') . '"] ;
    </script>']])  !!}
    @php
        $viewDocument = 0 ;
        $aSignersNames = [];
    @endphp
    @foreach($oDocument['document']['recipients'] as $recipient)
        @if($recipient['type'] == 'Signer' && $recipient['can_read'] == 1 )
            @php
                $aSignersNames[] = $recipient['display_name'] ;
            @endphp

            @if($recipient['user_id'] == nUserID() || $recipient['identifier'] == getIdentifier()  )
                @php $viewDocument = 1; @endphp
            @endif

        @endif
    @endforeach

    @if($viewDocument!=1)
        <div class="alert alert-info alert-styled-left alert-bordered"
             style="width: 990px;margin: 0 auto;margin-bottom: 15px;">

            Your document has been sent to <span class="signer_names">{{ implode($aSignersNames) }}</span> for
            signature. Please wait until he sign it and u will be notified, or you you can text <span
                    class="signer_names">{{ implode($aSignersNames) }}</span>.
        </div>

    @endif

    <form class="actions_container_form">
        <div class="actions-components">
            <div class="col-md-12 text-center">
                @foreach($oDocument['oActions'] as $oAction)
                    <a href="#" data-state_id='{{ $oAction['state_id'] }}' data-stage_id='{{ $oAction['stage_id'] }}' data-action_id='{{$oAction['id']}}' id="action{{$oAction['id']}}" class="{{ $oAction['action_type']=='sign' ? 'insert_sign' : 'action_button' }}  link-icon details-document details_button tooltip-tools"
                       data-popup="tooltip" title="{{$oAction['name']}}" data-placement="bottom"
                       data-original-title="{{$oAction['name']}}"><i class="{{$oAction['icon']}}"></i></a>
                @endforeach
            </div>
        </div>
    </form>


    <div class="content ">
        <div class="" id="view-content">

            <div class="pages_frame" id="">

                <div class="a4_paper_container" style="width: auto;width: 210mm;margin: 0 auto;">
                    <div class="paper_container">

                        @partial('A4.Layout',['aDocument' => $oDocument['document'],'id'=>$id ,'oDocumentNotes'=>
                        isset($oDocumentNotes) ? $oDocumentNotes : [] , 'bContentEditable' => false ] )
                        <div class="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@else

    <link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/components/printjs/dist/print.min.css') }}" rel="stylesheet" type="text/css">
    {!! Theme::asset()->add([ ['printjs', 'assets/components/printjs/dist/print.min.js',['bootstrap']]]) !!}
    {!! Theme::asset()->add([ ['printcss', 'assets/components/printjs/dist/print.min.css',['bootstrap']]]) !!}
    {!! Theme::asset()->add([ ['sticky-notes-js', 'assets/components/sticky-notes/sticky-notes.js',['jquery-ui-js']]]) !!}
    {!! Theme::asset()->add([ ['sticky-notes-css', 'assets/components/sticky-notes/sticky-notes.css',['jquery-ui-css']]]) !!}

    {!! Theme::asset()->themePath()->add([ ['typeahead', 'js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',['bootstrap']]]) !!}
    {!! Theme::asset()->themePath()->add([ ['handlebars', 'js/plugins/forms/inputs/typeahead/handlebars.min.js',['typeahead']]]) !!}

    @if(isset($displayForwarded))
        <script>
            var forward_id = "{{ $forward_id }}";
        </script>
        {!! Theme::asset()->themePath()->add([ ['forward', 'partials/document/forward.js',['bootstrap']]]) !!}
    @endif

    {!! Theme::asset()->themePath()->add([ ['view-new', 'pages/document/view-new.js',['bootstrap']]]) !!}

    {!! Theme::asset()->themePath()->add([ ['workflow_actions', 'partials/workflow/actions-new.js',['bootstrap']]]) !!}
    {!! Theme::asset()->container('inline')->add([['custom-workflow', '
    <script type="text/javascript">
        var workflow_get_actions = "' .   route('workflow.getActions')  .'" ;
        var workflow_save_action = "' .   route('workflow.saveAction')  .'" ;
        var document_id = "' . $id . '" ;
        var current_user_id = "' . nUserID() . '" ;
        var account_roles_ids = ["' . implode([],'","') . '"] ;
    </script>']])  !!}

    <div id="iframe" style="display:none"></div>


    <!---- global modal ---->

    <div id="global_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="global_modal_form">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save-action">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!---- global modal ---->

    <div class="content-wrapper">

        <div class="panel panel-flat " style="padding-top: 0;">

            <div class="panel-body">
                @partial('document.wizard.dynamic',[ 'remote'=>'false', 'aWorkFlow'=>$aWorkFlow , 'aDocument'=>
                $oDocument['document'] ])

                <form class="actions_container_form">
                <div class="actions-components">
                    <div class="col-md-12 text-center">
                        @foreach($oDocument['oActions'] as $oAction)
                            <a href="#" data-state_id='{{ $oAction['state_id'] }}' data-stage_id='{{ $oAction['stage_id'] }}' data-action_id='{{$oAction['id']}}' id="action{{$oAction['id']}}" class="{{ $oAction['action_type']=='sign' ? 'insert_sign' : 'action_button' }}  link-icon tooltip-tools"
                               data-popup="tooltip" title="{{$oAction['name']}}" data-placement="bottom"
                               data-original-title="{{$oAction['name']}}"><i class="{{$oAction['icon']}}"></i></a>
                        @endforeach

                            <a href="/document/pdf/{{$id}}" target="_blank" id='pdf_a4' class="no-smoothState pdf_a4 link-icon  tooltip-tools"
                               data-popup="tooltip" title="PDF" data-placement="bottom"
                               data-original-title="PDF"><i class=" icon-file-pdf"></i></a>

                            <a href="#" id='print_a4' class="print_a4 link-icon  tooltip-tools"
                               data-popup="tooltip" title="Print" data-placement="bottom"
                               data-original-title="Print"><i class="icon-printer2"></i></a>

                            <a href="#" id='print_a4' data-operation='+' class="zoom_for_page link-icon  tooltip-tools"
                               data-popup="tooltip" title="Zoom In" data-placement="bottom"
                               data-original-title="Zoom In"><i class="icon-zoomin3"></i></a>

                            <a href="#" id='print_a4' data-operation='-' class="zoom_for_page link-icon  tooltip-tools"
                               data-popup="tooltip" title="Zoom In" data-placement="bottom"
                               data-original-title="Zoom In"><i class="icon-zoomout3"></i></a>


                    </div>
                </div>
                </form>
                <!-- Content area -->
                <div class="content ">
                    <div class="" id="compose-content">

                        <div class="pages_frame" id="">

                            <div class="a4_paper_container" style="width: auto;width: 210mm;margin: 0 auto;">
                                <div class="paper_container">

                                    <div class="">
                                        @partial('A4.Layout',['aDocument' => $oDocument['document'],'id'=>$id
                                        ,'bContentEditable'=> false ] )
                                        {{--@partial('document.compose.a4',['id'=> isset($id) ? $id : false , 'editor' => $editor,'displayWorkflow' => $displayWorkflow])--}}

                                        <img src="" id="canvas_data"/>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /content area -->

                <div class="panel panel-body border-top-teal">
                    <ul class="list-feed">
                        @foreach($oDocument['document']['document_notes'] as $aDocumentNote )
                        <li>
                            <div class="text-muted">{{ $aDocumentNote['created_at'] }}</div>
                            <a href="{{ route('profile.index',[ $aDocumentNote['user']['username'] ]) }}">{{ $aDocumentNote['user']['full_name'] }}</a> {{ $aDocumentNote['content'] }}
                        </li>
                        @endforeach

                    </ul>
                </div>
                {{--@partial('secondary_sidebar',['id'=>$id,'displayWorkflow'=>true])--}}

            </div>
        </div>

    </div>
    <div class="modal fade" id="workflow_stage_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Stage Details</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




    <script>
        var signDoc = '{{route('document.signDoc')}}';
        var getSign = '{{route('profile.getDefaultSignature',[getUniqueName()])}}';
        var statusDoc = '{{route('document.statusDoc')}}';
        var document_id = '{{ $id }}';
        var forward_id = '{{ isset($forward_id) ? $forward_id : '' }}';
        var view = '{{__('lang.view')}}';
        var displayWorkflow = '{{ $displayWorkflow }}';
    </script>
@endif