


<form class="form-horizontal " role="form" method="POST" action="{{ url('storeneedhelp') }}">
    {{ csrf_field() }}
    <div class="panel panel-body login-form" style="width:50% ">

        <a href="{{route('login')}}" class="col-md-3 col-sm-4"><i class="back-arrow-login glyphicon glyphicon-arrow-left"></i></a><br/>



        <div class="text-center">
            <h5 class="content-group"><span data-i18n="help.contact" >{{trans("auth.contact_us")}} </span><small class="display-block" data-i18n="help.required" >{{trans("auth.all_fields_required")}}</small></h5>
        </div>

        @if (session('status'))

            <?php $check = session()->pull('status') ?>


                @if($check == true)
                    <div class="alert alert-success">
                        {{ trans("auth.need_help_post_success") }}
                    </div>
                @endif


        @endif

        <div class="form-group has-feedback has-feedback-left " data-i18n="[placeholder]input.title" data-i18n-target=".title">

            {!! Form::text('title', null, ['class' => 'form-control title' , 'autocomplete'=>'off'  , 'placeholder'=> trans("auth.title") ]) !!}

            @if ($errors->has('title'))
                <span style="color : red"  >{{ $errors->first('title') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.your_name" data-i18n-target=".your_name">

            {!! Form::text('name', null, ['class' => 'form-control your_name' , 'autocomplete'=>'off' , 'id'=>'email' , 'placeholder'=> trans("auth.your_name") ]) !!}

            @if ($errors->has('name'))
                <span style="color : red">{{ $errors->first('name') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.your_email" data-i18n-target=".your_email">

            <input style="display:none" type="text" name="email"/>

            {!! Form::text('email', null, ['class' => 'form-control your_email' , 'autocomplete'=>'off' , 'id'=>'email' , 'placeholder'=> trans("auth.your_email") ]) !!}


            @if ($errors->has('email'))
                <span style="color : red">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left"  data-i18n="[placeholder]input.your_msg" data-i18n-target=".your_msg">

            {!! Form::textarea('message', null, ['class' => 'form-control your_msg' , 'autocomplete'=>'off'  , 'placeholder'=> trans("auth.your_message") ]) !!}


            @if ($errors->has('message'))
                <span style="color : red">{{ $errors->first('message') }}</span>
            @endif
        </div>

        <button type="submit" class="btn btn-primary btn-block btn-lg"><span data-i18n="signup.submit_btn" >{{trans("auth.submit")}}</span><i class="icon-circle-right2 position-right"></i></button>
    </div>
</form>

<!-- /advanced signup -->








