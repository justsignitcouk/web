$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    $(document).on('submit','form.link_account',function(e){
        var form_data = $(this).serialize() ;
        $.post("/profile/link_account",form_data,function(data){
           $('#link_account').modal('hide');

        }) ;
        e.preventDefault();
    });

    $(document).on('click','.current_account',function(e){
       var account_id = $(this).data('accountid');
       $.post('profile/current_account',{ 'account_id' : account_id } ,function(data){
           location.reload();
       },'json');
        e.preventDefault();
    });

    $(document).on('click','.deactivateAccount',function(e){
        var account_id = $(this).data('accountid');
        $(document).find('#accountId').val(account_id);
        e.preventDefault();
    });

    $(document).on('click','button.deactivate_account',function(e){
        var accountId = $(document).find('#accountId').val();
        var password = $("#deactivate_account").find('#password').val();
        var message = $("#deactivate_account").find('#reason').val();
        console.log(deactivate);

        $.post(deactivate,{ 'id ' : accountId,'password' : password,'message' : message } ,function(data){
            location.reload();
        },'json');

        e.preventDefault();
    });

    function drawUsers(oUsers){
    var html = '' ;
        $.each(oUsers, function(i, oUser) {
            $.each(oUser.accounts, function(i, oAccount) {

            html += '<div class="col-xs-12">';
            html += '<div data-id="' + oAccount.id + '" class="panel panel-' + ( oAccount.activated == 0 ? 'deactivated' : ( oAccount.id  == current_account ? 'success' : 'primary')) + '">'
            html += '<div class="panel-heading">';
            html += '<h6 class="panel-title">' +
                (oAccount.id  == current_account ? '<span class="glyphicon glyphicon-star"></span> ' : '' ) +
                (oAccount.id  == globals.account_id ? '<span class="glyphicon glyphicon-ok-sign"></span> <strong  >' + oUser.email + '</strong>' : '<a href="#" class="current_account" data-accountid="'+ oAccount.id +'">' + oUser.email + '</a>' ) + '</h6>';
            html += '</div>';
            html += '<div class="panel-body">';
                html += '<div class="col-xs-12"><span class="col-sm-3 col-xs-5 account-label"><span class="hidden-xs">Account </span>Name:</span><span class="col-sm-9 col-xs-7"> '+oAccount.account_name + '</span></div>';
            html += '<div class="col-xs-12"><span class="col-sm-3 col-xs-5 account-label"><span class="hidden-xs">Account </span>Date:</span><span class="col-sm-9 col-xs-7"> ' + oAccount.created_at + '</span></div>';
            if(oAccount.department) {
                html += '<div class="col-xs-12"><span class="col-sm-3 col-xs-5 account-label">Company:</span><span class="col-sm-9 col-xs-7"> ' + oAccount.department.branch.company.name + ' / ' + oAccount.department.branch.address + '</span></div>';
                html += '<div class="col-xs-12"><span class="col-sm-3 col-xs-5 account-label">Department:</span><span class="col-sm-9 col-xs-7"> ' + oAccount.department.name + '</span></div>';
            }
            html += '</div><div class="panel-footer'+( oAccount.id  == current_account && oAccount.id  == globals.account_id ? ' hidden' : '' )+'">';

            html += '<div class="account-options col-xs-12 text-right">' +
                '   <a  data-toggle="tab" class="current_account' + (oAccount.id  == globals.account_id ? ' hidden' : '' ) + '" data-accountid="'+ oAccount.id +'" >Use Account</a>' +
                '   <a class="make_default'+ (oAccount.id  == current_account ? ' hidden' : '' ) + '">Make default</a>' +
                ( oAccount.id  == current_account || oAccount.id  == globals.account_id  ? '' : '<a class="deactivateAccount"  data-accountid="'+ oAccount.id +'" data-toggle="modal" data-target="#deactivate_account" >Deactivate</a>' ) +
                '   </div> ';

            html += '</div></div></div>';
            });
        });

    $('.all_accounts').html(html);

    }



    $.post('profile/accounts',{},function(data){
  //console.log(data);
        //console.log('Current Account :' + globals.account_id);
        drawUsers(data);


    },'json');
});