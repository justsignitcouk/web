<?php

return [

    'my_profile' => 'My Profile',
    'documents' => 'الملفات',
    'connections' => 'Connections',
    'groups' => 'Groups',
    'my_folders' => 'My Folders',
    'sent' => 'Sent',
    'inbox' => 'Inbox',
    'draft' => 'Draft',
    'pending_action' => 'Pending Action',
    'completed' => 'Completed',
    'my_inbox' => 'My Inbox',
    'rejected' => 'Rejected',
    'templates' => 'Templates',
    'labels' => 'Labels',
    'define_layout' => 'Define Layout',
    'manage_templates' => 'Manage Templates',
    'compose_document' => 'Compose Document',
    'settings' => 'الصفحة الرئيسية',
    'connections' => 'جهات الاتصال',
    'setting' => 'الاعدادات',
    'logout' => 'تسجيل الخروج',

];