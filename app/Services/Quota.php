<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/19/17
 * Time: 4:11 PM
 */

namespace App\Services;


use GuzzleHttp\Client;

class Quota extends \App\Services\Services
{

    public static function get() {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('post')
                ->setUrl("quota")
                ->setBody()
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        session()->put("oQuota", serialize($response_array['data']));
        session()->save();
        return $response_array['data'];

    }


}