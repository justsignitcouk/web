
@extends('layouts.admin')

@push('scripts')

    {{--Select--}}
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/form_select2.js') }}"></script>


    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/style.css')}}">
{{--    <script src="{{asset('/assets/components/jquery-ui/external/jquery/jquery.js')}}"></script>--}}
    <script src="{{asset('/assets/components/jquery-ui/jquery-ui.js')}}"></script>
        <script>
            $( function() {
//                $( "#accordion" ).accordion();
                $( "#accordion-styled" ).sortable();
                $( "#accordion-styled" ).disableSelection();
            } );
        </script>
@endpush


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __('workflow.workflow.manage_workflow') }}</h6>
            <div class="row">
                <div class="col-md-12" >
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#add_stage">
                        <i class="icon-plus3 left"></i> {{ __('workflow.stage.add_stage') }}
                    </button>
                </div>
            </div>
        </div>
        <div class="panel-body">
            @if(count($oStages) > 0)
                <div class="panel-group" id="accordion-styled">
                    @foreach($oStages as $k => $oStage)
                        <div class="panel">
                            <div class="panel-heading {{$action_types[$oStage->action_type]}}">
                                <h6 class="panel-title">
                                    <div data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group{{$k}}">
                                        <div class="row">
                                            <div class="col-md-3" >{{ $oStage->name }}</div>
                                            <div class="col-md-4" >{{ $oStage->description }}</div>
                                            <div class="col-md-2" >{{ $oStage->action_type }}</div>
                                            <div class="col-md-3 pull-right" >
                                                {{--<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#edit_stage">--}}
                                                    {{--<i class="fa fa-edit"></i> Edit--}}
                                                {{--</button>--}}
                                                <button type="button" class="btn btn-default btn-sm" id="add_state_button" data-stage-id="{{ $oStage->id }}"  data-toggle="modal" data-target="#add_state">
                                                    <i class="fa fa-edit"></i> {{__('workflow.state.add_state')}}
                                                </button>
                                                <button type="button" class="btn btn-default btn-sm" id="delete_stage_button" data-stage-id="{{ $oStage->id }}" data-toggle="modal" data-target="#modal-default">
                                                    <i class="glyphicon glyphicon-trash left"></i> {{__('common.delete')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </h6>
                            </div>
                            <div id="accordion-styled-group{{$k}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-body">
                                        @if(count($oStage) > 0)
                                            @foreach($oStage->States as $oState)
                                                <div class="row" >
                                                    <div class="col-md-3" >{{$oState->name}}</div>
                                                    <div class="col-md-4" >{{$aStagesInfo[$oState->next_stage_id]}}</div>
                                                    <div class="col-md-1" >&nbsp;</div>
                                                    <div class="col-md-4 pull-right" >
                                                        <button type="button" class="btn btn-default btn-sm" id="delete_state_button" data-state-id="{{ $oState->id }}" data-toggle="modal" data-target="#modal-default">
                                                            <i class="glyphicon glyphicon-star left"></i> {{__('common.requirements')}}
                                                        </button>
                                                        <button type="button" class="btn btn-default btn-sm" id="delete_state_button" data-state-id="{{ $oState->id }}" data-toggle="modal" data-target="#modal-default">
                                                            <i class="glyphicon glyphicon-trash left"></i> {{__('common.delete')}}
                                                        </button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="well well-sm m-a-0">
                    <h3 class="m-a-0 text-center">{{__('common.there_is_no').' '.__('workflow.stage.stages')}}</h3>
                </div>
            @endif
        </div>

        <div class="modal fade" id="add_stage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.stage.add_stage') }}</h4>
                    </div>
                    {!! Form::open(['url'=> url('stage'), 'method'=>'POST', 'id'=>'add_stage_form', 'enctype'=>'multipart/form-data']) !!}
                        <div class="modal-body">
                            {{ csrf_field() }}
                            <fieldset title="2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.stage.stage') .' '. __('common.name') }}: <span class="text-danger">*</span></label><br>
                                                    <input name="stage_name" value="{{old('stage_name')}}" class="required">

                                                        <br/><font color="red" class="validation" id="stage_name_validation" ></font>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.stage.due_date')}}:</label><br>
                                                    <input name="due_date">
                                                    <br/><font color="red" class="validation" id="due_date_validation">{{ $errors->first('due_date') }}</font>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.stage.action_type') }}: <span class="text-danger">*</span></label><br>
                                                    <select name="action_type" class="select-fixed-single">
                                                        <option value="">__select__</option>
                                                        @foreach($action_types as $actionType => $color)
                                                            <option value="{{$actionType}}">{{$actionType}}</option>
                                                        @endforeach
                                                    </select>
                                                        <br/><font color="red" class="validation" id="action_type_validation">{{ $errors->first('action_type') }}</font>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.stage.stage').' '.__('common.description')}}: </label><br>
                                                    <textarea name="description" class="form-control"></textarea>
                                                    <input type="hidden" name="workflow_id" value="{{$oWorkflow->id}}" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                            <button type="button" id="save_stage" class="btn btn-primary">{{__('common.save')}}</button>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade" id="add_state" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.state.add_state') }}</h4>
                    </div>
                    {!! Form::open(['url'=> url('state'), 'method'=>'POST', 'id'=>'add_state_form', 'enctype'=>'multipart/form-data']) !!}
                        <div class="modal-body">
                            {{ csrf_field() }}
                            <fieldset title="2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.state.state') .' '. __('common.name') }}: <span class="text-danger">*</span></label><br>
                                                    <input name="name" value="{{old('name')}}" class="required">
                                                    <br/><font color="red" class="validation" id="name_validation" ></font>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{ __('workflow.state.next_stage') }}: <span class="text-danger">*</span></label><br>
                                                    <select name="next_stage" class="select-fixed-single">
                                                        <option value="">__select__</option>
                                                        @foreach($oStages as $oStage)
                                                            <option value="{{$oStage->id}}">{{$oStage->name}}</option>
                                                        @endforeach
                                                    </select>
                                                        <br/><font color="red" class="validation" id="next_stage_validation" ></font>
                                                    <input type="hidden" name="stage_id" id="stage_id" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                            <button type="button" id="save_state" class="btn btn-primary">{{__('common.save')}}</button>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>



@endsection

@push('scripts')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click", "#save_stage", function (e) {
                var data = $("#add_stage_form").serializeArray(),
                    url = $("#add_stage_form").attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    datatype: 'json',
                }).done(function(data) {
                    if(data.status == false){
                        validation_messages(data);
                    }else{
                        window.location.reload();
                    }
                });

            e.preventDefault();
            });


            $(document).on("click", "#save_state", function (e) {
                var data = $("#add_state_form").serializeArray(),
                    url = $("#add_state_form").attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    datatype: 'json',
                }).done(function(data) {
                    if(data.status == false){
                        validation_messages(data);
                    }else{
                        window.location.reload();
                    }
                });

            e.preventDefault();
            });

            $(document).on("click", "#add_state_button", function (e) {
                $('#stage_id').val( $(this).attr('data-stage-id'));
            });


            $(document).on("click", "#delete_stage_button", function (e) {
                var delete_stage_button = $(this);
                var id = delete_stage_button.attr('data-stage-id');
                $.ajax({
                    url: "{{url('stage')}}/"+id,
                    type: 'DELETE',
                    datatype: 'json',
                }).done(function(data) {
                    if(data.status == false){
                        new PNotify({
                            title: 'Alert',
                            text: data.message,
                            icon: 'icon-warning',
                            addclass: 'bg-danger'
                        });
                    }else {
                        delete_stage_button.closest('.panel').remove();
                        new PNotify({
                            title: 'Success',
                            text: data.message,
                            icon: 'icon-checkmark3',
                            addclass: 'bg-success'
                        });
                    }
                });
                e.preventDefault();
            });

            $(document).on("click", "#delete_state_button", function (e) {
                var delete_state_button = $(this);
                var id = delete_state_button.attr('data-state-id');
                $.ajax({
                    url: "{{url('state')}}/"+id,
                    type: 'DELETE',
                    datatype: 'json',
                }).done(function(data) {
                    if(data.status == false){
                        new PNotify({
                            title: 'Alert',
                            text: data.message,
                            icon: 'icon-warning',
                            addclass: 'bg-danger'
                        });
                    }else {
                        delete_state_button.closest('.row').remove();
                        new PNotify({
                            title: 'Success',
                            text: data.message,
                            icon: 'icon-checkmark3',
                            addclass: 'bg-success'
                        });
                    }
                });
                e.preventDefault();
            });

            function validation_messages(data) {
                $('.validation').html('');
                $.each(data.message, function (index, data2) {

                    var field_name = index + '_validation',
                        messages = data2;
                    var html = '';
                    $.each(messages, function (index3, data3) {
                        if(html != '') {
                            html = html + '<br/>';
                        }
                        html += data3;
                    });
                    $('#'+field_name).html(html);
                });
            }


        </script>
@endpush