
{!! Theme::asset()->themePath()->add([ ['recipient', 'partials/document/recipient.js',['handlebars']]]) !!}


@php

        $sTo = '' ;
        $sSigner = '' ;
        $sCC = '' ;
@endphp
@foreach($aDocument['recipients'] as $aRecipient )
    @php
        $identifier = $aRecipient['identifier'];
    @endphp
    @if($aRecipient['type'] == 'To')
        @php
            $sTo .= $identifier . "," ;
        @endphp
    @elseif($aRecipient['type'] == 'Signer')
        @php
            $sSigner .= $identifier . "," ;
        @endphp
    @elseif($aRecipient['type'] == 'CC')
        @php
            $sCC .= $identifier . "," ;
        @endphp
    @endif
@endforeach
@php
    $sTo = rtrim($sTo,',');
    $sSigner = rtrim($sSigner,',');
    $sCC = rtrim($sCC,',');
@endphp


<div class="col-md-6">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon" data-i18n="compose.to">To</span>
                <input type="text" value="{{ $sTo }}" data-role="materialtags" class="recipient_to-typeahead form-control" id="recipient_to">
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon" data-i18n="compose.cc">CC</span>
                <input type="text" value="{{ $sCC }}" data-role="materialtags" class="recipient_cc-typeahead form-control" id="recipient_cc">
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon" data-i18n="compose.signer">Signer</span>
                <input type="text" value="{{ $sSigner }}" data-role="materialtags" class="recipient_signer-typeahead form-control" id="recipient_signer">
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon" data-i18n="date">Date</span>
                <input type="text" class="form-control daterange-single" name="document_date_input" id="document_date_input" value="{{ $aDocument['document_date'] }}"
                       data-i18n="[placeholder]doc_date"  placeholder="{{ __('document.Document Date') }}" />
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon" data-i18n="title">Title</span>
                <input type="text" class="form-control" name="title" id="title_input" data-i18n="[placeholder]title" placeholder="{{ __("document.Title") }}" value="{{ $aDocument['title'] }}" />
            </div>
        </div>
    </div>
</div>


