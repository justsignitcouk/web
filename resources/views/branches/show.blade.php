@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

				<ul class="breadcrumb">
					<li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
					<li><a href="{{route('admin.companies.show' , $company[0]->id)}}"><i class="position-left"></i>{{ $company[0]->name }}</a></li>
					<li><a href="{{route('admin.branches' , $company[0]->id)}}"><i class="position-left"></i>{{ __("common.branches") }}</a></li>
					<li><a href="{{route('admin.combranches.show' , $company[0]->id)}}"><i class="position-left"></i>{{ $branch['name'] }}</a></li>
					<li class="active">{{ __("common.show") }}</li>
				</ul>

				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">

								@include('layouts/partials/tabs/branch_tabes')

						    	<div class="panel panel-body 
						    	pull-right 
						    	col-md-12">

									<table class="table">

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.name") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($branch->name))
												{{$branch->name}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.email") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($branch->email))
												{{$branch->email}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.license_no") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">

												@if(!empty($branch->license_no))
												{{$branch->license_no}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("branches.branche_type") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($branch->BranchType->name))
												{{$branch->BranchType->name}}
												@else
												  --
												@endif

												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.address") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
											   @if(!empty($branch->address))
												{{$branch->address}}
												@else
												  --
												@endif

												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.country") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($branch->Country->name))
												{{$branch->Country->name}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
											<label class="text-semibold">{{ __("common.pobox") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												 @if(!empty($branch->pobox))
												{{$branch->pobox}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
											<label class="text-semibold">{{ __("common.main") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if($branch->is_main == 1)
												{{ __("common.yes") }}
												@elseif($branch->is_main == 0)
												{{ __("common.no") }}
												@else
												--
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.phone") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($branch->phone))
												{{$branch->phone}}
												@else
												  --
												@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.fax") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">

												@if(!empty($branch->fax))
												{{$branch->fax}}
												@else
												  --
												@endif

												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.created_at") }}:</label>
											</td>
											<td>
											<span class="pull-right-sm">
												<?php

                                                $date = new DateTime($branch['created_at']) ;
                                                echo $date->format('g:ia \o\n l jS F Y');
                                                ?>
											</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.logo") }}:</label>
											</td>
											<td>
												<div class="pull-right-sm">
													<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
												</div>
											</td>
										</tr>

									</table>
							  	</div>
		             	</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endsection
