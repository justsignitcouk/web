<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::define('allowed-permission', function ($string,$permission) {

            //permissions by user ID
            $aPermmisions = DB::table('permission_user')
                ->join('permissions', 'permissions.id', '=', 'permission_user.permission_id')
                ->select('permissions.key')
                ->where('permission_user.user_id',Auth::user()->id)
                ->get()
                ->toArray();

            //permissions by user ID and Role
            $aRolePermmisions = DB::table('role_user')
                ->join('permission_role', 'permission_role.role_id', '=', 'role_user.role_id')
                ->join('permissions','permissions.id','=', 'permission_role.permission_id')
                ->select('permissions.key')
                ->where('role_user.user_id',Auth::user()->id)
                ->get()
                ->toArray();

            foreach ($aPermmisions as $per) {
                if($per->key == $permission){
                    return true ;
                }
            }

            foreach ($aRolePermmisions as $per) {

                if($per->key == $permission){
                    return true ;
                }
            }

            return false ;


        });


    }
}
