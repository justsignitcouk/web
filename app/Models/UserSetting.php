<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSetting
 */
class UserSetting extends Model
{
    protected $table = 'user_settings';

    public $timestamps = false;

    protected $fillable = [
        'setting_id',
        'user_id',
        'value'
    ];

    protected $guarded = [];

        
}