<?php

/********************* document routes*************************/
Route::group(['middleware' => ['MyComposeAdminAuth'],'prefix' => 'admin','as'=>'admin.'], function (){
 
//users module routes
Route::get('users','Admin\UsersController@index')->name('users');
Route::get('users/datatables','Admin\UsersController@datatables')->name('users.data');
Route::get('users/create','Admin\UsersController@create')->name('users.create');
Route::post('users/store','Admin\UsersController@store')->name('users.store');
Route::get('users/{id}/profile','Admin\UsersController@profile')->name('users.profile');
Route::get('users/{id}/edit','Admin\UsersController@edit')->name('users.edit');
Route::post('users/{id}/update','Admin\UsersController@update')->name('users.update');
Route::get('users/{id}/accounts','Admin\UsersController@accounts')->name('users.accounts');
Route::post('users/{id}/{status}/deactive','Admin\UsersController@deactive');

//user roles routes
Route::get('users/{id}/roles','Admin\UsersController@editRoles');
Route::post('users/{id}/roles','Admin\UsersController@updateRoles')->name('users.updateroles');

//user permissions routes
Route::get('users/{id}/permissions','Admin\UsersController@editPermissions');
Route::post('users/{id}/permissions','Admin\UsersController@updatePermissions')->name('users.updatepermissions');

    Route::get('users/{id}/plans','Admin\UsersController@plans');
    Route::post('users/{id}/plans','Admin\UsersController@getPlans')->name('users.plans');
    Route::post('users/{id}/plans/save','Admin\UsersController@savePlanToUser')->name('users.savePlanToUser');


//roles module routes
Route::get('roles', 'Admin\RolesController@index')->name('permissions');
Route::get('roles/datatables', 'Admin\RolesController@datatables')->name('roles.data');
Route::get('roles/create', 'Admin\RolesController@create')->name('roles.create');
Route::post('roles/store','Admin\RolesController@store')->name('roles.store');
Route::get('roles/{id}/edit','Admin\RolesController@edit')->name('roles.edit');
Route::put('roles/{id}/update','Admin\RolesController@update')->name('roles.update');
Route::post('roles/{id}/delete','Admin\RolesController@destroy');
Route::get('roles/{id}','Admin\RolesController@show');

//permissions module routes
Route::get('permissions','Admin\PermissionsController@index')->name('permissions');
Route::get('permissions/datatables', 'Admin\PermissionsController@datatables')->name('permissions.data');

//companies module routes
Route::get('companies','Admin\CompaniesController@index')->name('companies');
Route::get('companies/create', 'Admin\CompaniesController@create')->name('companies.create');
Route::post('companies/store','Admin\CompaniesController@store')->name('companies.store');
Route::get('companies/datatables', 'Admin\CompaniesController@datatables')->name('companies.data');
Route::get('companies/{id}/edit','Admin\CompaniesController@edit')->name('companies.edit');
Route::get('companies/{id}','Admin\CompaniesController@show')->name('companies.show');
Route::post('companies/{id}/update','Admin\CompaniesController@update')->name('companies.update');
Route::post('companies/{id}/delete','Admin\CompaniesController@destroy');

//branches module routes
Route::get('companies/{id}/branches','Admin\BranchesController@index')->name('branches');
Route::get('companies/{id}/createbranche','Admin\BranchesController@create')->name('company.branch');
Route::get('combranches/{id}/edit','Admin\BranchesController@edit')->name('combranches.edit');
Route::post('branches/store','Admin\BranchesController@store')->name('branches.store');
Route::get('combranches/{company_id}/datatables','Admin\BranchesController@datatables')->name('company.branch.datatables');
Route::get('combranches/{id}','Admin\BranchesController@show')->name('combranches.show');
Route::post('branches/{id}/update','Admin\BranchesController@update')->name('branches.update');
Route::post('branches/{id}/delete','Admin\BranchesController@destroy');
Route::post('branches/{company_id}/list','Admin\BranchesController@list');

//departments module routes
Route::get('branches/{id}/departments','Admin\DepartmentsController@index')->name('departments');
Route::get('branches/{id}/createdepartment','Admin\DepartmentsController@create');
Route::post('departments/store','Admin\DepartmentsController@store')->name('departments.store');
Route::get('departments/{branch_id}/datatables','Admin\DepartmentsController@datatables');
Route::get('branchdeps/{id}','Admin\DepartmentsController@show')->name('branchdeps.show');
Route::get('branchdeps/{id}/edit','Admin\DepartmentsController@edit');
Route::post('departments/{id}/update','Admin\DepartmentsController@update')->name('departments.update');
Route::post('departments/{id}/delete','Admin\DepartmentsController@destroy');
Route::post('departments/{branch_id}/list','Admin\DepartmentsController@list');
Route::get('branchdeps/{id}/roles','Admin\DepartmentsController@editRoles');
Route::post('branchdeps/{id}/roles','Admin\DepartmentsController@updateRoles')->name('branchdeps.updateroles');
Route::get('branchdeps/{id}/permissions','Admin\DepartmentsController@editPermissions');
Route::post('branchdeps/{id}/permissions','Admin\DepartmentsController@updatePermissions')->name('branchdeps.updatepermissions');

//accounts module routes
Route::get('accounts/accountsdatatables','Admin\AccountsController@accountsDatatables');
Route::get('users/{id}/accounts','Admin\AccountsController@index')->name('users.accounts');
Route::get('accounts/{user_id}/createaccount','Admin\AccountsController@create');
Route::post('accounts/store','Admin\AccountsController@store')->name('accounts.store');
Route::get('accounts/{user_id}/datatables','Admin\AccountsController@datatables');
Route::get('accounts/{id}/edit','Admin\AccountsController@edit');
Route::get('accounts/{id}','Admin\AccountsController@show');
Route::post('accounts/{id}/update','Admin\AccountsController@update')->name('accounts.update');
Route::get('accounts/{id}/roles','Admin\AccountsController@editRoles');
Route::post('accounts/{id}/roles','Admin\AccountsController@updateRoles')->name('accounts.updateroles');
Route::get('accounts/{id}/permissions','Admin\AccountsController@editPermissions');
Route::post('accounts/{id}/permissions','Admin\AccountsController@updatePermissions')->name('accounts.updatepermissions');
Route::post('accounts/{id}/{status}/delete','Admin\AccountsController@destroy');
Route::get('accounts','Admin\AccountsController@accounts');



    Route::resource('languages', 'Admin\LanguagesController');



//Workflow module routes
    Route::group(['prefix' => 'state','as'=>'state.'], function() {
        // state
        Route::get('datatables','Admin\Workflow\StateController@datatables')->name('data');
        Route::resource('','Admin\Workflow\StateController',[]);
        Route::get('/{id}','Admin\Workflow\StateController@show')->name('show');
        Route::post('/delete/{id}','Admin\Workflow\StateController@destroy')->name('delete');
        Route::post('details_table/{id}','Admin\Workflow\StateController@detailsDtatables')->name('details.data');

    });

    Route::group(['prefix' => '','as'=>'workflow.'], function() {
        // workflow
        Route::resource('workflow',  'Admin\Workflow\WorkflowController', []);

        Route::post('/workflow/delete','Admin\Workflow\WorkflowController@delete')->name('delete_wf');
        Route::get('/datatables','Admin\Workflow\WorkflowController@datatables')->name('data');
        // stage
        Route::resource('stage','Admin\Workflow\StageController', []);
        Route::post('/stage/sortable','Admin\Workflow\StageController@sortable')->name('stage.sortable');
        Route::post('/stage/add_state','Admin\Workflow\StageController@addState')->name('stage.state');
        Route::post('/stage/delete_stage','Admin\Workflow\StageController@deleteStage')->name('delete_stage');
        Route::post('/stage/delete_state','Admin\Workflow\StageController@deleteState')->name('delete_state');


        // requirement
        Route::get('actions/datatable','Admin\Workflow\ActionController@datatables')->name('action.data');
        Route::resource('action','Admin\Workflow\ActionController', []);

        Route::resource('stateActions','Admin\Workflow\StateActionController', []);

    });
});


