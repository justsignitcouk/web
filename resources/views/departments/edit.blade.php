@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

   <div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.edit") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
          		<li><a data-action="collapse"></a></li>
          		<li><a data-action="reload"></a></li>
        </ul>
      </div>
		</div>

   <div class="panel-body">
       <div class="tabbable">

           <ul class="breadcrumb">
               <li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
               <li><a href="{{route('admin.companies.show' , $company[0]->id)}}"><i class="position-left"></i>{{ $company[0]->name }}</a></li>
               <li><a href="{{route('admin.branches',$company[0]->id)}}"><i class="position-left"></i>{{ __("common.branches") }}</a></li>
               <li><a href="{{route('admin.combranches.show',$company[0]->id)}}"><i class="position-left"></i>{{ $branch->name }}</a></li>
               <li><a href="{{route('admin.departments',$branch->id )}}"><i class="position-left"></i>{{ __("common.departments") }}</a></li>
               <li><a href="{{route('admin.branchdeps.show' ,$oDepartment->id)}}"><i class="position-left"></i>{{ $oDepartment->name }}</a></li>
               <li class="active">{{ __("common.edit") }}</li>
           </ul>

            @include('layouts/partials/tabs/department_tabes')

            {!! Form::model($department , ['method' => 'POST', 'url' => route('departments.update',$department->id), 'class' => '', 'files' => true]) !!}

                @include('departments.form')

            {!! Form::close() !!}

        </div>
    </div>
  </div>
</div>

@endsection