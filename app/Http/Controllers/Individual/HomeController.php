<?php

namespace App\Http\Controllers\Individual;

use App\Models\User;
use App\Services\Account;
use Facuz\Theme\Facades\Theme;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $data = [] ;
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['badges'] =  \App\Services\User::badges()['badges']; //['inbox'=>'','pennding'=>''] ; //Account::badges()['badgets'];
        view()->share('bShowMainSidebar', false);
        view()->share('sActiveMenu',  'settings');
        return Theme::view( 'home.index', $this->data );
    }

    public function getInbox()
    {
        $oInbox = [] ; //Account::inbox(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oInbox);
    }


    public function pending()
    {

        $oPending = Account::pendingAction();
        view()->share('bShowMainSidebar', false);
        return view('home.pending')->with(['oPending' => $oPending, 'sActiveMenu' => 'pending']);
    }

    public function getPendingAction()
    {
        $oPending = Account::pendingAction();
        return \GuzzleHttp\json_encode($oPending);
    }


    public function sent()
    {

        $oSent = Account::sent(['account_id' => defaultAccountID()]);

        view()->share('bShowMainSidebar', false);
        return view('home.sent')->with(['oSent' => $oSent, 'sActiveMenu' => 'sent']);
    }

    public function getSent()
    {
        $oSent = Account::sent(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oSent);
    }



    public function compose()
    {
        $client = new \App\Services\Account();
        $response = $client->all();
        $aUsers = [];
        if ($response['response_code'])
            $aUsers = $response['to_whom_addresses'];

        view()->share('bShowMainSidebar', false);
        return view('home.compose', ['aUsers' => $aUsers]);
    }


    public function incomplete()
    {
        $user = User::find(1);


        return view('home.index');
    }


    public function approved()
    {
        $user = User::find(1);

        return view('home.index');
    }


    public function rejected()
    {

        $rejected = Account::rejected();
        view()->share('bShowMainSidebar', false);
        return view('home.rejected')->with(['rejected' => $rejected, 'sActiveMenu' => 'rejected']);
    }

    public function getRejected()
    {
        $rejected = Account::rejected();
        return \GuzzleHttp\json_encode($rejected);
    }



    public function archived()
    {
        //$user = User::find(1);

        return view('home.index');
    }


    public function draft()
    {
        $oDraft = Account::draft(['account_id' => defaultAccountID()]);
        view()->share('bShowMainSidebar', false);
        return view('home.draft')->with(['oDraft' => $oDraft, 'sActiveMenu' => 'draft']);
    }

    public function getDraft()
    {
        $oDraft = Account::draft(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oDraft);
    }



    public function anyData()
    {
        return Datatables::of(User::query())->make(true);
    }
}
