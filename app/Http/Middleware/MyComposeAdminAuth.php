<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

class MyComposeAdminAuth
{

    protected $except = [
        ''
    ];

    public function handle($request, Closure $next)
    {

        updateUser();

        if (session()->has('sToken') && hasRole('Super Admin')) {
            return $next($request);
        }

        throw new AuthenticationException('Unauthenticated.', array('code' => 401));
    }
}
