<?php

namespace App\Http\Controllers\Individual;

use App\Http\Requests\setDefaultAccount;
use App\Services\Account;
use App\Services\Services;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(){

        $oAccounts = Account::get();
        return view('account.index')->with(['oAccounts'=>$oAccounts]);
    }

    public function make_default(setDefaultAccount $request){

        $oMakeDefault = Account::make_default($request['id']);
        $oUpdateDefaultAccount = Account::updateDefaultAccount();

        return  response()
            ->json($oMakeDefault);
    }

//    public function getContactForRecipient(Request $request){
//        $aEmail = [] ;
//        $aContacts = Account::getContactsForRecipient(['q'=>$request->get('q')]);
//        return \GuzzleHttp\json_encode(  $aContacts  ) ;
//    }

}
