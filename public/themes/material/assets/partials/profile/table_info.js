$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });


    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "New password must not match previous");

    validator = $("#change_password").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            password_input: {
                required:true,
                minlength: 8,
                notEqual:"#current_password"
            },
            current_password: {
                required:true,
            },
            repeat_password_input: {
                required:true,
                equalTo: "#password_input"
            }
        },
        messages: {
            required: i18n.t("signup.this_field_is_required"),
            password:i18n.t("signup.enter_password"),
            repeat_password_input:{
                equalTo:'The new password mismatch'
            },
            custom: {
                required: i18n.t("signup.this_field_is_required")
            }
        },
        submitHandler: function(form) {
            return true;
        }
    });



    jQuery.validator.addMethod("usernmae_validation", function(value, element) {

        if(value.indexOf(" ") < 0 && value != ""){
            return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
        }

        return false;
    }, "Invalid username");



    validator = $("#change_username").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            username: {
                required:true,
                minlength: 4,
                "remote":
                    {
                        url: "/" + globals.unique_name + '/checkAvailable',
                        type: "post"
                    },
                usernmae_validation:true
            },

        },
        messages: {
            required: i18n.t("signup.this_field_is_required"),
            username: {
                remote: "Username taken"
            }
        },
        submitHandler: function(form) {
            var username = $("#username_input").val() ;

            $.post( "/" + globals.unique_name + "/profile/change_username",{'username' : username},function(data){
                new PNotify({
                    title: 'saved.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
            },'json')
        }
    });



    validator = $("#change_reference").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            reference_input: {
                required:true,

            },

        },
        messages: {
            required: i18n.t("signup.this_field_is_required"),
        },
        submitHandler: function(form) {
            var reference_input = $("#reference_input").val() ;

            $.post( "/" + globals.unique_name + "/profile/reference_document",{'reference_input' : reference_input},function(data){
                new PNotify({
                    title: 'saved.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
            },'json')
        }
    });










    $('.edit-primary-name').on('click',function(e){
        if($('.edit_name_table').hasClass('hide')){
            $('.edit_name_table').removeClass("hide");
        }else{
            $('.edit_name_table').addClass("hide");
        }
        e.preventDefault();
    });

    $('.edit-primary-email').on('click',function(e){
        if($('.edit_email_table').hasClass('hide')){
            $('.edit_email_table').removeClass("hide");
        }else{
            $('.edit_email_table').addClass("hide");
        }

        e.preventDefault();
    });

    $('.edit-primary-mobile').on('click',function(e){
        if($('.edit_mobile_table').hasClass('hide')){
            $('.edit_mobile_table').removeClass("hide");
        }else{
            $('.edit_mobile_table').addClass("hide");
        }
        e.preventDefault();
    });

    $('.cancel_primary').on('click',function(e){
        $(this).closest('.edit_box').addClass('hide');
        e.preventDefault();
    }) ;

    $('.save_primary').on('click',function(e){
        var type = $(this).data('type');
        var element = $(this).closest('.edit_box').find('input[name=primary_'+ type +']:checked');
        var value = element.data('value');

        $.post( updatePrimary ,{'type':type,'value':value }, function(data){
            //location.reload();
            console.log(data);
        },'json');

        e.preventDefault();
    });


});