@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

   <div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

   <div class="panel-body">
			<div class="tabbable">

        @include('layouts/partials/tabs/roles_tabes')

        {!! Form::model($role , ['method' => 'PUT', 'url' => route('roles.update',$role['id']), 'class' => 'submit-roles', 'files' => true]) !!}

            @include('roles.form')
            
        {!! Form::close() !!}

        </div>
      </div>
    </div>
</div>

@endsection