<?php

namespace App\Http\Controllers\Individual\Documents;

use App\Http\Controllers\Individual\Controller;
use App\Services\Document;
use App\Services\Services;
use App\Services\Templates;
use Illuminate\Http\Request;
use PhpParser\Comment\Doc;

class TemplatesController extends Controller
{
    public function templates()
    {

        view()->share('bShowMainSidebar', false);
        return view('home.templates')->with(['sActiveMenu' => 'templates']);
    }

    public function getTemplates()
    {
        $oTemplates = Templates::get();
        return \GuzzleHttp\json_encode($oTemplates);
    }


    public function chooseTemplate(){
        $oTemplates = Templates::get();
        return Theme()->view('document.choose_template', ['oTemplates'=>$oTemplates]);
    }

    public function deleteTemplate(Request $request){
        $oTemplates = Templates::delete($request->all());
        return \GuzzleHttp\json_encode($oTemplates);
    }

}
