<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentAction
 */
class DocumentAction extends Model
{
    protected $table = 'document_action';

    public $timestamps = true;

    protected $fillable = [
        'notes',
        'document_id',
        'account_id',
        'action_id'
    ];

    protected $guarded = [];

        
}