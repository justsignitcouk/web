<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plans\Plan;
use App\Models\Plans\UserPlan;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Requests\Admin\UpdateUserRole;
use App\Http\Requests\Admin\UpdateUserPermissions;
use App\Models\Permission;
use App\Models\Module;
use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\PermissionUser;
use App\Services\UsersService;
use Yajra\Datatables\Datatables;
use Redirect;
use DB;
use Gate;

class UsersController extends Controller
{

    /**
     * @var user
     */
    protected $user;
    /**
     * @var user
     */
    protected $usersService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, UsersService $usersService)
    {
//        $this->middleware('auth');
       $this->user   = $user;
       $this->usersService   = $usersService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');

            return view('users.index');



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            //all permissions in database
            $all_permissions = Permission::all();
            //roles in database
            $all_roles = Role::all();

            return view('users.create', compact('all_permissions', 'all_roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $userRequest)
    {


            if ($this->user->storeUser($userRequest)) {
                return redirect('/admin/users');
            }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

            //all permissions in database
            $all_permissions = Permission::all();
            //all roles in database
            $all_roles = Role::all();
            //user permissions
            $user_permissions = PermissionUser::where('user_id', '=', $id)->get()->toArray();
            $aUserPermission = [];
            foreach ($user_permissions as $user_permission) {
                array_push($aUserPermission, $user_permission['permission_id']);
            }
            $user_permissions = $aUserPermission;
            //user roles
            $user_roles = RoleUser::where('user_id', '=', $id)->get()->toArray();
            $aUserRole = [];
            foreach ($user_roles as $user_role) {
                array_push($aUserRole, $user_role['role_id']);
            }
            $user_roles = $aUserRole;

            //user data
            $oUser = User::find($id);
            $user = $oUser->toArray();
            //to active selected tag
            $active_edit = 1;

            $sImageProfileName = $oUser->ImageProfile ? $oUser->ImageProfile->hash_name : '';
            $sImageProfilePath = $oUser->ImageProfile ? $oUser->ImageProfile->path : '';

            view()->share('sImageProfileName', $sImageProfileName);
            view()->share('sImageProfilePath', $sImageProfilePath);
            return view('users.edit', compact('all_roles', 'all_permissions', 'user_roles', 'user_permissions', 'user', 'active_edit'));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $userRequest, $id)
    {
            if ($this->user->updateUser($userRequest, $id)) {
                return redirect('/admin/users');
            }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function deactive($id, $status)
    {


            $user = User::find($id);
            $user->activated = $status;
            $user->save();

    }

    /**
     * User details
     */
    public function profile($id)
    {

        $oUser = User::find($id);
        $user = $oUser->toArray();
        $sImageProfileName = $oUser->ImageProfile ? $oUser->ImageProfile->hash_name : '';
        $sImageProfilePath = $oUser->ImageProfile ? $oUser->ImageProfile->path : '';
        $active_profile = 1;

        view()->share('sImageProfileName', $sImageProfileName);
        view()->share('sImageProfilePath', $sImageProfilePath);

        $roles = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('roles.name')
            ->where('role_user.user_id', '=', $id)
            ->get()
            ->toArray();


        return view('users.profile', compact('user',
            'active_profile',
            'roles'));


    }

    /**
     * Index list for ajax data table
     */
    public function datatables()
    {

        $aCompanies = User::all()->toArray();
        return Datatables::of($aCompanies)->make(true);

    }

    /**
     * User roles dislay and edit
     */
    public function editRoles($id)
    {

        $user = User::find($id);
        $checked_user_roles = RoleUser::select('role_id')->where('user_id', $id)->get()->toArray();
        $user_roles = Role::where('type_id', 1)->get();
        $active_roles = 1;

        return view('users.roles', compact('user',
            'active_roles',
            'roles',
            'user_roles',
            'checked_user_roles'
        ));


    }

    /**
     * User roles update
     */
    public function updateRoles(UpdateUserRole $updateUserRole, $id)
    {
        if ($this->user->updateRoles($updateUserRole, $id)) {
            return redirect('/admin/users/' . $id . '/roles');
        }
        return redirect('/users/' . $id . '/roles');
    }

    /**
     * User permissions dislay and edit
     */
    public function editPermissions($id)
    {


        $user = User::find($id);
        $active_permissions = 1;

        $checked_user_permissions = $this->usersService
            ->checkedUserPermissions($id);

        //users module permissions
        $users_permissions = $this->usersService
            ->Permissions('users');

        //companies module permissions
        $companies_permissions = $this->usersService
            ->Permissions('companies');

        //accounts module permissions
        $accounts_permissions = $this->usersService
            ->Permissions('accounts');

        //permissions module permissions
        $permissions_permissions = $this->usersService
            ->Permissions('permissions');
        //branches module permissions
        $branches_permissions = $this->usersService
            ->Permissions('branches');

        //roles module permissions
        $roles_permissions = $this->usersService
            ->Permissions('roles');

        //roles module permissions
        $departments_permissions = $this->usersService
            ->Permissions('departments');
        //workflow module permissions
        $workflow_permissions = $this->usersService
            ->Permissions('workflow');
        //requirements module permissions
        $requirements_permissions = $this->usersService
            ->Permissions('requirements');


        return view('users.permissions', compact('user',
            'users_permissions',
            'active_permissions',
            'checked_user_permissions',
            'companies_permissions',
            'accounts_permissions',
            'permissions_permissions',
            'branches_permissions',
            'roles_permissions',
            'departments_permissions',
            'workflow_permissions',
            'requirements_permissions'
        ));


    }


    public function plans($id){
        $user = User::find($id);
        $oPlans = Plan::pluck('title','id')->all();
        //$oUserPlan =
        $active_plans = 1 ;
        return view('users.plans', compact('user',
            'active_plans',
        'id',
        'oPlans'
        ));

    }

    public function savePlanToUser($id,Request $request){
        $plan_id = $request->get('plans');
        $user_id = $id;
        $checkPlanForUser = UserPlan::where("plan_id",$plan_id)->Where("user_id",$user_id)->get();
        if($checkPlanForUser->count() > 0){
            foreach($checkPlanForUser as $checkPlan) {


                if( $checkPlan->expired_at > date('Y-m-d H:i:s') ) {
                    session()->flash("warning_message","This user has this plan");
                    return redirect(route('admin.users.plans',[$id])) ;
                }
            }
        }
        $oUserPlan = new UserPlan();
        $oUserPlan->user_id = $user_id;
        $oUserPlan->plan_id = $plan_id;
        $oUserPlan->start_at = date('Y-m-d H:i:s');
        $oUserPlan->expired_at = (date('Y')+1). date('-m-d H:i:s');
        $oUserPlan->save();
        session()->flash("success_message","The plan added to user");
        return redirect(route('admin.users.plans',[$id])) ;
    }

    public function getPlans($id){

        $aUserPlans = UserPlan::with(['Plan'])->where('user_id',$id)->get()->toArray();

        return \Yajra\DataTables\Facades\DataTables::of($aUserPlans)->make(true);
    }
    /**
     * User permissions update
     */
    public function updatePermissions(UpdateUserPermissions $updateUserPermissions, $id)
    {

        if ($this->user->updatePermissions($updateUserPermissions, $id)) {
            return redirect('/users/' . $id . '/permissions');
        }

    }


}
