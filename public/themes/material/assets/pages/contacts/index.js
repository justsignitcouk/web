$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });



    $(document).on('click', '#add_group', function (e) {
        $(this).prop("disabled", "disabled");
        var name = $('#group_name_modal').val();
        $.post('/' + globals.unique_name + '/contacts/addgroup', {'name': name}, function (data) {
            $('#add_group').removeAttr("disabled");
            location.reload();
            console.log(data);
        });

    });


    $('#loading').on('click', function () {
        var light_3 = $(this).parent();
        $(light_3).block({
            message: '<i class="icon-spinner3 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        window.setTimeout(function () {
            $(light_3).unblock();
        }, 3000);
    });

    var columns = [
        {data: 'display_name', name: 'display_name', className: "","defaultContent": "<i>Not set</i>"
            // ,render: function (data,cols,row) {
            //
            //     if(row.register==true){
            //         return data + " <small>( Registred )</small>" ;
            //     }
            //
            //     return data;
            // }
        },
        {data: 'email', name: 'email', className: "","defaultContent": "<i>Not set</i>"},
        {data: 'mobile', name: 'mobile', className: "","defaultContent": "<i>Not set</i>"},
        {data: 'groupname', name: 'groupname', className: ""/* ,render: function (data) {return ' '} */ },
        {
            data: 'id', name: 'action', className: "time-column","width": '110px', render: function (data) {

                return '<div class="details_float_container"><div class="details_float">'+
                    '<a class="no-smoothState link-icon  edit-contact edit-button tooltip-tools" data-id="' + data + '" data-popup="tooltip" title="Edit" data-placement="bottom" data-original-title="Edit">' +
                    '<i   class="glyphicon  glyphicon-edit" data-id="' + data + '"></i></a>  ' +
                    '<a class="no-smoothState link-icon details-document delete-contact tooltip-tools" data-id="' + data + '" data-popup="tooltip" title="Delete" data-placement="bottom" data-original-title="Delete">' +
                    '<i   class="icon-details icon-trash" data-id="' + data + '"></i></a>  ' +
                    '</div></div>'
        }
        }


    ];



    var groupcolumns = [
        {data: 'name', name: 'name', className: ""},
        {data: 'id', name: 'id', className: "", render: function (data){return ""}} ,
        {data: 'id', name: 'id', className: "", render: function (data){return ""}} ,
        {
            data: 'id', name: 'action', className: "", render: function (data) {
            return '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'+
                '<i class="icon-menu9"></i> </a><ul class="dropdown-menu dropdown-menu-right">' +
                '<li><a  class="edit-group edit-button" data-id="' + data + '"><i></i> Edit</a></li> ' +
                '<li><a  class="delete-group" data-id="' + data + '"><i></i> Delete</a></li> ' +
                '</ul> ' +
                '</li> ' +
                '</ul> '
        }
        }


    ];

    $.extend($.fn.dataTable.defaults, {

        columnDefs: [{
            orderable: false,
            /*width: '100px',*/
            targets: [3],
            checkboxes: {
                seletRow: true
            }
        }],
        dom: '<"datatable-header"fl ><""t><"datatable-footer"ip>',
        language: {
            search: '<span>' + /*lang.search*/ 'search' + '</span> _INPUT_',
            searchPlaceholder: '',
            lengthMenu: '<span data-i18n="show">Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'},
            loadingRecords: "<i class=\"icon-spinner3 spinner\"></i>",

        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },


    });
    var t = $('#contact_table').dataTable({
        "ordering": false,
        "bProcessing": true,
        "serverSide": false,
        'autoWidth': true,
        "pageLength": 50,
        "ajax": {
            "url": '',
            "type": 'POST',
            "data": ''
        },
        columns: columns,
        "createdRow": function (row, data, index) {

            $(row).data("id", data.id);
        }

    });

    var t2 = $('#group_table').dataTable({
        "ordering": false,
        "bProcessing": true,
        'autoWidth': true,
        "ajax": {
            "url": '/' + globals.unique_name + '/contacts/groups',
            "type": 'GET',
            "data": ''
        },
        columns: groupcolumns,
        "createdRow": function (row, data, index) {

            $(row).data("id", data.id);

        }
    });

    var url = document.location.toString();
    if (url.match('#')) {
        $('#navbar-mixed a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    $('#navbar-mixed a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    });


    $('.nav-tabs a').on('shown.bs.tab', function (event) {
        var type = $(this).data('type');
        if (!$.fn.DataTable.isDataTable('#' + type + '_table')) {
            var t = $('#' + type + '_table').dataTable({
                "ordering": false,

                "bProcessing": true,
                'autoWidth': true,
                "ajax": {
                    "url": '',
                    "type": 'POST',
                    "data": {'filter': type}
                },
                columns: columns,
                "createdRow": function (row, data, index) {
                    $(row).data("id", data.id);

                    /*if (data.is_read == false) {
                     $(row).addClass('unread');
                     }*/
                }


            });
        } else {
            $('#' + type + '_table').dataTable().fnDraw();
        }

    });
    segmentedGauge("#segmented_gauge", 200, 0, 100, 5);
    $('.table-contact').on('click', 'tr td', function () {
        $this = $(this);
        /*if(!$this.hasClass('details-button')) {
         var id = $this.parent().data('id') ;
         /!*    window.location = "/document/" + id ;*!/

         }*/

    });

    // Setup chart
    function segmentedGauge(element, size, min, max, sliceQty) {

        // Main variables
        var d3Container = d3.select(element),
            width = size,
            height = (size / 2) + 20,
            radius = (size / 2),
            ringInset = 15,
            ringWidth = 20,

            pointerWidth = 10,
            pointerTailLength = 5,
            pointerHeadLengthPercent = 0.75,

            minValue = min,
            maxValue = max,

            minAngle = -90,
            maxAngle = 90,

            slices = sliceQty,
            range = maxAngle - minAngle,
            pointerHeadLength = Math.round(radius * pointerHeadLengthPercent);

        // Colors
        var colors = d3.scale.linear()
            .domain([0, slices - 1])
            .interpolate(d3.interpolateHsl)
            .range(['#66BB6A', '#EF5350']);


        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
            .attr('width', width)
            .attr('height', height);


        // Construct chart layout
        // ------------------------------

        // Donut
        var arc = d3.svg.arc()
            .innerRadius(radius - ringWidth - ringInset)
            .outerRadius(radius - ringInset)
            .startAngle(function (d, i) {
                var ratio = d * i;
                return deg2rad(minAngle + (ratio * range));
            })
            .endAngle(function (d, i) {
                var ratio = d * (i + 1);
                return deg2rad(minAngle + (ratio * range));
            });

        // Linear scale that maps domain values to a percent from 0..1
        var scale = d3.scale.linear()
            .range([0, 1])
            .domain([minValue, maxValue]);

        // Ticks
        var ticks = scale.ticks(slices);
        var tickData = d3.range(slices)
            .map(function () {
                return 1 / slices;
            });

        // Calculate angles
        function deg2rad(deg) {
            return deg * Math.PI / 180;
        }

        // Calculate rotation angle
        function newAngle(d) {
            var ratio = scale(d);
            var newAngle = minAngle + (ratio * range);
            return newAngle;
        }


        // Append chart elements
        // ------------------------------

        //
        // Append arc
        //

        // Wrap paths in separate group
        var arcs = svg.append('g')
            .attr('transform', "translate(" + radius + "," + radius + ")")
            .style({
                'stroke': '#fff',
                'stroke-width': 2,
                'shape-rendering': 'crispEdges'
            });

        // Add paths
        arcs.selectAll('path')
            .data(tickData)
            .enter()
            .append('path')
            .attr('fill', function (d, i) {
                return colors(i);
            })
            .attr('d', arc);



        // Wrap text in separate group
        var arcLabels = svg.append('g')
            .attr('transform', "translate(" + radius + "," + radius + ")");

        // Add text
        arcLabels.selectAll('text')
            .data(ticks)
            .enter()
            .append('text')
            .attr('transform', function (d) {
                var ratio = scale(d);
                var newAngle = minAngle + (ratio * range);
                return 'rotate(' + newAngle + ') translate(0,' + (10 - radius) + ')';
            })
            .style({
                'text-anchor': 'middle',
                'font-size': 11,
                'fill': '#999'
            })
            .text(function (d) {
                return d + "%";
            });


        //
        // Pointer
        //

        // Line data
        var lineData = [
            [pointerWidth / 2, 0],
            [0, -pointerHeadLength],
            [-(pointerWidth / 2), 0],
            [0, pointerTailLength],
            [pointerWidth / 2, 0]
        ];

        // Create line
        var pointerLine = d3.svg.line()
            .interpolate('monotone');

        // Wrap all lines in separate group
        var pointerGroup = svg
            .append('g')
            .data([lineData])
            .attr('transform', "translate(" + radius + "," + radius + ")");

        // Paths
        pointer = pointerGroup
            .append('path')
            .attr('d', pointerLine)
            .attr('transform', 'rotate(' + minAngle + ')');


        // Random update
        // ------------------------------

        // Update values
        function update() {
            var ratio = scale(Math.random() * max);
            var newAngle = minAngle + (ratio * range);
            pointer.transition()
                .duration(2500)
                .ease('elastic')
                .attr('transform', 'rotate(' + newAngle + ')');
        }

        update();

        // Update values every 5 seconds
        setInterval(function () {
            update();
        }, 5000);
    }

    //edit contact modal function

    $(document).on('click', '.edit-button', function (e) {

        var id = $(this).data('id');
        $.post('contacts/getcontact', {'id': id}, function (data) {

            if(data.aContact.emails != null ) {
                $('#email_edit_modal').val(data.aContact.emails.email);
            }

            $('#group_input_edit').val( data.aContact.groups );

            $('#name_edit_modal').val(data.aContact.display_name);

            if(data.aContact.mobiles != null ) {
            $('#mobile_edit_modal').val(data.aContact.mobiles.mobile);
            }

            $('#id_edit_modal').val(id);
            $('#delete_contact').data('id',id);

            $('#modal_edit').modal('show');
            renderGroupTag('#group_input_edit');


        }, 'json');

        e.preventDefault();
    });

    //save button on edit modal function
    $(document).on('click', '#edit_contact', function () {
        $(this).prop("disabled", "disabled");
        var email = $('#email_edit_modal').val();
        var name = $('#name_edit_modal').val();
        var mobile = $('#mobile_edit_modal').val();
        var groups = $('#group_input_edit').val();
        var id = $('#id_edit_modal').val();

        $.post('contacts/edit', {'id': id, 'email': email, 'name': name, 'mobile': mobile,'groups':groups}, function (data) {
            $('#edit_contact').removeAttr("disabled");
            location.reload();
            console.log(data);
        });

    });

    //delete button on edit modal function

    //--------------------

    //edit group modal function
    $(document).on('click', '.edit-group', function () {

        var id = $(this).data('id');
        $.post('contacts/getgroup', {'id': id}, function (data) {
            $('#group_name_edit_modal').val(data.name);
            $('#group_id_edit_modal').val(id);
            $('#group_input_edit').val(id);
            $('#delete_group').data('id',id);
            $('#modal_group_edit').modal('show');
        }, 'json')

    });

    //save button on edit group modal function
    $(document).on('click', '#edit_group', function () {
        $(this).prop("disabled", "disabled");
        var name = $('#group_name_edit_modal').val();
        var id = $('#group_id_edit_modal').val();

        $.post('contacts/editgroup', {'id': id, 'name': name}, function (data) {
            $('#edit_contact').removeAttr("disabled");
            location.reload();
            console.log(data);
        });

    });

    //delete button on edit modal function
    $(document).on('click', '.delete-group', function () {
        $(this).prop("disabled", "disabled");
        var id = $(this).data('id');
        var conf = confirm('Are you sure you want to delete this record?');
        if(conf) {
            $.post('contacts/deletegroup', {'id': id}, function (data) {
                $('#delete_contact').removeAttr("disabled");
                location.reload();
                console.log(data);
            });
        }

    });
    //------------------

    $(document).on('click','.import_contact',function(){
        $(this).addClass("disabled");
    });


    validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            email_modal: {
                required:true,
                email:true
            },
            mobile_modal: {
                required:true,
                minlength: 3
            },
            name_modal: {
                required:true,
                minlength: 3
            }
        },
        messages: {
            required: i18n.t("signup.this_field_is_required"),
            password:i18n.t("signup.enter_password"),
            repeat_password:i18n.t("signup.enter_repeat_password"),
            custom: {
                required: i18n.t("signup.this_field_is_required")
            }
        },
        submitHandler: function(form) {


            $('#add_contact').prop("disabled", "disabled");
            var email = $('#email_modal').val();
            var name = $('#name_modal').val();
            var mobile = $('#mobile_modal').val();
            var groups = $('#group_input_add').val();

            $.post( '/' + globals.unique_name + '/contacts/add', {'email': email, 'name': name, 'mobile': mobile,'groups':groups}, function (data) {
                $('#add_contact').removeAttr("disabled");
                location.reload();
                console.log(data);
            });


        }
    });


    $(document).on('click', '#add_contact', function (e) {

        $('#add_contact_form').submit();
        // $(this).prop("disabled", "disabled");
        // var email = $('#email_modal').val();
        // var name = $('#name_modal').val();
        // var mobile = $('#mobile_modal').val();
        // $.post( '/' + globals.unique_name + '/contacts/add', {'email': email, 'name': name, 'mobile': mobile}, function (data) {
        //     $('#add_contact').removeAttr("disabled");
        //     location.reload();
        //     console.log(data);
        // });

    });


    $(document).on('click','.delete-contact',function(e){
        var id = $(this).data('id');
        var button_this = $(this);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this contact!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(isConfirm){
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
            if (isConfirm){
                $.post('contacts/delete', {'id':id},function(data){

                    swal({
                        title: "Deleted!",
                        text: "Your row has been deleted.",
                        type: "success",
                        timer: 3000
                    });
                    //$('#contact_table').dataTable().fnDraw();
                    button_this.closest("tr").hide();
                },'json');

            } else {

            }
        });
e.preventDefault();
    });



    /********** tags input **************/


    $('#modal_default').on('shown.bs.modal', function (e) {
       renderGroupTag('#group_input_add');

    })

function renderGroupTag(id_input){
    //
    // Typeahead implementation
    //

    // Matcher
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {

                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                }
            });
            cb(matches);
        };
    };

    // Data


    // Attach typeahead
    $(id_input).tagsinput('input').typeahead(
        {
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'sGroups',
            displayKey: 'value',
            source: substringMatcher(sGroups)
        }
    ).bind('typeahead:selected', $.proxy(function (obj, datum) {
        this.tagsinput('add', datum.value);
        this.tagsinput('input').typeahead('val', '');
    }, $('.tagsinput-typeahead')));

}

    $('#modal_edit').on('hidden.bs.modal', function () {
        $('#group_input_edit').tagsinput('destroy');
    })



});
