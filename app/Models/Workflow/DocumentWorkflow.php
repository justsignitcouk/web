<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class DocumentWorkflow extends Model
{
    protected $table = 'wf_document_workflow';

    public $timestamps = true;

//    protected $fillable = [];

    protected $guarded = [];

    public function Workflow()
    {
        return $this->belongsTo('App\Models\Workflow\Workflow');
    }

    public function Document()
    {
        return $this->belongsTo('App\Document');
    }
}
