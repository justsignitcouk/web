<?php

namespace App\Http\Controllers\Individual;

use App\Services\Contact;
use App\Services\SocialMedia;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

use App\Services\User;
use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Facades\Socialite;

class ContactsController extends Controller
{
    public function index(){

        $oGroups = User::groups();
        $sGroups= '' ;
        foreach($oGroups as $oGroup){
            $sGroups .= "'" . $oGroup['name'] . "'" . ",";
        }
        $sGroups = trim($sGroups,',');
        view()->share('bShowMainSidebar', false);

        view()->share('sActiveMenu',  'inbox');
        return Theme::view( 'contacts.index', ['sGroups'=>$sGroups]);

    }

    public function getContacts(){
        $oContacts = User::contacts();

        return DataTables::of($oContacts)->make(true);
    }
    public function getContact(Request $request){
        $oContacts = User::getContact($request->all());
        return \GuzzleHttp\json_encode($oContacts);
    }

    public function add(Request $request){
        $data = $request->all();
        $oContacts = User::addContact($data) ;
        session()->flash('success', 'success');
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }

    public function edit(Request $request){
        $data = $request->all();
        $oContacts = User::editContact($data) ;
        //session()->flash('send_request', 'You send request to ' . $data['email'] );
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }


    public function import(Request $request){

        if ($request->has('code')) {

                $user = \Socialite::driver('google')->redirectUrl(route('contacts.import'))->user();

        }else {
            $token = session()->get('google_token');
            if (empty($token)) {

                return \Socialite::driver('google')->scopes(['openid', 'profile', 'email', \Google_Service_People::CONTACTS_READONLY])->with(['auth_type' => 'rerequest'])->redirectUrl(route('contacts.import'))->redirect();

            }
        }


        $google_client_token = [
            'access_token' => $user->token,
            'refresh_token' => $user->refreshToken,
            'expires_in' => $user->expiresIn
        ];

        $client = new \Google_Client();
        $client->setApplicationName("Laravel");
        $client->setDeveloperKey(env('GOOGLE_SERVER_KEY'));
        $client->setAccessToken(json_encode($google_client_token));

        $service = new \Google_Service_People($client);
        $optParams = array('requestMask.includeField' => 'person.phone_numbers,person.names,person.email_addresses','pageSize' => 2000);
        $results = $service->people_connections->listPeopleConnections('people/me',$optParams);


        $connections = $results->connections;
        $aContacts = [] ;
        foreach($connections as $connection){
            $aPeople = [
                'resource_name' => $connection->resourceName,
                'names' => [],
                'emails' => [],
                'mobiles' => [],

            ];

            if(is_array($connection->names) && sizeof($connection->names)>0) {
                foreach($connection->names as $name)
                    $aPeople['names'][] = [
                                                'display_name'=> $name->displayName,
                                                'last_name'=> $name->familyName,
                                                'first_name'=> $name->givenName,
                                                'middle_name'=> $name->middleName ] ;
            }

            if(is_array($connection->emailAddresses) && sizeof($connection->emailAddresses)>0) {
                foreach($connection->emailAddresses as $emailAddress)
                    $aPeople['emails'][] = ['value'=>$emailAddress->value,'type'=>$emailAddress->type] ;
            }

            if(is_array($connection->phoneNumbers) && sizeof($connection->phoneNumbers)>0) {
                foreach($connection->phoneNumbers as $phoneNumber)
                    $aPeople['mobiles'][] = ['value'=> $phoneNumber->value,'type'=> $phoneNumber->type] ;
            }

            $aContacts[] = $aPeople ;

        }

        $aResponses = [];
        for($i=0; $i< sizeof($aContacts); $i+=100){

            $aPartContacts = array_slice($aContacts, $i, 100);

            $aResponses[] = Contact::saveContacts($aPartContacts);
        }

        return redirect()->to(route('profile.contacts.index',[getUniqueName()])) ;


    }
    public function delete(Request $request){
        $data = $request->all();
        $oContacts = User::deleteContact($data) ;
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }

    public function getGroups(){
        $oGroups = User::groups();
        return DataTables::of($oGroups)->make(true);
    }

    public function getGroup(Request $request){
        $oGroups = User::getGroup($request->all());
        return \GuzzleHttp\json_encode($oGroups);
    }

    public function addGroup(Request $request){
        $data = $request->all();
        $oGroups = User::addGroup($data);
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }

    public function editGroup(Request $request){
        $data = $request->all();
        $oGroups = User::editGroup($data) ;
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }

    public function deleteGroup(Request $request){
        $data = $request->all();
        $oGroup = User::deleteGroup($data) ;
        return \GuzzleHttp\json_encode(['status'=>'success']);
    }

}
