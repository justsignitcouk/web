{!! Theme::asset()->add([ ['signature_pad', 'assets/components/signature_pad/dist/signature_pad.js', ['editable']]]) !!}
{!! Theme::asset()->add([ ['signature_pad_app', 'assets/components/signature_pad/example/js/app.js', ['signature_pad']]]) !!}

{{--{!! Theme::asset()->add([ ['app', 'js/app.js', ['signature_pad']]]) !!}--}}
{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js', ['app']]]) !!}
{!! Theme::asset()->themePath()->add([ ['switchery', 'js/plugins/forms/styling/switchery.min.js', ['plupload_full']]]) !!}
{!! Theme::asset()->themePath()->add([ ['switch', 'js/plugins/forms/styling/switch.min.js', ['switchery']]]) !!}
{!! Theme::asset()->themePath()->add([ ['spectrum', 'js/plugins/pickers/color/spectrum.js', ['switch']]]) !!}
{!! Theme::asset()->themePath()->add([ ['editable', 'js/plugins/forms/editable/editable.min.js', ['anytime']]]) !!}
{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js', ['editable']]]) !!}
{!! Theme::asset()->themePath()->add([ ['fancybox', 'js/plugins/media/fancybox.min.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js', ['fancybox']]]) !!}
{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js', ['uniform']]]) !!}
{!! Theme::asset()->themePath()->add([ ['gallery_library', 'js/pages/gallery_library.js', ['datatables']]]) !!}


<link href="{{  asset('assets/components/signature_pad/example/css/signature-pad.css') }}" rel="stylesheet type="
      text/css">
<link href="{{  asset('theme/default/assets/css/colors.css') }}" rel="stylesheet type=" text/css">

<style>
    .fp {
        opacity: 0.65;
        margin-top: 50px;
        margin-left: auto;
        margin-right: auto;
        width: 100px;
        height: 150px;
        background-color: #C51D31;
        border-radius: 60%/ 80%;
        background-image: repeating-radial-gradient(circle 4px at 25px 40px, rgba(165, 114, 87, 0.8), transparent),
        radial-gradient(circle at top, #FFDFC4, #F0D5BE, #FFDCB2);

    }

</style>

{!! Theme::asset()->themePath()->add([ ['profile_index', 'pages/profile/index.js',['app']]]) !!}

<!-- Main content -->
<div class="content-wrapper">


    <!-- Cover area -->
    <div class="profile-cover">
        <div class="profile-cover-img" style="background-image: url(/theme/default/assets/images/cover4.jpg)"></div>
        <div class="media">
            <div class="media-left" style="padding-right: 0">
                <div class="profile_pic">
                    <a href="#" class="profile-thumb">
                        <img src="{{currentAccount()->profile_image['url'] . "/files/" . $aAccount['profile_image']['path'] . '/' . $aAccount['profile_image']['hash_name']}}"
                             class=" " alt="" style="width: 100px!important;height: 100px!important;display: block">
                    </a>

                </div>
            </div>

            <div class="media-body" style="padding-left: 20px">
                <h1>  {{ $aAccount['user']['first_name'] }}  {{ $aAccount['user']['last_name'] }}
                    <small class="display-block"></small>
                </h1>
            </div>

            <div class="media-right media-middle">
                <ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
                </ul>
            </div>
        </div>
    </div>
    <!-- /cover area -->

    <!-- Toolbar -->
    <div class="navbar navbar-default navbar-xs content-group">
        <ul class="nav navbar-nav visible-xs-block">
            <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i
                            class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-filter">
            <ul class="nav navbar-nav">
                <li class=""><a href="{{route('settings.accounts')}}"><i class="icon-arrow-left52 position-left"></i></a></li>
                <li class="active"><a href="#info" data-toggle="tab"><i
                                class="icon-menu7 position-left"></i> <span data-i18n="profile_nav.info">{{__('profile.info')}}</span></a></li>

            </ul>


        </div>
    </div>
    <!-- /toolbar -->


    <!-- Content area -->
    <div class="content">

        <!-- User profile -->
        <div class="row">
            <div class="col-lg-12">
                <div class="tabbable">
                    <div class="tab-content">


                        <!-- Basic modal -->
                        <div id="modal_default" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title"><span data-i18n="Signatures.main">{{__('profile.signatures')}}</span></h5>
                                    </div>

                                    <div class="modal-body">

                                        <div class="checkbox checkbox-switch">
                                            <label>
                                                <input type="checkbox" data-on-color="danger" data-off-color="primary"
                                                       data-on-text="{{__('profile.signature')}}"
                                                       data-off-text="{{__('profile.initials')}}" class="switch"
                                                       checked="checked">

                                            </label>
                                        </div>

                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                                <li class="active"><a href="#signatures_draw" id="signatures_draw_href"
                                                                      data-toggle="tab"><span data-i18n="Signatures.draw">{{ __('profile.draw') }}</span></a>
                                                </li>
                                                <li><a href="#signatures_upload_image"
                                                       data-toggle="tab"><span data-i18n="Signatures.upload">{{ __('profile.upload_image') }}</span></a></li>
                                                <li><a href="#signatures_phone"
                                                       data-toggle="tab"><span data-i18n="Signatures.mobile">{{ __('profile.phone') }}</span></a></li>
                                                <li><a href="#signatures_type" data-toggle="tab"
                                                       class="hide"><span data-i18n="type">{{ __('profile.type') }}</span></a></li>

                                            </ul>

                                            <div class="tab-content">

                                                <div class="tab-pane active" id="signatures_draw"
                                                     style="position: relative;">

                                                    <div id="signature-pad" class="m-signature-pad">
                                                        <div class="m-signature-pad--body">
                                                            <canvas width="698" height="366"
                                                                    style="touch-action: none;"></canvas>


                                                        </div>

                                                        <div class="col-md-4">
                                                            <input type="text"
                                                                   class="form-control colorpicker-show-input"
                                                                   data-preferred-format="rgb" value="#000"
                                                                   value="#E63E3E">
                                                        </div>
                                                        <div class="m-signature-pad--footer">
                                                            <div class="description"><span data-i18n="sign_above"></span></div>
                                                            <div class="left">
                                                                <button type="button"
                                                                        class="button clear btn btn-primary"
                                                                        data-action="clear"><span data-i18n="clear"></span>
                                                                </button>
                                                            </div>
                                                            <div class="right">
                                                                <button type="button"
                                                                        class="button save btn btn-primary"
                                                                        id="save_draw_sign" data-action="save-png"><span data-i18n="save"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                {{--ask--}}<div class="tab-pane" id="signatures_upload_image">

                                                    {!! Plupload::make('my_uploader_id', action('Individual\UserController@uploadSign'))
                                                        ->setOptions([
                                                            'filters' => [
                                                                'max_file_size' => '1mb',
                                                                'mime_types' => [
                                                                    ['title' => "Image files", 'extensions' => "jpg,gif,png"],
                                                                ],
                                                            ],
                                                        ])
                                                        ->render() !!}

                                                </div>{{--ask--}}
                                                <div class="tab-pane" id="signatures_phone">
                                                    <div class="row">
                                                        <h4 class="col-xs-4">
                                                            <img src="{{asset('/assets/images/sign-mobile.png')}}"
                                                                 style="width: 100%;">
                                                        </h4>
                                                        <div class="col-xs-8">
                                                            <form class="form-inline">
                                                                <div class="form-group">
                                                                    <label for="phone" data-i18n="Signatures.phone"></label>
                                                                    <input type="phone" class="form-control" id="phone">
                                                                </div>
                                                                <button type="submit" class="btn btn-default" data-i18n="submit">
                                                                </button>
                                                            </form>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="tab-pane hide" id="signatures_type">

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /basic modal -->


                        <div class="tab-pane fade in active profile_info" id="info">

                            <div class="panel panel-flat">


                                <div class="panel-body panel-accounts ">


                                    <table class="table">
                                        <tr>
                                            <th><span  data-i18n="email"></span></th>
                                            <td>
                                                {{ $aAccount['user']['email'] }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><span data-i18n="first_name"></span></th>
                                            <td>
                                                {{ $aAccount['user']['first_name'] }}</td>
                                        </tr>
                                        <tr>
                                            <th><span data-i18n="last_name"></span></th>
                                            <td>{{ $aAccount['user']['last_name'] }}</td>
                                        </tr>
                                        <tr>
                                            <th><span data-i18n="mobile">{{ $aAccount['user']['mobile'] }}</span></th>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th><span data-i18n="create_date"></span></th>
                                            <td>{{ $aAccount['created_at'] }}</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- /user profile -->

    </div>
    <!-- /content area -->

</div>
<script>
    var profile_no_signature = '{{__("profile.no_signature")}}';
    var profile_no_initials = '{{__("profile.no_initials")}}';
    var profile_deleteSign = '{{route('profile.deleteSign')}}';
    var profile_updateEditable = '{{route( 'profile.updateEditable') }}';
    var currentUserID = '{{currentUserID()}}';
    var account_make_default = '{{route("account.make_default")}}';
    var deactivate = '{{route("profile.deactivate")}}';

</script>
<!-- /main content -->

