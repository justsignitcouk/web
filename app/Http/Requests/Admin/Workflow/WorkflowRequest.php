<?php

namespace  App\Http\Requests\Admin\Workflow;

use Illuminate\Foundation\Http\FormRequest;

class WorkflowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'workflow_name' => 'required',
//            'wf_stage' => 'array|array2DRequired:name,description,action_type,due_date'
        ];
    }
}
