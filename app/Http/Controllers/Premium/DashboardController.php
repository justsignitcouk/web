<?php

namespace App\Http\Controllers\Premium;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyDetailsRequest;
use App\Services\Account;
use App\Services\Company;
use App\Services\Lookup;
use App\Services\Plans;
use App\Services\Quota;
use App\Services\Services;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{

    public function index(){
        $oCompaniesQuota = getQuota('number_of_companies');
        $oEmployeesQuota = getQuota('number_of_employees');
        $oWorkFlowsQuota = getQuota('number_of_workflows');

        //$aPlans = Plans::getPlanByUserID();
        return Theme::view('premium.settings.index', ['oCompaniesQuota'=>$oCompaniesQuota,'oEmployeesQuota'=>$oEmployeesQuota,'oWorkFlowsQuota'=>$oWorkFlowsQuota]);
    }

}
