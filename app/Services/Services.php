<?php
namespace App\Services;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Facade;
use Jenssegers\Agent\Facades\Agent;

class Services extends Facade {

    public static $_instance;
    protected $oClient;
    protected $sApiUrl;
    protected $sToken;
    protected $sUrl;
    protected $sMethod = 'GET';
    private $aMethodAllowed = ['GET','POST','PUT','PATCH','DELETE'] ;
    private $aHeader = [] ;
    private $aBody = [] ;
    private $aQuery = [] ;
    private $aMultiPart = [] ;
    private $bMultiPart = false ;
    private $bWithToken = false ;
    private $oRequest;
    public static $oService;

    public static function getInstance()
    {
        if ( ! ( self::$_instance instanceof self) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct(){
        if ( ! ( self::$oService instanceof self) ) {
            self::$oService = new Client();
        }
        $this->oClient = self::$oService;
        $this->sApiUrl = config()->get('app')['api_url'];
        $this->sToken = session()->get("sToken") ;
        return $this->oClient;
    }

    public function setBaseUrl($sUrl){
        $this->sApiUrl = $sUrl;
        return $this;
    }

    public function setUrl($sUrl){
        $sUrl = $this->sApiUrl . '/' . $sUrl ;
        if (filter_var($sUrl, FILTER_VALIDATE_URL) === FALSE) {
            die('h');
            return false;
        }
        $this->sUrl = $sUrl;

        return $this;
    }

//    public function setMethod($sMethod){
//
//        if(!in_array(strtoupper($sMethod),$this->aMethodAllowed)){
//            throw new \Exception('THE METHOD NOT ALLOWED');
//        }
//
//        $this->sMethod = $sMethod ;
//        return $this;
//    }

    public function setMethod($sMethod){
        $this->bWithToken = false;
        if(!in_array(strtoupper($sMethod),$this->aMethodAllowed)){
            throw new \Exception('THE METHOD NOT ALLOWED');
        }

        $this->sMethod = $sMethod ;
        return $this;
    }

    public function request(){

        //$this->oClient->setDefaultOption('verify', '/etc/ssl/certs/apache-selfsigned.crt');
        try{
            $this->oRequest = $this->oClient->request($this->sMethod ,$this->sUrl, [ 'verify'=>false,
                'headers'         => $this->addIfWithToken(),
                ($this->bMultiPart ? 'multipart' : 'form_params')         => $this->aBody,
                'query' => $this->aQuery,
                'http_errors' => true
            ]);

        }catch (\Exception $exception){

            throw $exception;
        }


        return $this;
    }

    public function setHeader($aParams=[]){

        $this->aHeader = $aParams ;

        return $this;
    }

    public function appendHeader($aParams=[]){
        foreach($aParams as $k=>$value){
            $this->aHeader[$k] = $value ;
        }
        return $this;
    }

    public function setBody($aParams=[]){

        $this->aBody = $aParams ;

        return $this;
    }

    public function setQuery($aParams=[]){

        $this->aQuery = $aParams ;

        return $this;
    }

    public function setMultiPart($bStatus = true){

        $this->bMultiPart = $bStatus ;

        return $this;
    }

    public function appendBody($aParams=[]){
        foreach($aParams as $k=>$value){
            $this->aBody[$k] = $value ;
        }

        return $this;
    }

    public function withToken(){

        $this->bWithToken = true;
        return $this;
    }

    private function addIfWithToken(){
        if($this->bWithToken){
            return array_merge($this->aHeader,array('Authorization'=> 'Bearer ' . $this->sToken));
        }
        return $this->aHeader ;
    }

    public function getContent(){
        return $this->oRequest->getbody()->getContents();
    }

    public function getUrl(){
        return $this->sUrl ;
    }

    public function find($aParams=[]){

        $oServices = self::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;

            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public function prepare($aParams) {

        if(isset($aParams['method'])){
            $this->setMethod($aParams['method']);
        }

        if(isset($aParams['url'])){
            $this->setUrl($aParams['url']);
        }

        if(isset($aParams['aHeader'])){
            $this->setHeader($aParams['aHeader']);
        }

        if(isset($aParams['aBody'])){
            $this->setBody($aParams['aBody']);
        }

        return $this;
    }

    public static function getDeviceInfo() {

        $browser = Agent::browser();
        $browser_version = Agent::version($browser);
        $platform = Agent::platform();
        $platform_version = Agent::version($platform);
        $languages = Agent::languages();
        $device = Agent::device();
        $aBody = [
            'os' => $platform,
            'platform' => $platform,
            'platform_version' => $platform_version,
            'browser' => $browser,
            'browser_version' => $browser_version,
            'languages' => $languages,
            'ip' => request()->ip(),
            'device' => $device
        ];

        return $aBody;

    }
}