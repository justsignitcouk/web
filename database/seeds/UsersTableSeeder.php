<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => "1",
            'user_name' => "KnowMx",
            'first_name' => "Admin",
            'last_name' => "KnowMx",
            'email' => 'admin@knowmx.com',
            'password' => bcrypt('dev123456'),
            'activated' => '1',
        ]);

    }
}
