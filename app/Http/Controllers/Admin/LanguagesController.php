<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\LanguagesDataTable;
use App\DataTables\LanguagesDataTablesEditor;
use App\Http\Requests\Admin\LanguagesRequest;
use App\Models\Language;
use App\Models\LanguagesA;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the CRUD entries.
     *
     * @param   void
     * @return  \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Language::query())->toJson();
        }
        $html = $builder->columns([
            ['data' => 'id', 'footer' => 'Id'],
            ['data' => 'name', 'footer' => 'Name'],
            ['data' => 'created_at', 'footer' => 'Created At'],
            ['data' => 'updated_at', 'footer' => 'Updated At'],
            ]);

        return view('languages.index', compact('html'));
    }

    public function store(LanguagesRequest $request)
    {
        $name = $request->get('name');
        $direction = $request->get('direction');
        $oLanguage = Language::find('name',$name);
        $aResponse = ['message'=>'failed','status'=>0];
        if(!$oLanguage) {
            $oLanguage = new Language();
            $oLanguage->name = $name ;
            $oLanguage->direction = $direction ;
            $oLanguage->save();
            $aResponse = ['message'=>'successfully','status'=>1,'oLanguage'=>$oLanguage->toArray()];
        }
        return \GuzzleHttp\json_encode($aResponse);
    }

    /**
     * Display the CRUD create form.
     *
     * @param   void
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $crud = crud_entry(new \App\Languages);

        return view('crud::scaffold.bootstrap3-form', ['crud' => $crud]);
    }

    /**
     * Show the form for editing the current CRUD entry.
     *
     * @param   int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crud = crud_entry(\App\Languages::findOrFail($id));

        return view('crud::scaffold.bootstrap3-form', ['crud' => $crud]);
    }

    /**
     * Update the CRUD entry.
     *
     * @param   \Illuminate\Http\Request  $request
     * @param   int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = \App\Languages::findOrFail($id);
        $validator = $model->crudEntry()->getValidator();

        $validator->validateRequest($request);

        if ($validator->passes())
        {
            $validator->save();
        }

        return $validator->redirect();
    }

    /**
     * Destroy the current CRUD entry.
     *
     * @param   int  $id
     * @param   string $csrf
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id, $csrf)
    {
        if (csrf_token() != $csrf)
        {
            abort(403);
        }

        $model = \App\Languages::findOrFail($id);

        $model->delete();

        return redirect()->back();
    }
}
