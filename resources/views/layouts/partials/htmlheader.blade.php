<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('htmlheader_title', 'MyCompose')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Theme stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    @if(false)
    <link href="{{ asset('/theme/default/rtl/assets/css/icons/icomoon/styles.css') }}"  rel="stylesheet type="text/css">
    <link href="{{ asset('/theme/default/rtl/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/theme/default/rtl/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/theme/default/rtl/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    @else
    <link href="{{ asset('/theme/default/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/theme/default/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/theme/default/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/theme/default/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    @endif
    <link href="{{ asset('/theme/default/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/main.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('/theme/default/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <!-- /Theme stylesheets -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/components/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/components/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/components/datatables-responsive/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/components/datatables-responsive/js/responsive.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/components_notifications_pnotify.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/form_validation.js') }}"></script>
    <!-- /Theme JS files -->


    <!-- Dropzone JS -->
     <script type="text/javascript" src="{{ asset('/assets/components/dropzone-4.0.1/dist/dropzone.js') }}"></script> 
    <link href="{{ asset('/assets/components/dropzone-4.0.1/dist/dropzone.css') }}" rel="stylesheet" type="text/css">
    <!-- /Dropzone JS -->

    <!-- Datatable -->
    {{--{%   do assets.addCss("assets/components/datatables-responsive/css/responsive.dataTables.css") %}--}}
    {{--{%   do assets.addCss("assets/components/datatables-responsive/css/responsive.bootstrap.css") %}--}}
    <!-- /Datatable -->


 {{--   <script>
        $(document).ready(function(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: true,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    "sProcessing":   "{{ lang().get('components/datatables.sProcessing') }}",
                    "sLengthMenu":   "{{ lang().get('components/datatables.sLengthMenu') }}",
                    "sZeroRecords":  "{{ lang().get('components/datatables.sZeroRecords') }}",
                    "sInfo":         "{{ lang().get('components/datatables.sInfo') }}",
                    "sInfoEmpty":    "{{ lang().get('components/datatables.sInfoEmpty') }}",
                    "sInfoFiltered": "{{ lang().get('components/datatables.sInfoFiltered') }}",
                    "sInfoPostFix":  "{{ lang().get('components/datatables.sInfoPostFix') }}",
                    "sSearch":       "{{ lang().get('components/datatables.sSearch') }}:",
                    "sUrl":          "{{ lang().get('components/datatables.sUrl') }}",
                    "oPaginate": {
                        "sFirst":    "{{ lang().get('components/datatables.oPaginate.sFirst') }}",
                        "sPrevious": "{{ lang().get('components/datatables.oPaginate.sPrevious') }}",
                        "sNext":     "{{ lang().get('components/datatables.oPaginate.sNext') }}",
                        "sLast":     "{{ lang().get('components/datatables.oPaginate.sLast') }}"
                    }
                }
            });
        });
    </script>--}}


    @stack('scripts')
</head>