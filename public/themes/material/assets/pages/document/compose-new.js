$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });



    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": "-",
        }
    });

    var interval_id ;
    $(window).on("blur focus", function(e) {
        var prevType = $(this).data("prevType");

        if (prevType != e.type) {   //  reduce double fire issues
            switch (e.type) {
                case "blur":
                    clearInterval(interval_id);
                    interval_id = 0;
                    break;
                case "focus":
                    if (!interval_id) {
                        interval_id = setInterval(function(){
                            $.fn.saveDraft() }, 60000);
                    }
                    break;
            }
        }

        $(this).data("prevType", e.type);
    })


    $(document).on('click', '#saveTemplate', function (e) {

        var button = $(this);
        button.prop('disabled', 'true');

        var elements = $.fn.getElements();

        elements.status = 'template';

        $.post(globals_routeSendDocument, elements, function (data) {

            if(data.status=='false'){
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

                button.removeAttr('disabled');
                return false;
            }

            if (data.status == 'true') {
                new PNotify({
                    title: 'saved.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
            }
        window.location = "/document/compose/" + data.document_id;
        },'json').error(function(response){
            //check if response has errors object
            $.each(response.responseJSON.errors,function(k,v){
                $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


            });

            button.removeAttr('disabled');

        });


        e.preventDefault();


    });

    $(document).on('click', '#saveDocument', function (e) {
        var buttonThis = $(this);
        buttonThis.prop('disabled', 'true');
        var elements = $.fn.getElements();
        if (!elements) {
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return;
        }

        $.post(globals_routeSendDraft, elements, function (data) {
            var response = jQuery.parseJSON(data);
            if (response.status == 1) {
                new PNotify({
                    title: 'success.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
                buttonThis.removeAttr('disabled');
            } else {
                buttonThis.removeAttr('disabled');
            }

        });


        e.preventDefault();


    });


    var workflow_obj = $(".workflow_steps");
    $(document).on('click', '#sendDocument', function (e) {
        var button = $(this);
        button.prop('disabled', 'true');

        var elements = $.fn.getElements();

        if(elements.title == '' ){
            new PNotify({
                title: 'Please enter title',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }

        if(elements.content == '' ){
            new PNotify({
                title: 'Please enter content',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }


        if(Object.keys(elements.to_users).length === 0  ) {
            new PNotify({
                title: 'Please enter one reception at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }

        if(  Object.keys(elements.signers_users).length === 0  ) {
            new PNotify({
                title: 'Please enter one signer at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }

        if (!elements) {
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return;
        }
        elements['status'] = 'inprogress' ;
        $.post(globals_routeSendDocument, elements, function (data) {
            if(data.status=='false'){
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

                button.removeAttr('disabled');
                return false;
            }

            if (data.status == 'true') {
                new PNotify({
                    title: 'success.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
                console.log(data);
                window.location = '/document/' + data.document_id;


                console.log('hi');
                $(this).removeAttr('disabled');
            } else {
                $(this).removeAttr('disabled');
            }

        },'json').error(function(response){
            //check if response has errors object
            $.each(response.responseJSON.errors,function(k,v){
                $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


            });

            button.removeAttr('disabled');

        });


        e.preventDefault();


    });

    function next_step(){
        $.fn.goToNextStage();

    }

    $('.sendDocument').click(function(){
        $('#sendDocument').trigger('click');
    });


    $(document).mouseup(function(e)
    {
        var container = $("div.editable");
        var container2 = $(".mce-tinymce , .mce-container");

        // if the target of the click isn't the container nor a descendant of the container
        if ( (!container.is(e.target) && container.has(e.target).length === 0) && (!container2.is(e.target) && container2.has(e.target).length === 0) )
        {
            tinymce.remove("div.editable");
        }else{
            tinymce.init({
                content_css  : "/assets/css/custom_fonts.css",
                style_formats: [
                    {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                    {title: 'damase_v2', inline: 'span', styles: { 'font-family':'damase_v2'}},
                    {title: 'DejaVuSans', inline: 'span', styles: { 'font-family':'DejaVuSans'}},
                    {title: 'ayar', inline: 'span', styles: { 'font-family':'ayar'}},
                    {title: 'Abyssinica_SIL', inline: 'span', styles: { 'font-family':'Abyssinica_SIL'}},
                    {title: 'Aegean', inline: 'span', styles: { 'font-family':'Aegean'}},
                    {title: 'Aegyptus', inline: 'span', styles: { 'font-family':'Aegyptus'}},
                    {title: 'Akkadian', inline: 'span', styles: { 'font-family':'Akkadian'}},
                    {title: 'lannaalif', inline: 'span', styles: { 'font-family':'lannaalif'}},
                    {title: 'Uthman', inline: 'span', styles: { 'font-family':'Uthman'}},
                    {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                    {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                    {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                    {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                    {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                    {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                    {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                    {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                    {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                    {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                    {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                    {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
                ],
                font_formats : "Andale Mono=andale mono,times;"+
                "damase_v2=damase_v2;"+
                "ayar=ayar;"+
                "Abyssinica_SIL=Abyssinica_SIL;"+
                "Aegean=Aegean;"+
                "Aegyptus=Aegyptus;"+
                "Akkadian=Akkadian;"+
                "lannaalif=lannaalif-v1-03;"+
                "Uthman=Uthman;"+
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                "DejaVuSans=DejaVuSans;",
                selector: 'div.editable',
                inline: false,
                fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'paste, mention',
                    'searchreplace visualblocks code fullscreen paste',
                    'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                ],
                mentions: {
                    queryBy:'title',
                    delimiter: ['#'],
                    source: function (query, process, delimiter) {
                        // Do your ajax call
                        // When using multiple delimiters you can alter the query depending on the delimiter used
                        if (delimiter == '#') {
                            $.post('/document/ajax/list_document',{}, function (data) {
                                //call process to show the result
                                process(data)
                            },'json');
                        }
                    },
                    highlighter: function(text) {
                        return text;
                        //make matched block italic
                        return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                            return '<i>' + match + '</i>';
                        });
                    },
                    insert: function(item) {
                        return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.outbox_number + ')</span>&nbsp;';
                    }
                },
                toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
                ]
            });
        }
    });











    $('.choose_layout').click(function () {
        var route = $('.layout_modal').data('route');
        $.post(route, {}, function (data) {
            var html = '<div class="modal-body">';
            var layout_count = 1;
            $.each(data, function (k, v) {
                console.log(v);
                html += "<h1>" +  v.name + "</h1>" +  '<div class="layout_box" data-id="'+ v.id +'">';
                html += " <div class='header_component'>" + v.header + "</div>";
                html += "</div>";
            });

            html += "</div>";
            $('.layout_modal').find('.modal-content').html(html);
            $('.layout_modal').modal('show');
        }, 'json');



        $(document).on('click', '.layout_box', function () {

            var id = $(this).data('id');

            $('#document_layout_input').val(id);

            $.fn.saveDraft();
            setTimeout(function(){
                location.reload();
            },500) ;
            $('.modal').modal('hide');


        });

        $.fn.saveDraft();
    });



    $.fn.drawTemplates = function(){

        var html = '' ;

        if(aTemplates!==undefined){

            $.each(aTemplates,function(k,v){

                html += '<div class="col-md-12"><a href="/document/compose/'+ v.id +'" data-key="' + v.id + '" class="set_by_template"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 20 24" class="document_list"><defs><style>.cls-1{fill:#003375;}</style></defs><title>' + v.title + '</title><g id="a"><path class="cls-1" d="M20,3V24H0V3H4.7L2,5.8V22H18V5.8L15.4,3ZM16.4,7,13,3.4V3a3,3,0,0,0-3-3A3,3,0,0,0,7,3v.4L3.7,7ZM10,2a.94.94,0,0,1,1,1,.94.94,0,0,1-1,1A.94.94,0,0,1,9,3,.94.94,0,0,1,10,2ZM5,17H15v1H5Zm0-1H15V15H5Zm0-2H15V13H5Zm0-2H15V11H5Z"/></g></svg><span class="document_list">'+ (v.title !=null ? v.title : 'No subject') +'</span></a></div>';
            });
            $('#template_container').html(html);
        }
    }
    $.fn.getTemplate();
    $.fn.drawTemplates();

    $(document).on('click','#saveDraftButton',function(e){
        new PNotify({
            title: 'success.',
            text: 'Saved',
            icon: 'icon-checkmark3',
            type: 'success'
        });
        $.fn.saveDraft();
        e.preventDefault();
    })




    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
            $('.fab-menu-bottom-left, .fab-menu-bottom-right').addClass('reached-bottom');
        }
        else {
            $('.fab-menu-bottom-left, .fab-menu-bottom-right').removeClass('reached-bottom');
        }
    });


    $('.pages_frame').slimScroll({
        height: '380px',
        width: '1020px',
    });



});