<!DOCTYPE html>
<html lang="{{ getLangCode() }}">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@get('title')</title>


        @if( getLangCode() == 'ar')
        @styles('rtl')
        @else
            @styles('ltr')
        @endif
        @styles()
    </head>

    <body class="{{ $sSideBar }}">


    @partial('header')

        <!-- Page container -->
        <div class="page-container" >

            <!-- Page content -->
            <div class="page-content">
        @partial('sidebar',['oBadges'=>$oBadges])

                <div class="main_content_ajax" id="body">
                @content()
                </div>
            </div>
        </div>



        @partial('footer')
        @scripts('inline')
        @scripts()


        @if(\App::getLocale() == 'ar')

        @endif

        <div class="mask_full_screen" style="    position: fixed;width: 100%;height: 100%;background: #fff;z-index: 9999999999;display: none;top: 0;left: 0;"></div>
    </body>
</html>