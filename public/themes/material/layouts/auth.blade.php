<!DOCTYPE html>
<html lang="en">

<head>
    {!! meta_init() !!}
    <meta name="keywords" content="@get('keywords')">
    <meta name="description" content="@get('description')">
    <meta name="author" content="@get('author')">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@get('title')</title>
    @styles('auth')



</head>
<body class="login-container pace-done">

@partial('auth.header')

<div class="page-container">

    <div class="page-content">

        <div class="content-wrapper">

            <div class="content">
                @content()
            </div>
        </div>
    </div>
</div>
@partial('auth.footer')

@scripts('auth')
</body>
</html>



