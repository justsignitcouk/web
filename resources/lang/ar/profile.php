<?php

return [


    'email' => 'البريد الالكتروني',
    'user_name' => 'اسم المستخدم',
    'first_name' => 'الاسم الاول',
    'last_name' => 'الاسم الاخير',
    'mobile' => "الهاتف",
    'created_at' => "تاريخ الاضافة",
    'about' => "نبذه",
    'info' => "معلومات",
    'accounts' => "الحسابات",
    'signature' => "التوقيع",
    'signatures' => "التواقيع",
    'fingerprint' => "بصمة الاصبع",
    'attachments' => "المرفقات",
    'settings' => "الاعدادات",
    'schedule' => "الاجندة",
    'activity' => "النشاطات",
    'initials' => "علامة",
    'signature' => "توقيع",
    'add_new_signatures' => "انشاء توقيع جديد",
    'no_signature' => "ليس لديك اي توقيع حتى الان , اضغط على زر اضافة توقيع جديد لاضافة توقيع",
    'no_initials' => "ليس لديك اي علامة حتى الان ، اضغط على زر اضافة توقيع جديد لاضافة علامة",
];
