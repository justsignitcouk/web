@foreach($permissions_permissions as $key => $permission)
    <div class="form-check">
      <label class="permission-check-label">
        <input class="permission-check-input" type="checkbox" name ="{{'permission-'.$key}}" id="{{$permission->key}}" value="{{$permission->id}}"

        @if(!empty($checked_user_permissions))
            @foreach($checked_user_permissions as $check)

                @if($check['permission_id'] == $permission->id)
                checked
                @endif
         
            @endforeach
        @endif
       
        >
        {{$permission->name}}
      </label>
    </div>
@endforeach