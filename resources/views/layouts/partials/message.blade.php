@if(session('userCreated'))
   <div class="alert alert-success">
       {{ __("users.created") }}
   </div>
@endif