@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<ul class="breadcrumb">
				<li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
				<li><a href="{{route('admin.companies.show' , $company['id'])}}"><i class="position-left"></i>{{ $company['name'] }}</a></li>
				<li class="active">{{ __("common.show") }}</li>
			</ul>
			<div class="tabbable">

				<div class="tab-content">

					<div class="content-group">

						<div class="col-lg-12 col-md-12">

								@include('layouts/partials/tabs/companies_tabs')
						    	<div class="container panel panel-body
						    	pull-right
						    	col-md-12">

									<table class="table">

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("common.name") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$company['name']}}</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("common.slug") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$company['slug']}}</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("companies.license_no") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">{{$company['license_no']}}</span>
										</td>
									</tr>

									<tr class="form-group mt-5">
										<td>
											<label class="text-semibold">{{ __("common.created_at") }}:</label>
										</td>
										<td>
											<span class="pull-right-sm">
												<?php

												$date = new DateTime($company['created_at']) ;
												echo $date->format('g:ia \o\n l jS F Y');
												?>
											</span>
										</td>
									</tr>

									<tr class="form-group">
										<td>
											<label class="text-semibold">{{ __("companies.logo") }}:</label>
										</td>
										<td>
											<div class="pull-right-sm">
												<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
											</div>
										</td>

									</tr>

									</table>

							  	</div>
		             	</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endsection
