{!! Theme::asset()->themePath()->add([ ['d3', 'js/plugins/visualization/d3/d3.min.js', ['blockui']]]) !!}
{!! Theme::asset()->themePath()->add([ ['d3_tooltip', 'js/plugins/visualization/d3/d3_tooltip.js', ['d3']]]) !!}
{!! Theme::asset()->themePath()->add([ ['switchery', 'js/plugins/forms/styling/switchery.min.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['bootstrap_multiselect', 'js/plugins/forms/selects/bootstrap_multiselect.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['moment', 'js/plugins/ui/moment/moment.min.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['daterangepicker', 'js/plugins/pickers/daterangepicker.js', ['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['dashboard', 'pages/dashboard/dashboard.js',['daterangepicker']]]) !!}
{!! Theme::asset()->themePath()->add([ ['ripple', 'js/plugins/ui/ripple.min.js',['daterangepicker']]]) !!}


<!-- Main content -->
<div class="content-wrapper">


    <!-- Content area -->
    <div class="content">

    {{ csrf_field() }}
    <!-- Dashboard content -->

        <div class="row">
            {{-- left side--}}
            <div class=" col-md-8">
                {{--waiting my sign--}}
                <div class="panel panel-flat   uploded-document">

                    <div class="panel-heading bg-blue-primery" {{--style="background: #2D3092 !important; "--}}>
                        <h6 class="panel-title">
                            <svg xmlns="http://www.w3.org/2000/svg" id="feth_svg" data-name="Layer 1" viewBox="0 0 23 20.5" style="
    height: 27px;
    width: 27px;
">
                                <title>Feather Pen</title>
                                <path d="M17.4,11.5c-1.7,2.2-4.1,4.5-6.9,4.1A42.33,42.33,0,0,0,8,19.8l-2.1.7a60,60,0,0,1,6.8-10.4,34.7,34.7,0,0,0-4.6,4c-1.4-3,.1-5.8,2.3-8a4,4,0,0,0,.7,2.4,7.89,7.89,0,0,1,.7-3.7,17.37,17.37,0,0,1,3.6-2.4,3.36,3.36,0,0,0,.2,2.2,5.7,5.7,0,0,1,1.1-2.8A11.81,11.81,0,0,1,23,0a17.43,17.43,0,0,1-2.1,5.5,7.71,7.71,0,0,1-2.9,1,6.17,6.17,0,0,0,2.3.2,37.18,37.18,0,0,1-1.9,3.4,8.79,8.79,0,0,1-3.6,1,3.91,3.91,0,0,0,2.6.4ZM4,19.5H0v1H4Z" style="
fill: #fff"></path>
                            </svg>
                            <span class="title" data-i18n="dashboard.my_sig"></span></h6>
                        <div class="heading-elements">
                            <a class="btn icon-btn btn-default view_btn legitRipple" href="#">
                                <span data-i18n="view_all"></span>
                              <!-- defining icon -->

                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table text-nowrap">


                            <tr id="needs_my_signature">

                            </tr>


                        </table>
                    </div>
                </div>
                {{--waiting my sign--}}



                {{--waiting from others--}}
                <div class="panel panel-flat  uploded-document">
                    <div class="panel-heading" style="background: #025BA4 !important; ">
                        <h6 class="panel-title">
                            <svg xmlns="http://www.w3.org/2000/svg" id="feth_svg" data-name="Layer 1" viewBox="0 0 23 20.5" style="
    height: 27px;
    width: 27px;
">
                                <title>Feather Pen</title>
                                <path d="M17.4,11.5c-1.7,2.2-4.1,4.5-6.9,4.1A42.33,42.33,0,0,0,8,19.8l-2.1.7a60,60,0,0,1,6.8-10.4,34.7,34.7,0,0,0-4.6,4c-1.4-3,.1-5.8,2.3-8a4,4,0,0,0,.7,2.4,7.89,7.89,0,0,1,.7-3.7,17.37,17.37,0,0,1,3.6-2.4,3.36,3.36,0,0,0,.2,2.2,5.7,5.7,0,0,1,1.1-2.8A11.81,11.81,0,0,1,23,0a17.43,17.43,0,0,1-2.1,5.5,7.71,7.71,0,0,1-2.9,1,6.17,6.17,0,0,0,2.3.2,37.18,37.18,0,0,1-1.9,3.4,8.79,8.79,0,0,1-3.6,1,3.91,3.91,0,0,0,2.6.4ZM4,19.5H0v1H4Z" style="
fill: #fff"></path>
                            </svg>
                            <span class="title" data-i18n="dashboard.other_sig"></span></h6>
                        <div class="heading-elements">
                            <a class="btn icon-btn btn-default view_btn legitRipple" href="#">
                                <span data-i18n="view_all"></span>
                             {{--   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="512px" style="enable-background:new 0 0 512 512;display: inline;    position: relative;
    top: 5px;height: 18px;width: 18px;fill: #fff;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve"><style type="text/css">
                                        &lt;
                                        !
                                        [CDATA[
	.st0 {
                                            display: inline;
                                        }

                                        .st1 {
                                            fill: none;
                                            stroke: #000000;
                                            stroke-width: 16;
                                            stroke-linecap: round;
                                            stroke-linejoin: round;
                                            stroke-miterlimit: 10;
                                        }

                                        .st2 {
                                            display: none;
                                        }

                                        ]
                                        ]
                                        &gt;
                                    </style>
                                    <g class="st2" id="layer">
                                        <g class="st0">
                                            <polyline class="st1" points="411,256 260,407 411,256 260,105   "></polyline>
                                            <polyline class="st1" points="252,256 101,407 252,256 101,105   "></polyline>
                                        </g>
                                    </g>
                                    <g id="layer_copy">
                                        <g>
                                            <path d="M260,415c-2.048,0-4.095-0.781-5.657-2.343C252.781,411.095,252,409.048,252,407s0.781-4.095,2.343-5.657L399.687,256    L254.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.314,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C264.095,414.219,262.048,415,260,415z"></path>
                                        </g>
                                        <g>
                                            <path d="M101,415c-2.047,0-4.095-0.781-5.657-2.343C93.781,411.095,93,409.048,93,407s0.781-4.095,2.343-5.657L240.686,256    L95.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.313,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C105.095,414.219,103.047,415,101,415z"></path>
                                        </g>
                                    </g></svg> --}}<!-- defining icon -->

                            </a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table text-nowrap">




                            <tr id="waiting_from_others">

                            </tr>
                        </table>
                    </div>
                </div>
                {{--waiting from others--}}

                {{--updated document--}}
                <div class="panel panel-flat uploded-document">
                    <div class="panel-heading bg-blue-primery">
                        <h6 class="panel-title">
                            <svg xmlns="http://www.w3.org/2000/svg" id="feth_svg" data-name="Layer 1" viewBox="0 0 23 20.5" style="
    height: 27px;
    width: 27px;
">
                                <title>Feather Pen</title>
                                <path d="M17.4,11.5c-1.7,2.2-4.1,4.5-6.9,4.1A42.33,42.33,0,0,0,8,19.8l-2.1.7a60,60,0,0,1,6.8-10.4,34.7,34.7,0,0,0-4.6,4c-1.4-3,.1-5.8,2.3-8a4,4,0,0,0,.7,2.4,7.89,7.89,0,0,1,.7-3.7,17.37,17.37,0,0,1,3.6-2.4,3.36,3.36,0,0,0,.2,2.2,5.7,5.7,0,0,1,1.1-2.8A11.81,11.81,0,0,1,23,0a17.43,17.43,0,0,1-2.1,5.5,7.71,7.71,0,0,1-2.9,1,6.17,6.17,0,0,0,2.3.2,37.18,37.18,0,0,1-1.9,3.4,8.79,8.79,0,0,1-3.6,1,3.91,3.91,0,0,0,2.6.4ZM4,19.5H0v1H4Z" style="
fill: #fff"></path>
                            </svg>
                            <span class="title"  data-i18n="dashboard.up_doc"></span></h6>
                        <div class="heading-elements">
                            <a class="btn icon-btn btn-default view_btn legitRipple" href="#">
                                <span data-i18n="view_all"></span>
                      {{--          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="512px" style="enable-background:new 0 0 512 512;display: inline;    position: relative;
    top: 5px;height: 18px;width: 18px;fill: #fff;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve"><style type="text/css">
                                        &lt;
                                        !
                                        [CDATA[
	.st0 {
                                            display: inline;
                                        }

                                        .st1 {
                                            fill: none;
                                            stroke: #000000;
                                            stroke-width: 16;
                                            stroke-linecap: round;
                                            stroke-linejoin: round;
                                            stroke-miterlimit: 10;
                                        }

                                        .st2 {
                                            display: none;
                                        }

                                        ]
                                        ]
                                        &gt;
                                    </style>
                                    <g class="st2" id="layer">
                                        <g class="st0">
                                            <polyline class="st1" points="411,256 260,407 411,256 260,105   "></polyline>
                                            <polyline class="st1" points="252,256 101,407 252,256 101,105   "></polyline>
                                        </g>
                                    </g>
                                    <g id="layer_copy">
                                        <g>
                                            <path d="M260,415c-2.048,0-4.095-0.781-5.657-2.343C252.781,411.095,252,409.048,252,407s0.781-4.095,2.343-5.657L399.687,256    L254.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.314,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C264.095,414.219,262.048,415,260,415z"></path>
                                        </g>
                                        <g>
                                            <path d="M101,415c-2.047,0-4.095-0.781-5.657-2.343C93.781,411.095,93,409.048,93,407s0.781-4.095,2.343-5.657L240.686,256    L95.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.313,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C105.095,414.219,103.047,415,101,415z"></path>
                                        </g>
                                    </g></svg>--}} <!-- defining icon -->

                            </a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 box-sign">
                                <div class="col-xs-5 ">
                                        <div class="media-left media-middle">
                                            <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                                <span class="letter-icon">A</span>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle Doney</a>
                                            <div class="text-muted text-size-small"><span class="status-mark border-blue position-left"></span> Active</div>
                                        </div>
                                    </div>
                                <div class=" col-xs-3"><div class="user_td"><span>1</span><span class="i icon-user"></span><span>2</span></div></div>
                                <div class="col-xs-4"><td class="prog_td "><span data-i18n="progress"></span><div class="progress_complete" style="display: inline-block;"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100px">
                                                completed</div></div><span>   </span><span>a day ago</span></td></div>

                            </div>


                        </div>
                    </div>
                </div>
                {{-- /updated document--}}
            </div>

            {{-- right side--}}
            <div class="col-md-4">

                {{--users activities--}}
                <div class="panel panel-flat user_active">
                    {{--header--}}
                    <div class="panel-heading">
                        <h6 class="panel-title"><span data-i18n="online_users"></span><a class="heading-elements-toggle"><i class="icon-more"></i></a>
                        </h6>
                        <div class="heading-elements">
                            <a class="btn icon-btn btn-default view_btn_sm legitRipple" href="{{url('profile').'#activities'}}">
                                <span data-i18n="view_all"></span>
                  {{--              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     height="512px" style="enable-background:new 0 0 512 512;display: inline;    position: relative;
    top: 5px;height: 16px;width: 16px;fill: #2D3092;" version="1.1" viewBox="0 0 512 512" width="512px"
                                     xml:space="preserve"><style type="text/css">

                                        .st0 {
                                            display: inline;
                                        }

                                        .st1 {
                                            fill: none;
                                            stroke: #000000;
                                            stroke-width: 16;
                                            stroke-linecap: round;
                                            stroke-linejoin: round;
                                            stroke-miterlimit: 10;
                                        }

                                        .st2 {
                                            display: none;
                                        }

                                    </style>
                                    <g class="st2" id="layer">
                                        <g class="st0">
                                            <polyline class="st1"
                                                      points="411,256 260,407 411,256 260,105   "></polyline>
                                            <polyline class="st1"
                                                      points="252,256 101,407 252,256 101,105   "></polyline>
                                        </g>
                                    </g>
                                    <g id="layer_copy">
                                        <g>
                                            <path d="M260,415c-2.048,0-4.095-0.781-5.657-2.343C252.781,411.095,252,409.048,252,407s0.781-4.095,2.343-5.657L399.687,256    L254.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.314,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C264.095,414.219,262.048,415,260,415z"></path>
                                        </g>
                                        <g>
                                            <path d="M101,415c-2.047,0-4.095-0.781-5.657-2.343C93.781,411.095,93,409.048,93,407s0.781-4.095,2.343-5.657L240.686,256    L95.343,110.657c-3.125-3.124-3.125-8.189,0-11.313c3.124-3.124,8.189-3.124,11.313,0l151,151    c1.562,1.562,2.343,3.609,2.343,5.657c0,2.048-0.781,4.095-2.343,5.657l-151,151C105.095,414.219,103.047,415,101,415z"></path>
                                        </g>
                                    </g></svg>--}} <!-- defining icon -->

                            </a>
                        </div>
                    </div>

                    <!-- Tabs content -->
                    <div class="tab-content">
                        <div class="tab-pane fade has-padding active in" id="messages-fri">
                            <ul class="media-list" id="user-activity">


                            </ul>
                        </div>
                    </div>
                    <!-- /tabs content -->

                </div>
                {{--users activities--}}


                {{--online users --}}
                <div class="panel panel-flat user_active">
                    <div class="panel-heading">
                        <h6 class="panel-title"><span data-i18n="online_users"></span><a class="heading-elements-toggle"><i class="icon-more"></i></a>
                        </h6>
                        <div class="heading-elements">
                            <a class="btn icon-btn btn-default view_btn_sm legitRipple" href="#">
                                <span data-i18n="view_all"></span>
                           <!-- defining icon -->

                            </a>
                        </div>
                    </div>


                    <!-- Tabs -->
                    <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300"
                        style="
    display: none;
">
                        <li class="">
                            <a href="#messages-tue" class="text-size-small text-uppercase legitRipple" data-toggle="tab"
                               aria-expanded="false">
                                Tuesday
                            </a>
                        </li>

                        <li class="">
                            <a href="#messages-mon" class="text-size-small text-uppercase legitRipple" data-toggle="tab"
                               aria-expanded="false">
                                Monday
                            </a>
                        </li>

                        <li class="active">
                            <a href="#messages-fri" class="text-size-small text-uppercase legitRipple" data-toggle="tab"
                               aria-expanded="true">
                                Friday
                            </a>
                        </li>
                    </ul>
                    <!-- /tabs -->


                    {{--online users --}}
                    <div class="tab-content">
                        <div class="tab-pane fade has-padding" id="messages-tue">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="/assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                        <span class="badge bg-danger-400 media-badge">8</span>
                                    </div>

                                    <div class="media-body">
                                        <a href="#">
                                            James Alexander
                                            <span class="media-annotation pull-right">14:58</span>
                                        </a>

                                        <span class="display-block text-muted">The constitutionally inventoried precariously...</span>
                                    </div>
                                </li>

                            </ul>
                        </div>

                        <div class="tab-pane fade has-padding active in" id="messages-fri">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{cdn_userpic()}}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#">
                                            Owen Stretch
                                            <span class="media-annotation pull-right"><p class="on_cir"><i
                                                            class=" icon-circle2"></i></p>
                                            <p class="on_p"><span data-i18n="online"></span></p>
                                            </span>
                                        </a>

                                        <span class="display-block text-muted">Tardy rattlesnake seal raptly earthworm...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{--online users --}}
            </div>
        </div>
        <!-- Dashboard content -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->

<script>

    var userActivitiesAjax        = "{!! route('userActivitiesAjax' ) !!}";
    var limitedUserActivitiesAjax = "{!! route('limitedUserActivitiesAjax' ) !!}";
    var needsMySignatureAjax      = "{!! route('needsMySignatureAjax' ) !!}";
    var waitingFromOthersSignatureAjax = "{!! route('waitingFromOthersSignatureAjax' ) !!}";
    var cdn_userpic               = "{!! cdn_userpic() !!}";
    var cdn_user                  = "{!! cdn_user() !!}";
    var document_route            = "{!! url('document' ) !!}";

</script>
