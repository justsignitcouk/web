
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($company['id']))
            <h5 class="panel-title">{{ __("companies.add_company") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 
	        <div class="col-md-6">

                {{ csrf_field() }}

                <meta name="csrf-token" content="{{ csrf_token() }}">

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.name") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="name"
		                   class="form-control"
		                   placeholder='{{ __("common.name") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('name') }}"
			                    @else
			                    	value="{{ $company['name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('name') }}"
		                   @endif >

	                @if ($errors->has('name'))
		                <font color="red">{{ $errors->first('name') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("companies.license_no") }}
			              :
		            </label>

                    <input type="text"
		                   name="license_no"
		                   class="form-control"
		                   placeholder='{{ __("companies.license_no") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('license_no') }}"
			                    @else
			                    	value="{{ $company['license_no'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('license_no') }}"
		                   @endif >

	                @if ($errors->has('license_no'))
		                <font color="red">{{ $errors->first('license_no') }}</font>
		        	@endif

		        </div>
		        
		    </div>

		    <div class="col-md-6">
            	<div class="form-group">
		            
		            <label>
			            {{ __("common.slug") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="slug"
		                   class="form-control"
		                   placeholder='{{ __("common.slug") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('slug') }}"
			                    @else
			                    	value="{{ $company['slug'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('slug') }}"
		                   @endif >

	                @if ($errors->has('slug'))
		                <font color="red">{{ $errors->first('slug') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">

		            <label>
			            {{ __("companies.logo") }}
			             :
		            </label>

		        	<div>
	                    <input type="file" name="logo" class="pull-left">
	                    
	                    @if(!empty($company['id']))
							<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	                    @endif

                    </div>

					@if ($errors->has('profile_image'))
		                <font color="red">{{ $errors->first('profile_image') }}</font>
		        	@endif
		        </div>
		    </div>
		   
	    </div>
	    @if(empty($active_edit))
        <hr/>
	    <div class="row"> 
	        <div class="col-md-6">

		        @if(empty($company['id']))
	            <h5 class="panel-title">
	            {{ __("branches.add_branch") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a>
	            </h5>
	            @endif
	            </br>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.branch_name") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="branch_name"
		                   class="form-control"
		                   placeholder='{{ __("branches.branch_name") }}'
		                   @if(!empty($company['branch_name']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_name') }}"
			                    @else
			                    	value="{{ $company['branch_name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_name') }}"
		                   @endif >

	                @if ($errors->has('branch_name'))
		                <font color="red">{{ $errors->first('branch_name') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.license_no") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="branch_license_no"
		                   class="form-control"
		                   placeholder='{{ __("branches.license_no") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_license_no') }}"
			                    @else
			                    	value="{{ $company['branch_license_no'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_license_no') }}"
		                   @endif >

	                @if ($errors->has('branch_license_no'))
		                <font color="red">{{ $errors->first('branch_license_no') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.address") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="branch_address"
		                   class="form-control"
		                   placeholder='{{ __("branches.address") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_address') }}"
			                    @else
			                    	value="{{ $company['branch_address'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_address') }}"
		                   @endif >

	                @if ($errors->has('branch_address'))
		                <font color="red">{{ $errors->first('branch_address') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.pobox") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="branch_pobox"
		                   class="form-control"
		                   placeholder='{{ __("branches.pobox") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_pobox') }}"
			                    @else
			                    	value="{{ $company['branch_pobox'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_pobox') }}"
		                   @endif >

	                @if ($errors->has('branch_pobox'))
		                <font color="red">{{ $errors->first('branch_pobox') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.phone") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="branch_phone"
		                   class="form-control"
		                   placeholder='{{ __("branches.phone") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_phone') }}"
			                    @else
			                    	value="{{ $company['branch_phone'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_phone') }}"
		                   @endif >

	                @if ($errors->has('branch_phone'))
		                <font color="red">{{ $errors->first('branch_phone') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.fax") }}
			              :
		            </label>

                    <input type="text"
		                   name="branch_fax"
		                   class="form-control"
		                   placeholder='{{ __("branches.fax") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_fax') }}"
			                    @else
			                    	value="{{ $company['branch_fax'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_fax') }}"
		                   @endif >

	                @if ($errors->has('branch_fax'))
		                <font color="red">{{ $errors->first('branch_fax') }}</font>
		        	@endif

		        </div>

		        
		    </div>
            <br/><br/>
		    <div class="col-md-6">

            	<div class="form-group">
		            
		            <label>
			            {{ __("branches.email") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="email"
		                   name="branch_email"
		                   class="form-control"
		                   placeholder='{{ __("branches.email") }}'
		                   @if(!empty($company['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('branch_email') }}"
			                    @else
			                    	value="{{ $company['branch_email'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('branch_email') }}"
		                   @endif >

	                @if ($errors->has('branch_email'))
		                <font color="red">{{ $errors->first('branch_email') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("branches.branche_type") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <select  class="col-md-12"  name="branch_type">  
                         <option disabled selected value>
                         	{{ __("common.select") }}
                         </option>
			        	@foreach($branchTypes as $branchType)
                            
                        
                            @if(count($errors) > 0)

                            <option
                            @if(old('branch_type')==$branchType['id'])
								selected
                            @endif
		                    value="{{$branchType['id']}}"
		                    >
		                    {{$branchType['name']}}
		                    </option>

		                    @elseif(!empty($branch['branch_type']) && $branch['branch_type_id'] == $branchType['id'])

		                    <option selected
		                    value="{{$branch['branch_type']}}" >
		                    {{$branchType['name']}}
		                    </option>

                            @else

	                        <option 
		                    value="{{$branchType['id']}}" >
		                    {{$branchType['name']}}
		                    </option>

		                    @endif

                        @endforeach
		        	</select>
                    <br/>
	                @if ($errors->has('branch_type'))
		                <font color="red">{{ $errors->first('branch_type') }}</font>
		        	@endif

		        </div>

		        <br/><br/>
		        <div class="form-group">
		            
		            <label>
			            {{ __("common.country") }}
			              <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:
		            </label>

                    <select  class="col-md-12"  name="branch_country_id"> 
                        <option disabled selected value>
                         	{{ __("common.select") }}
                        </option> 
			        	@foreach($countries as $countriy)
                           
                            @if(count($errors) > 0)
                            <option 
                            @if(old('branch_country_id')==$countriy->id)
								selected 
                            @endif
		                    value="{{$countriy->id}}" 
		                    >
		                    {{$countriy->name}}
		                    </option>
		                    @elseif(!empty($branch['country_id']) && $branch['country_id'] == $countriy->id)

		                    <option selected
		                    value="{{$countriy->id}}" >
		                    {{$countriy->name}}
		                    </option>

                            @else
	                        <option 
		                    value="{{$countriy->id}}" >
		                    {{$countriy->name}}
		                    </option>
		                    @endif
                        @endforeach
		        	</select>
					<br/>
	                @if ($errors->has('branch_country_id'))
		                <font color="red">{{ $errors->first('branch_country_id') }}</font>
		        	@endif

		        </div>
                <br/><br/><br/>
		        <div class="form-group">
		            
		            <label>
			            {{ __("common.main") }} :    
		            </label>

                    <input type="checkbox"
		                   name="is_main"
		                   class="js-switch"

					       @if(count($errors) > 0)

								@if(old('is_main')== true)
						        checked
						        @endif

						   @endif

		                   @if(!empty($company['branch_is_main']) && $company['branch_is_main'] == 1)
		                    checked
		                   @else

		                   @endif >

	                @if ($errors->has('branch_is_main'))
		                <font color="red">{{ $errors->first('branch_is_main') }}</font>
		        	@endif

		        </div>
                <br/>
		        <div class="form-group">

		            <label>
			            {{ __("common.logo") }}
			             @if(!empty($company['id']))
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>  
			             @endif
			             :
		            </label>

		        	<div>
	                    <input type="file" name="branch_logo" class="pull-left">
	                    
	                    @if(!empty($company['id']))
							<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	                    @endif
                    </div>

					@if ($errors->has('branch_logo'))
		                <font color="red">{{ $errors->first('branch_logo') }}</font>
		        	@endif
		        </div>
		              
		    </div>
		   
	    </div>
	    @endif

	    <div class="row col-md-12">

         	{{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

            @if(!empty($company["id"]))
            @can('allowed-permission', ['companies-delete','companies-delete'])
            <div class="btn btn-danger pull-right"   class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_theme_danger">{{__("common.delete")}}
            </div>
            @endcan
            @endif
           

	    </div>    
    </div>
</div>

@if(!empty($company["id"]))
<div id="modal_theme_danger" class="modal fade delete-caution" >
 <input id='company-data' type="hidden" data-id='{{$company["id"]}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("companies.delete_company")}}
				</p>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
				<button type="button" class="btn btn-danger" id='delet-role'>
				{{__("common.delete")}}
				</button>
			</div>
		</div>
	</div>
</div></div>
@endif
<div id="modal_theme_danger1" class="modal fade cant-delete">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("companies.cant_delete_company")}}
				</p>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    var elem = document.querySelector('.js-switch');
	var init = new Switchery(elem);

	$('#delet-role').click(function() {
        var id = $('#company-data').data("id");
        var _token = $('input[name="_token"]').val();

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('companies' ) !!}" + "/" + id +'/delete',
            data: { _token : _token },
	        success: function( msg ) {

                if(msg == 'cant'){

                    $('.delete-caution').modal('hide');
                     
					$('.cant-delete').modal('show');
 
	        	}else if(msg == 'can'){

                	location.replace("/companies");
	        	}

	        },
	        error: function( data ) {

	        }
	    });
     });

</script>
