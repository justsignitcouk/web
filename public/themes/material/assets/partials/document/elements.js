$(document).ready(function(){

    $('#document_date_input').focus(function() {
        $('.document_date').addClass('edit_dotted');
    }).blur(function() {
        $('.document_date').removeClass('edit_dotted');
    });



    $('#document_date_input').on('change',function(){

        var val = $(this).val();
        if(val!=''){
            $('.document_date').html('<span style="display: inline-block;float:left">Date :</span>   <span class="document_date_text">' + val+ '</span>' );
        }else{
            $('.document_date').html('');
        }

    });


    $('#document_number_input').focus(function() {
        $('.document_number').addClass('edit_dotted');
    }).blur(function() {
        $('.document_number').removeClass('edit_dotted');
    });


    $('#document_number_input').on('keyup',function(){
        var val = $(this).val();
        if(val!=''){
            $('.document_number').html('<span style="display: inline-block;float:left">Document Reference :</span>   <span class="document_number_text">' + val+ '</span>' );
        }else{
            $('.document_number').html('');
        }


    });

    $('#title_input').focus(function() {
        $('.subject_page').addClass('edit_dotted');
    }).blur(function() {
        $('.subject_page').removeClass('edit_dotted');
    });

    $('#title_input').on('keyup',function(){
        var val = $(this).val();

        if(val != ''){
            $('.subject_page').html( 'Title : ' + val );
        }else{
            $('.subject_page').html('' );
        }


    });

    $(document).on( 'keyup', '.subject_page', function() {
        var val = $(this).text();
        val = val.replace('Title : ','');
        $('#title_input').val(val);
    });



});