<ul class="nav nav-tabs nav-justified">


        
			<li class="@if (!empty($active_profile) &&  $active_profile == 1)) {{'active'}} @endif">
				<a href='{{url("/admin/users/$user[id]/profile")}}'>
				{{ __("common.profile") }}
				</a>
			</li>





			<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
				<a href='{{url("/admin/users/$user[id]/edit")}}' >
					{{ __("common.edit") }}
				</a>
			</li>






			<li class="@if (!empty($active_roles) &&  $active_roles == 1)) {{'active'}} @endif">
				<a href='{{url("/admin/users/$user[id]/roles")}}' >
					{{ __("common.roles") }}
				</a>
			</li>





	<li class="@if (!empty($active_permissions) &&  $active_permissions == 1)) {{'active'}} @endif">
		<a href='{{url("/admin/users/$user[id]/permissions")}}' >
			{{ __("common.permissions") }}
		</a>
	</li>


	<li class="@if (!empty($active_plans) &&  $active_plans == 1)) {{'active'}} @endif">
		<a href='{{url("/admin/users/$user[id]/plans")}}' >
			{{ __("common.plans") }}
		</a>
	</li>





</ul>