<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\File;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Branch
 */
class Branch extends Model
{
    use SoftDeletes;
    
    /*define variables*/
    protected $table = 'branches';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [
        'name',
        'address',
        'pobox',
        'phone',
        'fax',
        'email',
        'is_main',
        'license_no',
        'branch_type_id',
        'country_id',
        'company_id',
        'logo'
    ];
    

    /*relationships*/
    public function Logo(){
        return $this->hasOne('App\Models\File', 'id', 'logo');
    }
    public function Country(){
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }
    public function BranchType(){
        return $this->hasOne('App\Models\BranchType', 'id', 'branch_type_id');
    }
    public function Departments(){
       return $this->hasMany('App\Models\Department');
    }
    public function Company(){

        return $this->belongsTo('App\Models\Company');
    }

    /*new branch*/
    public function storeBranch($branchData){

        $branch = new Branch;
      
        //store branch data
        $branch->name = $branchData->name;
        $branch->email = $branchData->email;
        $branch->company_id = $branchData->company_id;
        $branch->license_no = $branchData->license_no;
        $branch->branch_type_id = $branchData->branch_type;
        $branch->address = $branchData->address;
        $branch->country_id = $branchData->country_id;
        $branch->pobox = $branchData->pobox;
        $branch->phone = $branchData->phone;
        $branch->fax = $branchData->fax;
        $branchData->is_main == null ? $branch->is_main = 0 : $branch->is_main = 1;

        if(!empty($branchData->logo)){
            $nImageProfile = File::put($branchData->logo);
            $branch->logo  = $nImageProfile;
        }

        if(!empty($branchData->branch_logo)){
            $nImageProfile = File::put($branchData->branch_logo);
            $branch->logo  = $nImageProfile;
        }

        $branch->save();
        return true ;
    }

    public function updateBranch($branchData , $id){

        $branch = Branch::find($id);

        //update branch data
        $branch->name = $branchData->name;
        $branch->email = $branchData->email;
        $branch->company_id = $branchData->company_id;
        $branch->license_no = $branchData->license_no;
        $branch->branch_type_id = $branchData->branche_type;
        $branch->address = $branchData->address;
        $branch->country_id = $branchData->country_id;
        $branch->pobox = $branchData->pobox;
        $branch->phone = $branchData->phone;
        $branch->fax = $branchData->fax;
        $branchData->is_main == null ? $branch->is_main = 0 : $branch->is_main = 1;
        
        if(!empty($branchData->logo)){
            $nImageProfile = File::put($branchData->logo);
            $branch->logo  = $nImageProfile;
        }
        

        $branch->save();
        return true ;
    }

        
}