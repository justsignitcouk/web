<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'=>'تسجيل الدخول',
    'remember'=>'تذكر',
    'next'=>'التالي',
    'forgot'=>'هل نسيت كلمة السر',
    'need_help'=>'المساعدة ؟',
    'or_sign_with'=>'أو تسجيل الدخول عبر',
    'dont_account'=>'ليس لديك حساب ؟',
    'sign_up'=>'سجل',
    'submit'=>'تأكيد',
    'password'=>'كلمة السر',
    'email_or_phone'=>'البريد الالكتروني او رقم الهاتف',
    'all_fields_required'=>'جميع الحقول مطلوبة ',
    'your_credentials'=>'شهاداتك',
    'user_name'=>'اسم المستخدم',
    'first_name'=>'الاسم الاول ',
    'last_name'=>'الاسم الاخير',
    'repeat_password'=>'اعد كتابة كلمة السر',
    'email'=>'البريد الالكتروني',
    'create_account'=>'انشاء حساب',
    'all_field_required'=>'جميع الحقول مطلوبة',
    'password_confirmation'=>'تأكيد كلمة السر',
    'mobile'=>'موبايل',
    'fill_password'=>'يرجى كتابة كلمة السر',
    'wrong_password'=>'كلمة السر غير صحيحة',
    'mail_or_phone_error'=>'يرجى كتابة البريد الالكتروني أو رقم الهاتف',
    'mail_or_phone_not_exist'=>'بريد الكتروني غير موجود',
    'my_compose'=>'My Compose',
    'reset_password'=>'اعادة تعيين كلمة السر',
    'your_email'=>'بريدك الالكتروني',
    'reset'=>'اعادة تعيين',
    'reset_password_message'=>'سيصلك ايميل لاعادة تعيين كلمة السر الرجاء الضغط على الرابط',
    'reset_password_message_worong'=>'نعتذر عنوان البريد الالكتروني خاطئ',
    'contact_us'=>'اتصل بنا',
    'your_name'=>'الاسم',
    'your_message'=>'رسالتك',
    'wrong_phone_email'=>'خطأ في الايميل او رقم الهاتف',
    'title'=>'العنوان',
    'need_help_post_success'=> 'وصلتنا رسالتك , سوف نبعث الرد في اقرب وقت',
    'cancle'=>'انهاء',
    'email_phone'=>'البريد الالكتروني او رقم الهاتف',
    'inactive_user'=>'هذا الحساب غير نشط',

];
