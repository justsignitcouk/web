<?php

namespace App\Services;

class UserActivities extends \App\Services\Services
{



    public static function getUserActivities($limit , $account_id , $lang){


        $oServices = Parent::getInstance();

        $url   = 'user_activities/all' ;
        $query = ['account_id' => $account_id,'limit'=>$limit,'lang'=>$lang] ;

        try{

            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->setQuery($query)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            return $exception->getMessage() ;
        }

        return json_decode($oClient,true);


    }


}