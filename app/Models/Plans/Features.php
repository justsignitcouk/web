<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class Features extends Model
{

    use SoftDeletes;
    protected $table = 'prices_features';

    protected $fillable = [
        'key', 'title', 'default_value', 'custom_icon_class', 'route'
    ];


}
