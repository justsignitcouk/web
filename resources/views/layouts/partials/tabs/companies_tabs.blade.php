
<ul class="nav nav-tabs nav-justified">


		<li class="@if (!empty($active_show) &&  $active_show == 1)) {{'active'}} @endif">
			<a href='{{url("/admin/companies/$company[id]")}}'>
			{{ __("common.show") }}
			</a>
		</li>

	

		<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
			<a href='{{url("/admin/companies/$company[id]/edit")}}' >
				{{ __("common.edit") }}
			</a>
		</li>



		<li class="@if (!empty($active_branches) &&  $active_branches == 1)) {{'active'}} @endif">
			<a href='{{url("/admin/companies/$company[id]/branches")}}' >
				{{ __("branches.branches") }}
			</a>
		</li>

	
</ul>