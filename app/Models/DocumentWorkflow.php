<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentWorkflow
 */
class DocumentWorkflow extends Model
{
    protected $table = 'document_workflow';

    public $timestamps = false;

    protected $fillable = [
        'workflow_id',
        'document_id'
    ];

    protected $guarded = [];

        
}