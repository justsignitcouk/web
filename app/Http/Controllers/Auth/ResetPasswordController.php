<?php

namespace App\Http\Controllers\Auth;

use Facuz\Theme\Facades\Theme;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Services\Register;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

        /**
         * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request)
    {
        $data = $request->all();
        $checkResetCode = Register::verfiyResetCode($data);
        if($checkResetCode['status']){
            Theme::layout('auth');
            return Theme::view('auth.set_password',['verify_token' => $request->get('code') ]);
        }

        return redirect(route('login'));

    }

    public function resetPassword(UserResetPasswordRequest $request){
        $data = $request->all();
        $client = new \App\Services\Auth();
        $response = $client->resetPassword($data);

        if ($response['status']){
            $request->session()->push('reset_password_message', 'Password Changed successfully!<br/> You can login now');
            return redirect(route('login'));
        }else{
            $request->session()->push('reset_password_message', $response['message'] );
            return redirect(route('forgotPassword'));
        }
    }

}
