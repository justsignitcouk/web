<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentCategory
 */
class DocumentCategory extends Model
{
    protected $table = 'document_categories';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'parent_category_id'
    ];

    protected $guarded = [];

        
}