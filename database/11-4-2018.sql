/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.21-0ubuntu0.17.10.1 : Database - mycompose
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `document_workflow_action_history` */

DROP TABLE IF EXISTS `document_workflow_action_history`;

CREATE TABLE `document_workflow_action_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `document_id` int(11) unsigned NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `stage_id` int(11) unsigned NOT NULL,
  `contact_id` int(11) NOT NULL,
  `action_date` datetime DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_document_workflow_action_history` (`document_id`),
  KEY `FK_document_workflow_action_history_stage` (`stage_id`),
  CONSTRAINT `FK_document_workflow_action_history` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`),
  CONSTRAINT `FK_document_workflow_action_history_stage` FOREIGN KEY (`stage_id`) REFERENCES `wf_stage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `document_workflow_action_history` */

insert  into `document_workflow_action_history`(`id`,`document_id`,`action_id`,`stage_id`,`contact_id`,`action_date`,`note`,`created_at`,`updated_at`,`deleted_at`) values (1,4,2,2,1,'2018-04-08 15:12:14',NULL,'2018-04-08 15:12:14',NULL,NULL),(2,4,2,2,1,'2018-04-08 15:12:21',NULL,'2018-04-08 15:12:21',NULL,NULL),(3,4,2,2,1,'2018-04-08 15:32:19',NULL,'2018-04-08 15:32:19',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
