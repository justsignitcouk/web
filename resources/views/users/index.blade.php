@extends('layouts.admin')

@section('content')
    <!-- Ajax sourced data -->
    <div class="panel panel-flat">

        @include('layouts/partials/message')

        <div class="panel-heading">
            <h5 class="panel-title">{{ __("users.titles.manage_users") }}</h5>
            <div class="heading-elements hidden">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

              <a href="{{ url('/admin/users/create') }}" class="btn btn-primary" id="bootbox_form">{{ __("users.titles.add_user") }}</a>

            <table id="users-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __("users.username") }}</th>
                        <th>{{ __("users.first_name") }}</th>
                        <th>{{ __("users.last_name") }}</th>
                        <th>{{ __("users.email") }}</th>
                        <th>{{ __("users.mobile") }}</th>
                        <th>{{ __("common.activated") }}</th>
                        <th>{{ __("common.created_at") }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /ajax sourced data -->

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });


        var table = $('#users-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.users.data') !!}',
            columns: [
                { data: 'username', name: 'username' },
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'email', name: 'email' },
                { data: 'mobile', name: 'mobile' },
                { data: 'activated', render: function(data){
                    if(data == 1)return 'Yes' ; else return 'No'  ;
                } },
                { data: 'created_at', name: 'created_at' },
            ]
        });



        $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/admin/users/' + id +'/profile';

                });


    });
</script>

@endpush

