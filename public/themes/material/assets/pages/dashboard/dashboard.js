$(document).ready(function(){


    var _token = $('meta[name="csrf-token"]').attr('content') ;


    (function () {

        $.ajax({

            method: "POST",
            url:  limitedUserActivitiesAjax  ,
            data: { _token : _token },
            success: function( data ) {


                var html ='<div>' ;
                $.each(data, function (k, v) {

                    html +=
                        '<li class="media" >'+
                        '<div class="media-body">'+
                        '<a href="document/'+v.document_id+'">'+
                         v.message+
                        '</a>'+
                        ' '+
                        '<span>'+ moment(v.created_at.date).fromNow()+'</span>'+
                        '</div>'+
                        '</li>'


                });

                html +=  '</div>';
                $('#user-activity').html(html);


            },
            error: function( data ) {



            }
        });

    })();

    (function () {

        $.ajax({

            method: "POST",
            url:  needsMySignatureAjax  ,
            data: { _token : _token },
            success: function( data ) {

                var html ='' ;

                html +=
                    '<td>'+
                    '<span>Now waiting signatures</span>'+
                    '</td>';
                $('#needs_my_signature').append(html);


                    $.each(data.document, function( index, value ) {
                        $('#needs_my_signature').empty();

                        var wich_signed_counter = 0 ;
                        $.each(value.signer_account, function(i,v) {

                            if(v.is_sign == 1){

                                wich_signed_counter++ ;

                            }
                        });

                            var all_signers = value.signer_account.length ;
                            var wich_signed = wich_signed_counter ;
                            var percentage  = (wich_signed/all_signers)*100 ;

                            html +=
                                '<tr>'+
                                '<td class="w_33">'+
                                '<div class="media-left media-middle">'+
                                '<a href="'+document_route+'/'+value.id+'">'+
                                '<div><img src="'+cdn_user+'" class="img-circle img-xs" alt=""></div>'+
                                '</div>'+
                                '<div class="media-left">'+
                                '<span>'+value.text_content+'</span>'+
                                '<div class="text-muted text-size-small">'+
                                '<span  class="text-default text-semibold">'+value.text_content+'</span>\n'+
                                '</div>'+
                                '</a>'+
                                '</div>'+
                                '</td>'+
                                '<td class="user_td w_33">'+
                                '<span>'+wich_signed+'</span>'+
                                '<span class="i icon-user"></span>'+
                                '<span>'+all_signers+'</span>'+
                                '</td>'+
                                '<td class="prog_td w_33">'+
                                '<span data-i18n="progress">progress: </span>'+
                                '<div class="progress">'+
                                '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+percentage+'%">\n'+
                                percentage+'%'+
                                '</div>'+
                                '</div>'+
                                '<span>   </span>'+
                                '<span>'+moment(value.created_at).fromNow()+'</span>'+
                                '</td>'+
                            '</tr>' ;

                        html +=  '';
                        $('#needs_my_signature').append(html);

                    });





            },
            error: function( data ) {



            }
        });

    })();

    (function () {

        $.ajax({

            method: "POST",
            url:  waitingFromOthersSignatureAjax  ,
            data: { _token : _token },
            success: function( data ) {

                var html ='' ;

                html +=
                    '<td>'+
                    '<span>Now waiting signatures</span>'+
                    '</td>';
                $('#waiting_from_others').append(html);

                $.each(data.data.document, function( index, value ) {

                    $('#waiting_from_others').empty();

                    var wich_signed_counter = 0 ;

                    $.each(value.signer_account, function(i,v) {
                        if(v.is_sign == 1){
                            wich_signed_counter++ ;
                        }
                    });

                        var all_signers = value.signer_account.length ;
                        var wich_signed = wich_signed_counter ;
                        var percentage  = (wich_signed/all_signers)*100 ;

                        html +=
                            '<tr>'+
                            '<td class="w_33">'+
                            '<div class="media-left media-middle">'+
                            '<a href="'+document_route+'/'+value.id+'">'+
                            '<div><img src="'+cdn_user+'" class="img-circle img-xs" alt=""></div>'+
                            '</div>'+
                            '<div class="media-left">'+
                            '<span>'+value.text_content+'</span>'+
                            '<div class="text-muted text-size-small">'+
                            '<span  class="text-default text-semibold">'+value.text_content+'</span>\n'+
                            '</div>'+
                            '</a>'+
                            '</div>'+
                            '</td>'+
                            '<td class="user_td w_33">'+
                            '<span>'+wich_signed+'</span>'+
                            '<span class="i icon-user"></span>'+
                            '<span>'+all_signers+'</span>'+
                            '</td>'+
                            '<td class="prog_td w_33">'+
                            '<span>progress: </span>'+
                            '<div class="progress">'+
                            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+percentage+'%">\n'+
                            percentage+'%'+
                            '</div>'+
                            '</div>'+
                            '<span>   </span>'+
                            '<span>'+moment(value.created_at).fromNow()+'</span>'+
                            '</td>'+
                            '</tr>';

                    html +=  '';
                    $('#waiting_from_others').append(html);

                });


            },
            error: function( data ) {

            }
        });

    })();

    (function () {

        $.ajax({

            method: "POST",
            url:  waitingFromOthersSignatureAjax  ,
            data: { _token : _token },
            success: function( data ) {

                var html ='' ;

                html +=
                    '<td>'+
                    '<span>Now waiting signatures</span>'+
                    '</td>';
                $('#waiting_from_others').append(html);

                $.each(data.data.document, function( index, value ) {

                    $('#waiting_from_others').empty();

                    var wich_signed_counter = 0 ;

                    $.each(value.signer_account, function(i,v) {
                        if(v.is_sign == 1){
                            wich_signed_counter++ ;
                        }
                    });

                    var all_signers = value.signer_account.length ;
                    var wich_signed = wich_signed_counter ;
                    var percentage  = (wich_signed/all_signers)*100 ;

                    html +=
                        '<tr>'+
                        '<td class="w_33">'+
                        '<div class="media-left media-middle">'+
                        '<a href="'+document_route+'/'+value.id+'">'+
                        '<div><img src="'+cdn_user+'" class="img-circle img-xs" alt=""></div>'+
                        '</div>'+
                        '<div class="media-left">'+
                        '<span>'+value.text_content+'</span>'+
                        '<div class="text-muted text-size-small">'+
                        '<span  class="text-default text-semibold">'+value.text_content+'</span>\n'+
                        '</div>'+
                        '</a>'+
                        '</div>'+
                        '</td>'+
                        '<td class="user_td w_33">'+
                        '<span>'+wich_signed+'</span>'+
                        '<span class="i icon-user"></span>'+
                        '<span>'+all_signers+'</span>'+
                        '</td>'+
                        '<td class="prog_td w_33">'+
                        '<span>progress: </span>'+
                        '<div class="progress">'+
                        '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+percentage+'%">\n'+
                        percentage+'%'+
                        '</div>'+
                        '</div>'+
                        '<span>   </span>'+
                        '<span>'+moment(value.created_at).fromNow()+'</span>'+
                        '</td>'+
                        '</tr>';

                    html +=  '';
                    $('#waiting_from_others').append(html);

                });


            },
            error: function( data ) {

            }
        });

    })();


});



