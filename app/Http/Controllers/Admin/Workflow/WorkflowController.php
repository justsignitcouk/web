<?php

namespace App\Http\Controllers\Admin\Workflow;

use App\Http\Requests\Workflow\WorkflowRequest;
use App\Models\Company;
use App\Models\Workflow\Stage;
use App\Models\Workflow\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workflow\Workflow;
use App\Models\Workflow\Actions;
use App\Models\Account;
use Yajra\Datatables\Datatables;
use Validator;
use Gate;


class WorkflowController extends Controller
{

    public function __construct()
    {
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oCompoaines = Company::all();


        return view('workflow.index');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $oCompanies = Company::pluck('name', 'id')->all();

        return view('workflow.create', ['oCompanies' => $oCompanies]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        Validator::make($request->all(), [
            'workflow_name' => 'required|max:255',
            'workflow_company' => 'required|max:255',
        ])->validate();
        $oWorkflow = new Workflow();
        $oWorkflow->name = $request->get('workflow_name');
        $oWorkflow->description = $request->get('workflow_description');
        $oWorkflow->created_by = currentUserID();
        $oWorkflow->company_id = $request->get('workflow_company');
        if ($oWorkflow->save()) {
            #start stage
            $oStage = new \App\Models\Stage();
            $oStage->workflow_id = $oWorkflow->id;
            $oStage->name = 'start';
            $oStage->action_type = 'START';
            $oStage->active = 1;
            $oStage->sequence = 1;
            $oStage->created_by = 1;
            $oStage->save();

            #end stage
            $oStage = new \App\Models\Stage();
            $oStage->workflow_id = $oWorkflow->id;
            $oStage->name = 'end';
            $oStage->action_type = 'END';
            $oStage->active = 1;
            $oStage->sequence = 999999;
            $oStage->created_by = 1;
            $oStage->save();

            return redirect()->route('workflow.show', [$oWorkflow->id]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {

        if ($id != 0) {
            $oWorkflow = Workflow::with('Stages.StageState.State')->where(['id' => $id])->first();
            if (!$oWorkflow) {
                return redirect()->to(url('workflow'));
            }

            $oStates = State::pluck('name', 'id')->all();
            $aStages = $oWorkflow->Stages()->orderBy('sequence')->get();
            $aStagesInfo = [];
            foreach ($aStages as $oStage) {
                $aStagesInfo[$oStage->id] = $oStage->name;
            }
            $params = [
                'id' => $id,
                'oWorkflow' => $oWorkflow,
                'oStages' => $aStages,
                'aStagesInfo' => $aStagesInfo,
                'action_types' => Stage::getActionTypes(),
                'oStates' => $oStates,
                'aActions' => Actions::getSystemAction(),
                'aAccounts' => [],
            ];
            return view('workflow.view')->with($params);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('workflow.create')->with($params);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $oWorkflow = Workflow::find($id)->delete();
        if ($oWorkflow) {
            return response()->json([
                'id' => $id,
                'state' => 'success'
            ]);
        }
        return response()->json([
            'id' => $id,
            'state' => 'failed'
        ]);
    }

    public function datatables()
    {
        $workflows = Workflow::all();
        return Datatables::of($workflows->toArray())->make(true);


    }
}
