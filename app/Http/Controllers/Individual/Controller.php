<?php

namespace App\Http\Controllers\Individual;

use App\Services\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $oDefaultAccount = null ;
    public $oDefaultCompany = null ;
    public $user = null ;


    public function __construct()
    {
        view()->share('bShowMainSidebar',  true);
        view()->share('sSideBar',  'sidebar-normal');

        view()->share('sActiveMenu',  '');
        $oBadges = User::badges();
        view()->share('oBadges', $oBadges);

    }
}
