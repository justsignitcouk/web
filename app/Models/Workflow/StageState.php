<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class StageState extends Model
{
    protected $table = 'wf_stage_state';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'active'
    ];

    public function Stage()
    {
        return $this->belongsTo('App\Models\Workflow\Stage','stage_id','id');
    }

    public function State()
    {
        return $this->belongsTo('App\Models\Workflow\State', 'state_id', 'id');
    }

}
