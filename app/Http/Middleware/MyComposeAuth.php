<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

class MyComposeAuth
{

    protected $except = [
        'profile_requirements'
    ];

    public function handle($request, Closure $next)
    {

        updateUser();
        if (sizeof(aProfileRequirements()) > 0 && $request->route()->getName() != 'profile_requirements'
            && $request->route()->getName() != 'profile.uploadProfilePicture'
            && $request->route()->getName() != 'profile.deleteProfilePicture'
            && $request->route()->getName() != 'user.resendVerify'
            && $request->route()->getName() != 'user.verifyRegister') {

            return redirect(route('profile_requirements'));
        }

        if (session()->has('sToken')) {
            return $next($request);
        }

        throw new AuthenticationException('Unauthenticated.', array('code' => 401));
    }
}
