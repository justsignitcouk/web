<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentNotification
 */
class DocumentNotification extends Model
{
    protected $table = 'document_notification';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'notification_id'
    ];

    protected $guarded = [];

        
}