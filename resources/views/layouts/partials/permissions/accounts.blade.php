@foreach($accounts_permissions as $key => $account)
    <div class="form-check">
      <label class="account-check-label">
        <input class="account-check-input" type="checkbox" id="{{$account->key}}" name ="{{'accounts-'.$key}}" value="{{$account->id}}"

        @if(!empty($checked_user_permissions))
            @foreach($checked_user_permissions as $check)

                @if($check['permission_id'] == $account->id)
                checked
                @endif
         
            @endforeach
        @endif
       
        >
        {{$account->name}}
      </label>
    </div>
@endforeach