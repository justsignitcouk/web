$(document).ready(function(){
    var currentTime = new Date();
    var current_year = currentTime.getFullYear();
    var columns = [
        { data: 'id', name: 'id',className: "table-inbox-select-box details-button table-inbox-checkbox  rowlink-skip sel_row","render": function ( data, type, row, meta ) {
                return '<label class="checkbox-inline ">'+
                    '<input type="checkbox" class="styled  ' +data+ '" >'+
                    '</label><script>$(".styled, .multiselect-container input").uniform({radioClass: "choice"});</script>'


            } },
        { data: 'display_name', name: 'display_name',className: "table-inbox-name",responsivePriority: 2 },
        { data: 'description', name: 'description',className: "table-inbox-name",responsivePriority: 2 },
        { data: 'id', name: 'id',className:"text-center details-button","render": function ( data, type, row, meta ) {
                var html = '<ul class="icons-list">' +
                    '<li class="dropdown">' +
                    '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-menu9"></i>' +
                    '</a>' +
                    '<ul class="dropdown-menu dropdown-menu-right">' +
                    '<li><a href="#" data-toggle="modal" data-target="#details_table" data-id="' + data + '" class="details_button"><i class="icons-list"></i> <p data-i18n="Details">Details</p></a></li>' +
                    '</ul>' +
                    '</li>' +
                    '</ul>' ;
                return  html;
            } },
    ] ;

    $.extend( $.fn.dataTable.defaults, {

        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [ 0 ],
            checkboxes: {
                seletRow: true
            }
        }],
        dom: '<"datatable-header"fl ><""t><"datatable-footer"ip>',
        language: {
            search: '<span>' +  /*lang.search*/ 'search' +'</span> _INPUT_',
            searchPlaceholder: '',
            lengthMenu: '<span data-i18n="show">Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            loadingRecords: "<i class=\"icon-spinner3 spinner\"></i>",
            /* sInfo:         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
             Info: "show _START_to _END_from _TOTAL_entries "*/
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },


    });


    var t = $('#roles_table').dataTable({
        "ordering": false,
        "processing": true,
        "serverSide": true,
        'autoWidth': true,
        "ajax": {
            "url": getRoles,
            "type": 'POST',
            "data": ''
        },
        columns: columns,
        "createdRow": function (row, data, index) {
            $(row).data("id", data.id);

            if (data.is_read == false) {
                $(row).addClass('unread');
            }
        }


    });

    $('#roles_table').on('click','tr td',function(){
        $this = $(this);
        var table_id = $this.closest('table').attr('id');

        if(!$this.hasClass('details-button')) {
            var id = $this.parent().data('id') ;


                window.location = branch_details + '/' + id ;
                return true;

        }

    });

});