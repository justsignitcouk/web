


<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><a href="{{route('settings.index')}}"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Users list</h4>

            </div>

            <div class="heading-elements">

            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

<!-- Collapsible lists -->
<div class="row">
    <div class="col-md-12">

        <!-- Collapsible list -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"></h5>

            </div>

            <ul class="media-list media-list-linked">
                {{--<li class="media-header">Team leaders</li>--}}
                @foreach($aAccounts['accounts'] as $aAccount)
                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#user{{$aAccount['id']}}">
                        <div class="media-left"><img src="assets/images/demo/users/face1.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">{{ $aAccount['user']['first_name'] . ' ' . $aAccount['user']['last_name'] }}</div>
                            <span class="text-muted">{{ $aAccount['user']['email'] }}</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="user{{$aAccount['id']}}">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> {{ $aAccount['department']['name'] }}</li>
                                <li><i class="icon-user-tie position-left"></i>@foreach($aAccount['roles'] as $role) {{ $role['name'] }}@if(!$loop->last), @endif @endforeach</li>
                                <li><i class="icon-phone position-left"></i> {{ $aAccount['user']['mobile'] }}</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">{{ $aAccount['user']['email'] }}</a></li>
                                <li><i class="icon-mail5 position-left"></i> <a href="{{ route('settings.accounts.details',[ $aAccount['id'] ]) }}">Details</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                @endforeach
                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#jeremy">
                        <div class="media-left"><img src="assets/images/demo/users/face2.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Jeremy Victorino</div>
                            <span class="text-muted">Spotify</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="jeremy">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Hague</li>
                                <li><i class="icon-user-tie position-left"></i> Network engineer</li>
                                <li><i class="icon-phone position-left"></i> +1(389)384 9039</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">jeremy@victorino.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#margo">
                        <div class="media-left"><img src="assets/images/demo/users/face3.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Margo Baker</div>
                            <span class="text-muted">eBay</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="margo">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Dublin</li>
                                <li><i class="icon-user-tie position-left"></i> Marketing expert</li>
                                <li><i class="icon-phone position-left"></i> +3(431)589 3889</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">margo@baker.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#monica">
                        <div class="media-left"><img src="assets/images/demo/users/face4.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Monica Smith</div>
                            <span class="text-muted">Amazon</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="monica">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Paris</li>
                                <li><i class="icon-user-tie position-left"></i> Web master</li>
                                <li><i class="icon-phone position-left"></i> +5(342)543 2367</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">monica@smith.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media-header">Office staff</li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#bastian">
                        <div class="media-left"><img src="assets/images/demo/users/face5.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Bastian Miller</div>
                            <span class="text-muted">Yahoo</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="bastian">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> New York</li>
                                <li><i class="icon-user-tie position-left"></i> Lead developer</li>
                                <li><i class="icon-phone position-left"></i> +1(234)675 3904</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">bastian@miller.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#jordana">
                        <div class="media-left"><img src="assets/images/demo/users/face6.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Jordana Mills</div>
                            <span class="text-muted">Paypal</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="jordana">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Madrid</li>
                                <li><i class="icon-user-tie position-left"></i> UI/UX expert</li>
                                <li><i class="icon-phone position-left"></i> +2(564)234 9002</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">jordana@mills.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#buzz">
                        <div class="media-left"><img src="assets/images/demo/users/face7.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Buzz Brenson</div>
                            <span class="text-muted">Oracle</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="buzz">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Hamburg</li>
                                <li><i class="icon-user-tie position-left"></i> Engineer</li>
                                <li><i class="icon-phone position-left"></i> +4(234)543 2388</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">buzz@brenson.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#zachary">
                        <div class="media-left"><img src="assets/images/demo/users/face8.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Zachary Willson</div>
                            <span class="text-muted">Salesforce</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="zachary">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Berlin</li>
                                <li><i class="icon-user-tie position-left"></i> Accountant</li>
                                <li><i class="icon-phone position-left"></i> +4(231)653 3892</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">zachary@willson.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#william">
                        <div class="media-left"><img src="assets/images/demo/users/face9.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">William Miles</div>
                            <span class="text-muted">Bing</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="william">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Oslo</li>
                                <li><i class="icon-user-tie position-left"></i> Senior engineer</li>
                                <li><i class="icon-phone position-left"></i> +6(324)236 3689</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">william@miles.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media-header">Partners</li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#freddy">
                        <div class="media-left"><img src="assets/images/demo/users/face10.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Freddy Walden</div>
                            <span class="text-muted">Microsoft</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="freddy">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Amsterdam</li>
                                <li><i class="icon-user-tie position-left"></i> Accountant</li>
                                <li><i class="icon-phone position-left"></i> +3(234)653 5432</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">freddy@walden.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#dori">
                        <div class="media-left"><img src="assets/images/demo/users/face11.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Dori Laperriere</div>
                            <span class="text-muted">Google</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="dori">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> New York</li>
                                <li><i class="icon-user-tie position-left"></i> Usability expert</li>
                                <li><i class="icon-phone position-left"></i> +1(455)234 8994</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">dori@laperriere.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="media">
                    <div class="media-link cursor-pointer" data-toggle="collapse" data-target="#vanessa">
                        <div class="media-left"><img src="assets/images/demo/users/face12.jpg" class="img-circle img-md" alt=""></div>
                        <div class="media-body">
                            <div class="media-heading text-semibold">Vanessa Aurelius</div>
                            <span class="text-muted">Facebook</span>
                        </div>
                        <div class="media-right media-middle text-nowrap">
                            <i class="icon-menu7 display-block"></i>
                        </div>
                    </div>

                    <div class="collapse" id="vanessa">
                        <div class="contact-details">
                            <ul class="list-extended list-unstyled list-icons">
                                <li><i class="icon-pin position-left"></i> Rome</li>
                                <li><i class="icon-user-tie position-left"></i> Web developer</li>
                                <li><i class="icon-phone position-left"></i> +8(344)658 4598</li>
                                <li><i class="icon-mail5 position-left"></i> <a href="#">vanessa@aurelius.com</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /collapsible list -->