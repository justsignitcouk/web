<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\SocialMedia;


class SocialMediaController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');

    }



    public function redirectToFaceBookProvider(Request $request)
    {

        if ( $request->has('code')) {

            $socialMedia = new SocialMedia;
            try{

                $oUser = \Socialite::driver('facebook')->user();
                if($oUser->email == null){
                    return \Socialite::driver('facebook')->with(['auth_type' => 'rerequest'])->redirect();

                }

            }catch ( \Laravel\Socialite\Two\InvalidStateException  $e ){
                dd($e);
                return redirect()->route('login') ;
            }

            $checkMail = $socialMedia->checkMail($oUser->email);

            //do registration
            if ($checkMail == false) {
                $oRegister = $socialMedia->RegistByFaceBook($oUser);
            }

            $response = $socialMedia->LogInByFaceBook($oUser);

            if (isset($response['status']) && $response['status'] == 'success') {
                $sLoginType = session()->get('sLoginType');
                if ($sLoginType == 'user') {
                    return redirect()->route("home.home");
                }

                return redirect()->route("inbox.all");

            }

            return $response;

        }


        return \Socialite::driver('facebook')->redirect();

    }


    public function redirectToGoogleProvider()
    {

        if (Input::has('code')) {

            $socialMedia = new SocialMedia;

            try{
                $oUser = \Socialite::driver('google')->user();

                if($oUser->email == null){
                    return \Socialite::driver('google')->scopes(['openid', 'profile', 'email', \Google_Service_People::CONTACTS_READONLY])->with(['auth_type' => 'rerequest'])->redirect();

                }
            }catch ( \Laravel\Socialite\Two\InvalidStateException  $e ){
                return redirect()->route('login') ;
            }

            $checkMail = $socialMedia->checkMail($oUser->email);

            //do registration
            if ($checkMail == false) {

                $socialMedia->RegistByGoogle($oUser);

            }

            //do login
            $response = $socialMedia->LogInByGoogle($oUser);

            if (isset($response['status']) && $response['status'] == 'success') {
                session()->put('google_token', $oUser->token ) ;

                $sLoginType = session()->get('sLoginType');
                if ($sLoginType == 'user') {
                    return redirect()->route("home.home");
                }

                return redirect()->route("inbox.all");

            }

            return $response;

        }

        return \Socialite::driver('google')->scopes(['openid', 'profile', 'email', \Google_Service_People::CONTACTS_READONLY])->redirect();

    }


    public function redirectToLinkedinProvider()
    {


        if (Input::has('code')) {

            $socialMedia = new SocialMedia;

            try{
                $oUser = \Socialite::driver('linkedin')->user();
                if($oUser->email == null){
                    return \Socialite::driver('linkedin')->with(['auth_type' => 'rerequest'])->redirect();

                }
            }catch ( \Laravel\Socialite\Two\InvalidStateException  $e ){
                return redirect()->route('login') ;
            }

            $checkMail = $socialMedia->checkMail($oUser->email);

            //do registration
            if ($checkMail == false) {
                $socialMedia->RegistByLinkedin($oUser);
            }

            $response = $socialMedia->LogInByLinkedin($oUser);

            if (isset($response['status']) && $response['status'] == 'success') {

                $sLoginType = session()->get('sLoginType');
                if ($sLoginType == 'user') {
                    return redirect()->route("home.home");
                }

                return redirect()->route("inbox.all");

            }

            return $response;


        }

        return \Socialite::driver('linkedin')->redirect();

    }

    public function redirectToTwitterProvider()
    {

        if (Input::has('code')) {

            $socialMedia = new SocialMedia;

            $oUser = \Socialite::driver('twitter')->user();


            $aUserData = $socialMedia->userData($oUser->email);

            //do registration
            if ($aUserData['user_data'] == []) {

                $socialMedia->RegistByFaceBook($oUser);

                //do login
            } else {

                $socialMedia->LogInByFaceBook($aUserData['user_data'][0]['email'], $oUser->id);

            }

            return redirect('/login');

        }

        return \Socialite::driver('twitter')->redirect();

    }


}