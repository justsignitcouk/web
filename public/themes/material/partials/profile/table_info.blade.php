
{!! Theme::asset()->add([ ['validate', 'assets/components/validation/dist/jquery.validate.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['validate_additonal', 'assets/components/validation/dist/additional-methods.js',['validate']]]) !!}

{!! Theme::asset()->themePath()->add([ ['table_info', 'partials/profile/table_info.js', ['validate_additonal']]]) !!}
<div class="panel panel-flat">
    <div class="panel-body panel-accounts ">
    <div class="panel-heading">
        <h6 class="panel-title"><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
        </div>
    </div>

    <div class="panel-body">
        @if(session()->has('change_failed'))
            <div class="alert alert-danger">
                The current password is wrong
            </div>
        @endif
            @if(session()->has('change_successfully'))
                <div class="alert alert-success">
                    Password changed
                </div>
            @endif
        <form action="{{route('profile.change_password',[getUniqueName()])}}" class="form-validate-jquery" role="form" method="POST" id="change_password">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                        <label data-i18n="profile.current_password"></label>
                        <input type="password" id="current_password" name="current_password" data-i18n="[placeholder]profile.current_password" placeholder="" value="" class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <label data-i18n="profile.new_password"></label>
                            <input type="password" name="password_input" id="password_input" data-i18n="[placeholder]profile.new_password" placeholder="" class="form-control" value="" required="required">
                            <span class="help-block"></span>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <label data-i18n="profile.repeat_password"></label>
                        <input type="password" name="repeat_password_input" id="repeat_password_input" data-i18n="[placeholder]profile.repeat_password" placeholder="" class="form-control" value="">
                    </div>
                </div>
            </div>


            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple change_password_submit"><span data-i18n="save"></span> <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </form>
    </div>



</div></div>














@if(false)
<style>
    .left-side {
        display: inline-block;
        text-align: left;
    }

    .right-side {
        display: inline-block;
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><div class="panel-title" data-i18n="profile.Your emails"></div>
                <div class="heading-elements">
                    <span class="heading-text"><i class=" icon-add"></i> <a href="" class="add_email legitRipple" style="color:white" data-i18n="profile.Add email" data-toggle="modal" data-target="#add_email_modal"></a></span>
                </div>
            </div>
            <div class="panel-body">
                <table class="table text-nowrap">
                    <tr>
                        <td><b class="">{{ oUser()->email }}</b></td>
                        <td><a href="" class="edit-primary-email">Edit</a></td>
                    </tr>
                </table>

                <div class="edit_box edit_email_table hide">
                    <table class="table text-nowrap">
                    {{--@foreach( oContact()->emails as $aEmail)--}}
                        {{--<tr><td class="media-left media-middle">--}}
                            {{--@if($aEmail['verify'])--}}
                            {{--<input type="radio" name="primary_email" data-type="email"--}}
                                   {{--data-value="{{$aEmail['email'] }}"--}}
                                   {{--value="{{$aEmail['email'] }}" {{ $aEmail['is_primary'] ? "checked=checked" : "" }} />--}}
                        {{--@else--}}
                        {{--(<a data-i18n="profile.verify now">Verify now</a>)--}}
                        {{--@endif--}}
                            {{--</td><td>{{ $aEmail['email'] }} </td></tr>--}}
                    {{--@endforeach--}}
                    </table>
                    <div class="text-right">
                        <button class="btn btn-primary save_primary text-right" data-type="email">Save</button>
                        <button class="btn btn-default cancel_primary text-right">Cancel</button>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><div class="panel-title" data-i18n="profile.Your names">Your names</div>
                <div class="heading-elements">
                    <span class="heading-text"><i class=" icon-add"></i> <a href="" class="add_name legitRipple" style="color:white" data-i18n="profile.Add name" data-toggle="modal" data-target="#add_name_modal">Add name</a></span>
                </div>
            </div>
            <div class="panel-body">
                <table class="table text-nowrap">
                    <tr>
                        <td><b class="">{{ oUser()->full_name }}</b></td>
                        <td><a href="" class="edit-primary-name">Edit</a></td>
                    </tr>
                </table>

                <div class="edit_box edit_name_table hide">
                    <table class="table text-nowrap">
                        {{--@foreach( oContact()->names as $aName)--}}
                            {{--<tr>--}}
                                {{--<td class="media-left media-middle">--}}
                                    {{--<input type="radio" name="primary_name" data-type="name"--}}
                                           {{--data-value="{{$aName['display_name'] }}"--}}
                                           {{--value="{{$aName['display_name'] }}" {{ $aName['is_primary'] ? "checked=checked" : "" }} />--}}
                                {{--</td>--}}
                                {{--<td>{{$aName['display_name'] }}  </td>--}}
                                {{--<td class="col-lg-1">{{ $aName['is_primary'] ? '(primary)' : ''  }}</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                    </table>
                    <br/>
                    <div class="text-right">
                        <button class="btn btn-primary save_primary text-right" data-type="name">Save</button>
                        <button class="btn btn-default cancel_primary text-right">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><div class="panel-title" data-i18n="profile.Your mobiles">Your mobile</div>
                <div class="heading-elements">
                    <span class="heading-text"><i class=" icon-add"></i> <a href="" class="add_mobile legitRipple" style="color:white" data-i18n="profile.Add mobile" data-toggle="modal" data-target="#add_name_modal">Add mobile</a></span>
                </div>
            </div>
            <div class="panel-body">
                <table class="table text-nowrap">
                    <tr>
                        <td><b class="">{{ oUser()->mobile }}</b></td>
                        {{--@if(sizeof( oContact()->mobiles)>0)--}}
                            {{--<td><a href="" class="edit-primary-mobile">Edit</a></td>--}}
                        {{--@endif--}}
                    </tr>
                </table>


                <div class="edit_box edit_mobile_table hide">
                    <table class="table text-nowrap">
                        {{--@foreach( oContact()->mobiles as $aMobile)--}}
                            {{--<tr>--}}
                                {{--<td class="media-left media-middle">--}}
                                    {{--@if($aMobile['verify'])--}}
                                        {{--<input type="radio" name="primary_mobile" data-type="mobile"--}}
                                               {{--data-value="{{$aMobile['mobile'] }}"--}}
                                               {{--value="{{$aMobile['mobile'] }}" {{ $aMobile['is_primary'] ? "checked=checked" : "" }} />--}}
                                    {{--@else--}}
                                    {{--(<a data-i18n="profile.verify now">Verify now</a>)--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>{{ $aMobile['mobile'] }} </td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                    </table>
                    <div class="text-right">
                        <button class="btn btn-primary save_primary text-right" data-type="mobile">Save</button>
                        <button class="btn btn-default cancel_primary text-right">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


{{-- Modals --}}

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="add_email_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="add_name_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="add_mobile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>




{{-- Modals --}}

@endif




<script>
    var updatePrimary = '{{route('profile.updatePrimary',[ getUniqueName() ])}}';
</script>





@if(false)
    <table class="table">
        <tr>
            <th><span data-i18n="email"></span></th>
            <td>
                <a href="#" id="profile_email" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.email')}}"></a>
            </td>
        </tr>
        <tr>
            <th><span data-i18n="user_name"></span></th>
            <td>
                <a href="#" id="profile_user_name" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.user_name')}}"></a>
            </td>
        </tr>
        <tr>
            <th><span data-i18n="first_name"></span></th>
            <td>
                <a href="#" id="profile_first_name" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.first_name')}}">
                </a></td>
        </tr>
        <tr>
            <th><span data-i18n="last_name"></span></th>
            <td><a href="#" id="profile_last_name" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.last_name')}}"></a></td>
        </tr>
        <tr>
            <th><span data-i18n="mobile"></span></th>
            <td><a href="#" id="profile_mobile" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.mobile')}}"></a></td>
        </tr>
        <tr>
            <th><span data-i18n="create_date"></span></th>
            <td><a href="#" id="profile_created_at" data-type="text"
                   data-inputclass="form-control" data-pk="1"
                   data-title="{{__('profile.created_at')}}"></a></td>
        </tr>
    </table>
@endif
<script>
    var getCurrentUserRoute = '{{route('profile.getCurrentUser',[getUniqueName()])}}';
</script>