<?php

return [
    'setup_company' => 'Setup Company',
    'company_details' => 'Company details',
    'main_branch_details' => 'Main Branch details',
    'company_name' => 'Company Name',
    'slug' => 'Company Slug',
    'license_number' => 'License Number',
    'company_logo' => 'Company Logo',
    'notes' => 'Notes',
    'notes' => 'Notes',
    'notes' => 'Notes',
    'notes' => 'Notes',
    'main_branch_name' => 'Main Branch Name',
    'branch_email' => 'Branch Email',
    'branch_type' => 'Branch Type',
    'country' => 'Country',
    'phone' => 'Phone',
    'fax' => 'Fax',
];