<?php

return [


    'email' => 'E-Mail',
    'user_name' => 'User Name',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'mobile' => "Mobile",
    'created_at' => "Create Date",
    'about' => "About",
    'info' => "Info",
    'accounts' => "Accounts",
    'signature' => "Signature",
    'signatures' => "Signatures",
    'fingerprint' => "Fingerprint",
    'attachments' => "Attachments",
    'settings' => "Settings",
    'schedule' => "Schedule",
    'activity' => "activity",
    'initials' => "Initials",
    'signature' => "Signature",
    'add_new_signatures' => "Create New Signature",
    'no_signature' => "You are not having any signatures yet , click on create new signature tab to have one",
    'no_initials' => "You are not having any initials yet , click on create new signature tab to have one",
    'draw' => 'Draw',
    'upload_image' => 'Upload',
    'phone' => 'Mobile',
    'my_library' => 'My Library'

];
