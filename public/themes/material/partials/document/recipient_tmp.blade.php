{!! Theme::asset()->themePath()->add([ ['typeahead', 'js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',['bootstrap']]]) !!}
{{--{!! Theme::asset()->themePath()->add([ ['handlebars', 'js/plugins/forms/inputs/typeahead/handlebars.min.js',['typeahead']]]) !!}--}}
{!! Theme::asset()->themePath()->add([ ['recipient', 'partials/document/recipient.js',['typeahead']]]) !!}


<!-- Block buttons -->
<div class="sidebar-category">
    <div class="category-title">
        <span data-i18n="sec_sidbar.recipient">{{__('recipient')}}</span>
    </div>

    <div class="category-content">
        <div class="row">
            <div class="col-xs-12">

                {{--data-i18n="[placeholder]sec_sidbar.recipient"--}}
                <input type="text" class="form-control typeahead" name="recipient" id="recipient_input" value=""
                       data-provide="typeahead" placeholder=""/>


            </div>
        </div>
    </div>
    <div class="category-content">
        <div class="row">
            <div class="col-xs-12">
                <ul class="media-list recipient-list" id="recipient-list">
                    @foreach($aDocument['recipients'] as $aRecipients )
                        <?php
                        $id_identifier = str_replace("@", "___", $aRecipients['identifier']);
                        $id_identifier = str_replace(".", "---", $id_identifier);
                        $id_identifier = str_replace(" ", "", $id_identifier);
                        $id_identifier = str_replace("+", "", $id_identifier);
//                        $origin_display = $aRecipients['display_name'] . ' - ' . $aRecipients['identifier'] ;
                        $origin_display = $aRecipients['identifier'];

                        $type = $aRecipients['type'] ;
                        ?>

                        <li class="media recipient-user recipient-user-{{ $id_identifier }}"
                            id="recipient-user-{{ $id_identifier }}" data-id="{{ $id_identifier }}" data-type="{{$type}}"
                            data-text="{{ $aRecipients['identifier']  }}" data-elementtype="user"
                            data-elementid="{{ $id_identifier }}" data-origin="{{ $aRecipients['identifier']  }}">
                            <div class="media-left"><a href="" class="remove-recipient-user">X</a>
                            </div>
                            <div class="media-body media-middle"><span class="recipient-value">{{ $origin_display }}</span><i class="recipient-user-type pull-right">{{$type}}</i>
                            </div>
                            <div class="media-right media-middle">
                                <ul class="icons-list text-nowrap">
                                    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                    class="icon-arrow-down5"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="recipient-user-switch-to" data-type="To"><i
                                                            class="icon-mail5 pull-right"></i> To</a></li>
                                            <li><a href="#" class="recipient-user-switch-to" data-type="Signer"><i
                                                            class="icon-quill4 pull-right"></i> Signer</a></li>
                                            <li><a href="#" class="recipient-user-switch-to" data-type="CC"><i
                                                            class="icon-cc pull-right"></i> CC</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>

</div>
<!-- /block buttons -->

