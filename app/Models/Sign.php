<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sign
 */
class Sign extends Model
{
    protected $table = 'signs';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'file_id'
    ];

    protected $guarded = [];

        
}