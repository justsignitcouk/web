{!! Theme::asset()->themePath()->add([ ['steps', 'js/plugins/forms/wizards/steps.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['wizard_steps', 'js/pages/wizard_steps.js',['steps']]]) !!}

<style>
    .steps-starting-step  .actions a{
        display:none!important;
    }
    .current{
        cursor: pointer;
        display: block;
    }
</style>

<form class="steps-starting-step" action="#">
    @php $x = 1 @endphp
    @foreach($oWorkFlow->wf_stage as $k=>$stage)

        @if( $current_stage==$stage['id'])
            <script>
                var current_stage = {{$x}} ;
            </script>
        @endif
        @if($stage['action_type'] == 'REJECT')
            @continue
            @endif
    <h6>{{$stage['name']}}</h6>
    <fieldset>
        <div class="row">

        </div>
    </fieldset>
            @php $x += 1 @endphp
    @endforeach
    @if($oDocument->status=='completed')
        <script>
            var current_stage = {{$x}} ;
        </script>
    @endif

</form>
