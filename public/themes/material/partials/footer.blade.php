
<!-- Footer -->
<div class="navbar navbar-default navbar-fixed-bottom">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second">
        <div class="navbar-text">
            &copy; 2018. <a href="#">My Compose System</a> by <a href="http://mycompose.com" target="_blank">My Compose</a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#" data-i18n="footer.help_center">Help center</a></li>
                <li><a href="#" data-i18n="footer.policy">Policy</a></li>
                <li><a href="{{ route('plans.upgrade') }}" class="text-semibold" data-i18n="footer.upgrade_your_account">Upgrade your account</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-cog3"></i>
                        <span class="visible-xs-inline-block position-right">Settings</span>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-facebook"></i> facebook</a></li>
                        <li><a href="#"><i class="icon-twitter"></i> twitter</a></li>
                        <li><a href="#"><i class="icon-pinterest2"></i> Pinterest</a></li>
                        <li><a href="#"><i class="icon-github"></i> Github</a></li>
                        <li><a href="#"><i class="icon-stackoverflow"></i> Stack Overflow</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /footer -->
@if(false)

<div class="navbar navbar-default  bg-indigo"{{-- style="background: #1B1D55;"--}}>
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center legitRipple collapsed" data-toggle="collapse" data-target="#navbar-second" aria-expanded="false"><i class="icon-circle-up2"></i><span class="legitRipple-ripple" style="left: 50.5%; top: 56%; transform: translate3d(-50%, -50%, 0px); width: 201.556%; opacity: 0;"></span></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second" aria-expanded="false" style="height: 1px;">
        <div class="navbar-text" style="
    color: #fff;
    font-size: 15px;
    padding-top: 20px;
">
            © copyright - www.mycompose.com</div>


        <div class="navbar-right">
            <ul class="nav navbar-nav navbar-right" style="margin-right: 0;
    padding-right: 10px;margin-left: 0">
                <li class="dropdown language-switch">
                    <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false" style="    top: 4px;
    color: #fff;
    text-transform: uppercase;">English <i class="caret"></i></a>
                    <ul class="dropdown-menu active" style="    margin-right: 0;
    margin-left: 0;    position: absolute;
    top: -118px;">
                        <li ><a class="english">English</a></li>
                        <li><a class="arabic">العربية</a></li>
                        <li><a class="russian">russian</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav" style="    margin-right: 0;
    margin-left: 0">


                <li><a href="#" style="text-align: center" class="text-semibold legitRipple"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 24 24" style="
    height: 29px;
    width: 29px;
    display: inline-block;
"><defs><style></style></defs><title>Sidebar Help</title><g id="a"><path style="fill: #fff;" class="cls-1" d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm9,16.4-2.6-1.5a7.31,7.31,0,0,0,0-5.8L21,7.6a10.42,10.42,0,0,1,0,8.8ZM2,12A11.29,11.29,0,0,1,3,7.6L5.6,9.1a7.31,7.31,0,0,0,0,5.8L3,16.4A11.29,11.29,0,0,1,2,12Zm5,0a5,5,0,1,1,5,5A5,5,0,0,1,7,12Zm9.4-9L14.9,5.6a6.57,6.57,0,0,0-5.8,0L7.6,3a10.42,10.42,0,0,1,8.8,0ZM7.6,21l1.5-2.6a6.57,6.57,0,0,0,5.8,0L16.4,21a10.42,10.42,0,0,1-8.8,0Z"></path></g></svg>
                        <span class="help" style="
    position: relative;
    top: -10px;
    color: #fff;
    text-transform: uppercase;
    padding: 10px;
">help and support</span></a></li>

                {{--<li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown">
                        <i class="icon-cog3"></i>
                        <span class="visible-xs-inline-block position-right">Settings</span>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-dribbble3"></i> Dribbble</a></li>
                        <li><a href="#"><i class="icon-pinterest2"></i> Pinterest</a></li>
                        <li><a href="#"><i class="icon-github"></i> Github</a></li>
                        <li><a href="#"><i class="icon-stackoverflow"></i> Stack Overflow</a></li>
                    </ul>
                </li>--}}
            </ul>
        </div>
    </div>
</div>
    @endif