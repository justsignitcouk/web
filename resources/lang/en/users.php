<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    */

    'titles' => [
                    'add_user'     => 'Add User',
                    'manage_users' => 'Manage Users'
                 ],

    'shortname' => 'Users',
    'name'  => 'Name',
    'email' => 'Email',
    'first_name' => 'First Name',
    'last_name'  => 'Last Name',
    'username'  => 'User Name',
    'password'   => 'Password',
    're_password'=> 'Re Password',
    'status'     => 'Status',
    'profile_image' => 'Image',
    'password_confirmation'=>'Password Confirmation',
    'created'    =>'User created successfully',
    'roles'      =>'Roles',
    'delete_user'=>'Are you sure you want to delete this user',
    'deactive_user'=>'Are you sure you want to deactive this user',
    'deactive' => 'Deactive',
    'user_roles' => 'User Roles',
    'users_permissions' => 'Users Permissions',
    'companies_permissions' => 'Companies Permissions',
    'branches_permissions'  => 'Branches Permissions',
    'departments_permissions' => 'Departments Permissions' ,
    'roles_permissions' => 'Roles Permissions' ,
    'permissions_permissions' => 'Permissions Permissions',
    'accounts_permissions'=>'Accounts Permissions',
    'requirements_permissions'=>'Requirements Permissions',
    'workflow_permissions'=>'Workflow Permissions',
    'pass_caution'=>'password must contains numbers , small letters , capital letters and minimum length is 6 characters',


];