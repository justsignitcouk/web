<?php


namespace App\Http\Controllers\Individual;

use Illuminate\Http\Request;
use App\Services\Notifications;
use App ;
use Facuz\Theme\Facades\Theme;


class NotificationsController extends Controller
{


    public function notificationsAjax() {

        $notifications = Notifications::getUserNotifications(App::getLocale());
        return $notifications ;

    }


    public function changeReadStatus(Request $request) {

        $notification_id = $request->get('notification_id') ;
        $document_id = $request->get('document_id');
        $notifications = Notifications::changeReadStatus($notification_id,$document_id);
        return \GuzzleHttp\json_encode(['notifications'=>$notifications]) ;

    }





}