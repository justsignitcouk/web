@extends('layouts.admin')

@section('content')

<div class="col-md-12">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __("accounts.accounts") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="tabbable">
                

                <div class="tab-content">
                    <div class="content-group">
                        <div class="col-lg-12 col-md-12">

     
                            <table id="accounts-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>{{ __("common.account_name") }}</th>
                                        <th>{{ __("common.company") }}</th>
                                        <th>{{ __("common.branch") }}</th>
                                        <th>{{ __("common.department") }}</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</div>

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });

        
         var user_id = $('#user_data').data('user_id');

         var table = $('#accounts-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! url('accounts/accountsdatatables' ) !!}',
            columns: [
                { data: 'account_name', name: 'account_name' },
                { data: 'company_name', name: 'company_name' },
                { data: 'branch_name', name: 'branch_name' },
                { data: 'department_name', name: 'department_name' },
            ]
        });
       

    
        $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/accounts/' + id ;
                
                });
       
 
    });
</script>

@endpush
