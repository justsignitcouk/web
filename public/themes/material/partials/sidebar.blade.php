<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user-material">

            {{--{{cdn_url(currentUser()->profile_image , currentUser()->profile_image_path) }}--}}
            <div class="category-content" >
                <div class="sidebar-user-material-content">
                    <a href="{{route('profile.index',['display_name'=> getUniqueName() ])}}" style="border-radius: 50%;
    /* border: 1px solid #ddd; */
    height: 80px;
    width: 80px;
    overflow: hidden;"><img src="{{ user_profile() }}" class="{{--img-circle img-responsive--}}" alt=""></a>
                    <div style="color:#fff;margin: -20px; margin-top: 10px;  ">
                        <h6>{{ oUser()->full_name }}</h6>
                        <span class="user_position"></span>
                        <span class="text-size-small">{{  getIdentifier() }}</span>
                    </div>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li><a href="{{route('profile.index',['a'])}}" class="smState"><i class="icon-user-plus"></i> <span  data-i18n="my_profile"></span></a></li>
                    {{--<li><a href="#" data-popup="popover-custom" data-trigger="hover" data-placement="top" title="under construction" data-content="after 1/10/2017"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
                    --}}{{--<li class="divider"></li>--}}
                    <li><a href="#"><i class=" icon-cog3"></i> <span data-i18n="setting"></span></a></li>
                    <li><a href="{{route('logout')}}"><i class="icon-switch2"></i> <span data-i18n="logout"></span></a></li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding text-center">

                @if(session()->get('nQuotaSize') > session()->get('nUserTotalFilesSize'))
                <div class="btn-group compose-button" style="margin-top:10px">
                    <a href="{{  route('document.compose') }}" class="smState btn btn-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 30 30.12" style="&#10;    height: 22px;&#10;    width: 22px;&#10;position: absolute;left: 8px;"><title>Compose Document</title><g id="a"><path d="M26.4,10.8c-1.1,1.9-2.9,3.8-5.1,3.8a37.78,37.78,0,0,0-1.7,3.5l-1.6.6A40.22,40.22,0,0,1,22.6,10a22,22,0,0,0-3.3,3.5c-1.4-2.3-.4-4.6,1.2-6.5a2.5,2.5,0,0,0,.8,1.8,5.66,5.66,0,0,1,.2-2.9,17.83,17.83,0,0,1,2.6-2.2,2.49,2.49,0,0,0,.3,1.7,4.76,4.76,0,0,1,.7-2.3A9.54,9.54,0,0,1,30,1.2a11.69,11.69,0,0,1-1.3,4.5,6.25,6.25,0,0,1-2.2,1,6,6,0,0,0,1.9,0,23.58,23.58,0,0,1-1.2,2.8,6.72,6.72,0,0,1-2.8,1A2.36,2.36,0,0,0,26.4,10.8ZM9.1,15.6l.8,1,6-3.5-.8-1-6,3.5Zm8.7-.3-.8-1-6,3.5.8,1Zm5.9,0a4.28,4.28,0,0,1-1.6.5,9.25,9.25,0,0,1-.5,1l.4.4a1.88,1.88,0,0,1-.1,2.6c-.1.1-.2.2-.3.2-1,.6-6.7,4.2-8,5a5.13,5.13,0,0,0-1-2.4C12,21.9,5,13.4,3.2,11.1a2.14,2.14,0,0,1-.4-2.3A1.89,1.89,0,0,1,4.6,7.7,1.93,1.93,0,0,1,6.5,9.2a2,2,0,0,1-.8,2.1l1.7,2,9.7-5.6A4.26,4.26,0,0,0,14.5,0c-1.5,0-1,0-12,5.6A4.45,4.45,0,0,0,0,9.6a5.1,5.1,0,0,0,1.1,3.1c1.8,2.2,9.5,11.5,9.5,11.6a2,2,0,1,1-3.2,2.4,2,2,0,0,1-.4-.8,2,2,0,0,1,.8-2.1L6.3,22a4.07,4.07,0,0,0-1.7,3.1,4.53,4.53,0,0,0,6.8,4.4l11.8-7.4a4.37,4.37,0,0,0,1.9-3.6,3.84,3.84,0,0,0-.9-2.5l-.5-.7ZM13.8,2.7a1.82,1.82,0,0,1,2.4.7,1.73,1.73,0,0,1-.7,2.4L9,9.5A4.45,4.45,0,0,0,7.2,6.1C7.2,6,13.8,2.7,13.8,2.7Zm2.6,16.7.5-1.1.3-.8L12.9,20l.8,1Z" style="&#10;   fill : #ffffff;&#10;"/></g></svg>
                        <span class="button-title" data-i18n="sidebar.compose_doc" style="margin-left: 30px;"></span></a>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('document.compose') }}" class="smState"><i class="icon-menu7"></i> <span data-i18n="sidebar.blank_document"></span></a></li>
                        <li><a href="{{route('document.import')}}" class="smState"><i class="icon-menu7"></i> <span data-i18n="sidebar.import"></span></a></li>
                        <li><a href="{{route('document.choose-template')}}" class="smState"><i class="icon-screen-full"></i><span data-i18n="sidebar.from_template"></span></a></li>
                    </ul>
                </div>
                    @else
                    <div class="btn-group compose-button" style="margin-top:10px">
                        <a href="#" class="smState btn btn-primary storage_full">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 30 30.12" style="&#10;    height: 22px;&#10;    width: 22px;&#10;position: absolute;left: 8px;"><title>Compose Document</title><g id="a"><path d="M26.4,10.8c-1.1,1.9-2.9,3.8-5.1,3.8a37.78,37.78,0,0,0-1.7,3.5l-1.6.6A40.22,40.22,0,0,1,22.6,10a22,22,0,0,0-3.3,3.5c-1.4-2.3-.4-4.6,1.2-6.5a2.5,2.5,0,0,0,.8,1.8,5.66,5.66,0,0,1,.2-2.9,17.83,17.83,0,0,1,2.6-2.2,2.49,2.49,0,0,0,.3,1.7,4.76,4.76,0,0,1,.7-2.3A9.54,9.54,0,0,1,30,1.2a11.69,11.69,0,0,1-1.3,4.5,6.25,6.25,0,0,1-2.2,1,6,6,0,0,0,1.9,0,23.58,23.58,0,0,1-1.2,2.8,6.72,6.72,0,0,1-2.8,1A2.36,2.36,0,0,0,26.4,10.8ZM9.1,15.6l.8,1,6-3.5-.8-1-6,3.5Zm8.7-.3-.8-1-6,3.5.8,1Zm5.9,0a4.28,4.28,0,0,1-1.6.5,9.25,9.25,0,0,1-.5,1l.4.4a1.88,1.88,0,0,1-.1,2.6c-.1.1-.2.2-.3.2-1,.6-6.7,4.2-8,5a5.13,5.13,0,0,0-1-2.4C12,21.9,5,13.4,3.2,11.1a2.14,2.14,0,0,1-.4-2.3A1.89,1.89,0,0,1,4.6,7.7,1.93,1.93,0,0,1,6.5,9.2a2,2,0,0,1-.8,2.1l1.7,2,9.7-5.6A4.26,4.26,0,0,0,14.5,0c-1.5,0-1,0-12,5.6A4.45,4.45,0,0,0,0,9.6a5.1,5.1,0,0,0,1.1,3.1c1.8,2.2,9.5,11.5,9.5,11.6a2,2,0,1,1-3.2,2.4,2,2,0,0,1-.4-.8,2,2,0,0,1,.8-2.1L6.3,22a4.07,4.07,0,0,0-1.7,3.1,4.53,4.53,0,0,0,6.8,4.4l11.8-7.4a4.37,4.37,0,0,0,1.9-3.6,3.84,3.84,0,0,0-.9-2.5l-.5-.7ZM13.8,2.7a1.82,1.82,0,0,1,2.4.7,1.73,1.73,0,0,1-.7,2.4L9,9.5A4.45,4.45,0,0,0,7.2,6.1C7.2,6,13.8,2.7,13.8,2.7Zm2.6,16.7.5-1.1.3-.8L12.9,20l.8,1Z" style="&#10;   fill : #ffffff;&#10;"/></g></svg>
                            <span class="button-title" data-i18n="sidebar.compose_doc" style="margin-left: 30px;"></span></a>
                    </div>
                @endif
            </div>
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion ul_sid">
                    <!-- Main -->
                    {{--<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>--}}


                    {{--<li class="{{ $sActiveMenu == 'settings' ? 'active' : '' }}"><a href="{{route('home.home')}}"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 20 24" style="&#10;    height: 22px;&#10;    width: 22px;&#10;    display: inline-block;&#10;"><defs><style>.cls-1{fill:#003375;}</style></defs><title>Sidebar Dashboard</title><g id="a"><path class="cls-1" d="M20,3V24H0V3H4.7L2,5.8V22H18V5.8L15.4,3ZM16.4,7,13,3.4V3a3,3,0,0,0-3-3A3,3,0,0,0,7,3v.4L3.7,7ZM10,2a.94.94,0,0,1,1,1,.94.94,0,0,1-1,1A.94.94,0,0,1,9,3,.94.94,0,0,1,10,2ZM5,17H15v1H5Zm0-1H15V15H5Zm0-2H15V13H5Zm0-2H15V11H5Z"/></g></svg>--}}
                            {{--<span  data-i18n="sidebar.dashboard">{{__('sidebar.dashboard')}}</span></a></li>--}}
                    {{--<li class="navigation-header"><span>Document</span> <i class="icon-menu" title="Main pages"></i></li>--}}
                   <li class="{{ $sActiveMenu == 'inbox' ? 'active' : '' }}"><a href="{{route('inbox.all')}}" class="smState"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 20 20" style="&#10;    height: 22px;&#10;    width: 22px;&#10;    display: inline-block;&#10;"><defs><style>.cls-1{fill:#003375;}</style></defs><title>Sidebar Doucument</title><g id="a"><path class="cls-1" d="M5,18V5H18v6.16c0,3.32-4.89,2-4.89,2S14.38,18,11,18Zm15-6.14V3H3V20h8.65C14.4,20,20,13.89,20,11.86ZM12,13H8v1h4Zm4-3H8v1h8Zm0-2H8V9h8Zm2.57-5H3V19H2V2H19V3ZM17,1H1V17H0V0H17Z"/></g></svg>
                           <span  data-i18n="sidebar.documents"></span> @if(isset($oBadges['badges']) and ($oBadges['badges']['inbox'] + $oBadges['badges']['pending'] +  $oBadges['badges']['draft']) >0  ) <small style="color:red;font-weight:bold"> ({{ $oBadges['badges']['inbox'] + $oBadges['badges']['pending'] + $oBadges['badges']['draft']  }}) </small>  @endif</a></li>
                    <li class=""><a href="{{route('profile.contacts.index',[getUniqueName()])}}"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 20 14.11" style="&#10;    height: 22px;&#10;    width: 22px;&#10;    display: inline-block;&#10;"><defs><style>.cls-1{fill:#003375;}</style></defs><title>Sidemenu Connections</title><g id="a"><path class="cls-1" d="M8.4,10.1c1.9-.4,3.7-.8,2.8-2.5C8.6,2.6,10.6,0,13.3,0S18,2.7,15.4,7.6c-.9,1.6,1,2,2.8,2.5S20,11.3,20,12.7v.41H18.9c0-1.41.1-1.71-1-1.91-1.6-.4-3.1-.7-3.6-1.9a2.41,2.41,0,0,1,.1-2.2c1.1-2.1,1.4-3.9.8-5a2.38,2.38,0,0,0-3.3-.5c-.2.1-.3.3-.5.5-.6,1.1-.4,2.9.8,5a2.23,2.23,0,0,1,.2,2.1c-.6,1.2-2.1,1.6-3.7,1.9-1,.2-1,.5-1,3h-1c0-2.61-.2-3.61,1.7-4Zm-8.4,3H1.07c0-1.56-.2-1.17,1.46-1.56a2.52,2.52,0,0,0,1.85-1.27,1.86,1.86,0,0,0-.1-1.76C3.51,7,3.32,5.7,3.71,5a1.41,1.41,0,0,1,2-.2c.1,0,.2.1.29.2.78,1.27-.68,3.22-.88,4.1H6.72a7.74,7.74,0,0,0,1-3.31A2.53,2.53,0,0,0,5.48,3.11H5.17C3,3.11,1.55,5.18,3.52,9c.62,1.24-.72,1.55-2.17,1.86C.1,11.18,0,11.8,0,12.83Z"/></g></svg>
                            <span  data-i18n="sidebar.connections"></span></a></li>


                    @if(hasRole('Manager'))
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span data-i18n="sidebar.administrator"></span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li><a href="{{ route("settings.index") }}" class="legitRipple" data-i18n="sidebar.dashboard"></a></li>
                            <li><a href="{{ route("settings.companies") }}" class="legitRipple" data-i18n="sidebar.administrator"></a></li>


                        </ul>
                    </li>
                    @endif

                    <li class=""><a href="{{route('profile.index',[getUniqueName()])}}" class="smState"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 24 24" style="&#10;    width: 22px;&#10;    height: 22px;&#10;    display: inline-block;&#10;"><defs><style>.cls-1{fill:#003375;}</style></defs><title>Sidebar Settings</title><path class="cls-1" d="M24,14.16V9.74c-2.1-.8-2.7-.8-3-1.51S21.1,7,22.1,5L18.9,1.91c-2,1-2.5,1.41-3.2,1.1s-.8-.9-1.5-3H9.8C9,2,9,2.61,8.3,2.91s-1.2-.1-3.2-1L2,5C3,7.13,3.4,7.53,3.1,8.23s-1,.7-3.1,1.51v4.42c2.1.8,2.7.8,3,1.51s-.1,1.21-1.1,3.21L5.1,22c2-1,2.5-1.41,3.2-1.1s.7,1,1.5,3.11h4.4c.8-2.11.8-2.71,1.5-3s1.2.1,3.2,1.1L22,19c-1-2-1.4-2.51-1.1-3.21S21.9,15,24,14.16Zm-5,.78a3.79,3.79,0,0,0,.5,3.58l-1.09,1.09c-1.09-.5-2.19-1.09-3.58-.5A4,4,0,0,0,12.65,22H11.15A4,4,0,0,0,9,19.11a4.18,4.18,0,0,0-3.58.5L4.29,18.52a3.79,3.79,0,0,0,.5-3.58A3.85,3.85,0,0,0,2,12.75V11.25A4,4,0,0,0,4.89,9.06a3.79,3.79,0,0,0-.5-3.58L5.48,4.39a4.13,4.13,0,0,0,3.58.5A4,4,0,0,0,11.25,2h1.49a4,4,0,0,0,2.19,2.89,4.18,4.18,0,0,0,3.58-.5l1.09,1.09a3.79,3.79,0,0,0-.5,3.58A4,4,0,0,0,22,11.25v1.49c-1.29.4-2.39.8-3,2.19Zm-7.3-5.13a2.57,2.57,0,1,1-2.57,2.57A2.52,2.52,0,0,1,11.72,9.81Zm0-1.71A4.28,4.28,0,1,0,16,12.38,4.24,4.24,0,0,0,11.72,8.09Z"/></svg>
                            <span data-i18n="setting"></span></a></li>
                    <li><a href="{{route('logout')}}" class="smState"> <i class="icon-switch2"></i> <span data-i18n="logout"></span></a></li>

                </ul>

            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->



