$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    var contacts = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.identifier);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '/ajax/user/get_contact_for_recipient',
            prepare: function (settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                return settings;
            },
            cache: false
        }
    });

    var contacts_by_name = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.name);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '/ajax/user/get_contact_for_recipient',
            prepare: function (settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                return settings;
            },
            cache: false
        }
    });


    renderTagInput('recipient_to', 'To')
    renderTagInput('recipient_signer', 'Signer')
    renderTagInput('recipient_cc', 'CC')

    function renderTagInput(selector, type) {


        /***************** tag input ******************/
        $("#" + selector).tagsinput({freeInput: true,
            containerClass : 'form-control',

        });

        var $elt = $("#" + selector).tagsinput('input');

        $elt.typeahead(
            {
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: 'identifier',
                displayKey: 'identifier',
                display: 'identifier',
                source: contacts,
                limit: 10,
                engine: Handlebars,
                templates: {
                    suggestion: Handlebars.compile('<div>{{name}} - {{identifier}}</div>')
                }
            },
            {
                name: 'name',
                displayKey: 'name',
                display: 'name',

                source: contacts_by_name,
                limit: 10,
                engine: Handlebars,
                templates: {
                    suggestion: Handlebars.compile('<div>{{name}} - {{identifier}}</div>')
                },
            }).bind('typeahead:selected', $.proxy(function (obj, datum) {
            console.log(datum);
            datum['data_type'] = type;
            datum['element_type'] = 'user';
            var id_css = datum.identifier.replace("@", "___");
            id_css = id_css.split('.').join('---');
            id_css = id_css.split(' ').join('');
            id_css = id_css.split('+').join('');
            datum['id'] = id_css;
            if (!$("." + type.toLowerCase() + "_user_item_" + id_css).length) {
                $.fn.drawRecipient(datum, true, true);
            }

            this.tagsinput('add', datum.identifier);
            this.tagsinput('input').typeahead('val', '');

        }, $('.' + selector + '-typeahead')));

        $("#" + selector).on('beforeItemAdd', function (event) {
            if (!validateEmail(event.item)) {
                if (!is_numeric(event.item)) {
                    var mob = event.item.split('+').join('');
                    mob = mob.split(' ').join('');
                    console.log(mob);
                    if (!is_numeric(mob)) {
                        event.cancel = true;
                        return;
                    }

                }

            }

            var datum = [];
            datum['name'] = event.item;
            datum['identifier'] = event.item;
            datum['data_type'] = type;
            datum['element_type'] = 'user';
            var id_css = datum.identifier.replace("@", "___");
            id_css = id_css.split('.').join('---');
            id_css = id_css.split(' ').join('');
            id_css = id_css.split('+').join('');
            datum['id'] = id_css;
            if (!$("." + type.toLowerCase() + "_user_item_" + id_css).length) {
                $.fn.drawRecipient(datum, true, true);
            }


        });

        $("#" + selector).on('beforeItemRemove', function (event) {
            var tag = event.item;
            // Do some processing here
            var id_css = tag.replace("@", "___");
            id_css = id_css.split('.').join('---');
            id_css = id_css.split(' ').join('');
            id_css = id_css.split('+').join('');
            $('.' + type.toLowerCase() + '_user_item_' + id_css).remove();

        });

        $elt.on('keypress', function (e) {

            if (e.keyCode == 13) {
                if ($(".tt-suggestion:first-child").length > 0) {
                    $(".tt-suggestion:first-child").trigger('click');
                    return true;
                }
                //$('input').tagsinput('add', 'some tag');
                e.preventDefault();
            }
            ;
        });

    }

    /***************** tag input ******************/



    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function is_numeric(str) {
        return /^\d+$/.test(str);
    }


});