<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 */
class Country extends Model
{

	/*define variables*/
    protected $table = 'countries';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [
        'code',
        'name'
    ];
    

   
}