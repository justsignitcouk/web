{!! Theme::asset()->container('auth')->themePath()->add([ ['intlTelInput.js', 'js/intl-tel-input/build/js/intlTelInput.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['intlTelInput.css','js/intl-tel-input/build/css/intlTelInput.css',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['auth', 'pages/auth/signup.js',['bootstrap']]]) !!}


<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
    <div class="panel panel-body login-form">
        {{ $oPlan['title'] }} - {{ $oPlan['price'] }}

        <div class="login_logo_container">
            <img src="/assets/images/mycompose_logo.png" id="login_logo" style="width:60% ; margin-left:20%"/>
        </div>
        <div class="text-center" >
            <h5 class="content-group" ><span data-i18n="signup.main" ></span> <small class="display-block" data-i18n="signup.required" ></small></h5>
        </div>
        @if(session()->has('error'))
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">×</a>
                {!! session()->get('error') !!}
            </div>
        @endif
        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.user_name" data-i18n-target=".user_name">

            {!! Form::text('display_name', null, ['class' => 'form-control display_name' , 'autocomplete'=>'off' , 'id'=>'display_name' , 'placeholder'=> trans("auth.display_name") ]) !!}


            <div class="form-control-feedback">
                <i class="icon-user-check text-muted"></i>
            </div>
            @if ($errors->has('display_name'))
                <span style="color : red">{{ $errors->first('display_name') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.first_name" data-i18n-target=".first_name">

            {!! Form::text('first_name', null, ['class' => 'form-control first_name' , 'autocomplete'=>'off'  , 'placeholder'=> trans("auth.first_name") ]) !!}

            <div class="form-control-feedback">
                <i class="icon-user-check text-muted"></i>
            </div>
            @if ($errors->has('first_name'))
                <span style="color : red">{{ $errors->first('first_name') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.middle_name" data-i18n-target=".middle_name">

            {!! Form::text('middle_name', null, ['class' => 'form-control middle_name' , 'autocomplete'=>'off'  , 'placeholder'=> trans("auth.middle_name") ]) !!}

            <div class="form-control-feedback">
                <i class="icon-user-check text-muted"></i>
            </div>
            @if ($errors->has('middle_name'))
                <span style="color : red">{{ $errors->first('middle_name') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left"  data-i18n="[placeholder]input.last_name" data-i18n-target=".last_name">

            {!! Form::text('last_name', null, ['class' => 'form-control last_name' , 'autocomplete'=>'off'  , 'placeholder'=> trans("auth.last_name") ]) !!}

            <div class="form-control-feedback">
                <i class="icon-user-check text-muted"></i>
            </div>
            @if ($errors->has('last_name'))
                <span style="color : red">{{ $errors->first('last_name') }}</span>
            @endif
        </div>


        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.email" data-i18n-target=".email">

            {!! Form::text('email', null, ['class' => 'form-control email' , "id" => "email" ,'autocomplete'=>'off'  , 'placeholder'=> trans("auth.email") ]) !!}
            <span class="help-block"></span>
            <div class="form-control-feedback">
                <i class="icon-mention text-muted"></i>
            </div>
            @if ($errors->has('email'))
                <span style="color : red">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left dir_ltr" data-i18n="[placeholder]input.mobile" data-i18n-target=".mobile">


            {!! Form::tel('mobile', null, ['id'=>'demo','class' => 'form-control phon_num dir_ltr' , 'autocomplete'=>'off'  , 'data-i18n'=>'[placeholder]mobile' ,'style'=>'']) !!}
            <br/>
            @if ($errors->has('mobile'))
                <span style="color : red" class="dir_ltr">{{ $errors->first('mobile') }}</span>
            @endif


        </div>

        <div class="form-group has-feedback has-feedback-left  "  data-i18n="[placeholder]input.password" data-i18n-target=".password">

            {!! Form::password('password' , ['class' => 'form-control password' , 'id'=>'password','autocomplete'=>'off'  , 'placeholder'=> trans("auth.password") ]) !!}
            <span class="help-block"></span>
            <div class="form-control-feedback">
                <i class="icon-user-lock text-muted"></i>
            </div>
            @if ($errors->has('password'))
                <span style="color : red">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="form-group has-feedback has-feedback-left" data-i18n="[placeholder]input.password_confirmation" data-i18n-target=".password_confirmation">


            {!! Form::password('password_confirmation' , ['class' => 'form-control password_confirmation' , 'id'=>'password_confirmation','autocomplete'=>'off'  , 'placeholder'=> trans("auth.password_confirmation") ]) !!}
            <span class="help-block"></span>
            <div class="form-control-feedback">
                <i class="icon-user-lock text-muted"></i>
            </div>
            @if ($errors->has('password_confirmation'))
                <span style="color : red">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>


        <div class="form-group">


            <div class="checkbox">
                <label>
                    <input type="checkbox" class="styled">
                    Accept <a href="#">terms of service</a>
                </label>
            </div>
        </div>

        <input type="hidden" name="plan_id" value="{{ $oPlan['id'] }}" />
        <input type="hidden" name="amount" value="{{ $oPlan['amount'] }}" />
        <button  type="submit" class="btn btn-primary btn-block btn-lg " id="submit_register"><span  data-i18n="signup.submit_btn" >{{trans("auth.submitauth.submit")}}</span><i class="icon-circle-right2 position-right"></i></button>

        <a class="btn btn-basic btn-block" href="{{route('login')}}"><span   data-i18n="signup.cancel_btn" >{{ trans("auth.cancle") }}</span><i class="icon-circle-right2 position-right"></i></a>

    </div>
</form>

<!-- /advanced signup -->


