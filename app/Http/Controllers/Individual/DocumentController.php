<?php

namespace App\Http\Controllers\Individual;

use App\Http\Requests\DocumentRequest;
use App\Services\Access;
use App\Services\File;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Response;
use App\Services\Account;
use App\Services\Document;
use App\Services\Layouts;
use App\Services\User;
use App\Services\WorkFlow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;


class DocumentController extends Controller
{


    public function __construct()
    {

        parent::__construct();
        view()->share('bShowMainSidebar', false);
    }

    public function view($id){
        if( request()->ajax() ) {
            Theme::layout('blank');
        }


        $oDocumentAccess = Document::checkAccess($id);

        if(!$oDocumentAccess){
            session()->flash('access_denied', 'access_denied');
            return redirect()->to(route('inbox.all'));
        }
        $oDocument = Document::getDocumentById(['document_id'=>$id]) ;

        if($oDocument['document']['status'] == 'start'){
            if( nUserID() == $oDocument['document']['user_id'] ) {
                return redirect()->to(route('document.withDocumentId',[$id]));
            }
        }
        if(!$oDocument){
            session()->flash('access_denied', 'access_denied');
            return redirect()->to(route('inbox.all'));
        }
        $aWorkFlow = WorkFlow::getDefaultWorkFlow();
        view()->share('sSideBar',  'sidebar-normal');

        return Theme()->view( 'document.view',[  'oDocument'=>$oDocument , 'id'=>$id ,'editor'=>false,'displayWorkflow' => true,'aWorkFlow'=>$aWorkFlow]);
    }

    public function viewForwarded($id){

        $oDocumentForward = Document::getDocumentForwarded($id);
        $nDocumentId = $oDocumentForward['oDocumentForward']['document_id'];
        $aParams = ['document_id' => $nDocumentId, 'user_id' => nUserID()];
        $oDocument = Document::getDocumentById($aParams);
//        $oDocumentAccess = Document::checkAccess($nDocumentId);
//
//        if(!$oDocumentAccess){
//            session()->flash('access_denied', 'access_denied');
//            return redirect()->to(route('inbox.all'));
//        }
        $oDocumentNotes = Document::getForwardNotes(['forward_id'=>$id]);

        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view( 'document.view',[ 'oDocumentNotes' => isset($oDocumentNotes['notes']) ? $oDocumentNotes['notes'] : []  ,
            'oDocument'=>$oDocument,'id'=>$nDocumentId ,
            'editor'=>false,'forward_id'=>$id,
            'displayWorkflow' => true,
            'displayForwarded'=>true ]);
    }

    public function viewAjax($id,Request $request) {
        if($request->ajax()) {
            $aParams = ['document_id' => $id, 'user_id' => nUserID()];
            $oDocument = Document::getDocumentById($aParams);

            if (!$oDocument) {
                //return redirect('/home');
            }

            $workflow_id = !empty($oDocument['work_flow']) ? $oDocument['work_flow']['wf_workflow_id'] : '1';

            return \GuzzleHttp\json_encode($oDocument);
        }
    }

    public function loadDraft(Request $request) {

        if(session()->has('nDraftID')){
            $nDraftID = session()->get('nDraftID');
        }else{
            return \GuzzleHttp\json_encode([ 'status' => false ]);
        }

        if($request->ajax()) {
            $aParams = ['document_id' => $nDraftID, 'user_id' => nUserID()];
            $oDocument = Document::getDocumentById($aParams);

            if (!$oDocument) {
                //return redirect('/home');
            }

            return \GuzzleHttp\json_encode($oDocument);
        }
    }


    public function getDraft($id) {
        $aParams = ['document_id' => $id,'contact_id' => oContactID()] ;
        $oDocument = Document::getDraftDocumentById($aParams) ;

        $workflow_id = !empty($oDocument['work_flow']) ? $oDocument['work_flow']['wf_workflow_id'] : '1' ;

        return \GuzzleHttp\json_encode($oDocument['document']) ;
    }


    public function printDocument($id){
        $oDocumentAccess = Document::checkAccess($id);
        if(!$oDocumentAccess){
            session()->flash('access_denied', 'access_denied');
            return redirect()->to(route('inbox.all'));
        }
        $oDocument = Document::getDocumentById(['document_id'=>$id]) ;
            Theme::layout('blank');
            view()->share('sSideBar',  'sidebar-normal');
            return Theme()->view( 'document.print',[ 'oDocument'=>$oDocument, 'id'=>$id ,'displayWorkflow' => false]);
    }

    public function pdfDocument($id){
        $oDocumentAccess = Document::checkAccess($id);
        if(!$oDocumentAccess){
            session()->flash('access_denied', 'access_denied');
            return redirect()->to(route('inbox.all'));
        }
        $content = Document::getPdf($id) ;

        return \Illuminate\Support\Facades\Response::make($content, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; '.'11',
        ]);
    }

    public function pdfDraft($id){

        $content = Document::getDraftPDF($id) ;
        return \Illuminate\Support\Facades\Response::make($content, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; '.'11',
        ]);
    }



    public function getDocumentByOutboxNumber($sOutBoxNumber){

        view()->share('bShowMainSidebar',  false);
        $aParams = ['sOutboxNumber' => $sOutBoxNumber,'account_id' => currentAccountID()] ;
        $oDocument = Document::getDocumentByOutboxNumber($aParams) ;

        if(!$oDocument){
            return redirect('/home');
        }

        $bIsSinger = false;
        if (array_key_exists(currentAccountID(), $oDocument['signer'])){
            if ($oDocument['signer'][currentAccountID()]['is_sign'] == 0)
                $bIsSinger = true;
        }
        $with = [
            'oDocument' => (object) $oDocument['document'],
            'aTo' => $oDocument['to'],
            'aCC' => $oDocument['cc'],
            'aSigner' => $oDocument['signer'],
            'bIsSinger' => $bIsSinger,
        ];

        return view('document.print')->with($with);
    }


    public function compose()
    {
        if(session()->get('nQuotaSize') < session()->get('nUserTotalFilesSize')){
            session()->flash('storage_full', 'storage_full');
            return redirect(route('inbox.all' ));
        }

        $bDisplayWorkFlow = Access::can(['key'=>'choose-workflow']) ;


        $nDraftID = Document::insertNewDraft();

        return redirect(route('document.withDocumentId',[$nDraftID]));

    }


    public function importDocument()
    {
        if(session()->get('nQuotaSize') < session()->get('nUserTotalFilesSize')){
            session()->flash('storage_full', 'storage_full');
            return redirect(route('inbox.all' ));
        }

        $bDisplayWorkFlow = Access::can(['key'=>'choose-workflow']) ;


        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view('document.import', []);
    }

    public function sendImport(Request $request) {

        return \Plupload::file('file', function ($file) use ($request) {
            $oFile = File::putDocument($file,'document_import');

            // This will be included in JSON response result
            return [
                'success' => true,
                'message' => 'Upload successful.',
                'id' => $file->getClientOriginalName(),
                'data' => $oFile,
                'document_id' => $oFile['document_id']
                // 'url'       => $photo->getImageUrl($filename, 'medium'),
                // 'deleteUrl' => action('MediaController@deleteDelete', [$photo->id])
            ];
        });

    }


    public function compose_builder()
    {
        Theme::layout('builder');
        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view('document.builder.index', ['bShowMainSidebar' => false]);
    }

    public function editDocument($id)
    {
        Document::lock();
        $oDocument = Document::getDocumentById(['document_id'=>$id]) ;
        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view('document.compose', ['oDocument'=>$oDocument,'bShowMainSidebar' => false,'id'=>$id,'route'=>'document','editor'=>true, 'edit' => true ,'displayWorkflow'=>true]);
    }

    public function composeWithDraft($id)
    {
        if(session()->get('nQuotaSize') < session()->get('nUserTotalFilesSize')){
            session()->flash('storage_full', 'storage_full');
            return redirect(route('inbox.all' ));
        }

        if( request()->ajax() ) {
            Theme::layout('blank');
        }

        $bDisplayWorkFlow = Access::can(['key'=>'choose-workflow']) ;

        $nDraftID = $id;
        $oDocument = Document::getDocumentById(['document_id'=>$nDraftID]) ;
        if($oDocument['document']['status']=='template'){
            $nDraftID = Document::insertNewDraft(['template_id'=>$oDocument['document']['id'] ]);
            return redirect(route('document.withDocumentId',[$nDraftID]));
        }

        $aWorkFlow = WorkFlow::getDefaultWorkFlow();

        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view('document.compose', ['oDocument'=>$oDocument,'id'=>$nDraftID,'bShowMainSidebar' => false, 'editor'=>true,'displayWorkflow' => $bDisplayWorkFlow,'aWorkFlow'=>$aWorkFlow]);
    }

    public function sendDocument(DocumentRequest $request){
        $data = $request->all();
        $response = Document::sendDocument($data);

        return \GuzzleHttp\json_encode($response);

    }

    public function forwardDocument(Request $request){
        $data = $request->all();
        $response = Document::forwardDocument($data);

        return \GuzzleHttp\json_encode($response);

    }

    private function getDraftId($tab_number){
        $nDraftID = false;
        if(!session()->has('tab_numbers')){
            $nDraftID = Document::insertNewDraft();
            $aTab = ['id'=>$tab_number,'document_id' => $nDraftID ] ;
            session()->push('tab_numbers',$aTab);
        }else{
            $aTabNumbers = session()->get("tab_numbers") ;
            foreach($aTabNumbers as $aTabNumber){
                if($aTabNumber['id'] == $tab_number ){
                    $nDraftID = $aTabNumber['document_id'] ;
                }
            }
            if(!$nDraftID){
                $nDraftID = Document::insertNewDraft();
                $aTab = ['id'=>$tab_number,'document_id' => $nDraftID ] ;
                session()->push('tab_numbers',$aTab);
            }

        }

        session()->put('nDraftID', $nDraftID) ;
        return $nDraftID;
    }

    private function setDraftId($tab_number,$nDraftID){
        $nDraftID = false;
        $aTab = ['id'=>$tab_number,'document_id' => $nDraftID ] ;
        session()->push('tab_numbers',$aTab);
            if(!$nDraftID){
                $nDraftID = Document::insertNewDraft();
                $aTab = ['id'=>$tab_number,'document_id' => $nDraftID ] ;
                session()->push('tab_numbers',$aTab);
            }

        return $nDraftID;
    }

    public function sendDraft(Request $request){

        $data = $request->all();
        $client = new \App\Services\Document();
        $response = $client->sendDraft($data);

        return \GuzzleHttp\json_encode($response) ;
    }

    public function deleteDraft(Request $request){
        $data = $request->all();
        $client = new \App\Services\Document();
        $response = $client->deleteDraft($data);
        return \GuzzleHttp\json_encode($response);
    }

    public function sign(Request $request){
        $data = $request->all();
        $client = new \App\Services\Document();
        $response = $client->sign($data);
        return \GuzzleHttp\json_encode(['data'=>$response , 'status'=>true]);
    }

    public function setStatus(Request $request){
        $data = $request->all();
        $client = new \App\Services\Document();
        $response = $client->setStatus($data);
    }

    public function replyDocument($parent_id){
        $aParams = ['id' => $parent_id,'account_id' => currentAccountID()] ;
        $oDocument = Document::getDocumentById($aParams);
//        dd($oDocument);
    }

    public function getDocumentByQRID($QR){

        if(strpos($QR,'-')){
//            if(session()->has('step') && session()->get('step') >=1 &&  session()->get('step')<4 ) {
            $explode = explode("-", $QR ) ;
            $datetime = $explode[0] ;
            $nCompanyID = $explode[1] ;
            $nDepartmentID = $explode[2] ;
            $nAccountID = $explode[3] ;
            $nDocumentID = $explode[4] ;
            $nOldDocumentID = session()->get('nDocumentID') ;

//                if($nDocumentID !=$nOldDocumentID){
//                    return view('document.invalid');
//                }

            $oDocumentSignersID = session()->get('oDocumentSignersID') ;
            $aParams = ['id' => $nDocumentID,'account_id' => $nAccountID,'sign'=>$oDocumentSignersID] ;
            $oDocument = Document::checkDocumentSign($aParams) ;
            $counter = session()->get('step') ;
            session()->put('step',2) ;
            session()->put('oDocument',$oDocument);

            return view('document.valid',compact('oDocument'));
//            }
//            session()->forget('step') ;
//            session()->forget('oDocument');
//            session()->forget('nDocumentID');
//            session()->forget('oDocumentSignersID');
//            return redirect("/");
        }

        $ids = explode(".",$QR) ;
        $nDocumentID = $ids[0] ;
        $nAccountID = $ids[1] ;
        $oDocumentSignersID = $ids[2] ;
        $aParams = ['id' => $nDocumentID,'account_id' => $nAccountID,'sign'=>$oDocumentSignersID] ;
        $oDocument = Document::checkDocumentSign($aParams) ;
        if($oDocument){

            session()->put('step','1') ;
            session()->put('oDocument','1');
            session()->put('nDocumentID',$nDocumentID);
            session()->put('oDocumentSignersID',$oDocumentSignersID);

            return view('document.valid');
        }
        return view('document.invalid');
    }

    public function saveWfStateAction(Request $request) {

        $document_id = $request->get('document_id') ;
        $state_id = $request->get('state_id') ;
        $action_id = $request->get('action_id') ;
        $bWfStateAction = WorkFlow::saveWfStateAction(['document_id' => $document_id , 'state_id' => $state_id, 'action_id' => $action_id ]);
        return \GuzzleHttp\json_encode(['status' => $bWfStateAction ]) ;
    }

    public function getToWhomeAccounts(Request $request){
        $accountResponse = Account::all();

        $q = strtolower($request->get('q'));

        $aUsers = [];
        if ($accountResponse['response_code']) {
            $aUsers = $accountResponse['to_whom_addresses'];
        }
        $aUserFilter = $aUsers ;
        if(!empty($q)){
            $aUserFilter = [] ;
            foreach($aUsers as $user){
                $full_name = $user['first_name'] . ' ' . $user['last_name'] ;
                if(strpos(strtolower($full_name),$q)!==false || strpos(strtolower($user['email']),$q)!==false){
                    $aUserFilter[] = $user;
                }
            }
        }
        return \GuzzleHttp\json_encode(  $aUserFilter  ) ;
    }

    public function getToWhomeEmail(Request $request){
        $aEmail = [] ;
        $aUser = User::findBy(['operation'=>'like','key'=>'email','value'=>$request->get('q')]);

        $aEmail = $aUser['response'] ;

        return \GuzzleHttp\json_encode(  $aEmail  ) ;
    }

    public function getToWhomeMobile(Request $request){
        $aMobile = [] ;
        $aUser = User::findBy(['operation'=>'like','key'=>'mobile','value'=>$request->get('q')]);
        $aMobile = $aUser['response'] ;
        return \GuzzleHttp\json_encode(  $aMobile  ) ;
    }

    public function getToWhomeDepartments(Request $request){
        $accountResponse = Account::all();
        $company = new \App\Services\Company();
        $companyResponse = $company->getCompanyDepartments();
        $aUsers = [];
        $aDepartments = [];
        if ($accountResponse['response_code'])
            $aUsers = $accountResponse['to_whom_addresses'];

        if ($companyResponse['response_code'])
            $aDepartments = $companyResponse['company_departments'];

        return \GuzzleHttp\json_encode([ ['recipient' => 'recipient','value'=>'value1'],['recipient' => 'recipient2','value'=>'value2'] ]) ;
    }

    public function getLayouts(){
        $oLayouts = Layouts::all();
        return \GuzzleHttp\json_encode($oLayouts) ;
    }

    public function getLayout($id){
        $oLayouts = Layouts::getDefault();
        return \GuzzleHttp\json_encode($oLayouts) ;
    }

    public function getWorkFlowsForCurrentAccount(){
        $oWorkFlow = WorkFlow::getForCurrentAccount();
        return \GuzzleHttp\json_encode($oWorkFlow) ;
    }

    public function getNotes(Request $request){
        $oDocumentNotes = Document::getNotes($request);
        return \GuzzleHttp\json_encode($oDocumentNotes) ;
    }

    public function getForwardNotes(Request $request){
        $oDocumentNotes = Document::getForwardNotes($request->all());
        return \GuzzleHttp\json_encode($oDocumentNotes) ;
    }

    public function savePositionOfNote(Request $request){
        $oDocumentNotes = Document::savePositionOfNote($request->all());
        return \GuzzleHttp\json_encode($oDocumentNotes) ;
    }

    public function getDocumentListAjax(){
        $oDocuments = Document::getJson();
        return \GuzzleHttp\json_encode($oDocuments['document']);
    }

    public function getDocumentDetails(Request $request){
        $oDocument  = Document::getDocumentDetails($request->all());
        return \GuzzleHttp\json_encode($oDocument);
    }

    public function getDocumentDetailsAjax($id){
        if( request()->ajax() ) {
            Theme::layout('blank');
        }

        $oDocument = Document::getDocumentById(['document_id'=>$id]) ;
        $aWorkFlow = WorkFlow::getDefaultWorkFlow();
        return Theme()->view('document.details_box', [ 'aWorkFlow' => $aWorkFlow, 'oDocument' => $oDocument ,'bShowMainSidebar' => false ]);
    }

    public function getDocumentAttachments(Request $request){
        $oDocument  = Document::getDocumentAttachments($request->all());
        return \GuzzleHttp\json_encode($oDocument);
    }

    public function testDocument(){
        $bDisplayWorkFlow = Access::can(['key'=>'choose-workflow']) ;
        $nDraftID = '464';
        $oDocument = Document::getDocumentById(['document_id'=>$nDraftID]) ;
        if($oDocument['document']['status']=='template'){
            $nDraftID = Document::insertNewDraft(['template_id'=>$oDocument['document']['id'] ]);
            return redirect(route('document.withDocumentId',[$nDraftID]));
        }

        $aWorkFlow = WorkFlow::getDefaultWorkFlow();

        view()->share('sSideBar',  'sidebar-normal');
        return Theme()->view('document.test', ['oDocument'=>$oDocument,'id'=>$nDraftID,'bShowMainSidebar' => false, 'editor'=>true,'displayWorkflow' => $bDisplayWorkFlow,'aWorkFlow'=>$aWorkFlow]);

    }

}
