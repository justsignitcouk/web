process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

elixir(function(mix) {

	mix.styles( [
				'./assets/css/style-rtl.css'
				], './assets/dist/css/styles.css'
	);
	

	mix.scripts([
				'./assets/js/script.js',
        			"./assets/js/core/app.js"
				], './assets/dist/js/scripts.js'
	);

});
