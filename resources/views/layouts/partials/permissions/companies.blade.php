@foreach($companies_permissions as $key => $company)
    <div class="form-check">
      <label class="form-check-label">
        <input class="company-check-input" type="checkbox" name ="{{'companies-'.$key}}" id="{{$company->key}}"
        value="{{$company->id}}"

        @if(!empty($checked_user_permissions))
            @foreach($checked_user_permissions as $check)

                @if($check['permission_id'] == $company->id)
                checked
                @endif
         
            @endforeach
        @endif
       
        >
        {{$company->name}}
      </label>
    </div>
@endforeach