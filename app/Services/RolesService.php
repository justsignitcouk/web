<?php


namespace App\Services;

use App\Models\PermissionRole;
use DB ;


class RolesService{


    function __construct(){


    }

    public function checkedRolePermissions($role_id){

        return      PermissionRole::
                    select('permission_id')
                    ->where('role_id',$role_id)
                    ->get()
                    ->toArray(); 

    }

	
    public function Permissions($module_name){

        return  DB::table('modules')
                ->join('permissions','permissions.module_id','=','modules.id')
                ->select('modules.name','permissions.*')
                ->where('modules.name','=',$module_name)
                ->get();

    }


}