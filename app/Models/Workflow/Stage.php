<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;
use App\Models\Workflow\State;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stage extends Model
{
    protected $table = 'wf_stage';

    public $timestamps = true;
    use SoftDeletes;

    private static $actionTypes = [
        'START'     => 'bg-info',
        'In Progress'   => 'bg-primary',
        'END'       => 'bg-success',
    ];

    protected $fillable = [
        'name',
        'action_type',
        'description',
        'due_date_in_hours',
        'previous',
        'active',
        'created_by',
        'updated_by'
    ];

    protected $guarded = [];

    public function Workflow()
    {
        return $this->belongsTo('App\Models\Workflow\Workflow');
    }

    public function StageState()
    {
        return $this->hasMany('App\Models\Workflow\StageState', 'stage_id', 'id');
    }



    public static function getActionTypes() {
        return self::$actionTypes;
    }


    public function hasRelation(){
        $aRelations = State::where(['stage_id' => $this->id])
            ->orWhere(['next_stage_id' => $this->id])
            ->get();
        if(count($aRelations)){
            return true;
        }
        return false;
    }

    public static function nextSequence($nWorkFlowID){
        $oSequnce = Parent::where('action_type','!=','END')->Where('workflow_id',$nWorkFlowID)->orderBy('sequence','desc')->get()->first();
        return $oSequnce->sequence + 1 ;
    }
}
