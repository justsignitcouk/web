<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CompaniesController Language Lines
    |--------------------------------------------------------------------------
    */

    'shortname' => 'الشركات',
    'name' => 'الاسم',
    'number_of_branches' => 'عدد الافرع',
    'number_of_departments' => 'عدد الاقسام',
    'number_of_accounts' => 'عدد الحسابات',
    'titles' => [
                    'add_company'   => 'اضافة شركة',
                    'manage_companies' => 'ادارة الشركات'
                ],

];
