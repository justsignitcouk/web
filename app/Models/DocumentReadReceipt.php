<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentReadReceipt
 */
class DocumentReadReceipt extends Model
{
    protected $table = 'document_read_receipt';

    public $timestamps = true;

    protected $fillable = [
        'document_id',
        'account_id'
    ];

    protected $guarded = [];

        
}