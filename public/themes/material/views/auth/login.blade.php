{!! Theme::asset()->container('auth')->themePath()->add([ ['auth', 'pages/auth/index.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['auth', 'css/pages/auth/index.css',['bootstrap']]]) !!}
<style>
    .login_container {
        overflow: hidden;
    }

    .steps {
        display: none;
    }

    .step1 {
        display: block;
    }

    .steps_button{
        display:none;
    }
    .step1_button{
        display:block;
    }
    .need-help{
        margin-top: 15px;
    }
</style>

<div class="login_container">
    <div class="panel panel-body login-form">


        <div class="text-center">
            <img src="/assets/images/mycompose_logo.png" id="login_logo" style="max-width:60%;">
        </div>
        <br/>
        <div class="messages_alert">
            @if(Session::has('flash_message'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">×</a>
                    <strong>Heads Up!</strong> {!! Session::pull('flash_message')[0] !!}
                </div>
            @endif
            @if(Session::has('register_verify_message'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! Session::pull('register_verify_message')[0] !!}
                </div>
            @endif
            @if(Session::has('reset_password_message'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! Session::pull('reset_password_message')[0] !!}
                </div>
            @endif
            @if(session()->has('user_activated'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session()->get('user_activated') !!}
                </div>
            @endif
            @if(session()->has('thanks_for_registration'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session()->get('thanks_for_registration') !!}
                </div>
            @endif
        </div>
        {{ csrf_field() }}
        <div class="steps step1 hide">
            <input type="text" name="email"/>
        </div>

        <div class="steps step2 hide">
            <input type="password" name="password"/>
        </div>


        <div class="steps step1 Mail form-group has-feedback has-feedback-left ">

            <input type="text" name="identifier" class="form-control" id="identifier"
                   data-i18n="[placeholder]email_or_phone" placeholder="{{ __('login.email_or_phone') }}">
            <div class="form-control-feedback ">
                <i class="icon-user text-muted"></i>
            </div>
            <span class="help-block hide">{{ __('Enter an email or phone number') }}</span>
            <div class="need-help" data-i18n="login.need-help" data-i18n-target="span"><a tabindex="6"
                                                                                          href="{{url('/needhelp')}}"><i
                            class="icon-question4"></i> <span>{{ __("login.need_help") }}</span></a></div>
        </div>


        <div class="steps step2">
            <div class="">
                <p class="mail-value"></p>
            </div>
            <div class=" form-group has-feedback has-feedback-left ">
                <input type="hidden" id="user_id" name="user_id" value=""/>

                <input type="password" name="password" id="password" class="form-control"
                       data-i18n="[placeholder]password"  placeholder="{{ __('login.password') }}">
                <div class="form-control-feedback">
                    <i class="icon-lock5 text-muted"></i>
                </div>
                <span class="help-block hide">{{ __('Enter an email or phone number') }}</span>
            </div>



            <div tabindex="2" class="pull-left forgot-password" style="margin-bottom: 10% ; margin-top : -5%"><a
                        href="{{route('forgotPassword')}}"><i class="icon-lock5"></i> <span
                            data-i18n="login.forgot_pass">{{ __("login.forgot_password") }}</span></a></div>
        </div>
        <br/>



        <div class="form-group col-sm-12 Next">
            <button id="next_button" class="steps_button step1_button Next btn btn-primary btn-block"><span
                        data-i18n="login.next_btn">{{__('login.next')}}</span><i
                        class="icon-circle-right2 position-right" style="margin-left:8px;"></i></button>
        </div>


        <div class="col-md-12">

            <button tabindex="3" class="steps_button step2_button btn btn-primary btn-block col-sm-6 pull-right" id="submit-login"><span
                        data-i18n="signup.submit_btn">{{ trans("login.submit") }}</span><i
                        class="icon-circle-right2 position-right"  style="margin-left:8px;"></i></button>
            <button id="cancel_button" class="steps_button step2_button back-arrow-login btn btn-basic btn-block col-sm-6 pull-left"><span
                        data-i18n="signup.cancel_btn">{{ __("login.cancle") }}</span><i
                        class="icon-circle-right2 position-right"  style="margin-left:8px;"></i></button>

        </div>


        <div class="other-options">


            <div class="btn-group col-sm-12" style="margin-top: 4%">


                <!-- /simple login form -->
                <div class="content-divider text-muted form-group "><span
                            data-i18n="login.no_account">{{ __("login.no_account") }}</span></div>

                <div class="form-group col-sm-12 sign-up">
                    <a href="{{route('registerForm')}}" class="btn btn-default btn-block content-group"><span
                                data-i18n="login.sign_btn">{{ __("login.sign_up") }}</span><i
                                class="icon-circle-right2 position-right"  style="margin-left:8px;"></i></a>
                </div>

            </div>

        </div>

    </div>


    <div class="content-divider text-muted form-group" style="color: #3F51B5"><span
                data-i18n="login.sign_with">{{ trans("login.sign_with") }}</span></div>

    <ul class="list-inline form-group list-inline-condensed text-center">

        <li>
            <a href="{{url('login/facebook')}}" class="">
                <svg style="fill:   #3B5998" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                     viewBox="0 0 24 24">
                    <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3 8h-1.35c-.538 0-.65.221-.65.778v1.222h2l-.209 2h-1.791v7h-3v-7h-2v-2h2v-2.308c0-1.769.931-2.692 3.029-2.692h1.971v3z"/>
                </svg>
            </a>
        </li>

        <li>
            <a href="{{url('login/google')}}" class="">

                <svg width="40" height="40" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FBBB00;" d="M113.47,309.408L95.648,375.94l-65.139,1.378C11.042,341.211,0,299.9,0,256
	c0-42.451,10.324-82.483,28.624-117.732h0.014l57.992,10.632l25.404,57.644c-5.317,15.501-8.215,32.141-8.215,49.456
	C103.821,274.792,107.225,292.797,113.47,309.408z"/>
                    <path style="fill:#518EF8;" d="M507.527,208.176C510.467,223.662,512,239.655,512,256c0,18.328-1.927,36.206-5.598,53.451
	c-12.462,58.683-45.025,109.925-90.134,146.187l-0.014-0.014l-73.044-3.727l-10.338-64.535
	c29.932-17.554,53.324-45.025,65.646-77.911h-136.89V208.176h138.887L507.527,208.176L507.527,208.176z"/>
                    <path style="fill:#28B446;" d="M416.253,455.624l0.014,0.014C372.396,490.901,316.666,512,256,512
	c-97.491,0-182.252-54.491-225.491-134.681l82.961-67.91c21.619,57.698,77.278,98.771,142.53,98.771
	c28.047,0,54.323-7.582,76.87-20.818L416.253,455.624z"/>
                    <path style="fill:#F14336;" d="M419.404,58.936l-82.933,67.896c-23.335-14.586-50.919-23.012-80.471-23.012
	c-66.729,0-123.429,42.957-143.965,102.724l-83.397-68.276h-0.014C71.23,56.123,157.06,0,256,0
	C318.115,0,375.068,22.126,419.404,58.936z"/>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
</svg>

            </a>
        </li>

        {{--<li>--}}
            {{--<a href="{{url('login/linkedin')}}" class="">--}}
                {{--<svg style="fill:  #007bb6" xmlns="http://www.w3.org/2000/svg" width="40" height="40"--}}
                     {{--viewBox="0 0 24 24">--}}
                    {{--<path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2 16h-2v-6h2v6zm-1-6.891c-.607 0-1.1-.496-1.1-1.109 0-.612.492-1.109 1.1-1.109s1.1.497 1.1 1.109c0 .613-.493 1.109-1.1 1.109zm8 6.891h-1.998v-2.861c0-1.881-2.002-1.722-2.002 0v2.861h-2v-6h2v1.093c.872-1.616 4-1.736 4 1.548v3.359z"/>--}}
                {{--</svg>--}}
            {{--</a>--}}
        {{--</li>--}}

        {{--<li>
            <a href="{{url('login/twitter')}}" class="">
                <svg style="fill: #1DA1F2" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6.066 9.645c.183 4.04-2.83 8.544-8.164 8.544-1.622 0-3.131-.476-4.402-1.291 1.524.18 3.045-.244 4.252-1.189-1.256-.023-2.317-.854-2.684-1.995.451.086.895.061 1.298-.049-1.381-.278-2.335-1.522-2.304-2.853.388.215.83.344 1.301.359-1.279-.855-1.641-2.544-.889-3.835 1.416 1.738 3.533 2.881 5.92 3.001-.419-1.796.944-3.527 2.799-3.527.825 0 1.572.349 2.096.907.654-.128 1.27-.368 1.824-.697-.215.671-.67 1.233-1.263 1.589.581-.07 1.135-.224 1.649-.453-.384.578-.87 1.084-1.433 1.489z"/></svg>
            </a>
        </li>--}}

    </ul>


</div>


<script>
    var email_or_phone_post_url = "{!! url('validateEmailOrPhone' ) !!}";
    var login_route = "{!! route('user.login' ) !!}";
    var mail_or_phone_error = "{{ trans("auth.mail_or_phone_error") }}";
    var mail_or_phone_not_exist = "{{ trans("auth.mail_or_phone_not_exist") }}";
    var wrong_phone_email = "{{ trans("auth.wrong_phone_email") }}";
    var login = "{{route('login')}}";
    var translate_error = {
        'wrong_password': '{{ trans("auth.wrong_password") }}',
        'inactive_user': '{{ trans("auth.inactive_user") }}'
    };

</script>





@if(false)
    <div style="overflow: hidden">
        <div class="panel panel-body login-form">


            <div class="text-center">
                <img src="/assets/images/mycompose_logo.png" id="login_logo" style="max-width:60%;">
            </div>
            <br/>


            {{ csrf_field() }}

            <input style="display:none" type="text" name="email"/>
            <input style="display:none" type="password" name="password"/>
            @if(Session::has('flash_message'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">×</a>
                    <strong>Heads Up!</strong> {!! Session::pull('flash_message')[0] !!}
                </div>
            @endif
            @if(Session::has('register_verify_message'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! Session::pull('register_verify_message')[0] !!}
                </div>
            @endif
            @if(Session::has('reset_password_message'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! Session::pull('reset_password_message')[0] !!}
                </div>
            @endif

            @if(session()->has('user_activated'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session()->get('user_activated') !!}
                </div>
            @endif

            @if(session()->has('thanks_for_registration'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session()->get('thanks_for_registration') !!}
                </div>
            @endif

            <div class="Mail form-group has-feedback has-feedback-left ">

                <input type="text" name="email" class="form-control" id="email" data-i18n="[placeholder]email_or_phone">
                <div class="form-control-feedback ">
                    <i class="icon-user text-muted"></i>
                </div>

            </div>

            <div id="mail-or-phone-error" class="text-danger"></div>

            <div class="need-help" data-i18n="login.need-help" data-i18n-target="span"><a tabindex="6"
                                                                                          href="{{url('/needhelp')}}"><i
                            class="icon-question4"></i> <span>{{--{{ __("login.need_help") }}--}}</span></a></div>


            <div class="Password">
                <p class="mail-value"></p>
            </div>


            <div class="Password form-group has-feedback has-feedback-left " style="display:none ">
                <input type="password" name="password" id="password" class="form-control"
                       data-i18n="[placeholder]password">
                <div class="form-control-feedback">
                    <i class="icon-lock5 text-muted"></i>
                </div>

            </div>

            <div id="password-error" class="text-danger">{{ trans("auth.fill_password") }}</div>
            <div id="password-login-error" class="text-danger"></div>

            <br/>

            <div tabindex="2" class="pull-left forgot-password" style="margin-bottom: 10% ; margin-top : -5%"><a
                        href="{{route('forgotPassword')}}"><i class="icon-lock5"></i> <span
                            data-i18n="login.forgot_pass">{{--{{ __("login.forgot_password") }}--}}</span></a></div>

            <div class="form-group col-sm-12 Next">
                <button id="next_button" class=" Next btn btn-primary btn-block"><span
                            data-i18n="login.next_btn">{{--{{__('login.next')}}--}}</span><i
                            class="icon-circle-right2 position-right"></i></button>
            </div>


            <div class="col-md-12">

                <button tabindex="3" class="Submit btn btn-primary btn-block col-sm-6 pull-right" id="submit-login">
                    <span data-i18n="signup.submit_btn">{{--{{ trans("login.submit") }}--}}</span><i
                            class="icon-circle-right2 position-right"></i></button>
                <button class="Submit back-arrow-login btn btn-basic btn-block col-sm-6 pull-left"><span
                            data-i18n="signup.cancel_btn">{{--{{ __("login.cancle") }}--}}</span><i
                            class="icon-circle-right2 position-right"></i></button>

            </div>


            <div class="other-options">


                <div class="btn-group col-sm-12" style="margin-top: 4%">


                    <!-- /simple login form -->
                    <div class="content-divider text-muted form-group "><span
                                data-i18n="login.no_account">{{--{{ __("login.no_account") }}--}}</span></div>

                    <div class="form-group col-sm-12 sign-up">
                        <a href="{{route('registerForm')}}" class="btn btn-default btn-block content-group"><span
                                    data-i18n="login.sign_btn">{{--{{ __("login.sign_up") }}--}}</span><i
                                    class="icon-circle-right2 position-right"></i></a>
                    </div>

                </div>

            </div>

        </div>


        <div class="content-divider text-muted form-group" style="color: #3F51B5"><span
                    data-i18n="login.sign_with">{{--{{ trans("login.sign_with") }}--}}</span></div>

        <ul class="list-inline form-group list-inline-condensed text-center">

            <li>
                <a href="{{url('login/facebook')}}" class="">
                    <svg style="fill:   #3B5998" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                         viewBox="0 0 24 24">
                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3 8h-1.35c-.538 0-.65.221-.65.778v1.222h2l-.209 2h-1.791v7h-3v-7h-2v-2h2v-2.308c0-1.769.931-2.692 3.029-2.692h1.971v3z"/>
                    </svg>
                </a>
            </li>

            <li>
                <a href="{{url('login/google')}}" class="">

                    <svg width="40" height="40" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FBBB00;" d="M113.47,309.408L95.648,375.94l-65.139,1.378C11.042,341.211,0,299.9,0,256
	c0-42.451,10.324-82.483,28.624-117.732h0.014l57.992,10.632l25.404,57.644c-5.317,15.501-8.215,32.141-8.215,49.456
	C103.821,274.792,107.225,292.797,113.47,309.408z"/>
                        <path style="fill:#518EF8;" d="M507.527,208.176C510.467,223.662,512,239.655,512,256c0,18.328-1.927,36.206-5.598,53.451
	c-12.462,58.683-45.025,109.925-90.134,146.187l-0.014-0.014l-73.044-3.727l-10.338-64.535
	c29.932-17.554,53.324-45.025,65.646-77.911h-136.89V208.176h138.887L507.527,208.176L507.527,208.176z"/>
                        <path style="fill:#28B446;" d="M416.253,455.624l0.014,0.014C372.396,490.901,316.666,512,256,512
	c-97.491,0-182.252-54.491-225.491-134.681l82.961-67.91c21.619,57.698,77.278,98.771,142.53,98.771
	c28.047,0,54.323-7.582,76.87-20.818L416.253,455.624z"/>
                        <path style="fill:#F14336;" d="M419.404,58.936l-82.933,67.896c-23.335-14.586-50.919-23.012-80.471-23.012
	c-66.729,0-123.429,42.957-143.965,102.724l-83.397-68.276h-0.014C71.23,56.123,157.06,0,256,0
	C318.115,0,375.068,22.126,419.404,58.936z"/>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
</svg>

                </a>
            </li>

            <li>
                <a href="{{url('login/linkedin')}}" class="">
                    <svg style="fill:  #007bb6" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                         viewBox="0 0 24 24">
                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2 16h-2v-6h2v6zm-1-6.891c-.607 0-1.1-.496-1.1-1.109 0-.612.492-1.109 1.1-1.109s1.1.497 1.1 1.109c0 .613-.493 1.109-1.1 1.109zm8 6.891h-1.998v-2.861c0-1.881-2.002-1.722-2.002 0v2.861h-2v-6h2v1.093c.872-1.616 4-1.736 4 1.548v3.359z"/>
                    </svg>
                </a>
            </li>

            {{--<li>
                <a href="{{url('login/twitter')}}" class="">
                    <svg style="fill: #1DA1F2" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6.066 9.645c.183 4.04-2.83 8.544-8.164 8.544-1.622 0-3.131-.476-4.402-1.291 1.524.18 3.045-.244 4.252-1.189-1.256-.023-2.317-.854-2.684-1.995.451.086.895.061 1.298-.049-1.381-.278-2.335-1.522-2.304-2.853.388.215.83.344 1.301.359-1.279-.855-1.641-2.544-.889-3.835 1.416 1.738 3.533 2.881 5.92 3.001-.419-1.796.944-3.527 2.799-3.527.825 0 1.572.349 2.096.907.654-.128 1.27-.368 1.824-.697-.215.671-.67 1.233-1.263 1.589.581-.07 1.135-.224 1.649-.453-.384.578-.87 1.084-1.433 1.489z"/></svg>
                </a>
            </li>--}}

        </ul>


    </div>

    <script>
        var email_or_phone_post_url = "{!! url('validateEmailOrPhone' ) !!}";
        var login_route = "{!! route('user.login' ) !!}";
        var mail_or_phone_error = "{{ trans("auth.mail_or_phone_error") }}";
        var mail_or_phone_not_exist = "{{ trans("auth.mail_or_phone_not_exist") }}";
        var wrong_phone_email = "{{ trans("auth.wrong_phone_email") }}";
        var login = "{{route('login')}}";
        var translate_error = {
            'wrong_password': '{{ trans("auth.wrong_password") }}',
            'inactive_user': '{{ trans("auth.inactive_user") }}'
        };

    </script>
@endif