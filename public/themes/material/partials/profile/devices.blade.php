{!! Theme::asset()->themePath()->add([ ['sessions', 'partials/profile/devices.js', ['datatables']]]) !!}
<h3><span data-i18n="activity_header"></span></h3>
<table class="table table-bordered devices-table">
    <thead>
    <tr>
        <th data-i18n="Device"></th>
        <th data-i18n="Platform"></th>
        <th data-i18n="Platform_Version"></th>
        <th data-i18n="Browser"></th>
        <th data-i18n="IP"></th>
        <th data-i18n="Last_Active"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>