<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserForgotPasswordRequest;
use App\Services\Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Mail\resetPassword;
use Illuminate\Support\Facades\Mail;
use Facuz\Theme\Facades\Theme;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showForgotForm()
    {

        Theme::layout('auth');
        return Theme::view('auth.forgot');

    }

    public function getToken(UserForgotPasswordRequest $request){

        $response = Auth::getToken($request);

        if($response['status']){
            $message = 'success' ;
        }else{
            $message = 'faild' ;
        }

        session()->flash("status", $message);

        return redirect('forgotPassword') ;

    }


}
