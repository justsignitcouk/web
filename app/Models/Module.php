<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Module
 */
class Module extends Model
{

    /**
	 * table variables
	 */
	protected $table = 'modules';
    public $timestamps = true;


    /**
	 * model relations
	 */
    public function Permissions(){

        return $this->hasMany('App\Models\Permission','module_id','id');
    }


}
