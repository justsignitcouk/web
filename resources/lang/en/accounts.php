<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

   'accounts'     => 'Accounts' ,
   'add_account'  => 'Add account' ,
   'account_name' => 'Account Name' ,
   'active'       => 'Active' ,
   'is_default'   => 'Is Default' ,
   'verify'       => 'Verify' ,
   'deactive_account'      => 'Are you sure you want deactive this account',
   'edit_account' => 'Edit account' , 

];
