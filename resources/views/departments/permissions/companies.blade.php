@foreach($companies_permissions as $key => $company)
    <div class="form-check">
      <label class="form-check-label">
        <input class="company-check-input" type="checkbox" name ="{{'companies-'.$key}}" value="{{$company->id}}"

        @foreach($checked_user_permissions as $check)

            @if($check['permission_id'] == $company->id)
            checked
            @endif
     
        @endforeach
       
        >
        {{$company->name}}
      </label>
    </div>
@endforeach