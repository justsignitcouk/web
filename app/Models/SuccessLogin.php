<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SuccessLogin
 */
class SuccessLogin extends Model
{
    protected $table = 'success_logins';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'ip_address',
        'user_agent'
    ];

    protected $guarded = [];

        
}