<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepartmentPermission
 */
class DepartmentPermission extends Model
{
    protected $table = 'department_permission';

    public $timestamps = true;

    protected $fillable = [
        'department_id',
        'permission_id',
        'is_granted'
    ];

    protected $guarded = [];

        
}