<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
include("web/guest.php");
Route::group(['middleware' => ['MyComposeAuth'],['except' => 'profile_requirements']], function(){

    Route::get('/profile_requirements', 'Individual\UserController@viewProfileRequirements', [])->name('profile_requirements');
    Route::post('/profile_requirements/skip', 'Individual\UserController@skipProfileRequirements', [])->name('profile_requirements_skip');

    /********************* inbox *************************/
    Route::group(['prefix' => 'inbox','as'=>'inbox.'], function() {
        Route::resource('/',  'InboxController', []);


        Route::get('/', 'Individual\InboxController@index', [])->name('all');
        Route::post('/', 'Individual\InboxController@getInbox', []);
        Route::get('/sent', 'Individual\InboxController@sent', []);
        Route::post('/sent', 'Individual\InboxController@getSent', []);

        Route::get('/draft', 'Individual\InboxController@draft', [])->name('Draft');
        Route::post('/draft', 'Individual\InboxController@getDraft', []);



        Route::get('/pending', 'Individual\InboxController@pending', []);
        Route::post('/pending', 'Individual\InboxController@getPendingAction', []);

        Route::get('/rejected', 'Individual\InboxController@rejected', []);
        Route::post('/rejected', 'Individual\InboxController@getRejected', []);

        Route::get('/incomplete', 'Individual\InboxController@incomplete', []);
        Route::get('/approved', 'Individual\InboxController@approved', []);
        Route::get('/rejected', 'Individual\InboxController@rejected', []);
        Route::get('/archived', 'Individual\InboxController@archived', []);

    });



    include("old_route.php") ;
    include("settings/companies.php") ;
    include("web/ajax.php") ;
    include("documents/documents.php") ;
    include("admin/admin.php") ;

    Route::group(['prefix' => '{display_name}' ,'as'=>'profile.'], function (){

        Route::get('/', 'Individual\UserController@index', [])->name('index');
        Route::post('saveSign','Individual\UserController@saveSign',[])->name('saveSign') ;
        Route::post('signatures','Individual\UserController@getSignatures',[])->name('getSignatures') ;
        Route::post('default_signatures','Individual\UserController@getDefaultSignature',[])->name('getDefaultSignature') ;
        Route::post('uploadSignature','Individual\UserController@uploadSign',[])->name('uploadSign') ;
        Route::post('deleteSignature','Individual\UserController@deleteSign',[])->name('deleteSign') ;
        Route::post('Editable','Individual\UserController@updateEditable',[])->name('updateEditable') ;
        Route::post('link_account','Individual\UserController@link_account',[])->name('link_account') ;
        Route::post('accounts','Individual\UserController@allAccounts',[])->name('allAccounts') ;
        Route::post('current_account','Individual\UserController@updateCurrentAccount',[])->name('updateCurrentAccount') ;
        Route::post('devices','Individual\UserController@getDevices',[])->name('getDevices') ;
        Route::post('get_current_user','Individual\UserController@getCurrentUser',[])->name('getCurrentUser') ;
        Route::post('deactivate','Individual\UserController@deactivate',[])->name('deactivate') ;

        Route::post('uploadProfilePicture','Individual\UserController@uploadProfilePicture',[])->name('uploadProfilePicture') ;
        Route::post('deleteProfilePicture','Individual\UserController@deleteProfilePicture',[])->name('deleteProfilePicture') ;

        Route::post('profile/updateProfile','Individual\UserController@updatePrimary',[])->name('updatePrimary') ;

        Route::post('profile/change_password','Individual\UserController@changePassword',[])->name('change_password') ;
        Route::post('profile/change_username','Individual\UserController@changeUsername',[])->name('change_username') ;

        Route::post('profile/reference_document','Individual\UserController@setReference',[])->name('reference_document') ;

        Route::post('profile/save_sequence','Individual\UserController@saveSequence',[])->name('saveSequence') ;
        Route::post('profile/delete_sequence','Individual\UserController@deleteSequence',[])->name('deleteSequence') ;


        Route::post('/checkAvailable', 'Individual\UserController@checkAvailable')->name('checkAvailable');
        Route::post('/getLayouts', 'Individual\UserController@getLayouts', [])->name('getLayouts');
        Route::post('/saveLayout', 'Individual\UserController@saveLayout', [])->name('saveLayout');
        Route::post('/deleteLayout', 'Individual\UserController@deleteLayout', [])->name('deleteLayout');

        Route::group(['prefix' => 'contacts' ,'as'=>'contacts.'], function (){

        Route::get('/', 'Individual\ContactsController@index', [])->name('index');
        Route::post('/', 'Individual\ContactsController@getContacts', [])->name('getContacts');
        Route::post('/add', 'Individual\ContactsController@add', [])->name('add');
        Route::post('/getcontact', 'Individual\ContactsController@getContact', [])->name('getContact');
        Route::post('/edit', 'Individual\ContactsController@edit', [])->name('edit');
        Route::post('/delete', 'Individual\ContactsController@delete', [])->name('delete');
        Route::get('/groups', 'Individual\ContactsController@getGroups', [])->name('getGroups');
        Route::post('/addgroup', 'Individual\ContactsController@addGroup', [])->name('addGroup');
        Route::post('/getgroup', 'Individual\ContactsController@getGroup', [])->name('getGroup');
        Route::post('/editgroup', 'Individual\ContactsController@editGroup', [])->name('editGroup');
        Route::post('/deletegroup', 'Individual\ContactsController@deleteGroup', [])->name('deleteGroup');
    });

    });


    Route::get('/contacts/import', 'Individual\ContactsController@import', [])->name('contacts.import');

    Route::group(['prefix' => 'plans','as'=>'plans.'], function() {

        Route::get('/upgrade', 'Common\PlansController@showUpgradeForm', [])->name('upgrade');
        Route::get('/payment/{plan_id}', 'Common\PlansController@ShowPaymentForm', [])->name('payment');
        Route::post('/stripe_card_token', 'Common\PlansController@saveStipeCardToken', [])->name('saveStipeCardToken');
    });

});

