$(document).ready(function () {

    var oDocument ;

    $.post('/document/viewAjax/' + document_id ,{'json':true},function(data){
        oDocument = data;
    },'json');

    $.fn.enableSigner = function (data) {
        var html = '';
        console.log(oDocument);
        var document_recipients = oDocument.document.recipients;
        $.each(document_recipients, function (k, v) {

            if (v.user_id == globals.user_id) {

                if (v.type == 'Signer' && v.is_sign==0) {
                    $.post('/ajax/user/check_if_have_sign', {}, function (dataAjax) {
                        if (dataAjax.status == 'true') {
                            html = '<button class="btn btn-primary insert_sign" data-state_id="' + data.state_id + '" data-stage_id="' + data.stage_id + '" data-action_id="' + data.action_id + '">Insert Sign</button>';
                            $('.signer_contact_item_' + globals.user_id).append(html);
                        }else{
                            html =' You must define Signture in the first <a href="/' + globals.unique_name + '#signatures' + '" >Click hrer</a>' ;
                        }
                    }, 'json');
                }else if(v.is_sign==1) {
                        html =  '<img src="' + v.sign.sign_image + '" class="signer_sign_img" />' ;
                    }

            }

        });
        return html;
    };

    $(document).on('click', '.insert_sign', function (e) {

        var insert_sign = $(this);
        $.post('/' + globals.unique_name + '/default_signatures', {'json': true}, function (data) {
            $('.signer_user_item_' + globals.sIdentifiedS).find('.insert_sign').remove();
            $('.signer_user_item_' + globals.sIdentifiedS).append('<img src="' + data.sign.sign_data + '" class="signer_sign_img" />');
            $('.insert_sign').after("<a class='btn btn-default' href=''>Cancel</a>") ;
            $('.insert_sign').removeClass('insert_sign').addClass('action_button').text('Send');

            $('.action_button').closest('form').append('<input type="hidden" name="sign_id" value="' + data.sign.id + '" />');
        }, 'json');


        e.preventDefault();

    });

    $.fn.drawWorkFlowAction = function () {

        $.post(workflow_get_actions, {'document_id': document_id}, function (data) {

            console.log('-------------');
            console.log(data);
            console.log('------------');
            $.each(data.aStates, function (k, v) {

                var html = '';
                var StateActions = v.StateActions;
                $.each(StateActions, function (k2, v2) {
                    if (v2.Action.function.name == 'Sign') {
                        html += $.fn.enableSigner({
                            'state_id': v2.state_id,
                            'stage_id': data.oCurrentStage.id,
                            'action_id': v2.Action.id,
                            'action_name': v2.Action.name
                        });
                    }


                    $.each(v2.StateActionPermitsUser, function (k3, v3) {

                        if (v3.user_id == globals.user_id) {
                            html += '<button class="btn btn-primary action_button" data-state_id="' + v2.state_id + '" data-stage_id="' + data.oCurrentStage.id + '" data-action_id="' + v2.Action.id + '">' + v2.Action.name + '</button>';
                        }

                    });

                    $.each(v2.StateActionPermitsRole, function (k3, v3) {
                        console.log(globals.user_roles_ids);

                        if ($.inArray(v3.role_id, globals.user_roles_ids) !== -1) {
                            html += '<button class="btn btn-primary action_button" data-state_id="' + v2.state_id + '" data-stage_id="' + data.oCurrentStage.id + '" data-action_id="' + v2.Action.id + '">' + v2.Action.name + '</button>';
                        }
                    });

                });
                html += '    \n' +
                    '  \n' +
                    '';
                $('.actions_container').html(html);
            });
        }, 'json');


    }

    function editor() {
        window.location = '/document/' + document_id + '/edit';
    }

    function open_dialog_comments() {
        $('#global_modal').modal('show');
    }

    function open_dialog_forword() {

        //$('#global_modal').modal('show');
        $.fn.enable_forward_tools();

    }

    $.fn.enable_forward_tools = function () {
        $('body').addClass('add_note_cursor');
        var note_add = false;
        var recipt_add = true;

        if (recipt_add) {
            recipt_add = false;

            $('.action_button').closest('.sidebar-category-actions').before("<div class=\"sidebar-category-actions\">\n" +
                "    <div class=\"category-title\">\n" +
                "        <span data-i18n=\"sec_sidbar.recipient\" >recipient</span>\n" +
                "        <ul class=\"icons-list\">\n" +
                "            <li><a href=\"#\" data-action=\"collapse\">  </a></li>\n" +
                "        </ul>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"category-content\">\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-xs-12\">\n" +
                "\n" +
                "\n" +
                "                    <input type=\"text\" class=\"form-control typeahead\" name=\"recipient\" id=\"recipient_input\" value=\"\"\n" +
                "                           data-provide=\"typeahead\" placeholder=\"\"  data-i18n=\"[placeholder]sec_sidbar.recipient\" />\n" +
                "\n" +
                "\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"category-content\">\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-xs-12\">\n" +
                "                <ul class=\"media-list recipient-list\" id=\"recipient-list\">\n" +
                "\n" +
                "                </ul>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>");
            $('.action_button').after("<a href='' class='btn btn-default' style='margin-left: 10px;'>Cancel</a>") ;

            loadScript("/themes/material/assets/partials/document/recipient.js", function () {
                //initialization code
                $.fn.without_content = false;
            });
        }

        $('.A4').on('click', function (e) {
            $('body').removeClass('add_note_cursor');
            if (!note_add) {
                note_add = true;

                var elment = e.target.id;
                if (elment.indexOf("note_") >= 0 || $(e.target).parents(".note2").length) {
                } else {
                    var count = $(this).find('.note2').length;
                    count++;
                    $(this).append("<div class='editable_note note2' id='note_" + count + "' contentEditable='true' style='top: 0px;left: 0px;'></div>");

                    var x = (e.pageX - this.offsetLeft - 150) + 'px', y = (e.pageY - this.offsetTop - 50) + 'px';
                    $("#note_" + count).css({
                        "position": "absolute",
                        "top": y,
                        "left": x
                    });

                    $('#note_' + count).draggable({containment: "parent"}).on('dragstart',
                        function () {
                            $(this).zIndex(++noteZindex);
                        }).on('dragstop', function () {
                        var top = $(this).css('top');
                        var bottom = $(this).css('bottom');
                        var left = $(this).css('left');
                        var right = $(this).css('right');
                        var positions = 'top:' + top + ';bottom:' + bottom + ';left:' + left + ';right:' + right + ';';
                        var id = $(this).data('id');

                        // $.post('/document/notes/savePosition',{'positions':positions,'note_id':id},function(data){
                        //     console.log(data);
                        // },'json');

                    });


                }
            }

            $('.action_button').html('Send');

            $('.action_button').addClass('forward_action_button').removeClass('action_button');


        });
    }

    $(document).on('click','.forward_action_button',function(e){
        var user_forward = [] ;
        $(document).find('.recipient-user').each(function(){
            var id = $(this).data('id');
            var id_css = id.replace("@", "___");
            id_css = id_css.split('.').join('---');
            id_css = id_css.split(' ').join('');
            id_css = id_css.split('+').join('');

                user_forward.push(id_css);

        });

        if(user_forward.length==0){
            new PNotify({
                title: 'Please enter one recipient at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            return false;
        }

        //contact_forward
        var note_content = $('.editable_note').html();
        if(note_content==''){
            new PNotify({
                title: 'Please enter content.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            return false;
        }
        var note_content_position = $('.editable_note').attr('style');


        $.post("/document/forward",{
            'note_content':note_content,
            'note_content_position':note_content_position,
            'document_id':document_id,
            'forward_id':forward_id,
            'user_forward': user_forward
        },function(data){
            console.log(data);
             window.location = '/document/forward/' + data.forward_id;
        },'json');

        e.preventDefault();


    }) ;


    function loadScript(url, callback) {

        var script = document.createElement("script")
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);

    }

    $.fn.printDocument = function () {
        printJS('/document/pdf/' + document_id);
    }

    $(document).on('click', '.action_button', function (e) {
        var data = $(this).closest('form').serializeArray();
        var action_id = $(this).data('action_id');
        var stage_id = $(this).data('stage_id');
        var state_id = $(this).data('state_id');
        data.push({name: 'action_id', value: action_id});
        data.push({name: 'stage_id', value: stage_id});
        data.push({name: 'state_id', value: state_id});
        data.push({name: 'document_id', value: document_id});

        $.post(workflow_save_action, data, function (data) {

            if (data.action == 'reload') {
                location.reload();
            }

            if (data.action == 'system') {
                if (data.action_type == 'to_next_stage') {
                    location.reload();
                    return true;
                }
                eval(data.javascript);
                $('#global_modal').find('#global_modal_form').html(data.frontend);
                if (data.modal !== undefined) {
                    $('#global_modal').find('.modal-title').html(data.modal.title);
                    $('#global_modal').find('.save-action').html(data.modal.buttons.send);
                    $('#global_modal').find('.save-action').data('action_id', action_id);
                    $('#global_modal').find('.save-action').data('state_id', state_id);
                    $('#global_modal').find('.save-action').data('stage_id', stage_id);
                }

            }

        }, 'json');

        e.preventDefault();
    });

    $(document).on('click', '.save-action', function (e) {
        var action_id = $(this).data('action_id');
        var stage_id = $(this).data('stage_id');
        var state_id = $(this).data('state_id');

        var data = $('#global_modal_form').serializeArray();
        data.push({name: 'action_id', value: action_id});
        data.push({name: 'state_id', value: state_id});
        data.push({name: 'stage_id', value: stage_id});
        data.push({name: 'submit', value: true});
        data.push({name: 'document_id', value: document_id});

        $.post(workflow_save_action, data, function (data) {
            console.log(data);
            if (data.action == 'reload') {
                location.reload();
            }

            if (data.action == 'system') {
                if (data.action_type == 'to_next_stage') {
                    location.reload();
                    return true;
                }
                eval(data.javascript);
                $('#global_modal').find('.modal-body').html(data.frontend);
                if (data.modal !== undefined) {
                    $('#global_modal').find('.modal-title').html(data.modal.title);
                    $('#global_modal').find('.save-action').html(data.modal.buttons.send);
                    $('#global_modal').find('.save-action').data('id', action_id);
                    $('#global_modal').find('.save-action').data('state_id', state_id);
                    $('#global_modal').find('.save-action').data('stage_id', stage_id);
                }

            }

        }, 'json');
        e.preventDefault();
    });

    $.fn.drawWorkFlowAction();


});