<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 */
class Log extends Model
{
    protected $table = 'logs';

    public $timestamps = true;

    protected $fillable = [
        'object_id',
        'table_name',
        'action',
        'created_by',
        'old_value',
        'new_value'
    ];

    protected $guarded = [];

        
}