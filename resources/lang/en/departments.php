<?php

return [

    /*
    |--------------------------------------------------------------------------
    | UsersController Language Lines
    |--------------------------------------------------------------------------
    */

    
    'departments' => 'Departments' ,
    'add_department'=>'Add Department',
    'parent_department' => 'Parent Department' ,
    'delete_department' => 'Are you sure you want to delete this department'


];