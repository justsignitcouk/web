{!! Theme::asset()->themePath()->add([ ['d3', 'js/plugins/visualization/d3/d3.min.js', ['blockui']]]) !!}
{!! Theme::asset()->themePath()->add([ ['d3_tooltip', 'js/plugins/visualization/d3/d3_tooltip.js', ['d3']]]) !!}
{!! Theme::asset()->themePath()->add([ ['settings', 'pages/settings/js/index.js',['d3_tooltip']]]) !!}

<script>
    var companies_progress = {};
    companies_progress['percentage'] = '{{ $oCompaniesQuota->percentage }}' / 100 ;
    companies_progress['usage'] = '{{ $oCompaniesQuota->usage }}' ;
    companies_progress['total'] = '{{ $oCompaniesQuota->total }}' ;
    companies_progress['name'] = 'Companies Quota' ;

    var employees_progress = {};
    employees_progress['percentage'] = '{{ $oEmployeesQuota->percentage }}' / 100 ;
    employees_progress['usage'] = '{{ $oEmployeesQuota->usage }}' ;
    employees_progress['total'] = '{{ $oEmployeesQuota->total }}' ;
    employees_progress['name'] = 'Employees Quota' ;

    var workflows_progress = {};
    workflows_progress['percentage'] = '{{ $oWorkFlowsQuota->percentage }}' / 100 ;
    workflows_progress['usage'] = '{{ $oWorkFlowsQuota->usage }}' ;
    workflows_progress['total'] = '{{ $oWorkFlowsQuota->total }}' ;
    workflows_progress['name'] = 'Workflows Quota' ;
</script>
<style>
    .text-semibold {
        font-size: 100%;
    }
</style>


<div class="content-wrapper">
    <div class="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('alert-' . $msg) }}
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-4 text-center">

            <div class="panel text-center">
                <div class="panel-body">
                    <!-- Progress counter -->
                    <div class="content-group-sm svg-center position-relative" id="companies-progress"></div>
                    <!-- /progress counter -->

                    <a href="{{route('settings.companies')}}" id="my-companies" class="btn btn-primary legitRipple">My Companies</a>

                </div>
            </div>


        </div>
        <div class="col-md-4 text-center">

            <!-- Productivity goal -->
            <div class="panel text-center">
                <div class="panel-body">
                    <!-- Progress counter -->
                    <div class="content-group-sm svg-center position-relative" id="employees-progress"></div>
                    <!-- /progress counter -->
                    @if($oEmployeesQuota->total > $oEmployeesQuota->usage)
                        <a href="{{route('settings.employees')}}" id="my-employees" class="btn btn-primary legitRipple">My Employees</a>

                    @endif
                </div>
            </div>
            <!-- /productivity goal -->

        </div>

        <div class="col-md-4 text-center">

            <!-- Productivity goal -->
            <div class="panel text-center">
                <div class="panel-body">
                    <!-- Progress counter -->
                    <div class="content-group-sm svg-center position-relative" id="workflows-progress"></div>
                    <!-- /progress counter -->
                    @if($oWorkFlowsQuota->total > $oWorkFlowsQuota->usage)
                        <button  href="{{route('settings.workflows')}}" id="my-workflows" class="btn btn-primary legitRipple">My Workflow</button>

                    @endif
                </div>
            </div>
            <!-- /productivity goal -->

        </div>


    </div>
</div>