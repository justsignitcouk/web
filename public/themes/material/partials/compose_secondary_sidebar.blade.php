




{!! Theme::asset()->container('inline')->add([['custom-workflow', '
<script type="text/javascript">
    var workflow_findByCompany = "' .   route('workflow.findByCompany')  .'" ;
    var route_get_stage_details = "' . route('workflow.getStageDetails') . '" ;
</script>']])  !!}

<div class="sidebar sidebar-secondary sidebar-default" style="width: 30%;">
    <div class="sidebar-content">

        <!-- Sidebar tabs -->
        <div class="tabbable sortable ui-sortable">
            <ul class="nav nav-lg nav-tabs nav-justified">
                <li class="active">
                    <a href="#components-tab" data-toggle="tab" class="legitRipple">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 26.6 26.6" style="&#10;height: 20px;&#10;    width: 20px;&#10;    display: inline-block !important;vertical-align: middle;&#10;"><defs><style>.cls{fill:#fff;}.cls-2{fill:none;stroke:#2d3092;}.cls-3{fill:#2d3092;}</style></defs><title>Documents Category</title><g id="a"><circle class="cls" cx="13.3" cy="13.3" r="13.3"/><circle class="cls-2" cx="13.3" cy="13.3" r="12.8"/><path class="cls-3" d="M7.53,5.86,13.33,3a2.93,2.93,0,0,1,3.9,1.5,2.62,2.62,0,0,1,.2,1.5l-7,4A3.86,3.86,0,0,0,7.53,5.86Zm14.7,9.7a3.13,3.13,0,0,1-1.3,2.5c-.1.1.7-.4-7.1,4.5a3.41,3.41,0,0,0-.9-2.4c-1.3-1.4-7.6-8.7-7.6-8.8a2.79,2.79,0,0,1-.8-2,2.09,2.09,0,0,1,2.1-2,2.06,2.06,0,0,1,1.7,1,2.52,2.52,0,0,1-.6,3.4l1,1.2,7.9-4.5c1.1,1.3,4.3,4.4,5.1,5.4A2.86,2.86,0,0,1,22.23,15.56ZM11,15.26l6.1-3.6-.7-.6-6.1,3.6.7.6Zm7.5-2.1-.6-.6-6.2,3.6.6.7,6.2-3.7Zm1.5,1.5-.6-.6-6.3,3.7.6.7,6.3-3.8Zm-10.5,4.4A2.59,2.59,0,0,0,8,22.36c.1.3.3.5.4.8a2.29,2.29,0,0,0,1.7.8c1.3,0,2.5-1.2,1.4-2.6A28.29,28.29,0,0,0,9.53,19.06Z"/></g></svg>
                        <span class=position-right" style="vertical-align: middle;">Components</span>
                    </a>
                </li>
@if($displayWorkflow)
                <li>
                    <a href="#forms-tab" data-toggle="tab" class="legitRipple">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 900.736 900.736" style="enable-background:new 0 0 900.736 900.736;height: 20px;width: 20px;display: inline-block!important;" xml:space="preserve">
<g>
    <g>
        <g>
            <path d="M300.245,296.998c0-27.833-22.564-50.397-50.397-50.397H50.397C22.563,246.601,0,269.166,0,296.998v150.518     c0,8.549,6.929,15.479,15.476,15.479h269.292c8.548,0,15.477-6.93,15.477-15.479V296.998L300.245,296.998z" fill="#2d3092"/>
            <path d="M150.122,217.236c50.418,0,91.291-40.87,91.291-91.29V28.913c0-12.578-11.327-22.123-23.723-19.992     c-24.739,4.25-78.648,14.953-116.787,22.625C76.426,36.47,58.832,57.972,58.832,82.938v43.011     C58.832,176.366,99.704,217.236,150.122,217.236z" fill="#2d3092"/>
            <path d="M850.337,246.601h-199.45c-27.834,0-50.396,22.565-50.396,50.397v150.518c0,8.549,6.929,15.479,15.476,15.479h269.292     c8.547,0,15.478-6.93,15.478-15.479V296.998C900.734,269.165,878.17,246.601,850.337,246.601z" fill="#2d3092"/>
            <path d="M750.612,217.236c50.418,0,91.291-40.87,91.291-91.29V28.913c0-12.578-11.326-22.123-23.725-19.992     c-24.737,4.25-78.646,14.953-116.787,22.625c-24.477,4.924-42.069,26.426-42.069,51.393v43.011     C659.321,176.366,700.193,217.236,750.612,217.236z" fill="#2d3092"/>
            <path d="M550.092,675.72h-199.45c-27.834,0-50.396,22.563-50.396,50.396v150.519c0,8.547,6.929,15.479,15.476,15.479h269.292     c8.547,0,15.478-6.932,15.478-15.479V726.116C600.489,698.283,577.926,675.72,550.092,675.72z" fill="#2d3092"/>
            <path d="M517.934,438.039c-24.737,4.25-78.646,14.953-116.786,22.626c-24.478,4.924-42.071,26.425-42.071,51.392v43.011     c0,50.419,40.872,91.291,91.29,91.291s91.291-40.872,91.291-91.291v-97.033C541.657,445.454,530.331,435.909,517.934,438.039z" fill="#2d3092"/>
            <path d="M261.155,667.431c-25.656-17.588-48.937-38.132-69.58-61.356l21.357-12.363c9.994-5.785,9.977-20.22-0.03-25.98     l-102.803-59.192c-10.006-5.762-22.497,1.468-22.485,13.014l0.121,118.63c0.012,11.549,12.52,18.753,22.514,12.968l23.355-13.519     c26.539,31.72,57.115,59.431,91.255,82.702c9.461,6.449,22.377,3.768,28.381-5.982l13.544-21.998     C272.354,685.304,269.915,673.435,261.155,667.431z" fill="#2d3092"/>
            <path d="M839.66,541.626l-21.998-13.545c-9.044-5.568-20.912-3.13-26.917,5.631c-17.588,25.655-38.131,48.937-61.359,69.579     l-12.36-21.356c-5.785-9.993-20.221-9.979-25.981,0.028L631.85,684.766c-5.761,10.007,1.47,22.497,13.016,22.485l118.63-0.121     c11.548-0.012,18.752-12.521,12.967-22.515l-13.518-23.354c31.719-26.539,59.431-57.114,82.7-91.256     C852.093,560.545,849.411,547.629,839.66,541.626z" fill="#2d3092"/>
            <path d="M339.122,133.296c2.458,10.333,12.574,17,23.014,15.053c30.578-5.705,61.565-7.64,92.587-5.812l-6.359,23.843     c-2.977,11.157,7.243,21.352,18.392,18.351l114.548-30.838c11.149-3.001,14.869-16.944,6.697-25.102l-83.969-83.798     c-8.174-8.157-22.113-4.407-25.089,6.751l-6.956,26.071c-41.195-3.662-82.41-1.638-123.007,6.049     c-11.25,2.132-18.487,13.159-15.837,24.301L339.122,133.296z" fill="#2d3092"/>
        </g>
    </g>
</g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
</svg>
                        <span class=position-right" style="vertical-align: middle;">Element</span>
                    </a>
                </li>
@endif
              {{--  <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 26.6 26.6" style="&#10;    height: 20px;&#10;    width: 20px;&#10;    display: inline-block!important;&#10;"><defs><style>.docview{fill:#2d3092;}</style></defs><title>Document Preview</title><path class="docview" d="M13.3.5V1A12.3,12.3,0,1,1,4.6,4.6,12.26,12.26,0,0,1,13.3,1V0A13.3,13.3,0,1,0,26.6,13.3,13.3,13.3,0,0,0,13.3,0Z"/><path class="docview" d="M10.05,13.66l.6.7,4.2-2.5-.6-.7Zm6.2-.2-.6-.7-4.3,2.5.6.7Zm4.1,0a4.87,4.87,0,0,1-1.1.4,4.89,4.89,0,0,0-.3.7l.3.3a1.27,1.27,0,0,1-.1,1.8c-.1.1-.2.1-.2.2-.7.4-4.7,3-5.7,3.6a3.63,3.63,0,0,0-.7-1.7c-.4-.5-5.4-6.5-6.7-8.1a1.57,1.57,0,0,1-.3-1.6,1.34,1.34,0,0,1,1.3-.8,1.39,1.39,0,0,1,1.4,1.1,1.29,1.29,0,0,1-.6,1.5l1.2,1.4,6.9-3.9a3.06,3.06,0,0,0,.7-4.3A2.79,2.79,0,0,0,14,2.86c-1.1,0-.7,0-8.5,4a3.08,3.08,0,0,0-1.8,2.8,3.49,3.49,0,0,0,.8,2.2c1.3,1.6,6.7,8.2,6.8,8.2a1.36,1.36,0,0,1-.3,2,1.36,1.36,0,0,1-2-.3c-.1-.2-.2-.3-.2-.5a1.44,1.44,0,0,1,.5-1.5l-1.1-1.4A3,3,0,0,0,7,20.56a3.17,3.17,0,0,0,2.9,3.5,3.44,3.44,0,0,0,2-.4l8.4-5.3a3.32,3.32,0,0,0,1.3-2.6A3,3,0,0,0,21,14l-.6-.5Zm-7-8.9a1.09,1.09,0,0,1,.5-.1,1.23,1.23,0,0,1,1.1.7,1.28,1.28,0,0,1-.5,1.7L10,9.46a2.81,2.81,0,0,0-1.3-2.4Zm1.8,11.9.3-.8.2-.5L12.55,17l.6.7C13.25,17.56,15.15,16.46,15.15,16.46Z"/><path class="docview" d="M21.48,10.31a1.3,1.3,0,0,1-2.6,0v-.2a.75.75,0,0,0,1-.5.45.45,0,0,0,0-.5h.2C21,9.11,21.48,9.61,21.48,10.31ZM20.15,7.46a6.49,6.49,0,0,0-5,2.7s2,3.1,5,3.1c3.2,0,5-3.1,5-3.1A6.65,6.65,0,0,0,20.15,7.46Zm0,5a2.1,2.1,0,1,1,2.1-2.1h0A2.11,2.11,0,0,1,20.15,12.46Z"/></svg>
                        <span class="visible-xs-inline-block position-right">Other components</span>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#other-tab" data-toggle="tab"><i class="icon-task pull-right"></i> <span data-i18n="sec_sidbar.Other" ></span></a></li>
                        <li><a href="#custom-tab" data-toggle="tab"><i class="icon-googleplus5 pull-right"></i><span data-i18n="sec_sidbar.Custom_content" ></span> </a></li>
                    </ul>
                </li>--}}
                <li class="">
                    <a href="#choose_template" data-toggle="tab" class="legitRipple">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 26.6 26.6" style="&#10;    height: 20px;&#10;    width: 20px;&#10;    display: inline-block !important;vertical-align: middle;&#10;"><defs><style>.cls{fill:#fff;}.cls-2{fill:none;stroke:#2d3092;}.cls-3{fill:#2d3092;}</style></defs><title>Documents Category</title><g id="a"><circle class="cls" cx="13.3" cy="13.3" r="13.3"/><circle class="cls-2" cx="13.3" cy="13.3" r="12.8"/><path class="cls-3" d="M7.53,5.86,13.33,3a2.93,2.93,0,0,1,3.9,1.5,2.62,2.62,0,0,1,.2,1.5l-7,4A3.86,3.86,0,0,0,7.53,5.86Zm14.7,9.7a3.13,3.13,0,0,1-1.3,2.5c-.1.1.7-.4-7.1,4.5a3.41,3.41,0,0,0-.9-2.4c-1.3-1.4-7.6-8.7-7.6-8.8a2.79,2.79,0,0,1-.8-2,2.09,2.09,0,0,1,2.1-2,2.06,2.06,0,0,1,1.7,1,2.52,2.52,0,0,1-.6,3.4l1,1.2,7.9-4.5c1.1,1.3,4.3,4.4,5.1,5.4A2.86,2.86,0,0,1,22.23,15.56ZM11,15.26l6.1-3.6-.7-.6-6.1,3.6.7.6Zm7.5-2.1-.6-.6-6.2,3.6.6.7,6.2-3.7Zm1.5,1.5-.6-.6-6.3,3.7.6.7,6.3-3.8Zm-10.5,4.4A2.59,2.59,0,0,0,8,22.36c.1.3.3.5.4.8a2.29,2.29,0,0,0,1.7.8c1.3,0,2.5-1.2,1.4-2.6A28.29,28.29,0,0,0,9.53,19.06Z"/></g></svg>
                        <span class=position-right" style="vertical-align: middle;">Templates</span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active no-padding" id="components-tab">
                {{--@partial('document.recipient',[ 'aDocument'=>$aDocument , 'id'=> isset($id) ? $id : false ])--}}









                </div>
                @if($displayWorkflow)
                <div class="tab-pane  no-padding" id="forms-tab">

                <!-- Block buttons -->
                    <div class="sidebar-category">
                     {{--   <div class="category-title">
                            <span data-i18n="sec_sidbar.document_actions">{{__('document.actions')}}</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>--}}

                        <div class="category-content">
                            <div class="row">

                                {!! Form::select('WorkFlow', ['' => __('common.please_choose') ] + [], '', ['class' => 'workflow_select form-control m-bot15']) !!}

                                @partial('workflow.view')


                            </div>
                        </div>
                    </div>
                    <!-- /block buttons -->

                </div>
                @endif
                <div class="tab-pane  no-padding" id="choose_template">

                    <!-- Block buttons -->
                    <div class="sidebar-category">
                        <div class="category-content">
                            <div class="row" id="template_container">

                            </div>
                        </div>
                    </div>
                    <!-- /block buttons -->


                </div>
            </div>
        </div>
    </div>
</div>

