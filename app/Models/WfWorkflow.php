<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Workflow
 */
class Workflow extends Model
{
    protected $table = 'wf_workflow';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'company_id',
        'description',
        'active',
        'created_by',
        'updated_by'
    ];

    protected $guarded = [];

    public function WfStage()
    {
        return $this->hasMany('App\Models\Workflow\Stage', 'workflow_id', 'id');
    }
        
}