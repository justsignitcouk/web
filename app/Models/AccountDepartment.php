<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountDepartment
 */
class AccountDepartment extends Model
{
    protected $table = 'account_department';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'department_id',
        'active'
    ];

    protected $guarded = [];

        
}