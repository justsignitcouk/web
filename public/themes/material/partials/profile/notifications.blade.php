
@foreach($aNotifications as $aNotification)

    @if ($aNotification['read'])
        <li class="p-3 mb-2 {{--bg-primary--}} text-white " id="read_notification" data-notification_id="{{$aNotification['notification_id']}}" data-document_id="{{$aNotification['document_id']}}">
    @else
        <li class="p-3 mb-2 {{--bg-info--}} text-white" id="read_notification" data-notification_id="{{$aNotification['notification_id']}}" data-document_id="{{$aNotification['document_id']}}">
    @endif

            <a><ul class="media-list">
                    <li class="media">
                        <div class="media-left">
                            <img src="/assets/images/Userpic.png" class="img-circle img-sm" alt="">
                        </div>
                        <div class="media-body">
                            {{$aNotification['content']}}
                            <span class="display-block text-muted">Tardy rattlesnake seal raptly earthworm...</span>
                        </div>
                    </li>
                </ul>

            </a>
        </li>

@endforeach
