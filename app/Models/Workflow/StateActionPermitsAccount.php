<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class StateActionPermitsAccount extends Model
{
    protected $table = 'wf_state_action_permits_account';

    public $timestamps = true;

    public function Account()
    {
        return $this->belongsTo('App\Models\Account','account_id','id');
    }

}
