<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Access extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function can($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();

        $nPlanID = null;

        if(!isset($aParams['plan_id'])) {
            $aParams['plan_id'] = $nPlanID;
        }

        try{
            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("access")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception);
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return $response_array;
        return (object) $response_array['data'] ;
    }



}
