
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($user['id']))
            <h5 class="panel-title">{{ __("roles.add_role") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 
	        <div class="col-md-6">
                
                {{ csrf_field() }}

                <meta name="csrf-token" content="{{ csrf_token() }}">

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.name") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="name"
		                   class="form-control"
		                   placeholder='{{ __("users.first_name") }}'
		                   @if(!empty($role['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('name') }}"
			                    @else
			                    	value="{{ $role['name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('name') }}"
		                   @endif >

	                @if ($errors->has('name'))
		                <font color="red">{{ $errors->first('name') }}</font>
		        	@endif

		        </div>

            <div class="form-group">

              <label>
              	{{ __("common.description") }} :
              </label>

              {!! Form::textarea('description',null, ['rows' => 9 ,'class'=>'form-control input-sm','placeholder'=> __("common.description") ]) !!}

                @if ($errors->has('description'))
                    <font color="red">{{ $errors->first('description') }}</font>
              @endif

            </div>

		    </div>
		    <div class="col-md-6">
	         
	            <div class="form-group">

	            	<label>
			            {{ __("common.display_name") }}
			              :
		            </label>

                    {!! Form::text('display_name', null, ['class' => 'form-control input-sm required','placeholder'=> __("common.display_name")]) !!}
		            
	                @if ($errors->has('display_name'))
		                <font color="red">{{ $errors->first('display_name') }}</font>
		        	@endif
		            
		        </div>
		        <div class="form-group">
		            
		            <label>
			            {{ __("roles.role_type") }}
			              :
		            </label>

		            <select  class="col-md-12 role_type"  name="role_type">

	                    <option disabled selected value>
	                         	{{ __("common.select") }}
	                    </option>

			        	@foreach($role_types as $role_type)
             
                            
	                        <option 

                           
	                        @if(!empty($role['type_id']) && $role_type->id == $role['type_id'])
                            selected
                            @endif

                            data-type="{{$role_type->id}}"
		                    value="{{$role_type->id}}" >

		                    {{$role_type->name}}

		                    </option>

                        @endforeach
		        	</select>

	                @if ($errors->has('parent_department'))
		                <font color="red">{{ $errors->first('parent_department') }}</font>
		        	@endif

		        </div>

		    </div>
	    </div>

	    @include('layouts/partials/permissions')

	    <div class="row col-md-12">

         	{{ Form::button(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

            @can('allowed-permission', ['usersroles-indexdelete','roles-delete'])
	            @if(!empty($role["id"]))
	            <div class="btn btn-danger pull-right"   class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_theme_danger">{{__("common.delete")}}
	            </div>
	            @endif
            @endcan

	    </div>   
      </div>
    </div>
</div>
<!-- Bootstrab modals -->
@if(!empty($role["id"]))
<div id="modal_theme_danger" class="modal fade delete-caution" style="display: none;">
 <input id='role-data' type="hidden" data-id='{{$role["id"]}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("roles.delete_role")}}
				</p>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
				<button type="button" class="btn btn-danger" id='delet-role'>
				{{__("common.delete")}}
				</button>
			</div>
		</div>
	</div>
</div>
@endif
<div id="modal_theme_danger" class="modal fade delet-role-modal" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("roles.cant_delete_role")}}
				</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
			</div>
	    </div>
    </div>
</div>
<div id="modal_theme_danger" class="modal fade submit-role-modal" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.save")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("roles.save_caution")}}
				</p>
			</div>

			<div class="modal-footer">

			    <button type="button" class="save-role btn btn-link" >
				{{__("common.save")}}
				</button>
				<button type="button" data-dismiss="modal" class=" btn btn-link">
				{{__("common.cancel")}}
				</button>
				
			</div>
	    </div>
    </div>
</div>
<!-- /Bootstrab modals -->

<script type="text/javascript">

    $(".hide_per").hide();
    
   
    $( ".role_type" ).change(function() {

    	 var selected = $(this).find('option:selected');
         var type = selected.data('type');


          switch(type) {
		    case 1:

		        $(".users_permissions").show();
		        $(".roles_permissions").show();
		        $(".permissions_permissions").show();
		        $(".accounts_permissions").show();
		        $(".companies_permissions").show();
		        $(".branches_permissions").show();
		        $(".departments_permissions").show();
		        $(".workflow_permissions").show();
		        $(".requirements_permissions").show();
		 
		        break;

		    case 2:

		        $(".hide_per").hide();

		       	$(".departments_permissions").show();
		       	$(".accounts_permissions").show();

		    
		      
		        break;
		    case 3:

		        $(".hide_per").hide();

		       	$(".accounts_permissions").show();
		    
		       
		        break;    
		   
		}

    	
	
	});


	$('#delet-role').click(function() {
        var id = $('#role-data').data("id");
        var _token = $('input[name="_token"]').val();

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('roles' ) !!}" + "/" + id +'/delete',
            data: { _token : _token },
	        success: function( msg ) {
                if(msg = 'faild'){
              
                  //$('.delete-caution').modal('hide');
                  $('.delet-role-modal').modal('show');

                }else{
                  location.replace("/roles");
                } 
	        },
	        error: function( data ) {

	        }
	    });
     });

    //Caution that user did not check any of permissions
	$(".submit_button").click(function(){
	    if (!$('input[type="checkbox"]').is(":checked")) {
	    
	    	$('.submit-role-modal').modal('show');

		}else{

            $('.submit-roles').submit();

		}
	});
	$(".save-role").click(function(){

	    $('.submit-roles').submit();

	});
	

	

</script>
