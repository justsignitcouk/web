<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LayoutComponent
 */
class LayoutComponent extends Model
{
    protected $table = 'layout_components';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'content',
        'layout_id'
    ];

    protected $guarded = [];

        
}