<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Contact extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function get($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        $nContactID = oContact()->id;
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserId ."/contact")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }


    public static function getMyContacts($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserId = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserId ."/mycontact")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage()) ;
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return  $response_array['data']['contacts'] ;
    }


    public  static function inbox($aParams){
        $oServices = Parent::getInstance();
        $nContactID = oContact()->id;
        try{
            $oClient =  $oServices
                                    ->setMethod('GET')
                                    ->setUrl("contact/" . $nContactID ."/" . $aParams['sFilter'])
                                    ->setBody( $aParams )
                                    ->withToken()
                                    ->request()
                                    ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[]],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }



    public static function sent($aParams){
        $oServices = Parent::getInstance();
        $nAccountID = currentAccountID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("contact/" . $nAccountID ."/sent")
                ->setBody( $aParams )
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[]],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }


    public static function badges(){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("contact/badges")
                ->setBody(['account_id'=>currentAccountID()])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','rejected'=>[]],
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function findBy($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("account/findBy")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function findFirstById($id){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id )
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['account'] ;
    }

    public static function getContacts(){
        $oServices = Parent::getInstance();
        $id = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id . "/contacts" )
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['aContacts'] ;
    }

    public static function getContactsForRecipient($aParams){
        $oServices = Parent::getInstance();
        $id = currentAccountID();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("account/" . $id . "/contacts_for_recipient" )
                ->setQuery($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['aContacts'] ;
    }

    public static function saveContacts($aContacts){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/" . nUserID() . "/mycontact" )
                ->setBody(['aContacts'=>$aContacts])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }



}
