<?php
namespace App\Services;
use Illuminate\Support\Facades\Config;

class Editable extends \App\Services\Services
{
    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function save($aParams=[]){
        $oServices = Parent::getInstance();


        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/editable")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            return false;
        }

        $response_array = json_decode($oClient,true);
        return true;
    }


}