<!DOCTYPE html>
<html lang="en">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@get('title')</title>
        {{--@styles()--}}
        @scripts('builder')
        @styles('builder')

        <style>
            body,
            html {
                height: 100%;
                margin: 0;
            }
        </style>
    </head>

    <body>
                @content()
                @scripts('builder-after')


    </body>
</html>