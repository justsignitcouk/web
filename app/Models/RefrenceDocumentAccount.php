<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RefrenceDocumentAccount
 */
class RefrenceDocumentAccount extends Model
{
    protected $table = 'refrence_document_account';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'account_id'
    ];

    protected $guarded = [];

        
}