@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        
        {!! Form::open(['route'=>'admin.users.store','files'=>'true','method'=>'POST','enctype'=>'multipart/form-data']) !!}

            @include('users.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
