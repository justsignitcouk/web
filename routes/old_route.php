<?php

Route::post('/file/save', 'Individual\FileController@save')->name('upload_file');




/********************* companies *************************/
Route::get('companies', 'Individual\CompaniesController@index')->name('companies');
Route::get('companies/datatables', 'Individual\CompaniesController@datatables')->name('companies.data');
Route::get('companies/{id}', 'Individual\CompaniesController@details')->name('companies.details');
Route::get('companies/update/{id}', 'Individual\CompaniesController@update')->name('companies.update');

Route::get('account', 'Individual\AccountController@index')->name('account.update');
Route::post('account/make_default', 'Individual\AccountController@make_default')->name('account.make_default');


/********************* home *************************/
Route::group(['prefix' => 'home','as'=>'home.'], function() {
Route::resource('/',  'HomeController', []) ;

Route::get('/', 'Individual\HomeController@index', [])->name('home');
Route::post('/', 'Individual\HomeController@getInbox', [])->name('getInbox');

Route::get('/sent', 'Individual\HomeController@sent', [])->name('sent');
Route::post('/sent', 'Individual\HomeController@getSent', [])->name('getSent');

Route::get('/draft', 'Individual\HomeController@draft', [])->name('Draft');
Route::post('/draft', 'Individual\HomeController@getDraft', [])->name('getDraft');



Route::get('/pending', 'Individual\HomeController@pending', [])->name('pending');
Route::post('/pending', 'Individual\HomeController@getPendingAction', [])->name('getPendingAction');

Route::get('/rejected', 'Individual\HomeController@rejected', [])->name('rejected');
Route::post('/rejected', 'Individual\HomeController@getRejected', [])->name('getRejected');

Route::get('/incomplete', 'Individual\HomeController@incomplete', [])->name('incomplete');
Route::get('/approved', 'Individual\HomeController@approved', [])->name('approved');
Route::get('/rejected', 'Individual\HomeController@rejected', [])->name('rejected');
Route::get('/archived', 'Individual\HomeController@archived', [])->name('archived');

}) ;

/********************* notifications *************************/
Route::post('/notifications', 'Individual\NotificationsController@notificationsAjax', [])->name('notificationsAjax');
Route::post('/change_read_status', 'Individual\NotificationsController@changeReadStatus', [])->name('changeReadStatus');


/********************* dashboard  *************************/
Route::post('/activities', 'Individual\UserActivitiesController@userActivitiesAjax', [])->name('userActivitiesAjax');
Route::post('/limited_activities', 'Individual\UserActivitiesController@limitedUserActivitiesAjax', [])->name('limitedUserActivitiesAjax');

Route::post('/needs_my_signature', 'Individual\DashboardController@needsMySignatureAjax', [])->name('needsMySignatureAjax');
Route::post('/waiting_from_others_signature', 'Individual\DashboardController@waitingFromOthersSignatureAjax', [])->name('waitingFromOthersSignatureAjax');

Route::get('/logs', 'Individual\LogsController@index')->name('logs');










Route::get('/MyAdmin/scripts/setup.php',function(){
return view('404.welcome');
});










/********************* workflow routes*************************/
Route::group(['prefix' => 'workflow','as'=>'workflow.'], function (){
Route::get('/workflow', 'Individual\WorkflowController@findByCompany', [])->name('findByCompany');
Route::post('/workflow/stage/details', 'Individual\WorkflowController@getStageDetails', [])->name('getStageDetails');
Route::post('/workflow/stage/getActions', 'Individual\WorkflowController@getActions', [])->name('getActions');
Route::post('/stage/saveAction', 'Individual\WorkflowController@saveAction', [])->name('saveAction');


});
/********************* workflow routes*************************/

Route::get('/paypal', 'PayPalController@getIndex');
Route::get('paypal/ec-checkout', 'PayPalController@getExpressCheckout');
Route::get('paypal/ec-checkout-success', 'PayPalController@getExpressCheckoutSuccess');
Route::get('paypal/adaptive-pay', 'PayPalController@getAdaptivePay');
Route::post('paypal/notify', 'PayPalController@notify');


