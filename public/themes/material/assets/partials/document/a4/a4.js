var max_pages = 100;
var page_count = 0;

function snipMe() {
    page_count++;
    if (page_count > max_pages) {
        return;
    }
    var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    var children = $(this).children().toArray();
    var removed = [];
    while (long > 0 && children.length > 0) {
        var child = children.pop();
        $(child).detach();
        removed.unshift(child);
        long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    }
    if (removed.length > 0) {
        var a4 = $('<div class="A4" contenteditable="true"></div>');
        a4.append(removed);
        $(this).after(a4);
        $(this).trigger( 'keypress', [{preventDefault:function(){},keyCode:9}] );
        snipMe.call(a4[0]);

    }
}

$(document).ready(function() {
    $('.A4').each(function() {
        snipMe.call(this);
    });

    $('.respreate').on('click',function(){
        max_pages = 100;
        page_count = 0;
        $('.A4').each(function() {
            snipMe.call(this);
        });
    });

    $('.A4').on('keyup',function(){
        max_pages = 100;
        page_count = 0;
        $('.A4').each(function() {
            snipMe.call(this);
        });
    });

});
