<?php
namespace App\Services;
use Illuminate\Support\Facades\Config;

class WorkFlow extends \App\Services\Services
{
    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function saveWfStateAction($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("workflow/saveWfStateAction")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            return false;
        }

        $response_array = json_decode($oClient,true);
        return true;
    }

    public static function getForCurrentAccount($aParams=[]){
        $oServices = Parent::getInstance();
        $nAccountId = currentAccountID() ;
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("workflow/getByAccount")
                ->setBody([ 'account_id' => $nAccountId ])
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);
        dd($response_array);
        return $response_array['data'] ;
    }


    public static function getByCompany($aParams=[]){

        $oServices = Parent::getInstance();
        $nCompanyId = currentCompanyID() ;
        $aParams = $aParams + [ 'company_id' => $nCompanyId ];
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("workflow/getByCompany")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){

            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['workflow'] ;
    }

    public static function getStageDetails($aParams=[]){

        $oServices = Parent::getInstance();
        $nCompanyId = currentCompanyID() ;

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("workflow/getStageDetails")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){

            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function getActions($aParams=[]){

        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("actions/" . $aParams['document_id'] )
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function saveAction($aParams=[]){

        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("actions" )
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array ;
    }

    public static function getDefaultWorkFlow($aParams=[]){

        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("workflow/get_default" )
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['workflow'] ;
    }

}