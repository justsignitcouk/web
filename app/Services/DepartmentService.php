<?php


namespace App\Services;

use App\Models\PermissionUser;
use App\Models\DepartmentPermission;

use DB ;


class DepartmentService{


    function __construct(){


    }

    public function checkedDepartmentPermissions($department_id){

        return      DepartmentPermission::
                    select('permission_id')
                    ->where('department_id',$department_id)
                    ->get()
                    ->toArray(); 

    }

	
    public function Permissions($module_name){

        return  DB::table('modules')
                ->join('permissions','permissions.module_id','=','modules.id')
                ->select('modules.name','permissions.*')
                ->where('modules.name','=',$module_name)
                ->get();

    }


}