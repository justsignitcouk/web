@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        {!! Form::open(['route'=>'departments.store','files'=>'true','enctype'=>'multipart/form-data']) !!}

            @include('departments.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
