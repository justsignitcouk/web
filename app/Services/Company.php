<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/19/17
 * Time: 4:11 PM
 */

namespace App\Services;


use GuzzleHttp\Client;

class Company extends \App\Services\Services
{

    public function getCompanyDepartments(){
        $client = new Client();
        $api_url = config()->get('app')['api_url'];
        $sToken = session()->get("sToken") ;
        $company_id = currentCompanyID();
        $users = $client->request('POST' ,$api_url.'/companies/company_departments', [
            'headers'         => ['Authorization' => 'Bearer ' . $sToken],
            'form_params' => [
                'company_id' => $company_id
            ]
        ]);

        $response = $users->getbody()->getContents();
        $response_array = json_decode($response,true);
        return $response_array['data'] ;
    }

    public static function save($aParams){

        $oServices = Parent::getInstance();

        unset($aParams['_token']);

        try {

            $oClient = $oServices
                ->setMethod('post')
                ->setUrl("companies")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            dd($e);
            return $e->getMessage();
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public static function saveBranch($aParams){

        $oServices = Parent::getInstance();

        try {

            $oClient = $oServices
                ->setMethod('post')
                ->setUrl("companies/saveBranch")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            dd($e);
            return $e->getMessage();
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public static function checkAvailability($aParams){

        $oServices = Parent::getInstance();

        unset($aParams['_token']);

        try {

            $oClient = $oServices
                ->setMethod('post')
                ->setUrl("companies/exist")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {

            return $e->getMessage();
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];

    }

    public static function getCompanyByCreatorID($aParams=[]) {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("companies/getByCreator/" . nUserID() )
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public static function getCompanyBySlug($sSlug) {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("companies/" . $sSlug)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }


    public static function getEmployeesByCompanySlug($sSlug) {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("companies/" . $sSlug . '/employees')
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public static function getRolesByCompanySlug($sSlug) {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("companies/" . $sSlug . '/roles')
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public static function getBrachById($sSlug,$nBranchID) {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("companies/" . $sSlug . "/" . $nBranchID )
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }


}