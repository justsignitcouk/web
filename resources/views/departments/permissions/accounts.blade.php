@foreach($accounts_permissions as $key => $account)
    <div class="form-check">
      <label class="account-check-label">
        <input class="account-check-input" type="checkbox" name ="{{'accounts-'.$key}}" value="{{$account->id}}"

        @foreach($checked_user_permissions as $check)

            @if($check['permission_id'] == $account->id)
            checked
            @endif
     
        @endforeach
       
        >
        {{$account->name}}
      </label>
    </div>
@endforeach