{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}
{!! Theme::asset()->themePath()->add([ ['import', 'pages/document/import.js',['plupload_full']]]) !!}

{!! Theme::asset()->themePath()->add([ ['animatecss.css','css/extras/animate.min.css',['bootstrap']]]) !!}
<script>
    var route_import_document = "{{ route('document.sendImport') }}" ;
</script>
<style>
    #uploadfiles {
        display: none;
    }
</style>

<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="panel">
                    <div class="panel-body">
                        <h3>Import document</h3>

                        <div class="row">


                            <div class="col-md-12" style="text-align: center;">


                                <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                                <br/>

                                <div id="container">
                                    <button id="pickfiles" class="btn btn-success">Choose Document</button>
                                    <a id="uploadfiles" href="javascript:;">[Upload files]</a>
                                </div>

                                <hr/>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var route_upload_profile = "{{route('profile.uploadProfilePicture',[getUniqueName()])}}"
    var route_delete_profile = "{{route('profile.deleteProfilePicture',[getUniqueName()])}}"
</script>