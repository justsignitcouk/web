$(document).ready(function(){

    var bpassword_confirm_validation = 0 ;
    var bpassword_validation = 0 ;
    var bemail_validation = 0 ;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    $("#mobile").intlTelInput({
        hiddenInput: "full_mobile",
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        utilsScript: "/themes/material/assets/js/intl-tel-input/build/js/utils.js" // just for formatting/placeholders etc
    });


    $('.styled').uniform();

}) ;