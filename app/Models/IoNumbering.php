<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class IoNumbering
 */
class IoNumbering extends Model
{
    protected $table = 'io_numbering';

    public $timestamps = false;

    protected $fillable = [
        'prefix',
        'counter',
        'postfix',
        'sperator',
        'year',
        'type',
        'departmint_id'
    ];

    protected $guarded = [];

        
}