/* ------------------------------------------------------------------------------
 *
 *  # Change language without page reload
 *
 *  Specific JS code additions for internationalization_switch_direct.html page
 *
 *  Version: 1.0
 *  Latest update: Aug 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {


    // Configuration
    // -------------------------

    // Hide sidebar category titles on load
    $('.category-title > span').css('visibility', 'hidden');
var lang_code = '' ;
var use_cookie = true;

    if(typeof  globals !== 'undefined' ){
        lang_code  = globals.sLangCode;
        use_cookie= false;
    }else{


    }
    // Add options
    i18n.init({
            resGetPath: '/themes/material/assets/locales/__lng__.json',
            debug: false,
            optionsAttr: 'i18n-options',
            useOptionsAttr: true,
            parseDefaultValueFromContent: true,
            useCookie: use_cookie,
            fallbackLng: false,
            load: lang_code,
            lng: lang_code
        },
        function () {

            // Init
            $('body').i18n();

            // Show sidebar category titles after load
            $('.category-title > span').css('visibility', 'visible');
        });
    var switchContainer = $('.language-switch');
    //
    // // Change languages in dropdown
    // // -------------------------
    //
    // // English
    if (i18n.lng() === "en") {
        remove_rtl();
        // Set active class
        $('.english').parent().addClass('active');

        // Change language in dropdown
        $('.language-switch').children('.dropdown-toggle').html(
            $('.language-switch').find('.english').html() + ' <i class="caret" />'
        ).children('img').addClass('position-left');
    }
    //

    if (i18n.lng() === "ar") {

        insert_rtl();
        // Set active class
        $('.arabic').parent().addClass('active');

        // Change language in dropdown
        $('.language-switch').children('.dropdown-toggle').html(
            $('.language-switch').find('.arabic').html() + ' <i class="caret" />'
        ).children('img').addClass('position-left');
    }

    // // English
    $('.english').on('click', function () {
        remove_rtl();
        // Set language
        $.i18n.setLng('en', function () {
            $('body').i18n();
        });

        // Change lang in dropdown
        switchContainer.children('.dropdown-toggle').html(
            $('.english').html() + ' <i class="caret" />'
        ).children('img').addClass('position-left');

        // Set active class
        switchContainer.find('li').removeClass('active');
        $('.english').parent().addClass('active');

        $.post("/ajax/set_language",{'lang':'en'},function(){

        }) ;
    });

    //
    // // arabic
    $('.arabic').on('click', function () {
        insert_rtl();
        // Set language
        $.i18n.setLng('ar', function () {
            $('body').i18n();
        });

        // Change lang in dropdown
        switchContainer.children('.dropdown-toggle').html(
            $('.arabic').html() + ' <i class="caret" />'
        ).children('img').addClass('position-left');

        // Set active class
        switchContainer.find('li').removeClass('active');
        $('.arabic').parent().addClass('active');

        $.post("/ajax/set_language",{'lang':'ar'},function(){

        }) ;
    });

    var count = 0;

    function insert_rtl() {


        $('body').addClass("rtl") ;
        $('body').removeClass("ltr") ;
        $('link').each(function () {
            var url = $(this).attr('href');

            if (url.indexOf('bootstrap.css') >= 0) {
                if (url.indexOf('bootstrap-rtl.css') < 0) {
                    $('<link media="all" type="text/css" rel="stylesheet" href="/themes/material/assets/css/bootstrap-rtl.css">').insertAfter(this);
                }

            }

            if (url.indexOf('components.min.css') >= 0) {
                var newurl = url.replace('css/components.min.css', 'css/components-rtl.css');
                 $(this).attr('href',newurl);
            }

            if (url.indexOf('core.css') >= 0) {
                var newurl = url.replace('css/core.css', 'css/core-rtl.css');
                $(this).attr('href', newurl);
            }

            if (url.indexOf('style.css') >= 0) {
                var newurl = url.replace('css/style.css', 'css/style-rtl.css');
                $(this).attr('href', newurl);
            }


        });


    }

    function remove_rtl() {

        $('body').addClass("ltr") ;
        $('body').removeClass("rtl") ;
        $('link').each(function () {

            var url = $(this).attr('href');

            if (url.indexOf('bootstrap-rtl.css') >= 0) {
                $('<link media="all" type="text/css" rel="stylesheet" href="/themes/material/assets/css/bootstrap.css">').insertAfter(this);
                $(this).remove();
            }

            if (url.indexOf('css/components-rtl.css') >= 0) {
                var newurl = url.replace('css/components-rtl.css', 'css/components.min.css');
                $(this).attr('href', newurl);
            }

            if (url.indexOf('css/core-rtl.css') >= 0) {
                var newurl = url.replace('css/core-rtl.css', 'css/core.css');
                $(this).attr('href', newurl);
            }

            if (url.indexOf('css/style-rtl.css') >= 0) {
                var newurl = url.replace('css/style-rtl.css', 'css/style.css');
                $(this).attr('href', newurl);
            }


        });

    }

//     function replace_url(old_v, new_v) {
//         var window_url = window.location.href;
//         var url = window_url.replace(old_v, new_v);
//         window.history.pushState({urlPath: '/ar'}, "", url);
//         $('a').each(function () {
//
//             var window_url = $(this).attr('href');
//             if (typeof window_url !== typeof undefined && window_url !== false) {
//             var url = window_url.replace(old_v, new_v);
//             $(this).attr('href', url);
//         }
//
//
//     });
// }

});

