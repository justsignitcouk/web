{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['form_select2', 'js/pages/form_select2.js',['select2']]]) !!}

{!! Theme::asset()->add([ ['validate', 'assets/components/validation/dist/jquery.validate.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['validate_additonal', 'assets/components/validation/dist/additional-methods.js',['validate']]]) !!}

{!! Theme::asset()->themePath()->add([ ['inbox', 'pages/premium/settings/js/index.js',['form_select2']]]) !!}
{!! Theme::asset()->themePath()->add([ ['create_company', 'pages/premium/settings/js/company/create_company.js',['validate_additonal']]]) !!}



<div class="content-wrapper">
    <div class="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('alert-' . $msg) }}
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">{{ __('premium/dashboard/index.setup_company') }}<a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>
                {!! Form::open(['route' => ['settings.save_branch_details', $sSlug   ],'id'=>'create-branch-form' , 'class' => 'form-validate-jquery' ]) !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>
                                <legend class="text-semibold"><i class="icon-reading position-left"></i> {{ __('settings') }}
                                </legend>


                                <div class="form-group">
                                    <label>{{ __('premium/dashboard/index.license_number') }}</label>

                                    {{ Form::text('license_number', ( isset($oBranch) ? $oBranch['license_no'] : '' ), ['id'=>'license_number','class' => 'form-control','placeholder'=> 'Enter License Number' ]) }}
                                    @if ($errors->has('license_number'))<font color="red">{{ $errors->first('license_number') }}</font>@endif

                                </div>



                                <div class="form-group">
                                    <label>Notes:</label>
                                    <textarea rows="5" cols="5" class="form-control"
                                              placeholder="Enter your message here"></textarea>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="text-semibold"><i class="icon-city position-left"></i> {{ __('premium/dashboard/index.main_branch_details') }}
                                </legend>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('premium/dashboard/index.main_branch_name') }}</label>

                                            {{ Form::text('branch_name', ( isset($oBranch) ? $oBranch['main_branch']['name'] : '' ),['id'=>'branch_name','class' => 'form-control','placeholder'=> 'Enter Main Branch Name' ]) }}
                                            @if ($errors->has('branch_name'))<font color="red">{{ $errors->first('branch_name') }}</font>@endif

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('settings') }}</label>
                                            {{ Form::email('branch_email', ( isset($oBranch) ? $oBranch['main_branch']['email'] : '' ),['id'=>'branch_email','class' => 'form-control','placeholder'=> 'Enter Company Email' ]) }}
                                            @if ($errors->has('branch_email'))<font color="red">{{ $errors->first('branch_email') }}</font>@endif

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('premium/dashboard/index.branch_type') }}</label>
                                            {{ Form::select('branch_type', ['' =>'Please Select'] + $oBranchTypes , ( isset($oBranch) ? $oBranch['main_branch']['branch_type_id'] : '' ),['class'=>'form-control select-search']) }}
                                            @if ($errors->has('branch_type'))<font color="red">{{ $errors->first('branch_type') }}</font>@endif

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('premium/dashboard/index.country') }}</label>
                                            {{ Form::select('country_id', ['' =>'Please Select'] + $oCountries ,( isset($oBranch) ? $oBranch['main_branch']['country_id'] : '' ),['class'=>'form-control select-search']) }}
                                            @if ($errors->has('country_id'))<font color="red">{{ $errors->first('country_id') }}</font>@endif

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('premium/dashboard/index.phone') }}</label>
                                            {{ Form::text('phone', ( isset($oBranch) ? $oBranch['main_branch']['phone'] : '' ),['class' => 'form-control','placeholder'=> 'Enter phone number' ]) }}
                                            @if ($errors->has('phone'))<font color="red">{{ $errors->first('phone') }}</font>@endif

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('premium/dashboard/index.fax') }}</label>
                                            {{ Form::text('fax', ( isset($oBranch) ? $oBranch['main_branch']['fax'] : '' ),['class' => 'form-control','placeholder'=> 'Enter fax number' ]) }}
                                            @if ($errors->has('fax'))<font color="red">{{ $errors->first('fax') }}</font>@endif

                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ZIP code:</label>
                                            {{ Form::text('zip_code', ( isset($oBranch) && isset($oBranch['main_branch']['zip_code']) ? $oBranch['main_branch']['zip_code'] : '' ),['class' => 'form-control','placeholder'=> 'Enter ZIP Code' ]) }}
                                            @if ($errors->has('zip_code'))<font color="red">{{ $errors->first('zip_code') }}</font>@endif

                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City:</label>
                                            {{ Form::text('city', ( isset($oBranch) && isset($oBranch['main_branch']['city']) ? $oBranch['main_branch']['city'] : '' ),['class' => 'form-control','placeholder'=> 'Enter City' ]) }}
                                            @if ($errors->has('city'))<font color="red">{{ $errors->first('city') }}</font>@endif

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address line:</label>
                                            {{ Form::text('address', ( isset($oBranch) ? $oBranch['main_branch']['address'] : '' ),['class' => 'form-control','placeholder'=> 'Enter address' ]) }}
                                        @if ($errors->has('address'))<font color="red">{{ $errors->first('address') }}</font>@endif

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Additional message:</label>
                                    <textarea rows="5" cols="5" class="form-control" name="message"
                                              placeholder="Enter your message here">{{( isset($oBranch) && isset($oBranch['main_branch']['message']) ? $oBranch['main_branch']['message'] : '' )}}</textarea>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @if(isset($oBranch))
                        <input type="hidden" name="id" value="{{ $oBranch['id'] }}" />
                        @endif
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple">Save <i
                                    class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>