<?php

return [

    'sent' => 'المرسل',
    'inbox' => 'الوراد',
    'draft' => 'المحفوظات',
    'pending_actions' => 'بانتظار اجراء',
    'my_inbox' => 'وارداتي',
    'rejected' => 'المرفوض',

];