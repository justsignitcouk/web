<ul class="nav nav-tabs nav-justified">
 
        @can('allowed-permission', ['departments-details','departments-details'])
			<li class="@if (!empty($active_show) &&  $active_show == 1)) {{'active'}} @endif">
				<a href='{{url("branchdeps/$oDepartment->id")}}'>
				{{ __("common.show") }}
				</a>
			</li>
		@endcan
	
	    @can('allowed-permission', ['departments-edit','departments-edit'])
			<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
				<a href='{{url("branchdeps/$oDepartment->id/edit")}}' >
					{{ __("common.edit") }}
				</a>
			</li>
		@endcan

        @can('allowed-permission', ['departments-roles','departments-roles'])
		<li class="@if (!empty($active_roles) &&  $active_roles == 1)) {{'active'}} @endif">
			<a href='{{url("branchdeps/$oDepartment->id/roles")}}' >
				{{ __("common.roles") }}
			</a>
		</li>
		@endcan

        @can('allowed-permission', ['departments-permissions','departments-permissions'])
		<li class="@if (!empty($active_permissions) &&  $active_permissions == 1)) {{'active'}} @endif">
			<a href='{{url("branchdeps/$oDepartment->id/permissions")}}' >
				{{ __("common.permissions") }}
			</a>
		</li>
		@endcan

</ul>