@foreach($departments_permissions as $key => $department)
    <div class="form-check">
      <label class="form-check-label">
        <input class="department-check-input" type="checkbox" name ="{{'departments-'.$key}}" id="{{$department->key}}" value="{{$department->id}}"

        @if(!empty($checked_user_permissions))
            @foreach($checked_user_permissions as $check)

                @if($check['permission_id'] == $department->id)
                checked
                @endif
         
            @endforeach
        @endif
       
        >
        {{$department->name}}
      </label>
    </div>
@endforeach