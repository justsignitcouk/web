$(function() {
// Setup validation
// ------------------------------
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.page-content').addClass("animated fadeInUp ").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
        $(this).removeClass("animated fadeInUp ");
    });


// Initialize
    var validator;
    i18n.init({
            resGetPath: '/themes/material/assets/locales/__lng__.json',
            debug: true,
            fallbackLng: false,
            load: 'unspecific'
        },
        function () {


        validator = $(".form-validate-jquery").validate({
                ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                errorClass: 'validation-error-label',
                successClass: 'validation-valid-label',
                highlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },
                unhighlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },

                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: "validation-valid-label",
                success: function (label) {
                    label.addClass("validation-valid-label").text("Success.")
                },
                rules: {
                    password: {
                        minlength: 8
                    },
                    repeat_password: {
                        equalTo: "#password"
                    },
                    email: {

                        email_or_mobile: true,
                        "remote":
                            {
                                url: '/validateEmailOrPhone',
                                type: "post"
                            }
                    },
                    repeat_email: {
                        equalTo: "#email"
                    },
                    mobile:{
                        minlength: 8,
                        "remote":
                            {
                                url: '/validateEmailOrPhone',
                                type: "post"
                            }
                    }
                },
                messages: {
                    required: i18n.t("signup.this_field_is_required"),
                    first_name: i18n.t("signup.enter_first_name"),
                    last_name: i18n.t("signup.enter_last_name"),
                    password:i18n.t("signup.enter_password"),
                    repassword:i18n.t("signup.enter_repeat_password"),
                    repeat_email:i18n.t("signup.enter_repeat_email"),
                    custom: {
                        required: i18n.t("signup.this_field_is_required")
                    },
                    email: {
                        required: i18n.t("signup.enter_email"),
                        remote: i18n.t("signup.email_taken")
                    },
                    mobile: {
                        remote: i18n.t("signup.mobile_taken")
                    },
                    terms: i18n.t("signup.accept_our_policy")
                },
                submitHandler: function(form) {
                    return false;
                }
            });

            jQuery.validator.addMethod("email_or_mobile", function(value, element, params) {
                var email = $('#email').val();
                var mobile = $('#mobile').val();
                if(email !='' || mobile!=''){
                    return true;
                }
                if(email=='' && mobile==''){
                    return false;
                }

            }, jQuery.validator.format("Please enter A correct Email address or Valid Mobile Number"));

            jQuery.validator.addMethod("mobile_remote", function(value, element, params) {
                if(value!='') {
                    $.post('/validateEmailOrPhone', {'identifier': value}, function (data) {
                        if(data.oUser == false){
                            return true;
                        }
                        return false;
                    },'json');
                }
                return true;
            }, jQuery.validator.format("Mobile exist"));


            $('.form-validate-jquery').submit(function(e) {
                var errors = validator.numberOfInvalids();
                if(!errors){
                    var lock = $(this).closest('.panel-body');
                    var data = $(this).serialize();
                    $(lock).block({
                        message: '<i class="icon-sync spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    $.post("/register",data,function(data){
                        console.log(data);
                        if(data.status == 'true' ) {
                            lock.closest('.page-content').addClass("animated fadeOutDown ").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                                //$(this).removeClass("animated rotateOut");
                                window.location = '/inbox' ;
                            });
                        }else{
                            lock.unblock();
                        }


                    },'json') ;

                }
                e.preventDefault();
            });


// Reset form
            $('#reset').on('click', function () {
                validator.resetForm();
            });


        });


    // messages: {
    //     first_name: i18n.t("signup.enter_first_name"),
    //         last_name: i18n.t("signup.enter_last_name"),
    //         custom: {
    //         required: i18n.t("signup.this_field_is_required"),
    //     },
    //     email: {
    //         required: i18n.t("signup.enter_email"),
    //             remote: i18n.t("signup.email_taken")
    //     },
    //     agree: i18n.t("signup.accept_our_policy")
    // },





});