<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
    
    /**
	 * table variables
	 */
    protected $table = 'permissions' ;
    protected $fillable = ['status'];

    /**
	 * model relations
	 */
    public function Module(){
    
         return $this->belongsTo('App\Models\Module');
    }
    

}
