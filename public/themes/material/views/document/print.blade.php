<link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">
{{--<link href="{{ asset('/assets/components/printjs/dist/print.min.css') }}" rel="stylesheet" type="text/css">--}}

@partial('A4.Layout',['aDocument' => $oDocument['document'],'id'=>$id ,'oDocumentNotes'=>
isset($oDocumentNotes) ? $oDocumentNotes : [] , 'bContentEditable' => false ] )

{{--@partial('document.compose.a4',['id'=> isset($id) ? $id : false , 'editor' => false,'sticky_notes' => false])--}}
<script>
    setTimeout(function(){
        window.print();
    },100) ;

</script>
{{--<script>--}}
    {{--var signDoc = '{{route('document.signDoc')}}';--}}
    {{--var getSign = '{{route('profile.getDefaultSignature')}}';--}}
    {{--var statusDoc = '{{route('document.statusDoc')}}';--}}
    {{--var document_id = '{{ $id }}';--}}
    {{--var view = '{{__('lang.view')}}';--}}
    {{--var displayWorkflow  = '{{ $displayWorkflow }}';--}}
    {{--// setTimeout(function(){--}}
    {{--//     window.print();--}}
    {{--// },4000);--}}
{{--</script>--}}


