<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roles Language Lines
    |--------------------------------------------------------------------------
    */

    'roles' => 'Roles',
    'add_role'  => 'Add Role',
    'permissions'  => 'Permissions',
    'delete_role'  => 'Are you sure you want to delete this role',
    'name'  => 'Name',
    'dispaly_name'=>'Dispaly Name',
    'description'=>'Description',
    'permissions'=>'Permissions',
    'cant_delete_role'=>'Sorry you can not delete used role',
    'role_type'=>'Role Type',
    'save_caution'=>'Are you sure you want to save role with no permissions',
];