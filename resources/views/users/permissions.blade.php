@extends('layouts.admin')

@section('content')

<div class="col-md-12">

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="tabbable">

            <ul class="breadcrumb">
                <li><a href="{{route('admin.users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
                <li><a href="{{route('admin.users.profile' , $user['id'])}}"><i class="position-left"></i>{{ $user['username'] }}</a></li>
                <li class="active">{{ __("common.permissions") }}</li>
            </ul>
                
            @include('layouts/partials/tabs')

            <div class="tab-content">
              <div class="panel panel-flat">
                <div class="panel-body">

                  {!! Form::open(['route' => ['admin.users.updatepermissions', $user->id],'method'=>'POST']) !!}

                  @include('layouts/partials/permissions')
                  

                  {{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

                          
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')

<script type="text/javascript">
$(document).ready(function(){

   


});
</script>
@endpush
@endsection
