<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use Yajra\Datatables\Datatables;

class PermissionsController extends Controller
{

    /**
     * @var permission
     */
    protected $permission;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Permission $permission)
    {
        $this->middleware('auth');
        $this->permission   = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('permissions.index');
    }

    public function datatables(){
        $aPermissions = Permission::all()->toArray() ;  
        return Datatables::of($aPermissions)->make(true);
    }

    
}
