$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });


    var columns = [
        {
            data: 'name',
            name: 'name',
            className: "",
            "defaultContent": "<i>Not set</i>"
        },
        {
            data: 'id', name: 'action', className: "", render: function (data) {
                return '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-menu9"></i> </a><ul class="dropdown-menu dropdown-menu-right">' +
                    '<li><a  class="edit-button" data-id="' + data + '"><i></i> Edit</a></li> ' +
                    '<li><a  class="delete-layout" data-id="' + data + '"><i class="icon-file-excel delete-button"></i> Delete</a></li> ' +
                    '</ul> ' +
                    '</li> ' +
                    '</ul> '
                // '<button class="btn btn-primary edit-button" data-id="' + data + '">Edit</button>'
            }
        }


    ];


    $.extend($.fn.dataTable.defaults, {

        columnDefs: [{
            orderable: false,
            /*width: '100px',*/
            targets: [0],
            checkboxes: {
                seletRow: true
            }
        }],
        dom: '<"datatable-header"fl ><""t><"datatable-footer"ip>',
        language: {
            search: '<span>' + /*lang.search*/ 'search' + '</span> _INPUT_',
            searchPlaceholder: '',
            lengthMenu: '<span data-i18n="show">Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'},
            loadingRecords: "<i class=\"icon-spinner3 spinner\"></i>",

        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },


    });

    var t = $('#layouts_table').dataTable({
        "ordering": false,
        "serverSide": true,
        "bProcessing": true,
        'autoWidth': true,
        "ajax": {
            "url": layouts_user,
            "type": 'POST',
            "data": ''
        },
        columns: columns,
        "createdRow": function (row, data, index) {

            $(row).data("id", data.id);
        }

    });



    $("#addLayout").on("shown.bs.modal", function () {
        $('#layout_name').val('');
        $('#header_content').val('');
        $('#footer_content').val('');
        tinymce.init({
            selector: 'textarea',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'paste',
                'searchreplace visualblocks code fullscreen paste',
                'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
            ],
            toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
            toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
            toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
            ]
        });
    });


    $(document).on('click','#add_layout',function(e){
        var header = tinyMCE.get('header_content').getContent();
        var layout_name = $('#layout_name').val();
        var footer = tinyMCE.get('footer_content').getContent();
        if(layout_name==''){
            alert('enter name');
            return false;
        }
        $.post(layouts_user_save,{'layout_name':layout_name,'header':header,'footer':footer},function(data){
            $('#layouts_table').dataTable().fnDraw();
            $('.modal').modal('hide');
        });
        e.preventDefault();
    });

    $(document).on('click','.delete-layout',function(){
        var id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this layout!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(isConfirm){
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
            if (isConfirm){
                $.post(layouts_user_delete, {'id':id},function(data){
                    console.log(data);
                     if(data.hard_delete==false){
                         swal("Success", "delete was successful", "success");
                     }
                    swal("Success", "delete was successful", "success");
                    $('#layouts_table').dataTable().fnDraw();
                },'json');

            } else {

            }
        });

    })


});