<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Hash;
use App\Models\RoleUser;
use App\Models\PermissionUser;
use App\Models\Account;
use Auth;
use Request;
use App\Services\File;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'activated'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Model Relations
     */

    public function ImageProfile(){
    
        return $this->hasOne('App\Models\File', 'id', 'profile_image');
    }
    public function setUserDataInSession(){
    
        $user = User::find(Auth::id())->Account;
        if (isset($user[0]) && count($user[0])) {
            $aDefaultAccount = $user[0]->toArray();
            session()->put("aDefaultAccount", serialize($aDefaultAccount));
        }
    }

    /**
     * Store new user
     */
    public function storeUser($userData){
    
        $user = new User;

        //store user data
        $user->username = $userData->username;
        $user->email = $userData->email;
        $user->first_name = $userData->first_name;
        $user->last_name = $userData->last_name;
        $user->password = Hash::make($userData->password);
        $user->created_by = nUserID();
        $user->created_by_ip = Request::ip();
        $user->activated = 1;

        //store user image to CDN
        if(!empty($userData->profile_image)){
            $nImage = File::put($userData->profile_image);
            $user->profile_image = $nImage;
        }

        $user->save();

        //create new account 
        $account = new Account;
        $accountData =  (object) [
        'account_name'=> $user->username,
        'department_id'=> 1,
        'user_id'=> $user->id,
        'active'=> 1,
        'verify'=> 1,
        'is_default'=> 1,
         ];
        
        $account->storeAccount($accountData);

        return true;
    }

    /**
     * Update created user
     */
    public function updateUser($userData, $id){

        $user = User::find($id);

        //update user data
        $user->username = $userData->username;
        $user->first_name = $userData->first_name;
        $user->last_name = $userData->last_name;
        $user->email = $userData->email;
        $user->updated_by = nUserID();
        $user->updated_by_ip = Request::ip();

        
        if (!empty($userData->password)) {
            $user->password = Hash::make($userData->password);
        }


        if(!empty($userData->profile_image)){
            $nImage = File::put($userData->profile_image);
            $user->profile_image = $nImage;
        }

        $user->save();

        return true;
    }

    /**
     * Update user roles 
     */
    public function updateRoles($roleData ,$id){

        //delete user old roles
        $roleUsers =  RoleUser::where('user_id',$id)->get();
        foreach ($roleUsers as $roleUser) {

            $roleUser->delete();
            
        }

        //store user new roles
        foreach ($roleData->all() as $key => $role) {

            if($key == '_token'){
                continue ;
            }

            $roleUser = new RoleUser;
            $roleUser->user_id = $id ;
            $roleUser->role_id = $role ;

            $roleUser->save();

        }

        return true ;
       
    }

    /**
     * Update user permissions 
     */
    public function updatePermissions($permissionsData , $id){
        
        //delete user old permissios
        $permissionUser =  PermissionUser::where('user_id',$id)->get();
        foreach ($permissionUser as $permission) {

            $permission->delete();
            
        }

        //store user new permissios
        foreach ($permissionsData->all() as $key => $permission) {

            if($key == '_token'){
                continue ;
            }

            $permissionUser = new PermissionUser;
            $permissionUser->user_id = $id ;
            $permissionUser->permission_id = $permission ;

            $permissionUser->save();

        }

        return true ;        


    }

    

}
