{!! Theme::asset()->container('auth')->themePath()->add([ ['intlTelInput.js', 'js/intl-tel-input/build/js/intlTelInput.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['intlTelInput.css','js/intl-tel-input/build/css/intlTelInput.css',['bootstrap']]]) !!}


{!! Theme::asset()->container('auth')->themePath()->add([ ['animatecss.css','css/extras/animate.min.css',['bootstrap']]]) !!}


{!! Theme::asset()->container('auth')->add([ ['validate', 'assets/components/validation/dist/jquery.validate.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->add([ ['validate_additonal', 'assets/components/validation/dist/additional-methods.js',['validate']]]) !!}


{!! Theme::asset()->container('auth')->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['pace', 'js/plugins/loaders/pace.min.js',['uniform']]]) !!}

{!! Theme::asset()->container('auth')->themePath()->add([ ['auth', 'pages/auth/signup.js',['pace']]]) !!}
{!! Theme::asset()->container('auth')->themePath()->add([ ['validation_form', 'pages/auth/validation_form.js',['validate_additonal']]]) !!}

<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel registration-form">
            <div class="panel-body">
                {{ $oPlan['title'] }} - {{ $oPlan['price'] }}
                <div class="text-center">
                    <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                    <h5 class="content-group-lg"><span data-i18n="signup.main">{{__('auth.Create account')}}</span>
                        <small class="display-block" data-i18n="signup.required">{{__('auth.All fields are required')}}</small>
                    </h5>
                </div>
                <form class="form-validate-jquery" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
{{--                    <div class="form-group has-feedback">
                        {!! Form::text('display_name', null, ['class' => 'form-control display_name' , 'autocomplete'=>'off' , 'id'=>'display_name' , 'placeholder'=> trans("auth.Display name"),'required' => "required" ,"data-i18n" =>"[placeholder]signup.display_name" ]) !!}
                        <span class="help-block">{{ $errors->first('display_name') }}</span>
                        <div class="form-control-feedback">
                            <i class="icon-user-plus text-muted"></i>
                        </div>
                    </div>--}}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::text('first_name', null, ['class' => 'form-control first_name' , 'autocomplete'=>'off' , 'id'=>'first_name' , 'placeholder'=> trans("auth.First name"),'required' => "required" , "data-i18n" => "[placeholder]signup.first_name"]) !!}
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                                <div class="form-control-feedback">
                                    <i class="icon-user-check text-muted"></i>
                                </div>
                            </div>
                        </div>

{{--                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                {!! Form::text('middle_name', null, ['class' => 'form-control middle_name' , 'autocomplete'=>'off' , 'id'=>'middle_name' , 'placeholder'=> trans("auth.Middle name"),'required' => "required" ]) !!}
                                <span class="help-block">{{ $errors->first('middle_name') }}</span>
                                <div class="form-control-feedback">
                                    <i class="icon-user-check text-muted"></i>
                                </div>
                            </div>
                        </div>--}}
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::text('last_name', null, ['class' => 'form-control last_name' , 'autocomplete'=>'off' , 'id'=>'last_name' , 'placeholder'=> trans("auth.Last name"),'required' => "required", "data-i18n" => "[placeholder]signup.last_name" ]) !!}
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                                <div class="form-control-feedback">
                                    <i class="icon-user-check text-muted"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::password('password' , ['class' => 'form-control password' , 'id'=>'password','autocomplete'=>'off'  , 'placeholder'=> trans("auth.Create password"),'required' => "required", "data-i18n" => "[placeholder]signup.password"  ]) !!}
                                <span class="help-block">{{ $errors->first('password') }}</span>
                                <div class="form-control-feedback">
                                    <i class="icon-user-lock text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::password('repassword' , ['class' => 'form-control repassword' , 'id'=>'repassword','autocomplete'=>'off'  , 'placeholder'=> trans("auth.Repeat password"),'required' => "required", "data-i18n" => "[placeholder]signup.repeat_password"  ]) !!}
                                <span class="help-block">{{ $errors->first('repassword') }}</span>
                                <div class="form-control-feedback">
                                    <i class="icon-user-lock text-muted"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::email('email', null, ['class' => 'form-control email' , "id" => "email" ,'autocomplete'=>'off'  , 'placeholder'=> trans("auth.Your email"), "data-i18n" => "[placeholder]signup.your_email" ]) !!}
                                <span class="help-block"></span>
                                <div class="form-control-feedback">
                                    <i class="icon-mention text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {!! Form::email('repeat_email', null, ['class' => 'form-control repeat_email' , "id" => "repeat_email" ,'autocomplete'=>'off'  , 'placeholder'=> trans("auth.Repeat email"), "data-i18n" => "[placeholder]signup.repeat_your_email"  ]) !!}
                                <span class="help-block"></span>
                                <div class="form-control-feedback">
                                    <i class="icon-mention text-muted"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                {!! Form::tel('mobile', null, ['class' => 'form-control mobile' , "id" => "mobile" ,'autocomplete'=>'off'  , 'placeholder'=> trans("auth.Your mobile"), "data-i18n" => "[placeholder]signup.your_mobile" ]) !!}
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">

{{--                        <div class="checkbox hide">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Subscribe to monthly newsletter
                            </label>
                        </div>--}}
                        <div class="col-md-4">
                        <div class="checkbox form-group has-feedback">

                            <label>

                                <input type="checkbox" name="terms" class="form-control styled" required="required">
                                <span data-i18n="signup.accept">Accept</span> <a href="#" data-i18n="signup.terms_of_service">terms of service</a>

                            </label>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    </div>
                    <div class="text-right">
                        <input type="hidden" name="plan_id" value="{{ $oPlan['id'] }}" />
                        <a href="/login" class="btn btn-link legitRipple"><i
                                    class="icon-arrow-left13 position-left"></i> <span data-i18n="signup.back_to_login">Back to login form</span>
                        </a>
                        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10 legitRipple">
                            <b><i class="icon-plus3"></i></b> <span data-i18n="signup.create_account">Create account</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
