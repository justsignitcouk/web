<?php


namespace App\Services;

use App\Models\PermissionUser;
use DB ;


class UsersService{


    function __construct(){


    }

    public function checkedUserPermissions($user_id){

        return      PermissionUser::
                    select('permission_id')
                    ->where('user_id',$user_id)
                    ->get()
                    ->toArray(); 

    }

	
    public function Permissions($module_name){

        return  DB::table('modules')
                ->join('permissions','permissions.module_id','=','modules.id')
                ->select('modules.name','permissions.*')
                ->where('modules.name','=',$module_name)
                ->get();

    }


}