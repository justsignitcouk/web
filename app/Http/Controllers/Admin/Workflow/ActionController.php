<?php

namespace App\Http\Controllers\Admin\Workflow;

use App\Models\Workflow\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workflow\Actions;
use Yajra\Datatables\Datatables;
use Validator;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workflow/action/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'actionTypes' => Actions::getActionTypes()
        ];
        return view('workflow.action.create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'name' => 'required|max:255',
            'type' => 'required',
        ])->validate();
        $oAction = new Actions();
        $oAction->name = $request->get('name');
        $oAction->description = $request->get('description');
        $oAction->type = $request->get('type');
        $oAction->created_by = nUserID();
        $oAction->company_id = 1; // Todo: company will be in dropdown list
        if($oAction->save()){
            return redirect()->route('action');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        if ($id != 0) {
            $oWorkflow = Workflow::with('Stages.States')->where(['id' => $id])->limit(1)->get();
            if (count($oWorkflow) > 0){
                $oWorkflow = $oWorkflow[0];
            }
            $aStages = $oWorkflow->Stages;
            $aStagesInfo = [];
            foreach ($aStages as $oStage){
                $aStagesInfo[$oStage->id] = $oStage->name;
            }
            $params = [
                'oWorkflow' => $oWorkflow,
                'oStages' => $aStages,
                'aStagesInfo' => $aStagesInfo,
                'action_types' => wfStage::getActionTypes()
            ];
            return view('workflow.view')->with($params);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function datatables(){
        $aActions = Actions::all();
        return Datatables::of($aActions->toArray())->make(true);
    }
}
