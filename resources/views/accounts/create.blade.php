@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        {!! Form::open(['route'=>'accounts.store','files'=>'true','enctype'=>'multipart/form-data','method'=>'POST']) !!}

            @include('accounts.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
