@extends('layouts.admin')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __('workflow.workflow.add_workflow') }}</h6>
        </div>
        <div class="panel-body">

                {!! Form::open(['url'=> url('workflow'), 'method'=>'POST', 'id' => 'workflow', 'enctype'=>'multipart/form-data']) !!}
                {{ csrf_field() }}



                    <fieldset class="content-group">
                        <legend class=""></legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.workflow.workflow') .' '. __('common.name') }}: <span class="text-danger">*</span></label><br>
                                        <input name="workflow_name" id="workflow_name" class="required form-control">
                                        @if ($errors->has('workflow_name'))
                                            <br/><font color="red">{{ $errors->first('workflow_name') }}</font>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.workflow.workflow') .' '. __('common.description') }}:</label>
                                        <textarea name="workflow_description" id="workflow_description" class="form-control"></textarea>
                                        @if ($errors->has('workflow_description'))
                                            <br/><font color="red">{{ $errors->first('workflow_description') }}</font>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{ __('workflow.workflow.workflow') .' '. __('common.company') }}:</label>
                                    {!! Form::select('workflow_company', ['' => __('common.please_choose') ] + $oCompanies, '', ['class' => 'form-control m-bot15']) !!}
                                    @if ($errors->has('workflow_company'))
                                        <br/><font color="red">{{ $errors->first('workflow_company') }}</font>
                                    @endif
                                </div>
                            </div>
                        </div>

                            <button type="submit" class="btn btn-primary stepy-finish pull-right">Submit <i class="icon-check position-right"></i></button>
                    </fieldset>
            </div>
    </div>

        {!! Form::close() !!}


@endsection

@push('scripts')

@endpush