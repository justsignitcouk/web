{!! Theme::asset()->container('auth')->themePath()->add([ ['forgot', 'pages/auth/forgot.js',['bootstrap']]]) !!}


<div class="page-container" style="min-height:279px">


    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Unlock user -->
                {{ Form::open(array('url' => route('forgotPassword') ,'class'=>'login-form')) }}

                <div class="panel">
                        <div class="panel-body">

                            <a class="col-md-3 col-sm-4"><i class="back-arrow-forgot glyphicon glyphicon-arrow-left"></i></a><br/>
                            <br/>

                            <div class="login_logo_container" >
                                <img src="/assets/images/mycompose_logo.png" id="login_logo" style="height: 50%;width:50%;margin-left: 25%"/>
                            </div><br/>

                            <h6 class="content-group text-center text-semibold no-margin-top"><span data-i18n="forgot.main">{{ trans("auth.my_compose") }}</span>
                                <small class="display-block" data-i18n="forgot.reset">{{ trans("auth.reset_password") }}</small></h6>


                            <?php $check=false ?>
                            @if( session()->has('status') )

                                <?php $check = session()->get('status') ?>


                                    @if($check == 'success')
                                        <div class="alert alert-success">
                                            <span data-i18n="forgot.reset_password_message">{{ trans("auth.reset_password_message") }}</span>
                                        </div>
                                    @elseif($check == 'faild')
                                        <div class="alert alert-warning">
                                            <span data-i18n="forgot.reset_password_message_worong">{{ trans("auth.reset_password_message_worong") }}</span>
                                        </div>
                                    @endif


                            @endif
                        @if($check!='success')
                            <div class="form-group has-feedback">
                                <div class="form-group has-feedback has-feedback-left">

                                    <input style="display:none" type="text" name="email"/>
                                    <input type="text"  name="email" class="form-control" id="email" placeholder="{{ trans("auth.your_email") }}"  data-i18n="[placeholder]input.your_email"  >

                                    <div class="form-control-feedback">
                                        <i class="icon-mention text-muted"></i>
                                    </div><br/>
                                    @if ($errors->has('email'))
                                        <span style="color : red">{{ $errors->first('email') }}</span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group col-sm-12 sign-up">
                               {{-- <input type="submit" class="btn btn-primary btn-block" />--}}
                                <button type="submit" class="btn btn-primary btn-block" data-i18n="input.submit"></button>

                            </div>
                            @endif

                        </div>
                    </div>
                {{ Form::close() }}
                <!-- /unlock user -->


                <!-- Footer -->
                <div class="footer text-muted text-center">
                    © 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>