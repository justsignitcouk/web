$(document).ready(function(){

    $.fn.disabled_done_steps = function() {
        $('.steps').find('li.done').each(function (e) {
            $(this).addClass('disabled');

        });
    }


   var wf_object = $(".workflow_steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        saveState: false,
        enableAllSteps:false,
        enableKeyNavigation:false,
       startIndex: current_stage,
        enableFinishButton:false,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        },
        onInit: function(event, currentIndex){
            $(".workflow_steps").show();
            if(currentIndex!=0){
                //$('#compose-content').hide();
                //$('.compose-components').hide();
            }
            $(".workflow_steps").find('.current').show();
            $.fn.disabled_done_steps();
        },
        onStepChanging:function (event, currentIndex, newIndex) {
            $.fn.document_block('body');


            return true; },
        onStepChanged:function (event, currentIndex, priorIndex) {

            if(currentIndex!=0){
                //$('#compose-content').hide();
                //$('.compose-components').hide();

            }else{
                $('#compose-content').show();
                $('.compose-components').show();
            }

            $('.pages_frame').slimScroll({
                height: '380px',
                width: '1020px',
            });

            $.fn.document_unblock('body');

            $.fn.disabled_done_steps();
        }
    });

    $.fn.goToNextStage = function(){
        wf_object.steps("next",{});

    }



});