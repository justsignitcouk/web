{!! Theme::asset()->themePath()->add([ ['typeahead', 'js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['handlebars', 'js/plugins/forms/inputs/typeahead/handlebars.min.js',['typeahead']]]) !!}
{!! Theme::asset()->themePath()->add([ ['elements', 'partials/document/elements.js',['typeahead']]]) !!}

        <div class="col-md-12">



            <div class="col-md-4" style="display:none">
                <label for="email" data-i18n="doc_num"></label>
                <input type="text" class="form-control" name="document_number_input" id="document_number_input" value="{{ $aDocument['outbox_number'] }}"
                         data-i18n="[placeholder]doc_num" placeholder="{{ __('document.Document Number') }}" />
            </div>

            <input type="hidden" class="form-control" name="document_layout_input" id="document_layout_input"  value="{{ $aDocument['layout_id'] }}"
                   data-i18n="[placeholder]doc_num" placeholder="{{ __('document.Document Layout') }}" />
            <input type="hidden" name="document_id" id="document_id" value="{{$id}}" />

        </div>


<!-- /block buttons -->
