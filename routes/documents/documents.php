<?php

Route::group(['prefix' => 'templates','as'=>'templates.'], function() {
    Route::get('/templates', 'Individual\Documents\TemplatesController@templates', [])->name('index');
    Route::post('/templates', 'Individual\Documents\TemplatesController@getTemplates', [])->name('getTemplates');
    Route::post('/delete', 'Individual\Documents\TemplatesController@deleteTemplate', [])->name('deleteTemplate');


});


/********************* document routes*************************/
Route::group(['prefix' => 'document','as'=>'document.'], function (){

    Route::get('/testdocument', 'Individual\DocumentController@testDocument', [])->name('testDocument');


    Route::get('/document',  'Individual\DocumentController@document', [])->name('document');

    Route::post('/viewAjax/{id}', 'Individual\DocumentController@viewAjax', [])->name('viewAjax');
    Route::post('/load_draft', 'Individual\DocumentController@loadDraft', [])->name('loadDraft');

    Route::get('/compose/v2', 'Individual\DocumentController@compose_builder', [])->name('composev2');

    Route::get('/compose', 'Individual\DocumentController@compose', [])->name('compose');

    Route::get('/choose-template', 'Individual\Documents\TemplatesController@chooseTemplate', [])->name('choose-template');

    Route::post('/compose', 'Individual\DocumentController@sendDocument', [])->name('sendDoc');
    Route::post('/forward', 'Individual\DocumentController@forwardDocument', [])->name('forwardDoc');

    Route::post('/data/layouts', 'Individual\DocumentController@getLayouts')->name('getLayouts');
    Route::get('/data/Layout/{id}', 'Individual\DocumentController@getLayout')->name('getLayout');

    Route::get('/reply/{id}', 'Individual\DocumentController@replyDocument', [])->name('replyDoc');
    Route::post('/sign', 'Individual\DocumentController@sign', [])->name('signDoc');
    Route::post('/status', 'Individual\DocumentController@setStatus', [])->name('statusDoc');

    Route::post('/compose/draft', 'Individual\DocumentController@sendDraft', [])->name('sendDraft');
    Route::get('/compose/{id}', 'Individual\DocumentController@composeWithDraft', [])->name('withDocumentId');

    Route::get('/import', 'Individual\DocumentController@importDocument', [])->name('import');
    Route::post('/import', 'Individual\DocumentController@sendImport', [])->name('sendImport');


    Route::get('/{id}', 'Individual\DocumentController@view')->name('documents');



    Route::get('/forward/{forward_id}', 'Individual\DocumentController@viewForwarded')->name('viewForwarded');

    Route::get('/{id}/edit', 'Individual\DocumentController@editDocument')->name('editDocument');
    Route::get('/pending/{id}', 'Individual\DocumentController@view')->name('pendingDocument');
    Route::get('/rejected/{id}', 'Individual\DocumentController@view')->name('rejectedDocument');

    Route::get('/print/{id}', 'Individual\DocumentController@printDocument')->name('printDocument');

    Route::post('/draft/delete', 'Individual\DocumentController@deleteDraft')->name('deleteDraft');


    Route::get('/QR/{outbox_number}', 'Individual\DocumentController@getDocumentByOutboxNumber')->name('getFromQR');


    Route::post('/saveWfStateAction', 'Individual\DocumentController@saveWfStateAction')->name('saveWfAction');


    Route::get('/pdf/{id}', 'Individual\DocumentController@pdfDocument')->name('pdfDocument');
    Route::get('/draft/pdf/{id}', 'Individual\DocumentController@pdfDraft')->name('pdfDraft');

    Route::get('/draft/{id}', 'Individual\DocumentController@getDraft', [])->name('getDraft');

    Route::post('/get_wf_for_current_account', 'Individual\DocumentController@getWorkFlowsForCurrentAccount', [])->name('getWorkFlowsForCurrentAccount');

    Route::post('/forward_note', 'Individual\DocumentController@getForwardNotes', [])->name('getForwardNotes');
    Route::post('/notes', 'Individual\DocumentController@getNotes', [])->name('getDocumentNote');
    Route::post('/notes/savePosition', 'Individual\DocumentController@savePositionOfNote', [])->name('savePositionOfNote');


    Route::post('/ajax/list_document', 'Individual\DocumentController@getDocumentListAjax', [])->name('getDocumentListAjax');

    Route::post('/details', 'Individual\DocumentController@getDocumentDetails', [])->name('getDocumentDetails');
    Route::get('/details/{id}', 'Individual\DocumentController@getDocumentDetailsAjax', [])->name('getDocumentDetailsAjax');
    Route::post('/attachments', 'Individual\DocumentController@getDocumentAttachments', [])->name('getDocumentAttachments');




});


