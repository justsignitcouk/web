@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("departments.departments") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>

		<div class="panel-body">
			<div class="tabbable">

                <ul class="breadcrumb">
                    <li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
                    <li><a href="{{route('admin.companies.show' , $company[0]->id)}}"><i class="position-left"></i>{{ $company[0]->name }}</a></li>
                    <li><a href="{{route('admin.branches',$company[0]->id)}}"><i class="position-left"></i>{{ __("common.branches") }}</a></li>
                    <li><a href="{{route('admin.combranches.show' , $branch['id'])}}"><i class="position-left"></i>{{ $branch['name'] }}</a></li>
                    <li class="active">{{ __("common.deprtments") }}</li>
                </ul>

                @include('layouts/partials/tabs/branch_tabes')
                <input type="hidden" id="branch_data"data-branch_id="{{$branch->id}}">
				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">

                            @can('allowed-permission', ['departments-create','departments-create'])
                                <a href="{{ url('branches/'.$branch->id.'/createdepartment') }}"
    			                   class="btn btn-primary"
    			                   id="bootbox_form">
    			                    {{ __("departments.add_department") }}
    			                </a>
                            @endcan

			                <table id="departments-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
				                <thead>
				                    <tr>
				                        <th>{{ __("common.name") }}</th>
				                        <th>{{ __("departments.parent_department") }}</th>
                                        <th>{{ __("common.created_at") }}</th>
				                    </tr>
				                </thead>
				            </table>
		
		             	</div>
				    </div>
			  </div>
		</div>
	</div>
</div>

</div>

@endsection

@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });

        
        var branch_id = $('#branch_data').data('branch_id');

        var table = $('#departments-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! url('departments' ) !!}' + '/'  +branch_id+ '/datatables'
            ,
            columns: [
                { data: 'name', name: 'name' },
                { data: 'parent_department_name', name: 'parent_department_name' },
                { data: 'created_at',   render: function(data){
                    return data ;
                } },
               
            ]
        });
        
        @can('allowed-permission', ['departments-details','departments-details'])
        $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/branchdeps/' + id ;
                
                });
        @endcan
    
 
    });
</script>
@endpush

