<ul class="nav nav-tabs nav-justified">
 
        @can('allowed-permission', ['branches-details','branches-details'])
		<li class="@if (!empty($active_show) &&  $active_show == 1)) {{'active'}} @endif">
			<a href='{{url("combranches/$branch->id")}}'>
			{{ __("common.show") }}
			</a>
		</li>
		@endcan
	
	    @can('allowed-permission', ['branches-edit','branches-edit'])
		<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
			<a href='{{url("combranches/$branch->id/edit")}}' >
				{{ __("common.edit") }}
			</a>
		</li>
		@endcan

        @can('allowed-permission', ['departments-index','departments-index'])
		<li class="@if (!empty($active_departments) &&  $active_departments == 1)) {{'active'}} @endif">
			<a href='{{url("branches/$branch->id/departments")}}' >
				{{ __("departments.departments") }}
			</a>
		</li>
		@endcan
	
</ul>