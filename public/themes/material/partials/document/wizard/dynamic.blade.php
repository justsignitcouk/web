{!! Theme::asset()->themePath()->add([ ['wizard_static', 'partials/document/wizard/dynamic.js',['typeahead']]]) !!}
@if(!isset($bShowOnlySteps))
@php $bShowOnlySteps= false; @endphp
    @endif

@if(!isset($steps_id))
@php $steps_id= 'workflow_steps'; @endphp
    @endif


@php
    $startIndex = 0 ;
    $nextStage = 0;
    $bIsLastStage = 0 ;
    $bNextStage = false;
    $i = 0 ;
@endphp

<form class="steps-state-saving {{$steps_id}}" action="#" style="display:none">

    @foreach($aWorkFlow['stage'] as $aStage)
        @if($loop->last )
            @if($aStage['id'] == $aDocument['work_flow']['current_stage_id'])
                @php $bIsLastStage = true; @endphp
            @endif
        @endif
        @if($bNextStage==true)
            @php $nextStage = $i; @endphp
        @endif
        @if($aStage['id'] == $aDocument['work_flow']['current_stage_id'])
            @php $startIndex = $i; @endphp
        @endif

        <h6 class="{{ $aStage['id'] == $aDocument['work_flow']['current_stage_id'] ? 'current' : '' }}">{{ $aStage['name'] }}</h6>
        @if($i==0 || (isset($remote) && $remote=='false') || $bShowOnlySteps )
            <fieldset class="{{ $aStage['id'] == $aDocument['work_flow']['current_stage_id'] ? 'current' : '' }}"> @else
                    <fieldset data-mode="async" data-url="/document/{{$aDocument['id']}}"
                              class="{{ $aStage['id'] == $aDocument['work_flow']['current_stage_id'] ? 'current' : '' }}">@endif

                    </fieldset>

            @php $i++; @endphp
            @endforeach

</form>

@if(!$bShowOnlySteps)
<!-- Modal -->
<div class="modal fade" id="details_table" tabindex="-1" role="dialog" aria-labelledby="details_tableLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Document Details</h4>
            </div>
            <div class="modal-body details_modal_body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>



<!--- main modal -->

@if($bIsLastStage)
    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"
         style="width: 990px;margin: 0 auto;margin-bottom: 15px;">
        <span  data-i18n="document.sent_to_all_recipients">This document has been sent to all recipients, check deliver status</span>
        <a href="https://mycompose.com:85/inbox" id="cancel" class="link-icon details-document details_button tooltip-tools" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Cancel"><i class="fa fa-remove"></i></a>
    </div>
<div class="col-md-12 text-center">
    <a
            class="no-smoothState link-icon details-document details_button tooltip-tools" data-toggle="modal" data-target="#details_table" data-id="{{$aDocument['id']}}" data-popup="tooltip" title="Details"
            data-placement="bottom" data-original-title="Bottom tooltip">
                <i class="icon-details icon-info3" data-id="{{$aDocument['id']}}"></i></a>
</div>
@endif

@endif
<script>
    var current_stage = {{ $startIndex }} ;
    var next_stage = {{ $nextStage }} ;
    var remote = '{{ isset($remote) ? $remote : 'true' }}'
</script>
