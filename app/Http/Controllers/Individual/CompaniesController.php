<?php

namespace App\Http\Controllers\Individual;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Models\Company;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class CompaniesController extends Controller
{
    /**
     * Display a listing of the CRUD entries.
     *
     * @param   void
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {

        return view('companies.index');
    }

    public function datatables(){
        $aCompanies = Company::all()->toArray() ;
        foreach($aCompanies as $k=>$v){
            $nCompanyId = $v['id'] ;
            $aCompanies[$k]['number_of_branches'] = Branch::where(["company_id" => $nCompanyId ])->count() ;
            $aCompanies[$k]['number_of_departments'] = Branch::where(["company_id" => $nCompanyId ])->Join('departments','departments.branch_id','branches.id')->count() ;
            $aCompanies[$k]['number_of_accounts'] = Branch::where(["company_id" => $nCompanyId ])->Join('departments','departments.branch_id','branches.id')->Join('account_department','account_department.department_id','departments.id')->count() ;
        }

        return Datatables::of($aCompanies)->make(true);
    }

    public function details($id){

        try {
            $oCompany = Company::findOrFail($id);
        } catch (ModelNotFoundException $ex) {
            return response()->redirectTo('/companies') ;
        }

        return view('companies.details', ['oCompany' => $oCompany]);
    }

    /**
     * Display the CRUD create form.
     *
     * @param   void
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $crud = crud_entry(new Company);

        return view('crud::scaffold.bootstrap3-form', ['crud' => $crud]);
    }

    /**
     * Store the CRUD entry.
     *
     * @param   \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $crud = new Company;
        $validator = $crud->crudEntry()->getValidator();

        $validator->validateRequest($request);

        if ($validator->passes())
        {
            $validator->save();
        }

        return $validator->redirect();
    }

    /**
     * Show the form for editing the current CRUD entry.
     *
     * @param   int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crud = crud_entry(Company::findOrFail($id));

        return view('crud::scaffold.bootstrap3-form', ['crud' => $crud]);
    }

    /**
     * Update the CRUD entry.
     *
     * @param   \Illuminate\Http\Request  $request
     * @param   int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Company::findOrFail($id);
        $validator = $model->crudEntry()->getValidator();

        $validator->validateRequest($request);

        if ($validator->passes())
        {
            $validator->save();
        }

        return $validator->redirect();
    }

    /**
     * Destroy the current CRUD entry.
     *
     * @param   int  $id
     * @param   string $csrf
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id, $csrf)
    {
        if (csrf_token() != $csrf)
        {
            abort(403);
        }

        $model = Company::findOrFail($id);

        $model->delete();

        return redirect()->back();
    }
}
