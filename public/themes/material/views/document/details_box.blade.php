
<div class="col-md-12">
    <div class="details-box col-md-6">
        <dl>
            <dt>Subject</dt>
            <dd>{{ $oDocument['document']['title'] }}</dd>

            <dt>Date Sent</dt>
            <dd>{{ $oDocument['document']['created_at'] }}</dd>
            <dd></dd>


        </dl>
    </div>
    <div class="details-box col-md-6">
        <dl>
            <dt >Document No.</dt>
            <dd>{{ $oDocument['document']['outbox_number'] }}</dd>

            <dt>Status</dt>
            <dd>{{ $oDocument['document']['status'] }}</dd>

            <dt>Recipients</dt>
            @foreach($oDocument['document']['recipients'] as $rec)
            <dd>{{ $rec['display_name'] }} - {{ $rec['type'] }}</dd>
            @endforeach


        </dl>
    </div>
</div>

@partial('document.wizard.dynamic',[ 'steps_id'=>'workflow_steps_details' , 'aWorkFlow'=>$aWorkFlow , 'aDocument'=> $oDocument['document'],'bShowOnlySteps'=>true ])