
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($department['id']))
            <h5 class="panel-title">{{ __("departments.add_department") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 
	        <div class="col-md-6">
               

               {!! csrf_field() !!}

                <meta name="csrf-token" content="{{ csrf_token() }}">

                @if(!empty($department['id']))
                <input type="hidden" name="branch_id" value="{{$department['branch_id']}}">
                @else
                <input type="hidden" name="branch_id" value="{{$branch_id}}">
                @endif 

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.name") }}
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :
		            </label>

                    <input type="text"
		                   name="name"
		                   class="form-control"
		                   placeholder='{{ __("common.name") }}'
		                   @if(!empty($department['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('name') }}"
			                    @else
			                    	value="{{ $department['name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('name') }}"
		                   @endif >

	                @if ($errors->has('name'))
		                <font color="red">{{ $errors->first('name') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            
		            <label>
			            {{ __("common.description") }} :
		            </label>

			        {!! Form::textarea('description',null, ['rows' => 9 ,'class'=>'form-control input-sm']) !!}

		            @if ($errors->has('description'))
		                <font color="red">{{ $errors->first('description') }}</font>
		        	@endif

		        </div>
		        
		    </div>

		    <div class="col-md-6">
            	<div class="form-group">
		            
		            <label>
			            {{ __("departments.parent_department") }}
			              :
		            </label>

		            <select  class="col-md-12"  name="parent_department">  
                         <option disabled selected value>
                         	{{ __("common.select") }}
                         </option>
			        	@foreach($parent_departments as $parent_department)
                            
                        
                            @if(count($errors) > 0)

	                            <option 
	                            @if(old('parent_department')==$parent_department->id)
									selected 
	                            @endif
			                    value="{{$parent_department->id}}"
			                    >
			                    {{$parent_department->name}}
			                    </option>

		                    @elseif(!empty($department['parent_department_id']) && $department['parent_department_id'] == $parent_department->id)

			                    <option selected
			                    value="{{$parent_department->id}}" >
			                    {{$parent_department->name}}
			                    </option>

                            @else

	                        <option 
		                    value="{{$parent_department->id}}" >
		                    {{$parent_department->name}}
		                    </option>

		                    @endif

                        @endforeach
		        	</select>

                    

	                @if ($errors->has('parent_department'))
		                <font color="red">{{ $errors->first('parent_department') }}</font>
		        	@endif

		        </div>
                <br/><br/>
		        <div class="form-group">

		            <label>
			            {{ __("companies.logo") }}
			              :
		            </label>

		        	<div>
	                    <input type="file" name="logo" class="pull-left">
	                    
	                    @if(!empty($department['id']))
							<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	                    @endif

                    </div>

					@if ($errors->has('profile_image'))
		                <font color="red">{{ $errors->first('profile_image') }}</font>
		        	@endif
		        </div>
		    </div>
		   
	    </div>

	    <div class="row col-md-12">

         	{{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}

            @if(!empty($department["id"]))
            @can('allowed-permission', ['departments-delete','departments-delete'])
            <div class="btn btn-danger pull-right"   class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_theme_danger">{{__("common.delete")}}</div>
            @endcan
            @endif
            

	    </div>    
    </div>
</div>

@if(!empty($department["id"]))
<div id="modal_theme_danger" class="modal fade" style="display: none;">
 <input id='department-data' type="hidden" data-branch_id ="{{$department['branch_id']}}" data-id='{{$department["id"]}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">{{__("common.delete")}}</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
				<p>
					{{__("departments.delete_department")}}
				</p>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
				<button type="button" class="btn btn-danger" id='delet-department'>
				{{__("common.delete")}}
				</button>
			</div>
		</div>
	</div>
</div>
@endif

<script type="text/javascript">

	$('#delet-department').click(function() {

        var id = $('#department-data').data("id");
        var branch_id = $('#department-data').data("branch_id");
        var _token = $('input[name="_token"]').val();

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('departments' ) !!}" + "/" + id +'/delete',
            data: { _token : _token },
	        success: function(msg) {

                location.replace("/branches/"+branch_id+"/departments");

	        },
	        error: function( data ) {

	        }
	    });
     });

</script>
