
<!--- main modal -->

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="details_table" tabindex="-1" role="dialog" aria-labelledby="details_tableLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Document Details</h4>
            </div>
            <div class="modal-body details_modal_body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<!--- main modal -->


<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">

        <div class="col-md-12">
            <div class="panel panel-flat inbox_content" data-i18n="filter" data-i18n-target="dataTables_info">

                @if(session()->has('access_denied'))
                    <div class="alert alert-danger">
                        <span data-i18n="access.access_denied"></span>
                    </div>
                @endif

                    @if(session()->has('storage_full'))
                        <div class="alert alert-danger">
                            <span> {{ sprintf(__('common.storage_full'),formatSizeUnits(session()->get('nUserTotalFilesSize')),formatSizeUnits(session()->get('nQuotaSize')))  }}</span>
                        </div>
                    @endif

                <div class="panel-body">
                    <div class="tabbable tab-content-bordered">
                        <ul class="nav  nav-tabs nav-tabs-highlight nav-justified  nav-tabs-component">
                            <li class="active table-list"><a href="#justified-right-icon-inbox"
                                                             id="inbox_button loading" data-type="inbox"
                                                             data-toggle="tab"><span
                                            data-i18n="nav.inbox.inbox"></span> @if(isset($oBadges['badges']) && $oBadges['badges']['inbox']>0 )
                                        <small style="color:red;font-weight:bold"> ({{ $oBadges['badges']['inbox'] }})
                                        </small> @endif
                                </a></li>
                            <li class="table-list"><a href="#justified-right-icon-pending" id="pending_button"
                                                      data-type="pending"
                                                      data-toggle="tab"><span
                                            data-i18n="nav.inbox.pending"></span> @if(isset($oBadges['badges'])  && $oBadges['badges']['pending']>0 )
                                        <small style="color:red;font-weight:bold"> ({{ $oBadges['badges']['pending'] }}
                                            )
                                        </small> @endif
                                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1"
                                         viewBox="0 0 23.47 26" style="
    height: 20px;
    width: 19px;
    display: inline-block;
    vertical-align: middle;
    margin-top: -5px;
">
                                        <defs></defs>
                                        <g id="a">
                                            <path class="cls-1"
                                                  d="M17.57,2,15,12H5v1H15Zm3,.8v2.7c0,4.1-6,2.5-6,2.5s1.5,6-2.6,6H2V4H12.7A12.37,12.37,0,0,1,16,2.5V2H0V26H10.2c3.2,0,9.8-7.2,9.8-9.6V10.8ZM5,16H15V15H5ZM8.6,9H5v1H8.3Z"></path>
                                            <path class="cls-1"
                                                  d="M18.57,2.7S19.47,1,20,0l3.5,3.5c-1.1.6-2.7,1.4-2.7,1.4Zm-3.5,2.7A11.45,11.45,0,0,1,13.57,9l.3.3,1.7-1.7a2.19,2.19,0,0,0,.3-.5.55.55,0,1,1,1.1.1c0,.1-.1.2-.2.3a.37.37,0,0,1-.3.1.55.55,0,0,0-.5.3l-1.7,1.6.3.4a19.43,19.43,0,0,1,3.6-1.5L20,5.1l-1.6-1.6Z"></path>
                                        </g>
                                    </svg>
                                </a></li>
                            <li class="table-list"><a href="#justified-right-icon-sent" id="sent_button"
                                                      data-type="sent" data-toggle="tab"><span
                                            data-i18n="nav.inbox.sent"></span>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1"
                                         viewBox="0 0 22 24"
                                         style="&#10;    height: 20px;&#10;    width: 19px;&#10;    display: inline-block;&#10;    vertical-align: middle;&#10;    margin-top: -5px;&#10;">
                                        <defs></defs>
                                        <title>Documents Sent</title>
                                        <g id="a">
                                            <path class="cls-1"
                                                  d="M10,9c1.4-5.9,8-7,8-7V0l4,4L18,8V6S12.9,5.9,10,9Zm5,1H5v1H15Zm3,.8v2.7c0,4.1-6,2.5-6,2.5s1.5,6-2.6,6H2V2H12.7A12.37,12.37,0,0,1,16,.5V0H0V24H10.2c3.2,0,9.8-7.2,9.8-9.6V8.8ZM5,14H15V13H5ZM8.6,7H5V8H8.3Z"
                                                  style="&#10;    fill: #2d3092;&#10;"/>
                                        </g>
                                    </svg>
                                </a></li>

                            {{--                            <li class="table-list"><a href="#justified-right-icon-completed" id="completed_button" data-type="completed" data-toggle="tab"><span
                                                                        data-i18n="nav.inbox.Completed">{{__('lang.completed')}}</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1"
                                                                     viewBox="0 0 18.38 20"
                                                                     style="&#10;    height: 20px;&#10;    width: 19px;&#10;    display: inline-block;&#10;    vertical-align: middle;&#10;    margin-top: -5px;&#10;">
                                                                    <defs></defs>
                                                                    <title>Documents completed</title>
                                                                    <g id="a">
                                                                        <path class="cls-1"
                                                                              d="M4,10h8v1H4Zm6.58-2H4V9h7.59A1.76,1.76,0,0,0,10.58,8ZM4,6H9.47a5,5,0,0,1-.25-1H4ZM15,9.46v2.33c0,3.48-5.25,2.12-5.25,2.12S11.06,19,7.47,19H1V2H9.47a10.83,10.83,0,0,1,1.1-2H0V20H8.37s8.32-5.62,8.63-7.66V9A16.09,16.09,0,0,1,15,9.46Zm3.38-5.52A3.82,3.82,0,1,1,14.56.12,3.8,3.8,0,0,1,18.38,3.94Zm-1.87-.76-.59-.59L14.14,4.45l-.85-.76-.59.59,1.44,1.36,2.38-2.46Z"
                                                                              style="&#10;    fill: #2D3092;&#10;"/>
                                                                    </g>
                                                                </svg>
                                                            </a></li>--}}
                            <li class="table-list"><a href="#justified-right-icon-draft" id="draft_button"
                                                      data-type="draft" data-toggle="tab"><span
                                            data-i18n="nav.inbox.Draft"></span> @if(isset($oBadges['badges'])   && $oBadges['badges']['draft']>0 )
                                        <small style="color:red;font-weight:bold"> ({{ $oBadges['badges']['draft'] }})
                                        </small> @endif
                                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1"
                                         viewBox="0 0 24.35 24.22"
                                         style="&#10;    height: 20px;&#10;    width: 19px;&#10;    display: inline-block;&#10;    vertical-align: middle;&#10;    margin-top: -5px;&#10;">
                                        <defs></defs>
                                        <title>Documents Drafts</title>
                                        <g id="a">
                                            <path class="cls-1"
                                                  d="M10,13.22H6v-1h4ZM12.65,8.5l3.3,3.3-4.4,1Zm11.7-5.1L17,10.8l-3.4-3.4L20.95,0ZM18,12.82v.9c0,4.1-6,2.5-6,2.5s1.5,6-2.6,6H2v-20H16.1l2-2H0v24H10.2c3.2,0,9.8-7.2,9.8-9.6v-3.8Z"
                                                  style="&#10;    fill: #2D3092;&#10;"/>
                                        </g>
                                    </svg>
                                </a></li>

                            {{--                            <li class="table-list"><a href="#justified-right-icon-rejected" id="rejected_button" data-type="rejected" data-toggle="tab"><span
                                                                        data-i18n="nav.inbox.rejected">{{__('lang.rejected')}}</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1"
                                                                     viewBox="0 0 21.7 24.05"
                                                                     style="&#10;    height: 20px;&#10;    width: 19px;&#10;    display: inline-block;&#10;    vertical-align: middle;&#10;    margin-top: -5px;&#10;">
                                                                    <defs></defs>
                                                                    <title>Documents Rejected</title>
                                                                    <g id="a">
                                                                        <path class="cls-1"
                                                                              d="M5,12.05H15v1H5Zm7.8-3H5v1h9A10.59,10.59,0,0,1,12.8,9.05ZM5,7.05h6.5c-.1-.3-.2-.7-.3-1H5Zm13,4v2.5c0,4.1-6,2.5-6,2.5s1.5,6-2.6,6H2v-20h9.5a8.58,8.58,0,0,1,1.3-2H0v24H10.2c3.2,0,9.8-7.2,9.8-9.6v-3.9A6.53,6.53,0,0,1,18,11.05ZM17.2,0a4.5,4.5,0,1,0,4.5,4.5A4.48,4.48,0,0,0,17.2,0Zm2.1,3.1L17.9,4.5l1.4,1.4-.7.7L17.2,5.2,15.8,6.6l-.7-.7,1.4-1.4L15.1,3.1l.7-.7,1.4,1.4,1.4-1.4Z"
                                                                              style="&#10;    fill: #2d3092;&#10;"/>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                           </li>--}}
                        </ul>

                        <div class="tab-content ">
                            <span>maqtash</span>
                            <div class="tab-pane active" id="justified-right-icon-inbox">


                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive inbox table-lay"
                                           id="inbox_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                            </td>
                                            <th data-i18n="outbox"></th>
                                            <th data-i18n="from"></th>
                                            <th data-i18n="inbox_title"></th>
                                            <th data-i18n="content"></th>
                                            <th data-i18n="attachment"></th>
                                            <th data-i18n="time"></th>

                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane" id="justified-right-icon-pending">

                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive pending table-lay"
                                           id="pending_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                            </td>
                                            <th data-i18n="outbox"></th>
                                            <th data-i18n="from"></th>
                                            <th data-i18n="inbox_title"></th>
                                            <th data-i18n="content"></th>
                                            <th data-i18n="attachment"></th>
                                            <th data-i18n="time"></th>
                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane" id="justified-right-icon-sent">

                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive sent table-lay"
                                           id="sent_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                            </td>
                                            <th data-i18n="from"></th>
                                            <th data-i18n="inbox_title"></th>
                                            <th data-i18n="message"></th>
                                            <th data-i18n="attachment"></th>
                                            <th data-i18n="time"></th>
                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="tab-pane" id="justified-right-icon-draft">
                                <div class="col-md-12">
                                    <div class="navbar-collapse collapse" id="">

                                        <div class="btn-group navbar-btn">



                                            <div type="button" class="btn btn-default btn-icon btn-checkbox-all">
                                                <input type="checkbox" class=" all_select" id="checkall">
                                            </div>
                                            <button type="button" class="hide btn btn-default btn-icon dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu hide">
                                                <li><a href="#">Select all</a></li>
                                                <li><a href="#">Select read</a></li>
                                                <li><a href="#">Select unread</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Clear selection</a></li>
                                            </ul>
                                        </div>


                                        <div class="btn-group navbar-btn">
                                            <button type="button" class="btn btn-default"
                                                    id="delete_all_selected_draft"><i class="icon-bin"></i> <span
                                                        class="hidden-xs position-right">Delete</span></button>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="alert alert-info no-border" id="select_all_documents_in_draft"
                                         style="display: none;">
                                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                        <div class="select_message" style="display:inline-block">
                                            <span class="text-semibold">All documents on this page are selected.</span>
                                            Select all documents in Draft
                                        </div>
                                        <a href="#" class="alert-link select-all-rows" id="select-all-rows">here</a>.

                                    </div>
                                </div>
                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive completed table-lay"
                                           id="draft_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                            </td>
                                            <th data-i18n="from"></th>
                                            <th data-i18n="inbox_title"></th>
                                            <th data-i18n="message"></th>
                                            <th data-i18n="attachment"></th>
                                            <th data-i18n="time"></th>
                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>

                                </div>
                            </div>


                            {{--                            <div class="tab-pane" id="justified-right-icon-completed">

                                                            <div class="content-table">
                                                                <table class="table datatable-ajax table-inbox responsive completed table-lay" id="completed_table"
                                                                       style="width:100%;table-layout: auto !important;">
                                                                    <thead>
                                                                    <tr>
                                                                        <th data-i18n="select"></td>
                                                                        <th ></th>
                                                                        <th data-i18n="name"></th>
                                                                        <th data-i18n="inbox_title"></th>
                                                                        <th data-i18n="message"></th>
                                                                        <th data-i18n="time"></th>
                                                                        <th data-i18n="details"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody data-link="row" class="rowlink">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>--}}

                            {{--                            <div class="tab-pane" id="justified-right-icon-rejected">

                                                            <div class="content-table">
                                                                <table class="table datatable-ajax table-inbox responsive completed table-lay" id="rejected_table"
                                                                       style="width:100%;table-layout: auto !important;">
                                                                    <thead>
                                                                    <tr>
                                                                        <th data-i18n="select"></td>
                                                                        <th ></th>
                                                                        <th data-i18n="name"></th>
                                                                        <th data-i18n="inbox_title"></th>
                                                                        <th data-i18n="message"></th>
                                                                        <th data-i18n="time"></th>
                                                                        <th data-i18n="details"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody data-link="row" class="rowlink">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>--}}


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /content area -->

</div>
<!-- /main content -->
<script>
    var lang = {
        'search': '{{__('lang.search')}}',
        'search1': '{{__('search1')}}'
    };
</script>







