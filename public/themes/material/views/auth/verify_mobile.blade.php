{!! Theme::asset()->container('auth')->themePath()->add([ ['forgot', 'pages/auth/forgot.js',['bootstrap']]]) !!}


<div class="page-container" style="min-height:279px">


    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Unlock user -->
                {{ Form::open(array('url' => route('user.confirmMobile') ,'class'=>'login-form')) }}

                <div class="panel">
                        <div class="panel-body">

                            <a class="col-md-3 col-sm-4"><i class="back-arrow-forgot glyphicon glyphicon-arrow-left"></i></a><br/>
                            <br/>

                            <div class="login_logo_container" >
                                <img src="/assets/images/mycompose_logo.png" id="login_logo" style="height: 50%;width:50%;margin-left: 25%"/>
                            </div><br/>

                            <h6 class="content-group text-center text-semibold no-margin-top">{{ trans("auth.my_compose") }}<small class="display-block">{{ trans("auth.verifyMobile") }}</small></h6>
                            @if(session('thanks_for_registration'))

                                <div class="alert alert-success">
                                     {{ trans(session()->get('thanks_for_registration')) }}
                                </div>
                            @endif
                            @if (session('status'))


                                <div class="alert alert-success">

                                    {{ trans(session()->get('status')) }}

                                </div>
                            @endif

                            <div class="form-group has-feedback">
                                <div class="form-group has-feedback has-feedback-left">

                                    <input style="display:none" type="text" name="email"/>
                                    <input type="text"  name="code" class="form-control" id="verify_code" placeholder="{{ trans("auth.verify_code") }}"  >
                                    <div class="form-control-feedback">
                                        <i class="icon-mention text-muted"></i>
                                    </div><br/>
                                    @if ($errors->has('code'))
                                        <span style="color : red">{{ $errors->first('code') }}</span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group col-sm-12 sign-up">

                                <input type="submit" class="btn btn-primary btn-block" />
                                <a href="{{route("user.resendVerify")}}" class="btn btn-default btn-block resend_code">Resend Code</a>

                            </div>

                        </div>
                    </div>
                {{ Form::close() }}
                <!-- /unlock user -->


                <!-- Footer -->
                <div class="footer text-muted text-center">

                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>