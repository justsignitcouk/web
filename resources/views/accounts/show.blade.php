@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

				<ul class="breadcrumb">
					<li><a href="{{route('users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
					<li><a href="{{route('users.profile'  , $account->user_id)}}"><i class="position-left"></i>{{ $account->account_name }}</a></li>
					<li><a href="{{route('users.accounts' , $account->user_id)}}"><i class="position-left"></i>{{ __("common.accounts") }}</a></li>
					<li class="active">{{ __("common.show") }}</li>
				</ul>

				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">
							    @include('layouts/partials/tabs/account_tabes')
						    	<div class="panel panel-body 
						    	pull-right 
						    	col-md-12">
                                    <div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.name") }}:</label>
										<span class="pull-right-sm">
										@if(!empty($account->account_name))
										{{$account->account_name}}
										@else
                                          --
										@endif
										</span>
									</div>
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.company") }}:</label>
										<span class="pull-right-sm">
										@if(!empty($account->Department->Branch->Company->name))
										{{$account->Department->Branch->Company->name}}
										@else
                                          --
										@endif
										</span>
									</div>
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.branch") }}:</label>
										<span class="pull-right-sm">
										@if(!empty($account->Department->Branch->name))
										{{$account->Department->Branch->name}}
										@else
                                          --
										@endif
										</span>
									</div>
		
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.department") }}:</label>
										<span class="pull-right-sm">
										@if(!empty($account->Department->name))
										{{$account->Department->name}}
										@else
                                          --
										@endif
										</span>
									</div>
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.active") }}:</label>
										<span class="pull-right-sm">
                                        @if($account->active == 1)
                                        {{ __("common.yes") }}
                                        @elseif($account->active == 0)
                                        {{ __("common.no") }}
                                        @else
                                        --
                                        @endif
										</span>
									</div>
									
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.verified") }}:</label>
										<span class="pull-right-sm">
                                        @if($account->verify == 1)
                                        {{ __("common.yes") }}
                                        @elseif($account->verify == 0)
                                        {{ __("common.no") }}
                                        @else
                                        --
                                        @endif
										</span>
									</div>
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.is_default") }}:</label>
										<span class="pull-right-sm">
                                        @if($account->is_default == 1)
                                        {{ __("common.yes") }}
                                        @elseif($account->is_default == 0)
                                        {{ __("common.no") }}
                                        @else
                                        --
                                        @endif
										</span>
									</div>
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("common.profile_image") }}:</label>
										<div class="pull-right-sm">
											<img src="{{cdn_url($sImageName,$sImagePath)}}" alt="" style="max-width: 115px;max-height: 90px;">
										</div>
									</div>
							  	</div>
		             	</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endsection
