<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Theme
 */
class Theme extends Model
{
    protected $table = 'themes';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'link',
        'notes',
        'status',
        'taggable_id',
        'taggable_type'
    ];

    protected $guarded = [];

        
}