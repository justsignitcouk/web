<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentPrerequisite
 */
class DocumentPrerequisite extends Model
{
    protected $table = 'document_prerequisite';

    public $timestamps = true;

    protected $fillable = [
        'document_id',
        'prerequisite_id'
    ];

    protected $guarded = [];

        
}