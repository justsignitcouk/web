@extends('layouts.admin')

@section('content')

    <!-- Ajax sourced data -->
    <div class="panel panel-flat">

        @include('layouts/partials/message')

        <div class="panel-heading">
            <h5 class="panel-title">{{ __("workflow.workflow.titles.manage_state") }}</h5>
            <div class="heading-elements hidden">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

                <a class="btn btn-primary pull-right" id="bootbox_form"  data-toggle="modal" data-target="#add_state">{{ __("workflow.state.add_state") }}
                </a>

                {{--{{ model to add stage workflow }}--}}
                <div class="modal fade" id="add_state" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title" id="myModalLabel">{{ __('workflow.state.add_state') }}</h4>
                            </div>
                            {!! Form::open(['url'=> url('/admin/state'), 'method'=>'POST', 'id'=>'add_state_form', 'enctype'=>'multipart/form-data']) !!}
                            <div class="modal-body">
                                {{ csrf_field() }}
                                <fieldset title="2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.state.state') .' '. __('common.name') }}: <span
                                                            class="text-danger">*</span></label><br>
                                                <input name="state_name" value="{{old('state_name')}}" class="required form-control">

                                                <br/><font color="red" class="validation" id="stage_name_validation"></font>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.state.state').' '.__('common.description')}}
                                                    : </label><br>
                                                <textarea name="description" class="form-control"></textarea>
                                                <input type="hidden" name="workflow_id" value=""/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('workflow.state.state').' '.__('common.active')}}
                                                    : </label><br>
                                                {{ Form::select('active', [ '1' => 'Yes', '0' => 'No' ],null,['class'=>'form-control'] ) }}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                                <button type="button" id="save_state" class="btn btn-primary">{{__('common.save')}}</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                {{--{{ model to add stage workflow }}--}}





            <table id="workflow-state-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                <tr>
                    <th>{{ __("workflow.state.name") .' '. __("common.name") }}</th>
                    <th>{{ __("workflow.state.description") .' '. __("common.description") }}</th>
                    <th>{{ __("workflow.state.active")}}</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /ajax sourced data -->

        @endsection
        @push('scripts')
            <script>

                $(function() {

                    $.extend( $.fn.dataTable.defaults, {
                        autoWidth: true,
                        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                        language: {
                            "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                            "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                            "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                            "sInfo":         "{{ __('components/datatables.sInfo') }}",
                            "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                            "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                            "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                            "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                            "sUrl":          "{{ __('components/datatables.sUrl') }}",
                            "oPaginate": {
                                "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                                "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                                "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                                "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                            }
                        }
                    });

                    var table = $('#workflow-state-table').DataTable({

                        processing: true,
                        serverSide: true,
                        ajax: '{!! route('admin.state.data') !!}',
                        columns: [
                            { data: 'name', name: 'name' },
                            { data: 'description', name: 'description' },
                            { data: 'active', name: 'active' }
                        ]
                    });

                    $('.datatable-ajax').on('click', 'tbody td', function() {

                        var data = table.row(this).data();
                        var id = data.id ;
                        window.location = '/admin/state/' + id;
                    });


                    // Add State
                    $(document).on("click", "#save_state", function (e) {
                        var data = $("#add_state_form").serializeArray(),
                            url = $("#add_state_form").attr('action');
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: data,
                            datatype: 'json',
                        }).done(function (data) {
                            if (data.status == false) {
                                validation_messages(data);
                            } else {
                                window.location.reload();
                            }
                        });

                        e.preventDefault();
                    });


                    function validation_messages(data) {
                        $('.validation').html('');
                        $.each(data.message, function (index, data2) {

                            var field_name = index + '_validation',
                                messages = data2;
                            var html = '';
                            $.each(messages, function (index3, data3) {
                                if (html != '') {
                                    html = html + '<br/>';
                                }
                                html += data3;
                            });
                            $('#' + field_name).html(html);
                        });
                    }
                });
            </script>

    @endpush