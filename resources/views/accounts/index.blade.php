@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("accounts.accounts") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

                <ul class="breadcrumb">
                    <li><a href="{{route('users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
                    <li><a href="{{route('users.profile' , $user['id'])}}"><i class="position-left"></i>{{ $user['username'] }}</a></li>
                    <li class="active">{{ __("common.accounts") }}</li>
                </ul>
				
				@include('layouts/partials/tabs')

                <input type="hidden" id="user_data"data-user_id="{{$user['id']}}">

				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">

						    @can('allowed-permission', ['accounts-create','accounts-create'])
                            @if($deps > 0)
			                <a href="{{ url('accounts/'.$user['id'].'/createaccount') }}"
			                   class="btn btn-primary"
			                   id="bootbox_form">
			                    {{ __("accounts.add_account") }}
			                </a>
                            @endif
                            @endcan
			           

				            <table id="accounts-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
				                <thead>
				                    <tr>
				                        <th>{{ __("common.account_name") }}</th>
				                        <th>{{ __("common.company") }}</th>
				                        <th>{{ __("common.branch") }}</th>
                                        <th>{{ __("common.department") }}</th>
				                    </tr>
				                </thead>
				            </table>

		             	</div>
				    </div>
			    </div>
			</div>
		</div>
</div>

</div>

@endsection
@push('scripts')
<script>

    $(function() {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                "sInfo":         "{{ __('components/datatables.sInfo') }}",
                "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                "sUrl":          "{{ __('components/datatables.sUrl') }}",
                "oPaginate": {
                    "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                    "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                    "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                    "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                }
            }
        });

    
         var user_id = $('#user_data').data('user_id');

         var table = $('#accounts-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: '{!! url('accounts' ) !!}' + '/'  +user_id+ '/datatables',
            columns: [
                { data: 'account_name', name: 'account_name' },
                { data: 'company_name', name: 'company_name' },
                { data: 'branch_name', name: 'branch_name' },
                { data: 'department_name', name: 'department_name' },
            ]
        });
     
        @can('allowed-permission', ['accounts-details','accounts-details'])
        $('.datatable-ajax').on('click', 'tbody td', function() {

                    var data = table.row(this).data();
                    var id = data.id ;
                    window.location = '/accounts/' + id ;
                
                });
        @endcan
 
    });
</script>

@endpush
