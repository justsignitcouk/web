<?php

namespace App\Http\Controllers\Individual;

use App\Services\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function __construct()
    {

        parent::__construct();
        view()->share('bShowMainSidebar', false);
    }

    public function save()
    {
        return \Plupload::file('file', function($file) {
            $oFile = File::put($file,'document_attachment');
            $nID = $oFile['oFile']['id'] ;
            $nFileName = $oFile['oFile']['hash_name'] ;
            // This will be included in JSON response result

            return [
                'success'   => true,
                'message'   => 'Upload successful.',
                'id'        => $nID,
                'name'        => $nFileName,
                // 'url'       => $photo->getImageUrl($filename, 'medium'),
                // 'deleteUrl' => action('MediaController@deleteDelete', [$photo->id])
            ];
        });
    }
}
