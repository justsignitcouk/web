@foreach($branches_permissions as $key => $permission)
    <div class="form-check">
      <label class="branch-check-label">
        <input class="branch-check-input" type="checkbox" name ="{{'permission-'.$key}}" value="{{$permission->id}}"

        @foreach($checked_user_permissions as $check)

            @if($check['permission_id'] == $permission->id)
            checked
            @endif
     
        @endforeach
       
        >
        {{$permission->name}}
      </label>
    </div>
@endforeach