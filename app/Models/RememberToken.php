<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RememberToken
 */
class RememberToken extends Model
{
    protected $table = 'remember_tokens';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'token',
        'user_agent'
    ];

    protected $guarded = [];

        
}