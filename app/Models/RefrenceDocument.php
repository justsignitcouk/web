<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RefrenceDocument
 */
class RefrenceDocument extends Model
{
    protected $table = 'refrence_document';

    public $timestamps = false;

    protected $fillable = [
        'document_id',
        'refrence_id'
    ];

    protected $guarded = [];

        
}