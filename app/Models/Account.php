<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\File;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\AccountRole;
use App\Models\AccountPermission;

/**
 * Class Account
 */
class Account extends Model
{

    use SoftDeletes;
   
    protected $table = 'accounts';
    public $timestamps = true;
    protected $guarded = [];
    protected $fillable = [
        'account_name',
        'active',
        'verify',
        'user_id',
        'company_id',
        'is_default',
        'profile_image'
    ];

    /*Relations*/
    public function User(){
        return $this->belongsTo('App\Models\User');
    }
    public function ProfileImage(){
        return $this->hasOne('App\Models\File', 'id', 'profile_image');
    }
    public function Department(){
        return $this->hasOne('App\Models\Department', 'id', 'department_id');
    }

    public function storeAccount($accountData){

        $account = new Account ;

        $account->account_name = $accountData->account_name ;
        $account->department_id = $accountData->department_id ;
        $account->user_id = $accountData->user_id ;

        $accountData->active == null ? $account->active = 0 : $account->active = 1;
        $accountData->verify == null ? $account->verify = 0 : $account->verify = 1;
        $accountData->is_default == null ? $account->is_default = 0 : $account->is_default = 1;
        

        if(!empty($accountData->profile_image)){

            $nImageProfile = File::put($accountData->profile_image);
            $account->profile_image  = $nImageProfile;

         }
                

        $account->save();

        return true ;
       
    }

    public function updateAccount($accountData , $id){


        $account =  Account::find($id);

        $account->account_name = $accountData->account_name ;
        $account->department_id = $accountData->department_id ;
        $account->user_id = $accountData->user_id ;

        $accountData->active == null ? $account->active = 0 : $account->active = 1;
        $accountData->verify == null ? $account->verify = 0 : $account->verify = 1;
        $accountData->is_default == null ? $account->is_default = 0 : $account->is_default = 1;

        if(!empty($accountData->profile_image)){
            $nImageProfile = File::put($accountData->profile_image);
            $account->profile_image  = $nImageProfile;

        }
        

        $account->save();

        return true ;

    }

    /**
     * Update account roles 
     */
    public function updateRoles($roleData ,$id){

        //delete account old roles
        $accountRoles =  AccountRole::where('account_id',$id)->delete();


        //store account new roles
        foreach ($roleData->all() as $key => $role) {

            if($key == '_token'){
                continue ;
            }

            $roleAccount = new AccountRole;
            $roleAccount->account_id = $id ;
            $roleAccount->role_id = $role ;

            $roleAccount->save();

        }

        return true ;
       
    }

    /**
     * Update account permissions 
     */
    public function updatePermissions($permissionsData , $id){
        

        //delete account old permissios
        $accountPermissions =  AccountPermission::where('account_id',$id)->delete();

     

        //store account new permissios
        foreach ($permissionsData->all() as $key => $permission) {

            if($key == '_token'){
                continue ;
            }

            $accountPermission = new AccountPermission;
            $accountPermission->account_id = $id ;
            $accountPermission->permission_id = $permission ;

            $accountPermission->save();

        }

        return true ;        


    }

        
}