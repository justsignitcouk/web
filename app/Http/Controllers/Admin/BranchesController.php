<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreBranch;
use App\Http\Requests\Admin\UpdateBranch;
use App\Models\Branch;
use App\Models\Country;
use App\Models\Department;
use App\Models\Company;
use App\Models\BranchType;
use Yajra\Datatables\Datatables;
use DB;
use Gate;

class BranchesController extends Controller
{
    /**
     * @var branch
     */
    protected $branch;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Branch $branch)
    {

        //$this->middleware('auth');
        //$this->branch   = $branch;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $active_branches = 1;
        $company['id'] = $id;
        $company_name = Company::select('name')->where('id', $id)->get();
        $company['name'] = $company_name[0]->name;


        return view('branches.index', compact('active_branches', 'company','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        $countries = Country::all();
        $branchTypes = BranchType::all();
        $company_id = $id;

        return view('branches.create', compact('countries', 'branchTypes', 'company_id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranch $storeBranch)
    {

        if ($this->branch->storeBranch($storeBranch)) {
            return redirect('/companies/' . $storeBranch->company_id . '/branches');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $countries = Country::all();
        $branchTypes = BranchType::all()->toArray();
        $oBranch = Branch::find($id);
        $company = Company::select('name', 'id')->where('id', $oBranch->company_id)->get();
        $branch = $oBranch;
        $active_edit = 1;

        $sLogoName = $oBranch->Logo ? $oBranch->Logo->hash_name : '';
        $sLogoPath = $oBranch->Logo ? $oBranch->Logo->path : '';

        return view('branches.edit', compact('branch', 'countries', 'branchTypes', 'active_edit', 'sLogoName', 'sLogoPath', 'company'));

    }

    public function update(UpdateBranch $updateBranch, $id)
    {

        if ($this->branch->updateBranch($updateBranch, $id)) {
            return redirect('/companies/' . $updateBranch->company_id . '/branches');
        }

    }

    public function show($id)
    {

        $branch = Branch::find($id);
        $company = Company::select('name', 'id')->where('id', $branch['company_id'])->get();
        $sLogoName = $branch->Logo ? $branch->Logo->hash_name : '';
        $sLogoPath = $branch->Logo ? $branch->Logo->path : '';

        $active_show = 1;

        return view('branches.show', compact('branch', 'active_show', 'sLogoName', 'sLogoPath', 'company'));

    }

    public function destroy($id)
    {

        $check_departments = Department::where('branch_id', '=', $id)->get()->count();
        if ($check_departments == 0) {
            $branch = Branch::find($id);
            $branch->delete();
            return $msg = 'can';
        } else {
            return $msg = 'cant';
        }
    }

    /*for index by ajax*/
    public function datatables($company_id)
    {

        $aBranches = Branch::where('company_id', $company_id)
            ->with('Country')
            ->get()
            ->toArray();

        return Datatables::of($aBranches)->make(true);


    }

    /*list of branch by company ID*/
    public function list($company_id)
    {

        return $branches = Branch::where('company_id', $company_id)->get();

    }
}
