@extends('layouts.admin')

@section('content')

    <div class="col-md-12">

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">{{ __("common.plans") }}<a class="heading-elements-toggle"><i
                                class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tabbable">

                    <ul class="breadcrumb">
                        <li><a href="{{route('admin.users')}}"><i
                                        class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
                        <li><a href="{{route('admin.users.profile' , $user['id'])}}"><i
                                        class="position-left"></i>{{ $user['username'] }}</a></li>
                        <li class="active">{{ __("common.show") }}</li>
                    </ul>

                    @include('layouts/partials/tabs')
@if(session()->has('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success_message') }}
                        </div>

    @endif
                    @if(session()->has('warning_message'))
                        <div class="alert alert-warning" role="alert">
                            {{ session()->get('warning_message') }}
                        </div>

    @endif
                    <div class="tab-content">
                        <div class="panel panel-flat">
                            <div class="panel-body">

                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-lg add_plan_to_user" data-toggle="modal" data-target="#add_plan_to_user_modal">
                                    {{ __('plans.add_plan_to_user') }}
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="add_plan_to_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                            </div>
                                            {!! Form::open(['url'=> route('admin.users.savePlanToUser',[$id]), 'method'=>'POST', 'id'=>'add_plan_to_user_form', 'enctype'=>'multipart/form-data']) !!}
                                            <div class="modal-body">

                                                <div class="form-group">
                                                    <label>{{ __('plans.name')  }}: <span
                                                                class="text-danger">*</span></label><br>
                                                    {!! Form::select('plans', ['' => __('common.please_choose') ] + $oPlans, '', ['class' => 'form-control m-bot15']) !!}
                                                    <br/><font color="red" class="validation" id="stage_name_validation"></font>
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary save_plan_to_user_button">Save changes</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>



                                <table id="users-plans-table" class="table datatable-ajax responsive  dt-responsive"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>{{ __("common.name") }}</th>
                                        <th>{{ __("common.start_date") }}</th>
                                        <th>{{ __("common.end_date") }}</th>
                                        <th>{{ __("common.activated") }}</th>

                                    </tr>
                                    </thead>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('scripts')
    <script>

        $(function() {

            $.extend( $.fn.dataTable.defaults, {
                autoWidth: true,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                    "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                    "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                    "sInfo":         "{{ __('components/datatables.sInfo') }}",
                    "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                    "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                    "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                    "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                    "sUrl":          "{{ __('components/datatables.sUrl') }}",
                    "oPaginate": {
                        "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                        "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                        "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                        "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                    }
                }
            });
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                }
            });

            var table = $('#users-plans-table').DataTable({

                processing: true,
                serverSide: true,
                ajax: {
                    url : '{!! route('admin.users.plans',[$id]) !!}',
                    method : 'POST'
                },
                columns: [
                    { data: 'plan.title', name: 'plan.title' },
                    { data: 'start_at', name: 'start_at' },
                    { data: 'expired_at', name: 'expired_at' },
                    { data: 'is_active', render: function(data){
                            if(data == 1)
                                return '<div class="checkbox checkbox-switchery">\n' +
                                    '<label>\n' +
                                    '<input type="checkbox" class="switchery" checked="checked">\n' +
                                    'Unchecked switch\n' +
                                    '</label>\n' +
                                    '</div>'  ;
                            else return '<div class="checkbox checkbox-switchery">\n' +
                                '<label>\n' +
                                '<input type="checkbox" class="switchery">\n' +
                                'Unchecked switch\n' +
                                '</label>\n' +
                                '</div>'  ;
                        } }
                ],
                "initComplete": function(settings, json) {
                    // Switchery
                    // ------------------------------

                    // Initialize multiple switches
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                    elems.forEach(function(html) {
                        var switchery = new Switchery(html);
                    });

                    // Colored switches
                    var primary = document.querySelector('.switchery-primary');
                    var switchery = new Switchery(primary, { color: '#2196F3' });

                    var danger = document.querySelector('.switchery-danger');
                    var switchery = new Switchery(danger, { color: '#EF5350' });

                    var warning = document.querySelector('.switchery-warning');
                    var switchery = new Switchery(warning, { color: '#FF7043' });

                    var info = document.querySelector('.switchery-info');
                    var switchery = new Switchery(info, { color: '#00BCD4'});

                }
            });



            $('.datatable-ajax').on('click', 'tbody td', function() {

                var data = table.row(this).data();
                var id = data.id ;
                window.location = '/admin/users/' + {{ $id }} +'/plans/' + id;

            });


            $('.save_plan_to_user_button').on('click',function(){
                $(this).closest('form').submit();
            })
        });
    </script>

@endpush