<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license_number'    => 'required|min:2',
            'branch_name'       => 'required|min:2',
            'branch_email'      => 'required|min:2',
            'branch_type'       => 'required|numeric',
            'country_id'        => 'required|min:2',
        ];
    }

}
