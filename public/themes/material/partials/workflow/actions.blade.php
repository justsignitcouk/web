{!! Theme::asset()->themePath()->add([ ['workflow_actions', 'partials/workflow/actions.js',['bootstrap']]]) !!}
{!! Theme::asset()->container('inline')->add([['custom-workflow', '
<script type="text/javascript">
    var workflow_get_actions = "' .   route('workflow.getActions')  .'" ;
    var workflow_save_action = "' .   route('workflow.saveAction')  .'" ;
    var document_id = "' . $id . '" ;
    var current_user_id = "' . nUserID() . '" ;
    var account_roles_ids = ["' . implode([],'","') . '"] ;
</script>']])  !!}

<div class="sidebar-category-actions">
            <form class="actions_container_form">
                <div class="col-md-12 actions_container">

                </div>
            </form>

</div>


















@if(false)
    <!-- Block buttons -->
    <div class="sidebar-category">
        <div class="category-title">
            <span>Block buttons</span>
            <ul class="icons-list">
                <li><a href="#" data-action="collapse"></a></li>
            </ul>
        </div>

        <div class="category-content">
            <div class="row">
                <div class="col-md-12">
                    <a class="edit btn btn-primary" href="{{ route('document.editDocument',[ 'id'=>$id ]) }}">Edit</a>
                    <button class="saveSign btn btn-primary" disabled>Save</button>
                </div>

            </div>
            <br/>
            <div class="row">

                <div class="col-xs-6">
                    <button id="print"
                            class="btn bg-teal-400 btn-block btn-float btn-float-lg legitRipple"
                            type="button" data-id="{{ $id }}"><i class="glyphicon glyphicon-print"></i>
                        <span>Print</span>
                    </button>
                    <a href="{{route('document.pdfDocument',[$id])}}" target="_blank" id="pdf"
                       class="btn bg-teal-400 btn-block btn-float btn-float-lg legitRipple"
                       type="button" data-id="{{ $id }}"><i class="glyphicon glyphicon-print"></i> <span>PDF</span>
                    </a>

                </div>

            </div>
        </div>


    </div>
    <!-- /block buttons -->
    <!-- ACTIONS buttons -->
@endif