@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("roles.roles") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">
				
				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">

                                @include('layouts/partials/tabs/roles_tabes')

						    	<div class="panel panel-body 
						    	pull-right 
						    	col-md-12">
									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("roles.name") }}:</label>
										<span class="pull-right-sm">{{$role['name']}}</span>
									</div>

									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("roles.dispaly_name") }}:</label>
										<span class="pull-right-sm">{{$role['display_name']}}</span>
									</div>

									<div class="form-group mt-5">
										<label class="text-semibold">{{ __("roles.description") }}:</label>
										<span class="pull-right-sm">{{$role['description']}}</span>
									</div>

							  	</div>
		             	</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endsection
