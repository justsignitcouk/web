<?php

return [
    'member_login' => 'Member Login',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة السر',
    'login' => 'تسجيل الدخول',
    'next'=>'التالي',
    'need_help'=>'المساعدة',
    'contact_us'=>'اتصل بنا',
    'require'=>'جميع الحقول مطلوبة',
    'title'=>'العنوان',
    'your_name'=>'الاسم',
    'your_email'=>'البريد الالكتروني',
    'your_ms'=>'رسالتك',
    'submit'=>'تاكيد',
    'no_account'=>"ليس لديك حساب",
    'sign_up'=>'سجل',
    'sign_with'=>'سجل عبر',
    'forgot_password'=>'هل نسيت كلمة السر',
    'cancle'=>'إنهاء',
    'reset_password'=>'اعادة تعيين كلمة السر',
    'my_compose'=>'My compose'
];