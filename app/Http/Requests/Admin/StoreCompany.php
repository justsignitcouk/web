<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Input ;

class StoreCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if(Input::get('branch_fax')){

            return [
                'name'=>'required|unique:companies',
                'slug'=>'required',
                'license_no' =>'numeric',
                'branch_name'=>'required',
                'branch_email'=>'required|email|unique:branches,email,'.$this->id,
                'branch_license_no'=>'required|numeric',
                'branch_address'=>'required',
                'branch_pobox'=>'required|numeric',
                'branch_phone'=>'required|numeric',
                'branch_fax'=>'numeric',
                'branch_country_id' => 'required',
                'branch_type' => 'required',
            ];

        }else{

            return [
                'name'=>'required|unique:companies',
                'slug'=>'required',
                'license_no' =>'numeric',
                'branch_name'=>'required',
                'branch_email'=>'required|email|unique:branches,email,'.$this->id,
                'branch_license_no'=>'required|numeric',
                'branch_address'=>'required',
                'branch_pobox'=>'required|numeric',
                'branch_phone'=>'required|numeric',
                'branch_country_id' => 'required',
                'branch_type' => 'required',
            ];

        }

    }
}
