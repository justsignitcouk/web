$(document).ready(function () {


// Initialize
    var validator;
    i18n.init({
            resGetPath: '/themes/material/assets/locales/__lng__.json',
            debug: true,
            fallbackLng: false,
            load: 'unspecific'
        },
        function () {
            $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return ( this.optional(element) || re.test(value) ) &&  (value.indexOf(" ") < 0 && value != "");
                },
                "Please check your input."
            );

            validator = $(".form-validate-jquery").validate({
                ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                errorClass: 'validation-error-label',
                successClass: 'validation-valid-label',
                highlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },
                unhighlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },

                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: "validation-valid-label",
                success: function (label) {
                    label.addClass("validation-valid-label").text("Success.")
                },
                rules: {
                    company_name: {
                        required:true,
                        minlength: 3
                    },
                    slug: {
                        required:true,
                        minlength: 3,
                        regex:  "^[a-zA-Z'.\\s]{1,40}$" ,
                        "remote":
                            {
                                url: '/settings/create-company/check-slug',
                                type: "post"
                            }
                    },
                    license_number: {
                        required:true,
                        minlength: 3
                    },
                    branch_name: {
                        required:true,
                        minlength: 3
                    },
                    branch_email: {
                        required:true,
                        minlength: 3
                    },
                    branch_type: {
                        required:true
                    },
                    country_id: {
                        required:true
                    }
                },
                messages: {
                    required: i18n.t("signup.this_field_is_required"),
                    custom: {
                        required: i18n.t("signup.this_field_is_required")
                    },
                    slug: {
                        required: i18n.t("signup.enter_email"),
                        remote: i18n.t("company.slug_taken")
                    }
                    },
                submitHandler: function (form) {
                    return false;
                }
            });

            jQuery.validator.addMethod("email_or_mobile", function (value, element, params) {
                var email = $('#email').val();
                var mobile = $('#mobile').val();
                if (email != '' || mobile != '') {
                    return true;
                }
                if (email == '' && mobile == '') {
                    return false;
                }

            }, jQuery.validator.format("Please enter A correct Email address or Valid Mobile Number"));

            jQuery.validator.addMethod("mobile_remote", function (value, element, params) {
                if (value != '') {
                    $.post('/validateEmailOrPhone', {'identifier': value}, function (data) {
                        if (data.oUser == false) {
                            return true;
                        }
                        return false;
                    }, 'json');
                }
                return true;
            }, jQuery.validator.format("Mobile exist"));


            $('.form-validate-jquery').submit(function (e) {
                var errors = validator.numberOfInvalids();
                if (!errors) {
                    var lock = $(this).closest('.panel-body');
                    var data = $(this).serialize();
                    this.submit();
                }
            });


        });

});