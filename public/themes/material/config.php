<?php



//if(\App::getLocale() == 'en'){

    return array(

        /*
        |--------------------------------------------------------------------------
        | Inherit from another theme
        |--------------------------------------------------------------------------
        |
        | Set up inherit from another if the file is not exists, this
        | is work with "layouts", "partials", "views" and "widgets"
        |
        | [Notice] assets cannot inherit.
        |
        */

        'inherit' => null, //default

        /*
        |--------------------------------------------------------------------------
        | Listener from events
        |--------------------------------------------------------------------------
        |
        | You can hook a theme when event fired on activities this is cool
        | feature to set up a title, meta, default styles and scripts.
        |
        | [Notice] these event can be override by package config.
        |
        */

        'events' => array(

            'before' => function($theme)
            {
                $theme->setTitle('My Compose');
                $theme->setAuthor('My Compose');
            },

            'asset' => function($asset)
            {
                $asset->themePath()->add([ /*,['bootstrap']*/
                    ['script', 'js/script.js']
                ]);


                $asset->container('ltr')->themePath()->add([
                    ['bootstrap', 'css/bootstrap.css'],
                    ['core', 'css/core.css',['bootstrap']],
                    ['style', 'css/style.css',['bootstrap']],
                    ['custom', 'css/custom.css',['bootstrap']],
                    ['components', 'css/components.min.css'],

                ]);

                $asset->container('rtl')->themePath()->add([
                    ['bootstrap', 'css/bootstrap.css'],
                    ['bootstrap_rtl', 'css/bootstrap-rtl.css','bootstrap'],
                    ['rtl-core', 'css/core-rtl.css',['bootstrap_rtl']],
                    ['rtl-style', 'css/style-rtl.css',['bootstrap_rtl']],
                    ['custom', 'css/custom.css',['bootstrap_rtl']],
                    ['rtl-components', 'css/components-rtl.css'],

                ]);

                 $asset->themePath()->add([

                    ['colors', 'css/colors.css'],
                    ['styles', 'css/icons/icomoon/styles.css',['bootstrap']],
                     ['components', 'css/components.min.css',['styles']],
                    ['custom', 'css/custom.css',['bootstrap']],
                    ['fontawesome', 'css/icons/fontawesome/styles.min.css'],
                    ['jquery', 'js/core/libraries/jquery.min.js' ],
                    ['jquery-ui', '../../../assets/components/jquery-ui-1.12.1/jquery-ui.min.js', ['jquery']],
                    ['bootstrap', 'js/core/libraries/bootstrap.min.js', ['jquery']],
                    ['pace','js/plugins/loaders/pace.min.js', ['bootstrap']],
                    ['blockui','js/plugins/loaders/blockui.min.js', ['bootstrap']],
                    ['app','js/core/app.js',['blockui']],
                    ['components_popups','js/pages/components_popups.js',['bootstrap']],
                    ['moment','../../../assets/components/moment/moment.js',['bootstrap']],
                    ['anytime','js/plugins/pickers/anytime.min.js',['moment']],
                    ['pnotify','js/plugins/notifications/pnotify.min.js',['bootstrap']],
                    ['internationalization_switch_direct','js/pages/internationalization_switch_direct.js',['internationalization']],
                    ['internationalization','js/plugins/internationalization/i18next.min.js',['bootstrap']],
                    ['loginjs','js/pages/login.js',['bootstrap']],
                    ['switchery','js/plugins/forms/styling/switchery.min.js',['bootstrap']],
                     ['html2canvas', 'js/plugins/html2canvas.min.js', ['jquery']],
                     ['sweet_alert', 'js/plugins/notifications/sweet_alert.min.js', ['jquery']],
                     ['d3', 'js/plugins/visualization/d3/d3.min.js', ['blockui']],
                     ['d3_tooltip', 'js/plugins/visualization/d3/d3_tooltip.js', ['d3']],
                     ['datatables', 'js/plugins/tables/datatables/datatables.min.js', ['bootstrap']],
                     ['select2', 'js/plugins/forms/selects/select2.min.js', ['bootstrap']],
                     ['uniform', 'js/plugins/forms/styling/uniform.min.js', ['bootstrap']],
                     ['bootstrap-tagsinput-js', 'js/plugins/forms/tags/bootstrap-tagsinput.js', ['bootstrap']],
                     ['tokenfield', 'js/plugins/forms/tags/tokenfield.min.js', ['tagsinput']],
                     ['prism', 'js/plugins/ui/prism.min.js', ['tagsinput']],
                     ['typeahead', 'js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js', ['bootstrap']],
                     ['handlebars', 'js/plugins/forms/inputs/typeahead/handlebars.min.js', ['typeahead']],
                     ['inbox', 'pages/inbox/js/index.js', ['bootstrap']],
                     ['draft', 'pages/inbox/js/draft.js', ['bootstrap']],
                     ['steps', 'js/plugins/forms/wizards/steps.min.js', ['jquery']],
                     ['cookie', 'js/plugins/extensions/cookie.js', ['steps']]


                ]);


//                {!! Theme::asset()->themePath()->add([ ['materialize-tags-css', 'js/plugins/forms/tags/materialize-tags.min.css',['bootstrap']]]) !!}


                $asset->add([
                    ['custom', 'css/custom.css'],
                    ['custom_fonts', 'css/custom_fonts.css'],
                    ['global_functions', 'assets/globals/global_functions.js',['bootstrap']],
                    ['slimScroll', 'assets/components/slimScroll/jquery.slimscroll.min.js', ['jquery']],
                    ['smoothState', 'assets/components/smoothState/src/jquery.smoothState.js', ['jquery']],
                    ['datatables_responsive', 'assets/components/datatables-responsive/js/dataTables.responsive.js', ['datatables']],
                    ['datatables_fixedColumns', 'assets/components/datatables-fixedColumns/dataTables.fixedColumns.min.js', ['datatables']],
                    ['datatables_fixedColumns_css', 'assets/components/datatables-fixedColumns/fixedColumns.bootstrap.min.css', ['datatables']],
                ]);



                $asset->container('auth')->themePath()->add([
                    ['styles', 'css/icons/icomoon/styles.css'],
                    ['bootstrap', 'css/bootstrap.css'],
//                ['bootstrap', 'css/rtl/bootstrap.css'],
                    ['core', 'css/core.css'],
                    ['components', 'css/components.css'],
//                    ['style', 'css/style-rtl.css',['bootstrap']],
                    ['colors', 'css/colors.css'],
                    ['jquery', 'js/core/libraries/jquery.min.js' ],
                    ['bootstrap', 'js/core/libraries/bootstrap.min.js', ['jquery']],
                    ['pace','js/plugins/loaders/pace.min.js', ['bootstrap']],
                    ['blockui','js/plugins/loaders/blockui.min.js', ['bootstrap']],
                    ['app','js/core/app.js',['blockui']],
                    ['components_popups','js/pages/components_popups.js',['bootstrap']],
                    ['moment','../../../assets/components/moment/moment.js',['bootstrap']],
                    ['anytime','js/plugins/pickers/anytime.min.js',['bootstrap']],
                    ['internationalization_switch_direct','js/pages/internationalization_switch_direct.js',['internationalization']],
                    ['internationalization','js/plugins/internationalization/i18next.min.js',['bootstrap']],
                    ['loginjs','js/pages/login.js',['bootstrap']],


                ]);

                $asset->container('pdf')->themePath()->add([
                    ['jquery', 'js/core/libraries/jquery.min.js' ],
                    ['bootstrap', 'js/core/libraries/bootstrap.min.js', ['jquery']],
                    ['jspdf', 'js/plugins/jspdf.min.js', ['jquery']],
                    ['pdf_client','pages/document/pdf.js',['html2canvas']],
                ]);




                //// builder

                $asset->container('builder')->add([
                    ['grapescss', 'assets/components/grapesjs/dist/css/grapes.min.css'],
                    ['grapesjs', 'assets/components/grapesjs/dist/grapes.js'],
                    ['grapesjs-preset-webpage', 'assets/components/grapesjs/dist/grapesjs-preset-webpage.min.js',['grapesjs']],
                    ['grapesjs-preset-webpage-css', 'assets/components/grapesjs/dist/grapesjs-preset-webpage.min.css'],
                    ['grapesjs-editor', 'http://feather.aviary.com/imaging/v3/editor.js',['grapesjs'] ],
                    ['grapesjs-filestack', 'https://static.filestackapi.com/v3/filestack-0.1.10.js',['grapesjs'] ],

                ]);
                $asset->container('builder-after')->add([
                    ['custom_grapesjs', 'assets/components/grapesjs/custom.js']
                ]);

                $sIdentifiedString = str_replace("@","___",getIdentifier()) ;
                $sIdentifiedString = str_replace(".","---",$sIdentifiedString) ;
                $asset->container('inline')->add([['custom-inline-script', '

<script type="text/javascript">
    
    var globals = {
            baseUrl: "/",
            HOST: "' . config()->get('app')['url'] .'",
            CDN_HOST: "' . config()->get('app')['cdn_url'] .'",
            API_HOST: "' . config()->get('app')['api_url'] .'",
            HOSTSSL: "https://mycompose.local",
            language: "' .  app()->getLocale() .'",
            context: "browser",
            debug: false,
            env: "' . config()->get('app')['env'] .'",
            device: "pc",
            actualDevice: "pc",
            deepLink: "",
            androidPackage: "mycompose",
            androidMarket: "",
            iOSMarket: "",
            sDefaultSearch: "search",
            search_string: "",
            user_ip: "92.241.36.179",
			FBAPPID: "",
            bInAppMessage:"1",
            unique_name : "' . getUniqueName() . '",
            user_id : "' . nUserID() . '",
            sIdentified : "' . getIdentifier() . '",
            sIdentifiedS : "' . $sIdentifiedString . '",
            sLangCode : "' . getLangCode() . '",
            user_roles_ids:["3"]
        };
    var aTemplates ;
    var globals_routes = {
      templates : {getTemplates: "' . route('templates.getTemplates') . '" , index :"index"  },
  
    };

</script>']]) ;

                // You may use elixir to concat styles and scripts.
                /*
                $asset->themePath()->add([
                                            ['styles', 'dist/css/styles.css'],
                                            ['scripts', 'dist/js/scripts.js']
                                         ]);
                */

                // Or you may use this event to set up your assets.
                /*
                $asset->themePath()->add('core', 'core.js');
                $asset->add([
                                ['jquery', 'vendor/jquery/jquery.min.js'],
                                ['jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', ['jquery']]
                            ]);
                */
            },


            'beforeRenderTheme' => function($theme)
            {
                // To render partial composer
                /*
                $theme->partialComposer('header', function($view){
                    $view->with('auth', Auth::user());
                });
                */

            },

            'beforeRenderLayout' => array(

                'mobile' => function($theme)
                {
                    // $theme->asset()->themePath()->add('ipad', 'css/layouts/ipad.css');
                }

            )

        )

    );
