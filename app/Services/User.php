<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use ClientAPI;
class User extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function get($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/me")
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }

    public static function findBy($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/findBy")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public  static function inbox($aParams){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/" . $nUserID ."/" . $aParams['sFilter'])
                ->setBody( $aParams )
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['RecordsTotal'=>'0','inbox'=>[],'error' => $exception->getMessage()],
            ] ;
            dd($exception->getMessage());
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return   $response_array['data'] ;
    }


    public static function sent($aParams){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/" . $nUserID ."/sent")
                ->setBody( $aParams )
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function draft($aParams){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/" . $nUserID ."/draft")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;



            return $response_array['data'] ;
        }


        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }


    public static function templates($aParams){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/".$nUserID."/templateDocuments")
                ->setBody(['account_id'=>$aParams['account_id']])
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;


            return $response_array['data'] ;
        }


        $response_array = json_decode($oClient,true);


        return $response_array['data'] ;
    }

    public  static function pendingAction($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();
        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/". $nUserID ."/pending")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[]],
            ] ;

            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);


        return $response_array['data'] ;
    }

    public static function rejected($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/" . $nUserID . "/rejected")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','rejected'=>[]],
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }


    public static function badges($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserID ."/badges")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function contacts($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserID ."/contacts")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['mycontacts'] ;
    }

    public static function addContact($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/addContact")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }


    public static function editContact($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/editContact")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function getContactsForRecipient($aParams){
        $oServices = Parent::getInstance();
        $id = nUserID();
 try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/" . $id . "/mycontact" )
                ->setQuery($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['contacts' => []]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['contacts'] ;
    }

    public static function saveAvatar($aParams) {
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        try{
            $file = $aParams['file'] ;
            $file->move( storage_path() . '/tmp/'  , $file->getClientOriginalName() ) ;

            $aFile = ['name'     => 'file',
                'contents' => fopen( storage_path() . '/tmp/'  . $file->getClientOriginalName(),'r'),
                'filename' => $file->getClientOriginalName() ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("user/avatar")
                ->withToken()
                ->setMultiPart()
                ->setBody(['file' =>$aFile])
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

    public static function deleteAvatar($file_id) {
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("contact/delete_avatar")
                ->withToken()
                ->setBody(['file_id' =>$file_id, 'contact_id' => oContactID() ])
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

public static function skipRequirement($id) {
        $oServices = Parent::getInstance();
        $nUserId = nUserID();
        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("user/skip_requirement")
                ->withToken()
                ->setBody(['id' =>$id, 'contact_id' => oContactID() ])
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);
        return $response_array['data'] ;
    }


 
public static function getContact($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/getContact")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }


    public static function deleteContact($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/deleteContact")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function groups($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = nUserID();

        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("user/". $nUserID ."/groups")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['my_contact_group'] ;
    }

    public static function getGroup($aParams=[]){
        $oServices = Parent::getInstance();
        $nUserID = currentUserID();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/getGroup")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function addGroup($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/addGroup")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function editGroup($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/editGroup")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function deleteGroup($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('POST')
                ->setUrl("user/deleteGroup")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            $response_array = [
                'data' => ['RecordsTotal'=>'0','error'=>$exception->getMessage()],
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function updatePrimary($aParams=[]){

            $oClient =  ClientAPI::
                setMethod('POST')
                ->setUrl("user/update_primary")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;


        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }


    public static function changePassword($aParams=[]){

        try {
            $oClient = ClientAPI::
            setMethod('POST')
                ->setUrl("user/change_password")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        }catch (\Exception $exception){
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data']['response_code'] ;
    }

    public static function changeUsername($aParams=[]){

        try {
            $oClient = ClientAPI::
            setMethod('POST')
                ->setUrl("user/change_username")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        }catch (\Exception $exception){
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function setReference($aParams=[]){

        try {
            $oClient = ClientAPI::
            setMethod('POST')
                ->setUrl("user/set_reference")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        }catch (\Exception $exception){
            return false;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }

    public static function checkIfHaveSignture(){
            $oClient =  ClientAPI::
                setMethod('POST')
                ->setUrl("user/check_if_have_signture")
                ->withToken()
                ->request()
                ->getContent() ;


        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }

    public static function getAvatars(){
            $oClient =  ClientAPI::
                setMethod('POST')
                ->setUrl("user/getAvatars")
                ->withToken()
                ->request()
                ->getContent() ;
        $response_array = json_decode($oClient,true);

        return (object) $response_array['data'] ;
    }

    public static function setAvatar($nFileID){
            $oClient =  ClientAPI::
                setMethod('POST')
                ->setUrl("user/setAvatar")
                ->setBody(['file_id' => $nFileID ])
                ->withToken()
                ->request()
                ->getContent() ;
        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

    public static function checkAvailableUsername($sUsername) {
        $oClient =  ClientAPI::
        setMethod('POST')
            ->setUrl("user/unique")
            ->setBody(['field_db' => 'username' , 'field_value' => $sUsername ])
            ->withToken()
            ->request()
            ->getContent() ;
        $response_array = json_decode($oClient,true);
        return $response_array['data'] ;
    }

    public static function getLayouts($aParams) {
        $oClient =  ClientAPI::
        setMethod('GET')
            ->setUrl("user/" . nUserID() . '/layouts')
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;
        $response_array = json_decode($oClient,true);
        return $response_array['data']['layouts'] ;
    }

    public static function saveLayout($aParams) {
        $oClient =  ClientAPI::
        setMethod('POST')
            ->setUrl("user/save_layout")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data'];
    }

    public static function deleteLayout($aParams) {

        $oClient =  ClientAPI::
        setMethod('POST')
            ->setUrl("user/delete_layout")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data'];
    }

    public static function getSequences() {

        $oClient =  ClientAPI::
        setMethod('get')
            ->setUrl("user/" . nUserID() ."/sequences")
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data']['oIONumberSequences'];
    }


    public static function saveSequence($aParams) {

        $oClient =  ClientAPI::
        setMethod('post')
            ->setUrl("user/save_sequence")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data']['oIONumberSequences'];
    }


    public static function deleteSequence($aParams) {

        $oClient =  ClientAPI::
        setMethod('post')
            ->setUrl("user/delete_sequence")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data'];
    }

    public static function setLanguage($aParams) {

        $oClient =  ClientAPI::
        setMethod('post')
            ->setUrl("user/set_language")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent() ;

        $response_array = json_decode($oClient,true);
        return $response_array['data'];
    }


}
