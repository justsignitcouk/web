<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Image ;

/**
 * Class TranslatorTranslation
 */
class TranslatorTranslation extends Model
{
    protected $table = 'translator_translations';

    public $timestamps = true;

    protected $fillable = [
        'locale',
        'namespace',
        'group',
        'item',
        'text',
        'unstable',
        'locked'
    ];

    protected $guarded = [];

        
}