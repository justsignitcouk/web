<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleType
 */
class RoleType extends Model
{
    protected $table = 'role_types';

    public $timestamps = true;

    protected $fillable = [
       
    ];

    protected $guarded = [];

        
}