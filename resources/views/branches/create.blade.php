@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        {!! Form::open(['route'=>'branches.store','files'=>'true','enctype'=>'multipart/form-data','method'=>'POST']) !!}

            @include('branches.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
