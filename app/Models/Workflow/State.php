<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'wf_state';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'active'
    ];

    public function StateAction(){
        return $this->hasMany('\App\Models\Workflow\StateAction', 'state_id', 'id');
    }

    public function Stage()
    {
        return $this->belongsTo('App\Models\Workflow\Stage');
    }
}
