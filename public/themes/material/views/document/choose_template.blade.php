<link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/printjs/dist/print.min.css') }}" rel="stylesheet" type="text/css">

{!! Theme::asset()->themePath()->add([ ['view-new', 'pages/document/choose_template.js',['bootstrap']]]) !!}






<!---- global modal ---->

<div id="global_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="global_modal_form">
                    <p>Do you want to delete this template?</p>
                    <input type="hidden" name="template_id" id="template_id" value="" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary save-action" id="delete_template_submit">Yes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!---- global modal ---->

<div class="content-wrapper">

    <!-- Page header -->
    <!-- /page header -->


    <!-- Content area -->
    <div class="content ">

        <div class="panel panel-flat contacts_page">
            <div class="panel-body">
                <div class="pages-container" style=" ">

                   @foreach($oTemplates['templates'] as $oTemplate)

                <div class="col-md-4 text-center" style="margin-bottom: 50px;">
                    <a href="/document/compose/{{ $oTemplate['id'] }}" style="cursor: pointer;display:block;color:inherit;zoom:0.4;-moz-transform: scale(0.4);">
                    @partial('A4.Layout',['aDocument' => $oTemplate,'id'=>$oTemplate['id'] ,'bFirstPage'=>true ] )
                    </a>
                   <span>{{ $oTemplate['title'] !='' ? $oTemplate['title'] : 'No subject' }}</span> <i class="glyphicon glyphicon-trash delete_template" data-id="{{$oTemplate['id']}}" style="cursor:pointer;color:blue;position: absolute;right: 45px;bottom: 0px;top: auto;"></i>
                </div>
                    @endforeach

            </div>
            </div>

        </div>
    </div>

</div>



<script>

</script>
