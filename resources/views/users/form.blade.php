
 <div class="panel panel-flat">
    <div class="panel-heading">
        @if(empty($user['id']))
            <h5 class="panel-title">{{ __("users.titles.add_user") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
	        <div class="heading-elements">
	            <ul class="icons-list">
	                <li><a data-action="collapse"></a></li>
	                <li><a data-action="reload"></a></li>    
	            </ul>
	        </div>
        @endif
    </div>

    <div class="panel-body">
        <div class="row"> 
	        <div class="col-md-6">

                {{ csrf_field() }}

                <meta name="csrf-token" content="{{ csrf_token() }}">
                @if(!empty($user['id']))
                <input type="hidden" name="check">
                @endif
		        <div class="form-group">
		            <label>{{ __("users.username") }} <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :</label>
		            <input type="text"
		                   name="username"
		                   class="form-control"
		                   placeholder='{{ __("users.username") }}'
		                   @if(!empty($user['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('username') }}"
			                    @else
			                    	value="{{ $user['username'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('username') }}"
		                   @endif >
		            
		            @if ($errors->has('username'))
		                <font color="red">{{ $errors->first('username') }}</font>
		        	@endif

		        </div>

		        <div class="form-group">
		            <label>{{ __("users.first_name") }} <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :</label>

		            <input type="text"
		                   name="first_name"
		                   class="form-control"
		                   placeholder='{{ __("users.first_name") }}'
		                   @if(!empty($user['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('first_name') }}"
			                    @else
			                    	value="{{ $user['first_name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('first_name') }}"
		                   @endif >
		            
		            @if ($errors->has('first_name'))
		                <font color="red">{{ $errors->first('first_name') }}</font>
		        	@endif
		        </div>

		        <div class="form-group">
		            
		            <label>{{ __("users.password") }} :
			             @if(empty($user['id']))
			             <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> 
			             @endif
		            </label>
		            
		            <input type="password" 
		                   name="password"
		                   class="form-control"
		                   placeholder='{{ __("users.password") }}'/>

		            @if ($errors->has('password'))
		                <font color="red">{{ $errors->first('password') }}</font>
		            @endif
		        </div>

		        <div class="form-group">

					<label class =""><i>
					{{ __("users.pass_caution") }}</i>
					</label>

		        </div>

      
		    </div>
		    <div class="col-md-6">
	         
	            <div class="form-group">
		            <label>{{ __("users.email") }} <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i> :</label>
		            <input type="email" 
		                   name="email"
		                   class="form-control"
		                   placeholder='{{ __("users.email") }}'
		                   @if(!empty($user['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('email') }}"
			                    @else
			                    	value="{{ $user['email'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('email') }}"
		                   @endif >
		           
		            @if ($errors->has('email'))
		                <font color="red">{{ $errors->first('email') }}</font>
		        	@endif
		        </div>
		        
		        <div class="form-group">
		            <label>{{ __("users.last_name") }} <i class="fa  fa-asterisk text-danger" style="font-size:8px"></i>:</label>
		            <input type="text"
		                   name="last_name"
		                   class="form-control"
		                   placeholder='{{ __("users.last_name") }}'
		                   @if(!empty($user['id']))
			                    @if(count($errors) > 0)
			                        value="{{ old('last_name') }}"
			                    @else
			                    	value="{{ $user['last_name'] }}"
			                    @endif  
		                   @else
		                    value="{{ old('last_name') }}"
		                   @endif >

		            @if ($errors->has('last_name'))
		                <font color="red">{{ $errors->first('last_name') }}</font>
		        	@endif
		        </div>
		         
		        <div class="form-group">
		            <label>{{ __("users.password_confirmation") }} :
			             @if(empty($user['id']))
			             <i class="fa fa-asterisk text-danger" style="font-size:8px"></i> 
			             @endif
		            </label>

		            <input type="password"
		                   name="password_confirmation"
		                   class="form-control"
		                   placeholder='{{ __("users.password_confirmation") }}' ">
		            
		            @if ($errors->has('password_confirmation'))
		                <font color="red">{{ $errors->first('password_confirmation') }}</font>
		        	@endif
		        </div>
		        
	            <div class="form-group">
		            <label>{{ __("users.profile_image") }} :
			             @if(empty($user['id']))
			             
			             @endif
		            </label>

                    <div>
	                    <input type="file" name="profile_image" class="pull-left">
	                    
	                    @if(!empty($user['id']))
							<img src="{{cdn_url($sImageProfileName,$sImageProfilePath)}}" alt="" style="max-width: 115px;max-height: 90px;">
	                    @endif

                    </div>

					@if ($errors->has('profile_image'))
		                <font color="red">{{ $errors->first('profile_image') }}</font>
		        	@endif

		        	
		        </div>

		    </div>
	    </div>
	    <div class="row col-md-12">

            {{ Form::submit(__("common.save") , array('class' => 'btn btn-success pull-right submit_button','style'=>'margin-left:1%')) }}


		        @if(!empty($user["id"]))
		           @if($user["activated"] == 1)
		            	<div id="user-status" class="btn btn-danger pull-right"   data-toggle="modal" data-status="0" data-target="#modal_theme_danger">{{__("common.deactive")}}
		            	</div>
		           @elseif($user["activated"] == 0)
		          	 	<div id="user-status" class="delete-user btn btn-success pull-right" data-status="1" data-toggle="modal" >{{__("common.active")}}
		            	</div>
		           @endif
		           
	            @endif


	    </div>    
    </div>
</div>

<!-- delete modal -->
@if(!empty($user["id"]))
<div id="modal_theme_danger" class="modal fade" style="display: none;">
 <input id='user-data' type="hidden" data-id='{{$user["id"]}}' data-token="{{ csrf_token() }}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h6 class="modal-title">
				{{__("users.deactive")}}
				</h6>
			</div>

			<div class="modal-body">

				<h6 class="text-semibold">
					{{__("common.caution")}}
				</h6>
			
               
                @if($user["id"] == 1)
                    <p>
					 	{{__("common.caution_admin")}}
				    </p>
                @else
                	<p>
					 	{{__("users.deactive_user")}}
				    </p>
                @endif
				
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">
				{{__("common.cancel")}}
				</button>
				@if($user["id"] != 1)
					<button type="button" class="delete-user btn btn-danger" >
					{{__("common.deactive")}}
					</button>
				@endif
			</div>
		</div>
	</div>
  </div>
</div>
@endif


<script type="text/javascript">


    $('.delete-user').click(function() {
        var id = $('#user-data').data("id");
        var status = $('#user-status').data("status");
        var _token = $('input[name="_token"]').val();

       	$.ajax({
 
	        method: 'POST',
            url: "{!! url('users' ) !!}" + "/" + id + "/"+ status +'/deactive',
            data: { _token : _token },
	        success: function( msg ) {

                location.replace("/users");

	        },
	        error: function( data ) {

	        }
	    });
     });

    


</script>
