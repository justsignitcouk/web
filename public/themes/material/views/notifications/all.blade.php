{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['form_select2', 'js/pages/form_select2.js',['select2']]]) !!}
{!! Theme::asset()->add([ ['tinymce', 'assets/components/tinymce/js/tinymce/tinymce.min.js',['form_select2']]]) !!}
{!! Theme::asset()->themePath()->add([ ['interactions', 'js/core/libraries/jquery_ui/interactions.min.js',['tinymce']]]) !!}
{{--
{!! Theme::asset()->themePath()->add([ ['mail_list_write', 'js/pages/mail_list_write.js',['interactions']]]) !!}
--}}
{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}

{!! Theme::asset()->themePath()->add([ ['compose', 'pages/document/compose.js',['bootstrap']]]) !!}
<style>
    .img-thumbnail{
        max-height:90px;
    }
</style>
{{--jquery.tinymce.min.js--}}

<link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">




@partial('document.compose.a4',[])

@partial('compose_secondary_sidebar',[])

<script>
    var globals_route;
    var globals_routeSendDocument = '{{ route('document.sendDoc') }}';
    var globals_routeSendDraft = '{{ route('document.sendDraft') }}';
    var globals_route_getLayout = '{{ route('document.getLayout',['id' => 1]) }}';
</script>