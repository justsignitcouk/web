<?php

namespace App\Http\Controllers\Individual;

use App\Services\User;
use App\Services\Account;
use App\Services\Contact;
use Facuz\Theme\Facades\Theme;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class InboxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $data = [];

    public function __construct()
    {
        //$this->middleware('\App\Http\Middleware\auth');
        parent::__construct();


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share('bShowMainSidebar', false);
        view()->share('sActiveMenu', 'inbox');
        return Theme::view('inbox.index', $this->data);
    }

    public function getInbox(Request $request)
    {
        $filter = '';
        $data = $request->all();
        $data['sFilter'] = 'inbox';
        if (request()->has('filter')) {
            $filter = request()->get('filter');
            $data['sFilter'] = $filter;
        }

        switch ($filter) {
            case 'pending' :
                $oInbox = User::inbox( $data );
                $data = $oInbox['document'];
                break;
            case 'completed' :
                $oInbox = User::inbox( $data );
                $data = $oInbox['document'];
                break;
            case 'sent' :
                $oSent = User::inbox( $data );
                $data = $oSent['document'];
                break;
            case 'draft' :
                $oDraft = User::inbox( $data );
                $data = $oDraft['document'];
                break;
            case 'rejected' :
                $oDraft = User::inbox( $data );
                $data = $oDraft['document'];
                break;
            default :
                $oInbox = User::inbox( $data );
                $data = $oInbox['document'];
                break;
        }

        if(is_null($data)){
            $data = [];
        }
        if(sizeof($data)>0){
            usort($data, function ($a, $b) {
                return $b['id'] <=> $a['id'];
            });
        }

        return DataTables::of($data)->make(true);


    }


    public function pending()
    {
        $oPending = Account::pendingAction();
        view()->share('bShowMainSidebar', false);
        return view('home.pending')->with(['oPending' => $oPending, 'sActiveMenu' => 'pending']);
    }

    public function getPendingAction()
    {
        $oPending = Account::pendingAction();
        return \GuzzleHttp\json_encode($oPending);
    }


    public function sent()
    {

        $oSent = Account::sent(['account_id' => defaultAccountID()]);
        view()->share('bShowMainSidebar', false);
        return view('home.sent')->with(['oSent' => $oSent, 'sActiveMenu' => 'sent']);
    }

    public function getSent()
    {
        $oSent = Account::sent(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oSent);
    }


    public function compose()
    {
        $client = new \App\Services\Account();
        $response = $client->all();
        $aUsers = [];
        if ($response['response_code'])
            $aUsers = $response['to_whom_addresses'];

        view()->share('bShowMainSidebar', false);
        return view('home.compose', ['aUsers' => $aUsers]);
    }


    public function incomplete()
    {
        $user = User::find(1);
        return view('home.index');
    }


    public function approved()
    {

        $user = User::find(1);
        return view('home.index');
    }


    public function rejected()
    {

        $rejected = Account::rejected();
        view()->share('bShowMainSidebar', false);
        return view('home.rejected')->with(['rejected' => $rejected, 'sActiveMenu' => 'rejected']);
    }

    public function getRejected()
    {
        $rejected = Account::rejected();
        return \GuzzleHttp\json_encode($rejected);
    }


    public function archived()
    {
        $user = User::find(1);
        return view('home.index');
    }


    public function draft()
    {
        $oDraft = Account::draft(['account_id' => defaultAccountID()]);
        view()->share('bShowMainSidebar', false);
        return view('home.draft')->with(['oDraft' => $oDraft, 'sActiveMenu' => 'draft']);
    }

    public function getDraft()
    {
        $oDraft = Account::draft(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oDraft);
    }

    public function templates()
    {

        view()->share('bShowMainSidebar', false);
        return view('home.templates')->with(['sActiveMenu' => 'templates']);
    }

    public function getTemplates()
    {
        $oDraft = Account::templates(['account_id' => defaultAccountID()]);
        return \GuzzleHttp\json_encode($oDraft);
    }

    public function anyData()
    {
        return Datatables::of(User::query())->make(true);
    }
}
