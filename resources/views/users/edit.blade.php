@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

				<ul class="breadcrumb">
					<li><a href="{{route('admin.users')}}"><i class="icon-home2 position-left"></i>{{ __("common.users") }}</a></li>
					<li><a href="{{route('admin.users.profile' , $user['id'])}}"><i class="position-left"></i>{{ $user['username'] }}</a></li>
					<li class="active">{{ __("common.edit") }}</li>
				</ul>
				
				@include('layouts/partials/tabs')

				<div class="tab-content">
					{!! Form::open(['route' => ['admin.users.update', $user['id']],'method'=>'POST','files'=>'true']) !!}

							@include('users.form')

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

</div>

@endsection