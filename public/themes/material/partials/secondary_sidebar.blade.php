{{--@if($displayWorkflow)--}}
{{--{{ flowchart degram }}--}}
{!! Theme::asset()->add([ ['raphael', 'assets/components/flowchart_diagram/dist/raphael.min.js',['jquery']]]) !!}
{!! Theme::asset()->add([ ['flowchart', 'assets/components/flowchart_diagram/dist/flowchart.min.js',['raphael']]]) !!}
{!! Theme::asset()->add([ ['flowchart_custom', 'assets/components/flowchart_diagram/dist/jquery.flowchart.min.js',['flowchart']]]) !!}
{{--{{ flowchart degram }}--}}
{!! Theme::asset()->container('inline')->add([['custom-workflow', '
<script type="text/javascript">
    var workflow_findByCompany = "' .   route('workflow.findByCompany')  .'" ;
    var route_get_stage_details = "' . route('workflow.getStageDetails') . '" ;
</script>']])  !!}
{{--@endif--}}
<div class="sidebar sidebar-secondary sidebar-default">
    <div class="sidebar-content">

        <!-- Sidebar tabs -->
        <div class="tabbable sortable ui-sortable">
            <ul class="nav nav-lg nav-tabs nav-justified">

            </ul>

            @partial('workflow.view')
            @if($displayWorkflow)
            <div class="tab-content">
                <div class="tab-pane active no-padding" id="components-tab">
                    @partial('workflow.actions',['id' => $id])
                    <div class="sidebar-category">
                        <div class="category-content">
                            <div class="row">
                                <div class="col-xs-12 workflow_action">

                                    @if($displayWorkflow)
                                    @partial('workflow.view')
                                    @endif
                                </div>
                                <div class="col-xs-12 other_action">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                @endif
        </div>
    </div>
</div>


