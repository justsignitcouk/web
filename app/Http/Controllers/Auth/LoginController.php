<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\NeedHelpRequest;
use App\Services\Auth;
use Facuz\Theme\Facades\Theme;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session ;




class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/inbox';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','showNeedHelp');
        $this->redirectTo = config('auth.redirectTo') ;
    }


    public function showLoginForm(Request $request){


        Theme::layout('auth');
        return Theme::view('auth.login');

    }


    public function validateEmailOrPhone(Request $request){

        if($request->has('identifier')){
            $sIdentifier = $request->get('identifier');
        }

        if($request->has('email')){
            $sIdentifier = $request->get('email');
        }

        if($request->has('mobile')){
            $sIdentifier = $request->get('mobile');
        }

        $oUser = Auth::findIdByIdentifier($sIdentifier);
        if($request->has('email') || $request->has('mobile')) {
            if ($oUser['status'] == 'success') {
                return "false";
            } else {
                return "true";
            }
        }
        return \GuzzleHttp\json_encode($oUser);

    }


    public function login(LoginRequest $request){

        $response = Auth::authenticate($request);
        return \GuzzleHttp\json_encode($response);

    }

    public function logout(){
        session()->forget("sToken") ;
        session()->flush();
        return redirect("/");
    }


    public function saveUserData(Request $request){

        $data = $request->get('userData') ;
        $oResponse = Auth::saveData($request,['data'=>$data]) ;

        return \GuzzleHttp\json_encode(['status'=>serialize($oResponse)]) ;

    }

    public function showNeedHelp(){

        Theme::layout('auth');
        return Theme::view('auth.needhelp');

    }

    public function storeNeedHelp(NeedHelpRequest $request){

        $client   = new \App\Services\Auth();
        $response = $client->needHelp($request->toArray());

        if($response['data']['status']){
            $status = true ;
        }else{
            $status = false ;
        }

        session()->put("status", $status);

        return redirect('needhelp') ;


    }

    public function remoteValidation(){

    }




}
