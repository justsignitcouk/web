<?php

namespace App\Http\Controllers\Individual;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class LogsController extends Controller
{
    public function index()
    {
        $contents = Storage::get('logs/laravel.log');


        return view('logs.index')->with('content',$contents);
    }


}
