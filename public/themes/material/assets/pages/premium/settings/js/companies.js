$(document).ready(function() {


    var columns = [

        { data: 'name', name: 'name',className: "table-inbox-name",responsivePriority: 1, render: function(data, type, full, meta) {
        return '<a href="/settings/companies/' + full.slug +'" >' + data + '</a>'
    } },
        { data: 'slug', name: 'slug',className: "table-inbox-name", responsivePriority: 1},
        { data: 'license_no', name: 'license_no',className: "table-inbox-name", responsivePriority: 1},
        { data: 'created_at', name: 'created_at',className:"table-inbox-name","render": function ( data, type, row, meta ) {
                return  moment(data).fromNow();
                alert(data);
            } },

        { data: 'id', name: 'id',className:"text-center details-button","render": function ( data, type, row, meta ) {
                var html = '<ul class="icons-list">' +
                    '<li class="dropdown">' +
                    '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-menu9"></i>' +
                    '</a>' +
                    '<ul class="dropdown-menu dropdown-menu-right">' +
                    '<li><a href="#"><i class="icon-file-pdf"></i><p data-i18n="Export_to_pdf">Export to .pdf</p> </a></li>' +
                    '<li><a href="#"><i class="icon-file-excel"></i> <p data-i18n="Export_to_csv">Export to .csv</p></a></li>' +
                    '<li><a href="#"><i class="icon-file-word"></i> <p data-i18n="Export_to_doc">Export to .doc</p></a></li>' +
                    '</ul>' +
                    '</li>' +
                    '</ul>' ;
                return  html;
            } },


    ] ;


    $('#companies').DataTable( {
        "processing": true,
        "columns": columns,
        "serverSide": true,
        "ajax": {
            "url": "/settings/companies",
            "type": "POST"
        }
    } );



        });