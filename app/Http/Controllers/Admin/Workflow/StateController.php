<?php

namespace App\Http\Controllers\Admin\Workflow;

use App\Models\Account;
use App\Models\Role;
use App\Models\User;
use App\Models\Workflow\Actions;
use App\Models\Workflow\StateAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workflow\State;
use Validator;
use Yajra\Datatables\Datatables;
use Gate ;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workflow.state.index');
    }

    public function datatables(){


            $oStates = State::all();

            return \Yajra\DataTables\Facades\DataTables::of($oStates->toArray())->make(true);


    }

    public function detailsDtatables(Request $request,$id){


            $oStates = StateAction::with('Action','State')->where('state_id',$id)->get() ;
            return \Yajra\DataTables\Facades\DataTables::of($oStates->toArray())->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'state_name'  => "required|string",
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ]);
        }


        $data = $request->all();

        $oState = new State();
        //$oState->stage_id = $data['stage_id'];
        $oState->name = $data['state_name'];
        $oState->description = $data['description'];
        $oState->active = $data['active'];
        if ($oState->save()){
            return response()->json(['status' => true, 'message' => 'Added Successfully']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oState = State::find($id);
        if(!$oState){
            return redirect()->to( route('admin.state.index'));
        }
        $oAction = Actions::pluck('name','id')->all() ;
        $oAccounts = User::where(['activated' => 1])->get();
        $oRoles = Role::where(['company_id' => 1, 'active' => 1])->get();
        return view('workflow.state.details')->with(['id'=>$id,'oAction'=>$oAction,'oAccounts'=>$oAccounts,'oRoles'=>$oRoles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0)
    {
        if ($id == 0){
            return response()->json(['status'=> false, 'message' => 'Invalid Deletion.']);
        }

        $oState = State::findOrFail($id);
        if($oState){
            if ($oState->delete()){
                return response()->json(['status'=> true, 'message' => 'Deleted Successfully.']);
            }else{
                return response()->json(['status'=> false, 'message' => 'Something wrong!']);
            }
        }
    }
}
