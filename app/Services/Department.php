<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/19/17
 * Time: 3:24 PM
 */

namespace App\Services;

use GuzzleHttp\Client;

class Department extends \App\Services\Services
{

    public function all(){
        $client = new Client();
        $api_url = config()->get('app')['api_url'];
        $sToken = getToken() ;
        $company_id = currentCompanyID();
        $users = $client->request('POST' ,$api_url.'/departments', [
            'headers'         => ['Authorization' => 'Bearer ' . $sToken],
            'form_params' => [
                'company_id' => $company_id
            ]
        ]);

        $response = $users->getbody()->getContents();
        $response_array = json_decode($response,true);
        return $response_array['data'] ;
    }

}