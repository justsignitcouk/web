<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentTowhom
 */
class DocumentTowhom extends Model
{
    protected $table = 'document_towhom';

    protected $primaryKey = 'document_id';

	public $timestamps = false;

    protected $fillable = [
        'type',
        'towhom_id',
        'description',
        'to_cc'
    ];

    protected $guarded = [];

        
}