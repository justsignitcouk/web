<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 */
class Setting extends Model
{
    protected $table = 'settings';

    public $timestamps = false;

    protected $fillable = [
        'key',
        'title',
        'name',
        'description',
        'value',
        'field',
        'active'
    ];

    protected $guarded = [];

        
}