<?php

namespace App\Http\Controllers\Individual;
use App\Services\Auth;
use App\Services\User;
use App\Services\Editable;
use App\Services\Signatures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Services\Account;
use Facuz\Theme\Facades\Theme;
use App\Services\Notifications;
use App ;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('bShowMainSidebar', false);
    }

    //
    public function index()
    {

        $aNotifications = Notifications::getUserNotifications(App::getLocale());
        $aActivities    = UserActivitiesController::userActivitiesAjax();
        $aSequences = User::getSequences();
        view()->share('sSideBar',  'sidebar-normal');
        return Theme::view('profile.index',['aNotifications'=>[] ,'aSequences' => $aSequences,'aActivities'=>$aActivities]);
    }

    // post ajax to get accounts and linked account
    public function allAccounts(Request $request){
        $oAccounts = Account::get();
        return \GuzzleHttp\json_encode($oAccounts) ;

    }

    public function getCurrentUser(){
        $oUser = currentAccount() ;
        return \GuzzleHttp\json_encode($oUser) ;

    }


    public function updateCurrentAccount(Request $request){
        $account_id = $request->get('account_id') ;
        $oAccounts = Account::updateCurrentAccount($account_id);
        return \GuzzleHttp\json_encode(['status'=>'success']) ;
    }

    public function saveSign(Request $request)
    {
        $oSign = Signatures::save($request->all());
        return \GuzzleHttp\json_encode($oSign);

    }

    public function getSignatures(Request $request)
    {
        $oSign = Signatures::get();
        return \GuzzleHttp\json_encode($oSign);
    }


    public function getDefaultSignature(Request $request)
    {
        $oSign = Signatures::getDefault();
        return \GuzzleHttp\json_encode($oSign);
    }

    public function uploadSign(Request $request)
    {

        return \Plupload::file('file', function ($file) use ($request) {
            $sign_type = $request->get('sign_type');
            // Store the uploaded file
            $file->move(storage_path() . '/tmp/', $file->getClientOriginalName());
            $path = storage_path() . '/tmp/' . $file->getClientOriginalName();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            Storage::delete('/tmp/' . $file->getClientOriginalName());
            $oSign = Signatures::save(['sign_data' => $base64, 'sign_type' => $sign_type]);


            // This will be included in JSON response result
            return [
                'success' => true,
                'message' => 'Upload successful.',
                'id' => $file->getClientOriginalName(),
                'sign_data' => $base64,
                'oSign' => $oSign,
                // 'url'       => $photo->getImageUrl($filename, 'medium'),
                // 'deleteUrl' => action('MediaController@deleteDelete', [$photo->id])
            ];
        });
    }

    public function deleteSign(Request $request)
    {
        $nSignID = $request->get('sign_id');
        $oSign = Signatures::delete($nSignID);
        return \GuzzleHttp\json_encode($oSign);
    }

    public function updateEditable(Request $request){
        $name = $request->get('name') ;
        $value = $request->get('value') ;

        $oResponse = Editable::save(['name'=>$name,'value'=>$value]) ;
        if($oResponse){
            return \GuzzleHttp\json_encode(['status'=>0]) ;
        }
        return \GuzzleHttp\json_encode(['status'=>1]) ;

    }

    public function link_account(Request $request){
        $data = $request->all() ;

        $client = new \App\Services\Auth();
        $response = $client->link_account($request);
        if(isset($response['status']) && $response['status']=='true'){

            return ['status' => 'success' , 'redirect_to' => route("home.home")] ;

        }

        return $response;
    }

    public function getDevices(Request $request){
        $data = $request->all() ;

        $client = new \App\Services\Auth();
        $response = $client->getDevices($request);


        return $response;
    }

    public function deactivate(Request $request){
        $response = Auth::deactivate($data);
        return $response;
    }

    public function getContactForRecipient(Request $request){
        $aEmail = [] ;
        $aContacts = App\Services\User::getContactsForRecipient(['q'=>$request->get('q')]);

        return \GuzzleHttp\json_encode(  $aContacts  ) ;
    }


    public function checkIfHaveSignture(){
        $aCheck = App\Services\User::checkIfHaveSignture();
        return \GuzzleHttp\json_encode(  $aCheck  ) ;
    }

    public function viewProfileRequirements(){
        if(sizeof(aProfileRequirements())==0){
           return redirect(route('inbox.all'));
        }

        Theme::layout('auth');
        $aProfileRequirements = aProfileRequirements();
        $aProfileRequirement = reset($aProfileRequirements);

        if($aProfileRequirement['id']=='2'){

            Theme::layout('auth');
            return Theme::view('auth.verify_mobile',['aProfileRequirement' => $aProfileRequirement]);
        }
        return Theme::view('profile.requirements',['aProfileRequirement' => $aProfileRequirement]);

    }

    public function skipProfileRequirements(Request $request){
        $id = $request->get('requirment_id');
        $oSkip = App\Services\User::skipRequirement($id);
        return \GuzzleHttp\json_encode($oSkip) ;
    }


    public function getAvatars(Request $request){
        $oAvatars = App\Services\User::getAvatars();
        return \GuzzleHttp\json_encode($oAvatars) ;
    }

    public function setAvatar(Request $request){
        if($request->has('file_id')){
            $oAvatars = App\Services\User::setAvatar($request->get('file_id'));
            return \GuzzleHttp\json_encode($oAvatars) ;
        }
        return \GuzzleHttp\json_encode([]) ;
    }

    public function uploadProfilePicture($oSlug,Request $request){

        return \Plupload::file('file', function ($file) use ($request) {

            $oAvatar = App\Services\User::saveAvatar(['file'=>$file]);


            // This will be included in JSON response result
            return [
                'success' => true,
                'message' => 'Upload successful.',
                'id' => $file->getClientOriginalName(),
                'profile_image' => '',
                'oAvatar' => $oAvatar,
                // 'url'       => $photo->getImageUrl($filename, 'medium'),
                // 'deleteUrl' => action('MediaController@deleteDelete', [$photo->id])
            ];
        });
    }

    public function deleteProfilePicture($oSlug,Request $request){
        $file_id = $request->get('file_id');
        $oAvatar = App\Services\User::deleteAvatar($file_id);
    }

    public function updatePrimary(Request $request){
        $oResponse = User::updatePrimary($request->all());
        return \GuzzleHttp\json_encode($oResponse);
    }

    public function changePassword(Request $request){
        $oResponse = User::changePassword($request->all());
        if($oResponse==1){
            session()->flash("change_successfully","change_successfully");
        }else{
            session()->flash("change_failed","change_failed");
        }

        return redirect(route('profile.index',[getUniqueName()])) ;
        //return \GuzzleHttp\json_encode($oResponse);
    }

    public function changeUsername(Request $request){
        $sUsername = $request->get('username') ;
        if($sUsername==getUniqueName()){
            return \GuzzleHttp\json_encode(['response_code' => 1, 'response_message' => "success"]);
        }
        $oResponse = User::changeUsername($request->all());


        //return redirect(route('profile.index',[getUniqueName()])) ;
        return \GuzzleHttp\json_encode($oResponse);
    }

    public function setReference(Request $request){
        $sUsername = $request->get('reference_input') ;

        $oResponse = User::setReference($request->all());

        return \GuzzleHttp\json_encode($oResponse);
    }

    public function checkAvailable(Request $request){


        $sUsername = $request->get('username');
        if($sUsername==getUniqueName()){
            return "true";
        }
        $oResponse = User::checkAvailableUsername($sUsername);

        if($oResponse['status']== true){
            return "false";
        }
        return "true";
    }


    public function getLayouts(Request $request){
        $data = $request->all();
        $oLayouts = User::getLayouts($data) ;

        return DataTables::of($oLayouts)->make(true);

    }

    public function saveLayout(Request $request){
        $data = $request->all();
        $oLayout = User::saveLayout($data) ;
        return \GuzzleHttp\json_encode($oLayout);

    }

    public function deleteLayout(Request $request){
        $data = $request->all();
        $oLayout = User::deleteLayout($data) ;
        return \GuzzleHttp\json_encode($oLayout);

    }

    public function saveSequence(Request $request){
        $data = $request->all();
        $oSequence = User::saveSequence($data) ;
        return \GuzzleHttp\json_encode($oSequence);

    }

    public function deleteSequence(Request $request){
        $data = $request->all();
        $oSequence = User::deleteSequence($data) ;
        return \GuzzleHttp\json_encode($oSequence);

    }

    public function setLanguage(Request $request){
        $data = $request->all();
        $oSequence = User::setLanguage($data) ;
        return \GuzzleHttp\json_encode($oSequence);

    }

}
