$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
    });

$.fn.load_inbox_page = function() {



    $('#loading').on('click', function() {
        var light_3 = $(this).parent();
        $(light_3).block({
            message: '<i class="icon-spinner3 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        window.setTimeout(function () {
            $(light_3).unblock();
        }, 3000);
    });

    var currentTime = new Date();
    var current_year = currentTime.getFullYear();
    var columns = [
        { data: 'id', name: 'id',className: "table-inbox-select-box details-button table-inbox-checkbox  rowlink-skip sel_row","render": function ( data, type, row, meta ) {
            return '<label class="checkbox-inline ">'+
                '<input type="checkbox" class="styled  ' +data+ '" >'+
            '</label>'


        } },
        // { data: 'sender.avatar.file.hash_name', name: 'sender.avatar.file.hash_name',className: "table-inbox-image","render": function ( data, type, row, meta ) {
        //     var img = '' ;
        //     if(row.sender.avatar !== null){
        //         img = row.sender.avatar.url ;
        //         if(row.sender.avatar.path !== null ) {
        //             img += '/files/' + row.sender.avatar.path + '/' + data;
        //         }
        //     }
        //     return '<img src="'+  img +'" class="img-circle img-xs" />';
        // } },
        { data: 'outbox_number', name: 'outbox_number',className: "table-inbox-checkbox",responsivePriority: 2 },
        { data: 'sender.full_name', name: 'sender.full_name',className: "table-inbox-from",responsivePriority: 2 },
        { data: 'title', name: 'subject',className: "table-inbox-name", responsivePriority: 1},
        { data: 'text_content', name: 'text_content',className: "table-inbox-message" },
        { data: 'id', name: 'id',className: "table-inbox-select-box details-button table-inbox-checkbox","render": function ( data, type, row, meta ) {
            if(row.has_attachments==true){
                return '<span class="attachment_button" data-id="' + data + '"><i class="icon-attachment"></i></span>';
            }
                return '';
            }
        },
        { data: 'created_at', name: 'created_at',className:"time-column","render": function ( data, type, row, meta ) {
            var moment1 = moment(data).fromNow() ;
            var moment1_split = moment1.split(' ');

                var date1 = data.split(' ');
                var date_s = date1[0].split('-');
                var time = date1[1].split(':') ;


            var html = '<span class="time_float_container" tooltip="' + data + '">' ;



            if(moment1_split[0] <24 && moment1_split['1']=='hours' ) {
                html+= moment(data).fromNow();
            }else if(current_year>date_s[0]){
                html+=  moment(data).format('MMM D, YYYY HH:mm') ;

            }else{
                html+=  moment(data).format('MMM D,   HH:mm') ;

            }
            html+='</span>' ;
            html+= '<div class="details_float_container"><div class="details_float">'+
                '<a class="no-smoothState link-icon details-document details_button tooltip-tools" data-toggle="modal" data-target="#details_table" data-id="' + row.id + '" data-popup="tooltip" title="Details" data-placement="bottom" data-original-title="Bottom tooltip">' +
                '<i   class="icon-details icon-info3" data-id="' + row.id + '"></i></a>  ' +
                '</div></div>';
            return html;//return  moment(data).fromNow();
        } }

    ] ;

    var draft_columns = [
        { data: 'id', name: 'id',className: "table-inbox-select-box details-button table-inbox-checkbox  rowlink-skip sel_row","render": function ( data, type, row, meta ) {
                return '<label class="checkbox-inline ">'+
                    '<input type="checkbox" class="checkbox_document ' +data+ '" data-id="' +data+ '">'+
                    '</label>'


            } },
        { data: 'recipients', name: 'recipients',className: "table-inbox-recipients",responsivePriority: 2 },
        { data: 'title', name: 'subject',className: "table-inbox-name", responsivePriority: 1},
        { data: 'text_content', name: 'text_content',className: "table-inbox-message" },
        { data: 'id', name: 'id',className: "table-inbox-select-box details-button table-inbox-checkbox","render": function ( data, type, row, meta ) {
            return '<span class="attachment_button" data-id="' + data + '"><i class="icon-attachment"></i></span>';
        }
            },
        { data: 'created_at', name: 'created_at',className:"time-column table-inbox-name","render": function ( data, type, row, meta ) {
                var moment1 = moment(data).fromNow() ;
                var moment1_split = moment1.split(' ');

                var date1 = data.split(' ');
                var date_s = date1[0].split('-');
                var time = date1[1].split(':') ;


                var html = '<span class="time_float_container" tooltip="' + data + '">' ;



                if(moment1_split[0] <24 && moment1_split['1']=='hours' ) {
                    html+= moment(data).fromNow();
                }else if(current_year>date_s[0]){
                    html+=  moment(data).format('MMM D, YYYY HH:mm') ;

                }else{
                    html+=  moment(data).format('MMM D,   HH:mm') ;

                }
                html+='</span>' ;
                html+= '<div class="details_float_container"><div class="details_float">'+
                    '<a class="no-smoothState link-icon details-document details_button tooltip-tools" data-toggle="modal" data-target="#details_table" data-id="' + row.id + '" data-popup="tooltip" title="Details" data-placement="bottom" data-original-title="Bottom tooltip">' +
                    '<i   class="icon-details icon-info3" data-id="' + row.id + '"></i></a>  ' +
                    '</div></div>';
                return html;//return  moment(data).fromNow();
            } }
    ] ;

$(document).on('load','.table-inbox',function(){
    alert('hi');
});
    $(document).on('click','.attachment_button',function(){
        var id = $(this).data('id');
        $('.details_modal_body').html('');
        $.post('/document/attachments',{'id':id},function(data){
            var html = '<div class="attachment_container"><ul>' ;
            $.each(data,function(k,v){
                console.log(v);
                html += '<li><a href="' + v.url + '/files/' + v.path + '/' + v.hash_name + '" target="blank">'+ v.name +'</a></li>' ;
            }) ;
            html += '</ul></div>';
            $('.details_modal_body').html(html);
            $('#details_table').find('.modal-title').html('Document attachment');
            $('#details_table').modal('show');
        },'json');

    });


    //test checkall button
    $(document).on('click',".all_select",function(){

        if($(this).is(':checked')){
            $('#select_all_documents_in_draft').show();
            $(".checkbox_document").prop("checked", true);

        }else{
            $('#select_all_documents_in_draft').hide();
            $(".checkbox_document").prop("checked", false);

        }

    });

    var select_all = 0 ;
    $(document).on('click','.select-all-rows',function(){
        $(this).addClass('unselect-all-rows').removeClass('select-all-rows') ;
        $('.select_message').html('<span class="text-semibold">All documents in Primary are selected.</span> ');
        $(this).html('Clear selection');
        select_all = 1;
        $('#draft_table').dataTable().fnDraw();
    });

    $(document).on('click','.unselect-all-rows',function(){
        $(this).addClass('select-all-rows').removeClass('unselect-all-rows') ;
        $('.select_message').html('<span class="text-semibold">All documents on this page are selected.</span>' +
            '                                    Select all documents in Draft ');
        $(this).html('here') ;
        select_all =0 ;
        $('#draft_table').dataTable().fnDraw();
    });



    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });


    $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [ 4 ],
            checkboxes: {
                seletRow: true
            }
        }],
        dom: '<"datatable-header"fl ><""t><"datatable-footer"ip>',
        language: {
            search: '<span>' +  /*lang.search*/ 'search' +'</span> _INPUT_',
            searchPlaceholder: '',
            lengthMenu: '<span data-i18n="show">Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
                loadingRecords: "<i class=\"icon-spinner3 spinner\"></i>",
           /* sInfo:         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            Info: "show _START_to _END_from _TOTAL_entries "*/
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },


    });

  /*  $('.table-list').on('click',function(){

        var oTable = $('.table-inbox').dataTable();
        oTable.fnDraw()
        )},*/

    var t = $('#inbox_table').dataTable({
        "ordering": false,
        "processing": true,
        'autoWidth': true,
        "ajax": {
            "url": '/inbox',
            "type": 'POST',
            "data": ''
        },
        columns: columns,
        fixedColumns:   {
            leftColumns: 0,
            rightColumns: 1
        },
        "createdRow": function (row, data, index) {
            $(row).data("id", data.id);

            if (data.is_read == false) {
                $(row).addClass('unread');
            }
        },"drawCallback": function( settings ) {

        $('.tooltip-tools').tooltip({container: 'body'});
    }


    });




    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var type = $(this).data('type');
        if ( ! $.fn.DataTable.isDataTable( '#'+type+'_table' ) ) {

            if(type=='draft' || type=='sent' ) {
                var t = $('#' + type + '_table').dataTable({
                    "ordering": false,
                    "processing": true,
                    "serverSide": false,
                    'autoWidth': true,
                    "ajax": {
                        "url": '/inbox',
                        "type": 'POST',
                        "data": {'filter': type}
                    },
                    columns: draft_columns,
                    "createdRow": function (row, data, index) {
                        $(row).data("id", data.id);

                        if (data.is_read == false) {
                            $(row).addClass('unread');
                        }
                    },"drawCallback": function( settings ) {

                        if(type=='draft'){
                            if(select_all==1){
                                $(".checkbox_document").prop("checked", true);
                            }
                        }
                        $('.tooltip-tools').tooltip({container: 'body'});
                    }


                });
            }else{

                var t = $('#' + type + '_table').dataTable({
                    "ordering": false,
                    "processing": true,
                    "serverSide": false,
                    'autoWidth': true,
                    "ajax": {
                        "url": '/inbox',
                        "type": 'POST',
                        "data": {'filter': type}
                    },
                    columns: columns,
                    "createdRow": function (row, data, index) {
                        $(row).data("id", data.id);

                        if (data.is_read == false) {
                            $(row).addClass('unread');
                        }
                    },"drawCallback": function( settings ) {

                    $('.tooltip-tools').tooltip({container: 'body'});

                }


                });




            }

        }else{
            $('#' + type + '_table').dataTable().fnDraw();
            }

    });


    $('.table-inbox').on('click','tr td',function(){
        $this = $(this);
        var table_id = $this.closest('table').attr('id');

        if(!$this.hasClass('details-button') && !$this.hasClass('details-column') && !$this.hasClass('time-column') ) {
            var id = $this.parent().data('id') ;

            if(table_id=='draft_table'  ) {
                window.location = "/document/compose/" + id ;
                return true;
            }else{
                window.location = "/document/" + id ;
                return true;
            }
            window.location = "/document/" + id ;
            return true;
        }

    });




    $(document).on('click','.delete_draft_button',function(e){
        var id = $(this).data('id') ;
        var this_button = $(this);
        var row = this_button.closest('tr');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#FF7043",
                confirmButtonText: "Yes, delete it!"
            }, function(isConfirm){

                    if (isConfirm){
                        $.post('document/draft/delete', {'id':id},function(){
                            $('#draft_table').dataTable().fnDraw();
                        });
                        swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                    } else {

                    }
                });

e.preventDefault();
    });

    $(document).on('click','.delete_draft_submit',function(){
        $('#delete-draft-form').submit();
    });




    $('#delete_all_selected_draft').click(function(){
        var ids = [];
        var all = 0 ;
        if(select_all== 1 ){
            all = 1 ;
        }

        if(all==0){
            $('#draft_table').find(".styled").each(function(){
                if($(this).is(':checked')) {
                    var id = $(this).data('id');
                    ids.push(id);
                }
            });
        }


        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008EB8",
            confirmButtonText: "Yes, delete it!"
        }, function(isConfirm){

            if (isConfirm){

                if(all==0){
                    $.post('document/draft/delete', { 'ids' : JSON.stringify(ids) },function(){
                        $('#draft_table').dataTable().fnDraw();
                    });
                }else{
                    $.post('document/draft/delete', { 'all' : 'true' },function(){
                        $('#draft_table').dataTable().fnDraw();
                    });
                }
                $('#select_all_documents_in_draft').hide();
                $(".styled").closest('span').removeClass('checked') ;
                $(".all_select").prop("checked", false);
                all_select = 0 ;
                all= 0 ;
                swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
            } else {

            }
        });

    })


}

    $.fn.load_inbox_page();
}) ;

