$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    /*
     *
     * define functions
     *
     */
var current_step = 'step1' ;
    $.fn.goTo = function(class_name){
        $('.steps').hide() ;
        $('.steps_button').hide() ;
        $(class_name).show();
        $(class_name + "_button").show();
        $.fn.removeError();
    }

    $.fn.getUserID = function(identifier){
        $.post(email_or_phone_post_url,{'identifier' : identifier }, function(oResponse){
            console.log(oResponse);
            if(oResponse.status=='failed') {
                $.fn.addError('#identifier');
            }
            if(oResponse.status=='success') {
                $('#user_id').val(oResponse.oUser.id);
                $('.step2').find('.mail-value').html(identifier + " <a href='/login'><small>(" + i18n.t('login.change') + ")</small></a>");
                $.fn.goTo('.step2');
                current_step = 'step2';
                $.fn.removeError();
                $('#password').focus();
            }
            return true;
        },'json') ;
    }

    $.fn.submitLogin = function(user_id,password){
        $.post(login_route,{'user_id' : user_id, 'password' : password}, function(oResponse){
            console.log(oResponse);

            if(oResponse.status=='error') {
                $.fn.addError('#password');
            }

            if(oResponse.status=='success') {
                window.location = '/inbox' ;
                $.fn.removeError('#password');
            }
            return true;
        },'json') ;
    }

    $.fn.addError = function(id_name){
        $(id_name).closest('div').addClass('has-error');
        var value = $(id_name).val();

        if(id_name=='#identifier') {
        if(value==''){
            $(id_name).closest('div').find('.help-block').text(i18n.t('login.Enter an email or phone number'));
        }else{
            $(id_name).closest('div').find('.help-block').text(i18n.t('login.Wrong an email or phone number'));
        }
        }

        if(id_name=='#password') {
            if(value==''){
                $(id_name).closest('div').find('.help-block').text(i18n.t('login.Enter a password'));
            }else{
                $(id_name).closest('div').find('.help-block').text(i18n.t('login.Wrong password! Try again or click Forgot password to reset it'));
            }
        }


        $(id_name).closest('div').find('.help-block').removeClass('hide');
    }

    $.fn.removeError = function(id_name){
        $(id_name).closest('div').removeClass('has-error');

        $(id_name).closest('div').find('.help-block').addClass('hide');
    }


    $('#next_button').on('click',function(e){
        var identifier = $('#identifier').val();
        if(identifier==''){
            $.fn.addError('#identifier');
            return false;
        }
        $.fn.getUserID(identifier) ;
        e.preventDefault();
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(current_step == 'step1') {
                var identifier = $('#identifier').val();
                if(identifier==''){
                    $.fn.addError('#identifier');
                    return false;
                }
                $.fn.getUserID(identifier) ;
            }
            if(current_step == 'step2') {
                $('#submit-login').click();

            }
            e.preventDefault();
        }
    });

    $('#submit-login').click(function(){
        var password = $('#password').val();
        var user_id = $('#user_id').val();
        if(password==''){
            $.fn.addError('#password');
            return false;
        }else{
            $.fn.removeError('#password');
        }

        $.fn.submitLogin(user_id,password);
    });

    $( '#identifier' ).keyup(function(e) {
        if(e.which != 13)
        {
            $.fn.removeError('#identifier');
        }
    });

    $( '#password' ).keyup(function(e) {
        if(e.which != 13)
        {
            $.fn.removeError('#password');
        }
    });

$('#cancel_button').click(function(){
    location.reload();
});

    // function checkEmail(emailOrPhone){
    //
    //
    //         $.ajax({
    //             method: "POST",
    //             url:  email_or_phone_post_url +'/'+ emailOrPhone ,
    //             data: { _token : _token },
    //             success: function( data ) {
    //
    //                 if(data.data.email == true){
    //
    //                     $( ".Mail" ).hide();
    //                     $( ".Password" ).show();
    //                     $('#password').select();
    //                     $( ".Next" ).hide();
    //                     $( ".Submit" ).show();
    //                     $( ".other-options" ).hide();
    //                     $( ".remember-user" ).hide();
    //                     $( ".back-arrow " ).show();
    //                     $( ".back-arrow-login" ).show();
    //                     $("#mail-or-phone-error").hide();
    //                     $( ".forgot-password" ).show();
    //                     $( ".need-help" ).hide();
    //
    //                 }else if(data.data.email == false){
    //
    //                     $( ".forgot-password" ).hide();
    //                     $( ".need-help" ).show();
    //
    //                     $("#mail-or-phone-error").show();
    //                     $("#mail-or-phone-error").html("<p>"+mail_or_phone_not_exist+"</p>");
    //
    //                 }else if (data.data.email == 'other') {
    //
    //                     $( ".forgot-password" ).hide();
    //                     $( ".need-help" ).show();
    //
    //                     $("#mail-or-phone-error").show();
    //                     $("#mail-or-phone-error").html("<p>"+wrong_phone_email+"</p>");
    //
    //
    //                 }
    //
    //
    //                 $( ".Next" ).attr("disabled", false);
    //
    //             },
    //             error: function( data ) {
    //
    //                 $("#mail-or-phone-error").html("<p>"+mail_or_phone_error+"</p>");
    //
    //                 $( ".Next" ).attr("disabled", false);
    //
    //             }
    //         });
    //
    // }

    // function doLogin(emailOrPhone,password){
    //
    //     $.ajax({
    //
    //         method: "POST",
    //         url:  login_route  ,
    //         data: { _token : _token , 'emailOrPhone': emailOrPhone ,'password': password},
    //         success: function( data ) {
    //
    //             if(data.status=='error') {
    //
    //                 if(data.error=='Inactive User'){
    //                     $("#password-login-error").html(translate_error.inactive_user) ;
    //                 }else{
    //                     $("#password-login-error").html(translate_error.wrong_password) ;
    //                 }
    //                 $("#password-login-error").show();
    //                 $("#password-error").hide();
    //
    //             }
    //
    //             if(data.status=='success') {
    //
    //                 window.location = data.redirect_to;
    //             }
    //
    //             $( ".Submit" ).attr("disabled", false);
    //         },
    //         error: function( data ) {
    //
    //             $("#mail-or-phone-error").html("<p>mail_or_phone_error</p>");
    //
    //             $( ".Submit" ).attr("disabled", false);
    //
    //         }
    //     });
    //
    // }




    /*
     *
     * handel clicks
     *
     */

    //
    // $('#email').select();
    // $( ".forgot-password" ).hide();
    // $( ".need-help" ).show();
    //
    //
    // var next_counter = 0 ;
    // $( ".Next" ).click(function() {
    //
    //     next_counter ++ ;
    //     var   emailOrPhone = $( "#email" ).val();
    //
    //     if(next_counter > 4){
    //         setTimeout(function(){ checkEmail(emailOrPhone,next_counter) ; }, 3000);
    //     }else{
    //         checkEmail(emailOrPhone,next_counter);
    //     }
    //
    //     var email = $('#email').val();
    //
    //
    //     $('#password').val("");
    //     $('.mail-value').html(email);
    //
    //     $( ".Next" ).attr("disabled", true);
    //
    // });
    //
    // var submit_counter = 0 ;
    // $( ".Submit" ).click(function() {
    //
    //     var   emailOrPhone = $( "#email" ).val();
    //     var   password = $( "#password" ).val();
    //           submit_counter ++ ;
    //
    //
    //     if(submit_counter > 3){
    //
    //         setTimeout(function(){
    //
    //             if(password != ''){
    //                 $('#login_form').submit();
    //             }else{
    //                 $("#password-error").show();
    //             }
    //
    //
    //             }, 2000);
    //     }else{
    //
    //             if(password != ''){
    //                 $('#login_form').submit();
    //             }else{
    //                 $("#password-error").show();
    //             }
    //
    //     }
    //
    //
    // });
    //
    // $( ".sign-up" ).click(function() {
    //
    //     $( ".login-form-partial" ).hide();
    //     $( ".signup-form-partial" ).show();
    //     $( ".back-arrow " ).show();
    //
    // });
    //
    // $( ".back-arrow ").click(function() {
    //
    //     $( ".other-options" ).show();
    //     $( ".signup-form-partial" ).hide();
    //     $( ".back-arrow-login" ).hide();
    //
    // });
    //
    // $( ".back-arrow-login ").click(function() {
    //
    //     $( ".other-options" ).show();
    //     $( ".Password" ).hide();
    //     $( ".Mail" ).show();
    //     $( ".remember-user" ).show();
    //     $( ".back-arrow-login" ).hide();
    //     $( ".Submit" ).hide();
    //     $( ".Next" ).show();
    //     $( ".Next" ).prop( "disabled", false );
    //     $("#password-error").hide();
    //     $("#password-login-error").hide();
    //     $( ".forgot-password" ).hide();
    //     $( ".need-help" ).show();
    //
    // });
    //
    // $( "#submit-login").click(function() {
    //
    //     $( ".Submit" ).attr("disabled", true);
    //
    //     var   emailOrPhone = $( "#email" ).val();
    //     var   password = $( "#password" ).val();
    //
    //     doLogin(emailOrPhone,password);
    //
    // });
    //
    //
    // $("#email").keypress(function(e) {
    //     if(e.which == 13) {
    //
    //         $(".Next").click();
    //     }
    //
    // });
    //
    // $("#password").keypress(function(e) {
    //     if(e.which == 13) {
    //
    //         $( ".Submit").focus();
    //         $( "#submit-login").click();
    //
    //     }
    //
    // });



}) ;
