<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Layouts extends \App\Services\Services {

    private $oServices;
    static protected $sUrlService = 'document_layouts';
    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function all($aParams=[]){
    $oServices = Parent::getInstance();
    $oServices->setUrl(self::$sUrlService) ;
    return $oServices->find()['document_layouts'];
    }

    public static function getDefault($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('GET')
                ->setUrl("document_layouts/default")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array['data'] ;
    }



}
