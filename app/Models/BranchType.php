<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BranchType
 */
class BranchType extends Model
{
    protected $table = 'branch_type';

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

        
}