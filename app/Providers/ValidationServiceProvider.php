<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app['validator']->extend('apiUnique', function($attribute, $value, $parameters)
        {
            $model = "\App\Services\\".$parameters[0];
            if (class_exists($model)) {
                $client = new $model();
                $response = $client->unique($attribute, $value);
                return !$response;
            } else {
                return false;
            }
        }, 'The :attribute has already been taken.');

        $this->app['validator']->extend('apiExists', function($attribute, $value, $parameters)
        {

            $model = "\App\Services\\".$parameters[0];
            if (class_exists($model)) {

                $client = new $model();
                $response = $client->exists($attribute, $value);
                return $response;
            }else{
                return false;
            }
        }, 'The selected :attribute is invalid.');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
