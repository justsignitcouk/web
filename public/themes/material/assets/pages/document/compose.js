$(document).ready(function () {

    if(document_id !=null){
        //$.fn.getDocument();
    }



    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });



    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": "-",
        }
    });


    $(".content_page,.mce-floatpanel").on('mouseover',function(){
        $('.mce-floatpanel').css('opacity','1') ;
    });
    $(".content_page,.mce-floatpanel").on('mouseout',function(){
        if (  $(".content_page").is(":hover")) {
        }else{
            $('.mce-floatpanel').css('opacity', '0.2');
        }
    });

    $.fn.getDefaultLayout = function(){

        $.get(globals_route_getLayout,{},function(data){
            $.each(data.document_layouts[0].layout_components, function (k, v) {
                if(k=='header'){
                    $('.header_container').html(v);
                }

               if(k=='content'){
                    $('.a4_page_content').html(v);
                    $(".editable").load();
                    tinymce.init({
                        selector: 'div.editable',
                        inline: true,
                        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'paste, mention',
                            'searchreplace visualblocks code fullscreen paste',
                            'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                        ],
                        mentions: {
                            queryBy:'title',
                            delimiter: ['#'],
                            source: function (query, process, delimiter) {
                                // Do your ajax call
                                // When using multiple delimiters you can alter the query depending on the delimiter used
                                if (delimiter == '#') {
                                    $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                                        //call process to show the result
                                        process(data)
                                    },'json');
                                }
                            },
                            highlighter: function(text) {
                                return text;
                                //make matched block italic
                                return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                                    return '<i>' + match + '</i>';
                                });
                            },
                            insert: function(item) {
                                return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                            }
                            },
                        toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                        toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                        toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                        content_css: [
                            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                            '//www.tinymce.com/css/codepen.min.css']
                    });

                    tinymce.init({
                        selector: 'div.to_document_editor',
                        inline: true,
                        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'paste, mention',
                            'searchreplace visualblocks code fullscreen paste',
                            'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                        ],
                        mentions: {
                            queryBy:'title',
                            delimiter: ['#'],
                            source: function (query, process, delimiter) {
                                // Do your ajax call
                                // When using multiple delimiters you can alter the query depending on the delimiter used
                                if (delimiter == '#') {
                                    $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                                        //call process to show the result
                                        process(data)
                                    },'json');
                                }
                            },
                            highlighter: function(text) {
                                return text;
                                //make matched block italic
                                return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                                    return '<i>' + match + '</i>';
                                });
                            },
                            insert: function(item) {
                                return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                            }
                            },
                        toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                        toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                        toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                        content_css: [
                            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                            '//www.tinymce.com/css/codepen.min.css']
                    });


                   tinymce.init({
                       selector: 'div.signer_document_editor',
                       inline: true,
                       fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                       plugins: [
                           'advlist autolink lists link image charmap print preview anchor',
                           'paste, mention',
                           'searchreplace visualblocks code fullscreen paste',
                           'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                       ],
                       mentions: {
                           queryBy:'title',
                           delimiter: ['#'],
                           source: function (query, process, delimiter) {
                               // Do your ajax call
                               // When using multiple delimiters you can alter the query depending on the delimiter used
                               if (delimiter == '#') {
                                   $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                                       //call process to show the result
                                       process(data)
                                   },'json');
                               }
                           },
                           highlighter: function(text) {
                               return text;
                               //make matched block italic
                               return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                                   return '<i>' + match + '</i>';
                               });
                           },
                           insert: function(item) {
                               return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                           }
                       },
                       toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                       toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                       toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                       content_css: [
                           '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                           '//www.tinymce.com/css/codepen.min.css']
                   });

                   tinymce.init({
                       selector: 'div.subject_page',
                       inline: true,
                       fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                       plugins: [
                           'advlist autolink lists link image charmap print preview anchor',
                           'paste, mention',
                           'searchreplace visualblocks code fullscreen paste',
                           'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                       ],
                       mentions: {
                           queryBy:'title',
                           delimiter: ['#'],
                           source: function (query, process, delimiter) {
                               // Do your ajax call
                               // When using multiple delimiters you can alter the query depending on the delimiter used
                               if (delimiter == '#') {
                                   $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                                       //call process to show the result
                                       process(data)
                                   },'json');
                               }
                           },
                           highlighter: function(text) {
                               return text;
                               //make matched block italic
                               return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                                   return '<i>' + match + '</i>';
                               });
                           },
                           insert: function(item) {
                               return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                           }
                       },
                       toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                       toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                       toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                       content_css: [
                           '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                           '//www.tinymce.com/css/codepen.min.css']
                   });
                   tinymce.init({
                       selector: 'div.cc_document_editor',
                       inline: true,
                       fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                       plugins: [
                           'advlist autolink lists link image charmap print preview anchor',
                           'paste, mention',
                           'searchreplace visualblocks code fullscreen paste',
                           'insertdatetime media table contextmenu paste textcolor colorpicker directionality'
                       ],
                       mentions: {
                           queryBy:'title',
                           delimiter: ['#'],
                           source: function (query, process, delimiter) {
                               // Do your ajax call
                               // When using multiple delimiters you can alter the query depending on the delimiter used
                               if (delimiter == '#') {
                                   $.post('https://mycompose.com:82/document/ajax/list_document',{}, function (data) {
                                       //call process to show the result
                                       process(data)
                                   },'json');
                               }
                           },
                           highlighter: function(text) {
                               return text;
                               //make matched block italic
                               return text.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
                                   return '<i>' + match + '</i>';
                               });
                           },
                           insert: function(item) {
                               return '<span class="document_reference" data-id="' + item.id + '">(' + item.title + ' رقم  ' + item.id + ')</span>&nbsp;';
                           }
                       },
                       toolbar1: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify |  ltr rtl | bullist numlist outdent indent | link image ',
                       toolbar2: 'styleselect formatselect fontselect fontsizeselect | forecolor backcolor | table',
                       toolbar3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr|,fullscreen",
                       content_css: [
                           '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                           '//www.tinymce.com/css/codepen.min.css']
                   });




               }
                if(k=='footer'){
                    $('.footer_container').html(v);
                }


            });
        }, 'json');
    };
    $.fn.getDefaultLayout();

    $('.header_and_footer').click(function () {
        var route = $('.layout_modal').data('route');
        $.post(route, {}, function (data) {
            var html = '<div class="row">';

            $.each(data, function (k, v) {
                html += '<div class="layout_box" data-id="' + v.id + '">';
                $.each(v.layout_components, function (k2, v2) {

                    if(k2=='header'){
                        html += "<div class='header_component'>" + v2 + "</div>";
                    }

                    // if (v2.position == 'header') {
                    //     html += "<div class='header_component'>" + v2.content + "</div>";
                    // }
                    // if (v2.position == 'footer') {
                    //     html += "<div class='footer_component'>" + v2.content + "</div>";
                    // }
                    // if (v2.position == 'content') {
                    //     html += "<div class='content_component hide'>" + v2.content + "</div>";
                    // }

                });
                html += "</div>";
            });
            html += "</div>";
            $('.layout_modal').find('.modal-content').html(html);
            $('.layout_modal').modal('show');
        }, 'json');


    });

    $(document).on('click', '.layout_box', function () {

        var id = $(this).data('id');

        $('#document_layout_input').val(id);
        var to_page = $(this).find('.to_page').html();
        var content_page = $(this).find('.content_page').html();
        var cc_page = $(this).find('.cc_page').html();
        var signs_page = $(this).find('.signs_page').html();


        var header = $(this).find('.header_component').html();
        var footer = $(this).find('.footer_component').html();
        var content = $(this).find('.content_component').html();

        $('.header_container').html(header);
        $('.footer_container').html(footer);
        $('.a4_page_content').html(content);

        $.fn.saveDraft();
        $('.modal').modal('hide');


    });







    var interval_id ;
    $(window).on("blur focus", function(e) {
        var prevType = $(this).data("prevType");

        if (prevType != e.type) {   //  reduce double fire issues
            switch (e.type) {
                case "blur":
                    clearInterval(interval_id);
                    interval_id = 0;
                    break;
                case "focus":
                    if (!interval_id) {
                        interval_id = setInterval(function(){
                            $.fn.saveDraft() }, 50000);
                    }
                    break;
            }
        }

        $(this).data("prevType", e.type);
    })

    $.fn.loadDraft();

//send document
    $(document).on('click', '#sendDocument', function (e) {
        var button = $(this);
        button.prop('disabled', 'true');

        var elements = $.fn.getElements();

        if(Object.keys(elements.to_contacts).length === 0  ) {
            new PNotify({
                title: 'Please enter one reception at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }

        if(  Object.keys(elements.signs_contacts).length === 0  ) {
            new PNotify({
                title: 'Please enter one signer at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return false;
        }

        if (!elements) {
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return;
        }

        $.post(globals_routeSendDocument, elements, function (data) {
            if(data.status=='false'){
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

                button.removeAttr('disabled');
                return false;
            }

            if (data.status == 'true') {
                new PNotify({
                    title: 'success.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
                console.log(data);
                window.location = '/document/' + data.document_id;
                $(this).removeAttr('disabled');
            } else {
                $(this).removeAttr('disabled');
            }

        },'json').error(function(response){
                //check if response has errors object
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

            button.removeAttr('disabled');

            });


        e.preventDefault();


    });
// end send document

// save as template
    $(document).on('click', '#saveTemplate', function (e) {
        var button = $(this);
        button.prop('disabled', 'true');

        var elements = $.fn.getElements();
        elements.status = 'template';

        $.post(globals_routeSendDocument, elements, function (data) {
            if(data.status=='false'){
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

                button.removeAttr('disabled');
                return false;
            }

            if (data.status == 'true') {
                new PNotify({
                    title: 'saved.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
                $(this).removeAttr('disabled');
            } else {
                $(this).removeAttr('disabled');
            }

        },'json').error(function(response){
                //check if response has errors object
                $.each(response.responseJSON.errors,function(k,v){
                    $('input[name="' + k + '"').closest('div').addClass("has-error has-feedback");
                    $('input[name="' + k + '"').closest('div').find('.help-block').text(v);


                });

            button.removeAttr('disabled');

            });


        e.preventDefault();


    });
// end send document

    //save to draft




    $(document).on('click', '#saveDocument', function (e) {
        var buttonThis = $(this);
        buttonThis.prop('disabled', 'true');
        var elements = $.fn.getElements();
        if (!elements) {
            $('#sendDocument').removeAttr('disabled');
            $('#saveDocument').removeAttr('disabled');
            return;
        }

        $.post(globals_routeSendDraft, elements, function (data) {
            var response = jQuery.parseJSON(data);
            if (response.status == 1) {
                new PNotify({
                    title: 'success.',
                    text: '',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
                buttonThis.removeAttr('disabled');
            } else {
                buttonThis.removeAttr('disabled');
            }

        });


        e.preventDefault();


    });


    //end save to draft

    $(".xtool").click(function () {
        $("#mceu_38").fadeToggle();
    });



    var typingTimer;
    var doneTypingInterval = 2000;
    var $input = $('#title_input,#content_input');

//on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function () {
            $.fn.saveDraft()
        }, doneTypingInterval);
    });
    $(document).on('keyup', '.recipient-value', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function () {
            $.fn.saveDraft()
        }, doneTypingInterval);
    });




    $('.choose_layout').click(function () {
        var route = $('.layout_modal').data('route');
        $.post(route, {}, function (data) {
            var html = '<div class="row">';
            var layout_count = 1;
            $.each(data, function (k, v) {
                html += '<div class="layout_box" data-id="'+ layout_count +'">';
                $.each(v.layout_components, function (k2, v2) {

                    if (k2 == 'header') {
                        html += "<div class='header_component'>" + v2 + "</div>";
                    }
                    if (k2 == 'footer') {
                        html += "<div class='footer_component'>" + v2 + "</div>";
                    }
                    if (k2 == 'content') {
                        html += "<div class='content_component hide'>" + v2 + "</div>";
                    }
                    layout_count++ ;
                });
                html += "</div>";
            });
            html += "</div>";
            $('.layout_modal').find('.modal-content').html(html);
            $('.layout_modal').modal('show');
        }, 'json');
        $.fn.saveDraft();
    });

    $(document).on('click', '#workflow_menu', function (e) {
        $.post(globals_route_get_workflows, {}, function (data) {

            $('#workflow_menu').dropdown();
        });
        //$('#everything').html(data);

    });


    $.fn.drawTemplates = function(){
        var html = '' ;
        console.log(aTemplates);
        if(aTemplates!==undefined){

            $.each(aTemplates,function(k,v){
            html += '<div class="col-md-12"><a href="" data-key="' + k + '" class="set_by_template"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 20 24" class="document_list"><defs><style>.cls-1{fill:#003375;}</style></defs><title>' + v.document.title + '</title><g id="a"><path class="cls-1" d="M20,3V24H0V3H4.7L2,5.8V22H18V5.8L15.4,3ZM16.4,7,13,3.4V3a3,3,0,0,0-3-3A3,3,0,0,0,7,3v.4L3.7,7ZM10,2a.94.94,0,0,1,1,1,.94.94,0,0,1-1,1A.94.94,0,0,1,9,3,.94.94,0,0,1,10,2ZM5,17H15v1H5Zm0-1H15V15H5Zm0-2H15V13H5Zm0-2H15V11H5Z"/></g></svg><span class="document_list">'+ v.document.title +'</span></a></div>';
            });
                $('#template_container').html(html);
        }
    }
    //$.fn.drawTemplates();

$(document).on('click','.set_by_template',function(e){
var key = $(this).data('key');

var data = aTemplates[key];

    if (!$.isEmptyObject(data.document_layout)) {

        if (!$.isEmptyObject(data.document_layout.layout_components)) {
            $.each(data.document_layout.layout_components, function (k, v) {

                if (k == 'header') {
                    $('.header_container').html(v);
                }

                if (k == 'footer') {
                    $('.footer_container').html(v);
                }

                if(k =='content'){

                    $('.a4_page_content').html(v) ;
                    $('.subject_page').html(data.document.title);
                    $('#document_number_text').html(data.document.outbox_number);
                    $('#document_date').val(data.document.document_date);
                    $('#document_date').attr('disabed','disabled');
                    $('.content_page').html(data.document.content);

                    if(editor){
                        $.fn.load_editor();
                    }






                    //drawReceipts('to', 'account', data.document.to_account);
                    //drawReceipts('to', 'contact', data.document.to_contact);
                    $.fn.drawDocumentReceipts('contact', data.document.document_contact);
                    //drawReceipts('to', 'user', data.document.to_user);
                    //drawReceipts('to', 'department', data.document.to_department);
                    //drawReceipts('to', 'role', data.document.to_role);
                    //drawReceipts('to', 'invitations', data.document.to_invitations);

                    // drawReceipts('cc', 'account', data.document.cc_account);
                    // drawReceipts('cc', 'contact', data.document.cc_contact);
                    // drawReceipts('cc', 'user', data.document.cc_user);
                    // drawReceipts('cc', 'department', data.document.cc_department);
                    // drawReceipts('cc', 'role', data.document.cc_role);
                    // drawReceipts('cc', 'invitations', data.document.cc_invitations);
                    //
                    //
                    // drawReceipts('signer', 'account', data.document.signer_account);
                    // drawReceipts('signer', 'contact', data.document.signer_contact);


                    //$.fn.drawAttachment(data.document.files);


                    if(displayWorkflow){
                        $.fn.drawWorkFlow({'item' : data.document.work_flow.work_flow ,'current': data.document.work_flow.current_stage_id });
                        // $.fn.drawActualWorkFlow({'item' : data.document.work_flow.work_flow ,'current': data.document.work_flow.current_stage_id });
                    }




                 }

            });
            $.fn.fillInput(data);
        }
    }


    e.preventDefault();
});
});
