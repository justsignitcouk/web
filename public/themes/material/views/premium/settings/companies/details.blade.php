{!! Theme::asset()->themePath()->add([ ['datatables', 'js/plugins/tables/datatables/datatables.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->add([ ['datatables_responsive', 'assets/components/datatables-responsive/js/dataTables.responsive.js',['datatables']]]) !!}
{!! Theme::asset()->themePath()->add([ ['settings_companies', 'pages/premium/settings/js/company/details.js',['datatables_responsive']]]) !!}
{!! Theme::asset()->themePath()->add([ ['company_details_employees', 'pages/premium/settings/js/company/company_details_employees.js',['datatables_responsive']]]) !!}
{!! Theme::asset()->themePath()->add([ ['company_details_roles', 'pages/premium/settings/js/company/company_details_roles.js',['datatables_responsive']]]) !!}
{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['uniform', 'js/plugins/forms/styling/uniform.min.js',['bootstrap']]]) !!}


<script>
    var branch_details = "{{route('settings.branch_details',[$sSlug,''])}}";
    var getEmployees = "{{route('settings.getEmployees',[$sSlug])}}";
    var getRoles = "{{route('settings.getRoles',[$sSlug])}}";
</script>








<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><a href="{{route('settings.companies')}}"><i class="icon-arrow-left52 position-left"></i></a> <span
                            class="text-semibold">{{ $oCompany['company']['name'] }}</span>
                    - {{ $oCompany['company']['slug'] }}</h4>

            </div>

            <div class="heading-elements">
                {{--@partial('common.modal',['title'=>'Add Branch','buttons'=>['main_button'=>['title'=>'Add Branch']--}}
                {{--],'content'=> ['type'=>'partial','partial_name'=>'home.add_branch'] ])--}}


                <a href="{{route('settings.add-branch',[$oCompany['company']['slug']])}}" class="btn btn-primary">Add
                    Branch</a>
                <a href="{{route('settings.edit_company',[$oCompany['company']['slug']])}}"
                   class="btn btn-success">Edit</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="panel panel-flat border-top-success">
            <div class="panel-heading">
                <h6 class="panel-title"> {{$oCompany['company']['name']}}</h6>
            </div>

            <div class="panel-body">
                <div class="col-lg-5">
                    <dl class="dl-horizontal">

                        <dt>license No</dt>
                        <dd>{{$oCompany['company']['license_no']}}</dd>
                        <dt>Created at:</dt>
                        <dd>{{$oCompany['company']['created_at']}}</dd>
                        <dt>slug:</dt>
                        <dd>  {{$oCompany['company']['slug']}}</dd>
                        <dt>Main Email:</dt>
                        <dd><a href="mailto://{{$oCompany['company']['main_branch']['email'] }}"
                               class="text-navy"> {{$oCompany['company']['main_branch']['email'] }}</a></dd>
                        <dt>address:</dt>
                        <dd>    {{$oCompany['company']['main_branch']['address'] }}</dd>
                    </dl>
                </div>

                <div class="col-lg-7" id="cluster_info">
                    <dl class="dl-horizontal">

                        <dt>Last Updated:</dt>
                        <dd>{{$oCompany['company']['updated_at']}}</dd>
                        <dt>fax:</dt>
                        <dd>    {{$oCompany['company']['main_branch']['fax'] }} </dd>
                        <dt>Main Branch Country:</dt>
                        <dd>    {{$oCompany['company']['main_branch']['country']['name'] }} </dd>
                        <dt style="display:none">Participants:</dt>
                        <dd style="display:none" class="project-people">
                            <a href=""><img alt="image" class="img-circle" src="img/a3.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a4.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a5.jpg"></a>
                        </dd>
                    </dl>
                </div>

                <img src="{{ $oCompany['company']['logo']['url'] . '/files/' . $oCompany['company']['logo']['path'] . '/' . $oCompany['company']['logo']['hash_name'] }}"
                     class="" style="width:100px" alt="">
            </div>
        </div>
        <!-- Detached content -->
    </div>

    <!-- Content area -->
    <div class="content">

        <div class="col-md-12">
            <div class="panel panel-flat inbox_content" data-i18n="filter" data-i18n-target="dataTables_info">


                <div class="panel-body">
                    <div class="tabbable tab-content-bordered">
                        <ul class="nav  nav-tabs nav-tabs-highlight nav-justified  nav-tabs-component">
                            <li class="active table-list"><a href="#justified-right-icon-inbox"
                                                             id="inbox_button loading" data-type="inbox"
                                                             data-toggle="tab"><span
                                            data-i18n="nav.inbox.Branches">{{__('lang.branches')}}</span>
                                </a></li>
                            <li class="table-list"><a href="#employees"
                                                      id="inbox_button loading" data-type="employees"
                                                      data-toggle="tab"><span
                                            data-i18n="nav.inbox.employees">{{__('lang.employees')}}</span>
                                </a></li>
                            <li class="table-list"><a href="#roles"
                                                      id="inbox_button loading" data-type="roles"
                                                      data-toggle="tab"><span
                                            data-i18n="nav.inbox.roles">{{__('lang.roles')}}</span>
                                </a></li>
                        </ul>

                        <div class="tab-content ">
                            <!--- branches tab --->
                            <div class="tab-pane active" id="justified-right-icon-inbox">
                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive inbox table-lay"
                                           id="branches_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th data-i18n="select"></th>
                                            <th data-i18n="name"></th>
                                            <th data-i18n="address"></th>
                                            <th data-i18n="email"></th>
                                            <th data-i18n="phone"></th>
                                            <th data-i18n="license_no"></th>
                                            <th data-i18n="Created At"></th>
                                            <th data-i18n="details"></th>

                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--- branches tab --->
                            <!--- employees tab --->
                            <div class="tab-pane" id="employees">
                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive inbox table-lay"
                                           id="employees_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th data-i18n="select"></th>
                                            <th data-i18n="name"></th>
                                            <th data-i18n="Created At"></th>
                                            <th data-i18n="details"></th>

                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--- employees tab --->
                            <!--- roles tab --->
                            <div class="tab-pane" id="roles">
                                <div class="content-table">
                                    <table class="table datatable-ajax table-inbox responsive inbox table-lay"
                                           id="roles_table"
                                           style="width:100%;table-layout: auto !important;">
                                        <thead>
                                        <tr>
                                            <th data-i18n="select"></th>
                                            <th data-i18n="name"></th>
                                            <th data-i18n="description"></th>
                                            <th data-i18n="details"></th>

                                        </tr>
                                        </thead>
                                        <tbody data-link="row" class="rowlink">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--- roles tab --->


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>