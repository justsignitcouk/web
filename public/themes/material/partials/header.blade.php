{!! Theme::asset()->themePath()->add([ ['header', 'partials/header/header.js',['bootstrap']]]) !!}

{{ csrf_field() }}

<!-- Main navbar -->
<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header" style="background:#fff">
        <a class="navbar-brand" href="/"><img src="/assets/images/mycompose_logo.png" alt="MyCompose" style="margin: 0px 0 0 0;height: 36px;"></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            @if(false)
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-puzzle3"></i>
                    <span class="visible-xs-inline-block position-right">Git updates</span>
                    <span class="status-mark border-orange-400"></span>
                </a>

                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-heading">
                        Git updates
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-sync"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body width-350">
                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                            </div>

                            <div class="media-body">
                                Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                <div class="media-annotation">4 minutes ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
                            </div>

                            <div class="media-body">
                                Add full font overrides for popovers and tooltips
                                <div class="media-annotation">36 minutes ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                            </div>

                            <div class="media-body">
                                <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                                <div class="media-annotation">2 hours ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                            </div>

                            <div class="media-body">
                                <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                                <div class="media-annotation">Dec 18, 18:36</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                            </div>

                            <div class="media-body">
                                Have Carousel ignore keyboard events
                                <div class="media-annotation">Dec 12, 05:46</div>
                            </div>
                        </li>
                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>
            @endif
        </ul>

        {{--<p class="navbar-text">Updating:</p>--}}
        {{--<div class="navbar-progress">--}}
            {{--<div class="progress">--}}
                {{--<div class="progress-bar bg-warning-300 progress-bar-striped active" style="width: 60%;">--}}
                    {{--<span class="sr-only">60% Complete</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="navbar-right">

            <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{ user_profile() }}" alt="">
                    <span>{{ oUser()->first_name }}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{route('profile.index',[getUniqueName()])}}"><i class="icon-user-plus"></i> <span data-i18n="header.myprofile">My profile</span></a></li>
                    {{--<li><a href="#"><i class="icon-coins"></i> My balance</a></li>--}}
                    {{--<li><a href="#"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-warning pull-right">94</span></a></li>--}}
                    <li class="divider"></li>
                    {{--<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>--}}
                    <li><a href="{{route('logout')}}"><i class="icon-switch2"></i> <span data-i18n="header.logout">Logout</span></a></li>
                </ul>
            </li>
                @if(hasRole('Super Admin'))
                    <li class="nav-item"><a href="/admin/users/" class="navbar-nav-link legitRipple">Super Admin</a></li>

                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown language-switch">
                    <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false"><img src="/themes/material/assets/images/flags/gb.png" alt="" class="position-left"> English <i class="caret"></i></a>
                    <ul class="dropdown-menu">
                        <li><a class="english">English</a></li>
                        <li><a class="arabic">Arabic</a></li>
                    </ul>
                </li>
            </ul>
            <p class="navbar-text"><span class="label bg-success-400" data-i18n="header.online">Online</span></p>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bell2"></i>
                        <span class="visible-xs-inline-block position-right" data-i18n="header.activity">Activity</span>
                        <span class="status-mark border-orange-400"></span>
                    </a>

                    <div class="dropdown-menu dropdown-content ">
                        <div class="dropdown-content-heading" >
                            <span data-i18n="header.activity">Activity</span>
                            <ul class="icons-list">
                                <li><a href="#"><i class="icon-menu7"></i></a></li>
                            </ul>
                        </div>

                        <div class="media-list dropdown-content-body width-350 notification">
                                        {{-- {{ notification() }}--}}
                        </div>
                    </div>
                </li>

                <li class="dropdown hide">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bubble8"></i>
                        <span class="visible-xs-inline-block position-right">Messages</span>
                        <span class="status-mark border-orange-400"></span>
                    </a>

                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            Messages
                            <ul class="icons-list">
                                <li><a href=""><i class="icon-compose"></i></a></li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body">
                                {{--{{ messages() }}--}}
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /main navbar -->



@if(false)
<!-- Main navbar -->
<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header logo_header" >
        <a class="navbar-brand" href="/home" style="padding:8px 20px"><img src="" alt="System" style="margin-top: 0px;height: 36px;"></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav header-menu header-menu-custom">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            <li class="dropdown hide">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-puzzle3"></i>
                    <span class="visible-xs-inline-block position-right">Git updates</span>
                    <span class="status-mark border-orange-400"></span>
                </a>

                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-heading ">
                        Git updates
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-sync"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body width-350">
                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                            </div>

                            <div class="media-body">
                                Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                <div class="media-annotation">4 minutes ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
                            </div>

                            <div class="media-body">
                                Add full font overrides for popovers and tooltips
                                <div class="media-annotation">36 minutes ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                            </div>

                            <div class="media-body">
                                <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                                <div class="media-annotation">2 hours ago</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                            </div>

                            <div class="media-body">
                                <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                                <div class="media-annotation">Dec 18, 18:36</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                            </div>

                            <div class="media-body">
                                Have Carousel ignore keyboard events
                                <div class="media-annotation">Dec 12, 05:46</div>
                            </div>
                        </li>
                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>
        </ul>

        <div class="navbar-right calender_div">
            {{--<p class="navbar-text">{{__('common.hello')}}, {{ currentUser()->first_name . ' ' . currentUser()->last_name }}!</p>--}}
            {{--<p class="navbar-text"><input type="text" class="form-control" style="padding:0px;height:auto" id="anytime-both" value="June 4th 08:47"></p>--}}
            <p class="navbar-text anytime"  ><svg xmlns="http://www.w3.org/2000/svg" id="calender_svg" data-name="Layer 1" viewBox="0 0 25 26"><title>Calendar</title><g id="a"><path d="M25,0V26H0V0H3V3.4A2.06,2.06,0,0,0,5,5.5,2.06,2.06,0,0,0,7,3.4H7V0H18V3.4a2.06,2.06,0,0,0,2,2.1,2.13,2.13,0,0,0,2-2.1V0ZM23,9H2V24H23ZM21,1.69a1,1,0,0,0-2,0V3.6a1,1,0,0,0,2,0ZM6,3.7a1,1,0,0,1-1,1,1.07,1.07,0,0,1-1-1v-2a1,1,0,1,1,2,0Z"/></g></svg>
                <input  style="padding: 5px;
    position: relative;
    left: 21px;
    top: 7px; " type="text" class="form-control" style="padding:0px;height:auto" id="anytime-both" value="september 12,2017"></p>



            <ul class="nav navbar-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {{--<i class="icon-bubble8"></i>--}}
                        <svg xmlns="http://www.w3.org/2000/svg" id="msg_svg" data-name="Layer 1" viewBox="0 0 28.1 25.89"><title>Chat </title><g id="a"><path d="M28.1,0H0V20H5v5.89L13,20H28V0Z"/></g></svg>
                        <span class="msg_num">230</span>
                        <span class="visible-xs-inline-block position-right">Messages</span>

                    </a>

                    <div class="dropdown-menu dropdown-content width-350 msg_box">
                        <div class="dropdown-content-heading">
                            Messages
                            <ul class="icons-list">
                                <li><a href="#"><i class="icon-compose"></i></a></li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-left">
                                    {{--<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">--}}
                                    <span class="badge bg-danger-400 media-badge">5</span>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">James Alexander</span>
                                        <span class="media-annotation pull-right">04:58</span>
                                    </a>

                                    <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                    <span class="badge bg-danger-400 media-badge">4</span>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">Margo Baker</span>
                                        <span class="media-annotation pull-right">12:16</span>
                                    </a>

                                    <span class="text-muted">That was something he was unable to do because...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left"><img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">Jeremy Victorino</span>
                                        <span class="media-annotation pull-right">22:48</span>
                                    </a>

                                    <span class="text-muted">But that would be extremely strained and suspicious...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left"><img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">Beatrix Diaz</span>
                                        <span class="media-annotation pull-right">Tue</span>
                                    </a>

                                    <span class="text-muted">What a strenuous career it is that I've chosen...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left"><img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">Richard Vango</span>
                                        <span class="media-annotation pull-right">Mon</span>
                                    </a>

                                    <span class="text-muted">Other travelling salesmen live a life of luxury...</span>
                                </div>
                            </li>
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <svg xmlns="http://www.w3.org/2000/svg" id="not_svg" data-name="Layer 1" viewBox="0 0 22 25.7"><defs><style>.cls-1{fill:none;}</style></defs><title>Notification</title><rect class="cls-1" y="-0.5" width="21" height="26" style="fill: none!important;"/><path d="M14.46,4.3a2.18,2.18,0,0,1-1.12-2h0a2.34,2.34,0,0,0-4.69,0h0a2.42,2.42,0,0,1-1.12,2C2.44,7.2,5.4,16.9,0,18.6v1.9H22V18.6C16.7,16.9,19.66,7.2,14.46,4.3ZM11.25.87A1.1,1.1,0,1,1,10.15,2h0A1.11,1.11,0,0,1,11.25.87Zm3.2,21.63a3.22,3.22,0,0,1-3.2,3.2,3.38,3.38,0,0,1-3.3-3.2Z"/></svg>
                        <span class="notification_num">+99</span>
                        <span class="visible-xs-inline-block position-right">Activity</span>

                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading not_heading">
                            {{__('lang.Notifications')}}
                            <ul class="icons-list">
                                <li><a href="#"><i class="icon-menu7"></i></a></li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body width-350 notification">


                        </ul>
                        <p class="see_all_not"><a href="{!! url('profile' ).'#notifications' !!}" class="see_all">see all</a></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
@endif


<script>
    var  notificationsAjax = "{!! route('notificationsAjax' ) !!}"  ;
    var  changeReadStatus  = "{!! route('changeReadStatus' ) !!}"  ;
</script>



