<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 7/19/17
 * Time: 4:56 PM
 */

// return Object : logged in user information.
function oUser(){
    if(session()->has('oUser')){
        return (object) unserialize(session()->get('oUser'));
    }
    updateUser();
    return false;
}

// return Integer : logged in user id.
function nUserID(){

    if(oUser()){
        return oUser()->id;
    }
    return false;
}

function updateUser(){
    \App\Services\Auth::updateCurrentUser();
}

function oRoles(){
    return (object) unserialize(session()->get('oRoles'));
}

function aProfileRequirements(){
    if(is_array(unserialize(session()->get('aProfileRequirements')))){
        return unserialize(session()->get('aProfileRequirements'));
    }
    return [];
}

function getUniqueName(){
    return !empty(oUser()->username) ? oUser()->username : (!empty(oUser()->id) ? oUser()->id : false ) ;
}

function getIdentifier(){
    if(!oUser()){
        return false;
    }
    return !empty(oUser()->email) ? oUser()->email : oUser()->mobile ;
}

function getIdentifierType(){
    return !empty(oUser()->email) ? 'email' : 'mobile' ;
}

function getToken(){
    return session()->get("sToken");
}


function cdn_url($sImageName,$path){
    return config()->get('app')['cdn_url'] . '/files/' . $path . '/' . $sImageName ;
}

function user_profile(){

    return  isset( oUser()->avatar['full_url'] ) ? oUser()->avatar['full_url'] : '/images/profile_picture.png'  ;
}

function hasRole($sRoleName){
    if(session()->has('oRoles')){
        $aRoles = unserialize(session()->get('oRoles')) ;
        if( ArraySearch($aRoles,'name',$sRoleName) ) {
            return true;
        }
    }
    return false;
}

function oQuota(){
    $oQuota = \App\Services\Quota::get() ;
    if(! session()->has('oQuota')) {
        $oQuota = \App\Services\Quota::get() ;
    }
    return unserialize(session()->get('oQuota')) ;

}

function getQuota($sKey){
    $oQuota = oQuota();
    $count = 0;
    $usage = 0;
    $a = ArraySearch($oQuota,'key',$sKey);
    if($a){
        foreach($a as $oQ){
                if($oQ['value']!= null){
                    $count+=$oQ['value'];
                }else{
                    $count+=$oQ['default_value'];
                }
                $usage = !is_null($oQ['usage']) ? $oQ['usage'] : 0;
        }
    }

    $percentage = number_format(($usage / $count ) * 100 , 2 );
    return (object) [ 'usage'=>$usage, 'total'=>$count , 'percentage'=> $percentage ];
}

function getLangCode(){

    return isset(oUser()->lang_code) ? oUser()->lang_code : 'en';
}

function ArraySearch($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, ArraySearch($subarray, $key, $value));
        }
    }

    return $results;
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}