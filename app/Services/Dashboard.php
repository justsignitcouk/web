<?php


namespace App\Services;


class Dashboard extends \App\Services\Services
{


    public static function getNeedsUserSignature(){

        $oServices = Parent::getInstance();


        $url   = 'account/'.currentAccountID().'/needsMySign' ;


        try{

            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->withToken()
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;

            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);


        return $response_array['data'] ;

    }

    public static function getOthersSignature(){

        $oServices = Parent::getInstance();


        $url   = 'account/'.currentAccountID().'/waitingFromOthers' ;


        try{

            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->withToken()
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;
            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);


        return $response_array ;

    }

}