$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });




    var id = $('.A4').data('id');
    var route = $('.A4').data('route');
    var method = $('.A4').data('method');

if(method=='get') {


    $.get(route,{},function(data){
        console.log(data);
        var title = data.title  ;

        $('#title_input').val(title);
        $('#title_input').keyup();

        var content = data.content ;
        $('.content_page').html(content);

        var document_number = data.outbox_number;
        $('#document_number_input').val(document_number);
        $('#document_number_input').keyup();

        var document_date = data.document_date;
        $('#document_date_input').val(document_date);
        $('#document_date_input').change();




        drawReceipts('to', 'account', data.to_account);
        drawReceipts('to', 'user', data.to_user);
        drawReceipts('to', 'department', data.to_department);
        drawReceipts('to', 'role', data.to_role);
        //drawReceipts('to', 'invitations', data.to_invitations);

        drawReceipts('cc', 'account', data.cc_account);
        drawReceipts('cc', 'user', data.cc_user);
        drawReceipts('cc', 'department', data.cc_department);
        drawReceipts('cc', 'role', data.cc_role);
        //drawReceipts('cc', 'invitations', data.cc_invitations);

        $.fn.drawAttachment(data.files);

         drawReceipts('signer','account',data.signer_account);
         drawReceipts('signer','user',data.signer_user);
         drawReceipts('signer','department',data.signer_department);
         drawReceipts('signer','role',data.signer_role);


        //drawWorkFlow(data.work_flow, data);


        if (!$.isEmptyObject(data.layout)) {
            if (!$.isEmptyObject(data.layout.layout_component)) {
                $.each(data.layout.layout_component, function (k, v) {
                    if (v.position == 'header') {
                        $('.header_container').html(v.content);
                    }

                    if (v.position == 'footer') {
                        $('.footer_container').html(v.content);
                    }


                });
            }
        }


        $('#qr_code').html('<img src="' + data.qr_code.data + '" />');

    },'json');

}

if(method=='post'){

    $.post(route,{},function(data){
        console.log(data);
        var title = data.title  ;

        $('#title_input').val(title);
        $('#title_input').keyup();

        var content = data.content ;
        $('.content_page').html(content);

        var document_number = data.outbox_number;
        $('#document_number_input').val(document_number);
        $('#document_number_input').keyup();

        var document_date = data.document_date;
        $('#document_date_input').val(document_date);
        $('#document_date_input').change();




        drawReceipts('to', 'account', data.to_account);
        drawReceipts('to', 'user', data.to_user);
        drawReceipts('to', 'department', data.to_department);
        drawReceipts('to', 'role', data.to_role);
        drawReceipts('to', 'invitations', data.to_invitations);

        drawReceipts('cc', 'account', data.cc_account);
        drawReceipts('cc', 'user', data.cc_user);
        drawReceipts('cc', 'department', data.cc_department);
        drawReceipts('cc', 'role', data.cc_role);
        drawReceipts('cc', 'invitations', data.cc_invitations);

        $.fn.drawAttachment(data.files);

        drawReceipts('signer','account',data.signer_account);
        drawReceipts('signer','user',data.signer_user);
        drawReceipts('signer','department',data.signer_department);
        drawReceipts('signer','role',data.signer_role);


        //drawWorkFlow(data.work_flow, data);


        if (!$.isEmptyObject(data.layout)) {
            if (!$.isEmptyObject(data.layout.layout_component)) {
                $.each(data.layout.layout_component, function (k, v) {
                    if (v.position == 'header') {
                        $('.header_container').html(v.content);
                    }

                    if (v.position == 'footer') {
                        $('.footer_container').html(v.content);
                    }


                });
            }
        }


        $('#qr_code').html('<img src="' + data.qr_code.data + '" />');

    },'json');
}


var invite = 0 ;
    function drawReceipts(type, data_type, data) {

        if (data_type == 'account') {
            $.each(data, function (k, v) {
                var description = v.account.account_name;
                var account_id = v.account.id;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }

                var editor_content = '<div class="' + type + '_accounts_item ' + type + '_accounts_item_'+ account_id +'" id="' + type + '_accounts_item_'+  account_id +'" contenteditable="true" data-id="'+ account_id +'" data-type="' + type + '"  data-elementType="'+ data_type +'"  data-elementid="'+ account_id +'" data-origin="' + account_id +'">' + description +'</div>' ;
                $('.' + type + '_document_editor').append(editor_content);

                var content = '<li class="media recipient-account recipient-account-'+ account_id +'" id="recipient-account-'+ account_id +'" data-id="'+ account_id +'" data-type="'+ type +'" data-text="'+ description +'" data-elementType="'+ type +'" data-elementid="'+ account_id +'">' +
                    '<div class="media-left">'  +
                    '<a href="#">' +
                    '<img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt="">' +
                    '<a href="" class="remove-recipient-account">X</a>' +
                    '</a>' +
                    '</div>' +
                    '<div class="media-body media-middle">' + v.account.account_name + '<i class="recipient-account-type pull-right">'+ type +'</i>' +
                    '</div>' +
                    '<div class="media-right media-middle">' +
                    '<ul class="icons-list text-nowrap">' +
                    '<li>' +
                    '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>' +
                    '<ul class="dropdown-menu dropdown-menu-right">' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="To"><i class="icon-mail5 pull-right"></i> To</a></li>' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="Signer"><i class="icon-quill4 pull-right"></i> Signer</a></li>' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="CC"><i class="icon-cc pull-right"></i> CC</a></li>' +
                    '<li class="divider"></li>' +
                    '<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>' +
                    '</ul>' +
                    '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</li>' ;
                $('#recipient-list').append(content);
            });
        }
        if (data_type == 'user') {
            $.each(data, function (k, v) {

                var description = v.user.first_name + ' ' + v.user.last_name;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }

                $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
            });
        }

        if (data_type == 'department') {

            $.each(data, function (k, v) {
                var description = v.department.name;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }
                $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
            });

        }

        if (data_type == 'role') {
            $.each(data, function (k, v) {
                var description = v.role.name;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }
                $('.' + type + '_document_editor').append('<div class="' + type + '_item" data-origin="' + orign_description + '">' + description + '</div>');
            });
        }

        if (data_type == 'invitations') {
            console.log("---------------------");
            console.log(data);
            console.log("---------------------");
            $.each(data, function (k, v) {
                var invitaion_id =invite;
                var description = v.description;
                var orign_description = description;
                if (v.description !== null) {
                    description = v.description;
                }

                var editor_content = '<div class="' + type + '_accounts_item ' + type + '_accounts_item_n_'+ invitaion_id +'" id="' + type + '_accounts_item_n_'+  invitaion_id +'" contenteditable="true" data-id="'+ invitaion_id +'" data-type="' + data_type + '"  data-elementType="'+  'external' +'"  data-elementid="'+ invitaion_id +'" data-origin="' + v.invitations.email +  '">' + description +'</div>' ;
                $('.' + type + '_document_editor').append(editor_content);

                var content = '<li class="media recipient-account recipient-account-n_'+ invitaion_id +'" id="recipient-account-n_'+ invitaion_id +'" data-id="n_'+ invitaion_id +'" data-type="'+ type +'" data-text="'+ description +'" data-elementType="'+ type +'" data-elementid="n_'+ invitaion_id +'">' +
                    '<div class="media-left">'  +
                    '<a href="#">' +
                    '<img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt="">' +
                    '<a href="" class="remove-recipient-account">X</a>' +
                    '</a>' +
                    '</div>' +
                    '<div class="media-body media-middle">' + v.invitations.email + '<i class="recipient-account-type pull-right">'+ type +'</i>' +
                    '</div>' +
                    '<div class="media-right media-middle">' +
                    '<ul class="icons-list text-nowrap">' +
                    '<li>' +
                    '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>' +
                    '<ul class="dropdown-menu dropdown-menu-right">' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="To"><i class="icon-mail5 pull-right"></i> To</a></li>' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="Signer"><i class="icon-quill4 pull-right"></i> Signer</a></li>' +
                    '<li><a href="#" class="recipient-account-switch-to" data-type="CC"><i class="icon-cc pull-right"></i> CC</a></li>' +
                    '<li class="divider"></li>' +
                    '<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>' +
                    '</ul>' +
                    '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</li>' ;
                $('#recipient-list').append(content);
                invite++;
            });

        }
    }

    // function drawWorkFlow(document_workflow, oDocument) {
    //     var current_stage = document_workflow.wf_current_stage_id;
    //     var work_flow = document_workflow.work_flow;
    //     var stages = document_workflow.work_flow.wf_stage;
    //
    //     $.each(stages, function (stage_id, stage) {
    //
    //         if (current_stage !== stage.id) {
    //             return true;
    //         }
    //
    //         var states = stage.wf_state;
    //         drawSigners(oDocument, null);
    //
    //         $.each(states, function (state_id, state) {
    //             var state_actions = state.wf_state_action;
    //
    //             $.each(state_actions, function (state_action_id, action) {
    //                 var action = action.wf_action;
    //
    //                 if (action.name == 'Sign') {
    //                     //drawSigners(oDocument,state_id);
    //                 }
    //             });
    //
    //         });
    //
    //     });
    //
    // }

    function drawSigners(data, state_id) {

        var signer_accounts = data.signer_account;
        $.each(signer_accounts, function (account_id, signer_account) {
            var account = signer_account.account;
            var html = '<div class="sign_item">';

            if (signer_account.desciption == undefined) {
                html += signer_account.account.user.first_name + ' ' + signer_account.account.user.last_name;
            } else {
                html += signer_account.desciption;
            }

            if (signer_account.is_sign == 1) {
                html += '<img src="' + signer_account.document_sign_data.sign_image + '" class="sign_image" width="100%"/>';
            } else if (signer_account.is_sign == 0) {
                html += '<div class="input_signer"' +
                    'style="display: inline-block;">' +
                    '<img src="/assets/images/sign_here.png"' +
                    'class="sign"' +
                    'id="sign_' + signer_account.account_id + '"' +
                    'data-id="' + signer_account.account_id + '"' +
                    'data-state_id="' + state_id + '"' +
                    'style="width: 30px;cursor: pointer"/>' +
                    '</div>';
            }

            html += '</div>';
            $('.workflow_action').append(html);
            $('.signer_document_editor').append(html);


        });


        var signer_accounts = data.signer_invitations;
        $.each(signer_accounts, function (account_id, signer_account) {
            var account = signer_account.account;
            var html = '<div class="sign_item">';

            if (signer_account.desciption == undefined) {
                html += signer_account.invitations.email;
            } else {
                html += signer_account.desciption;
            }

            if (signer_account.is_sign == 1) {
                html += '<img src="' + signer_account.document_sign_data.sign_image + '" class="sign_image" width="100%"/>';
            } else if (signer_account.is_sign == 0 && signer_account.account_id == globals.account_id && data.status == 'inprogress') {
                html += '<div class="input_signer"' +
                    'style="display: inline-block;">' +
                    '<img src="/assets/images/sign_here.png"' +
                    'class="sign"' +
                    'id="sign_' + signer_account.account_id + '"' +
                    'data-id="' + signer_account.account_id + '"' +
                    'data-state_id="' + state_id + '"' +
                    'style="width: 30px;cursor: pointer"/>' +
                    '</div>';
            }

            html += '</div>';
            $('.workflow_action').append(html);
            $('.signer_document_editor').append(html);


        });


    }

    function drawAttachment(files) {
console.log('----------------');
console.log(files);
console.log('----------------');
        var html = '';
        var html_sidebar = '';
        $.each(files, function (key, file) {
            html += '<li>' +
                '<span class="mail-attachments-preview">' +
                '<i class="icon-file-picture icon-2x"></i>' +
                '</span>' +
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li><a target="blank"' +
                'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">View</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</li>';
            html_sidebar += '<div class="filelist">' +
                '<div id="o_1bva2amgirlau6d1r2n1t4b3t8" class="alert alert-file"><div class="filename">' + file.file.name +
                '<button type="button" class="close cancelUpload" style="display: block;" data-id="' + file.file.id +  '">×</button>' +
                '</div><div class="progress" style="display: none;"><div class="progress-bar" style="width: 100%;"></div></div>' +
                '<img src="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" class="img-responsive img-thumbnail">' +
                '</div>' +
                '<input type="hidden" name="my_uploader_id_files[]" value="' + file.file.id +  '" id="o_1bva2amgirlau6d1r2n1t4b3t8-hidden">' +
                '</div>';
        });
        $('.mail-attachments').html(html);
        $('.filelist').html(html_sidebar);



    }
});
