<?php

namespace App\Services;

use ClientAPI;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Document extends \App\Services\Services
{

    public static function getDocumentById($aParams)
    {

        $oServices = Parent::getInstance();

        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("document/" . $aParams['document_id'])
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function getDocumentForwarded($forward_id)
    {
        $oServices = Parent::getInstance();

        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("document/" . $forward_id . '/forwarded' )
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {

            return false;
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function getPdf($id)
    {
        $oServices = Parent::getInstance();

        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("document/" . $id . '/pdf')
                ->setBody([])
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {
dd($e->getMessage());
            return '';
        }


        return $oClient;
    }

    public static function getDraftPDF($id)
    {
        $oServices = Parent::getInstance();

        if (!$id) {
            $aParams['content'] = ' ';
            try {

                $oClient = $oServices
                    ->setMethod('post')
                    ->setUrl("draft/pdf")
                    ->setBody($aParams)
                    ->withToken()
                    ->request()
                    ->getContent();
            } catch (\Exception $e) {
                return $e;
            }

            return $oClient;

        }

        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("draft/" . $id . "/pdf")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $e) {
            return $e;
        }

        return $oClient;

    }

    public static function getDraftDocumentById($aParams)
    {
        $oServices = Parent::getInstance();

        try {

            $oClient = $oServices
                ->setMethod('get')
                ->setUrl("draft/" . $aParams['document_id'])
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {

            return false;
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function getDocumentWorkFlow($workflow_id)
    {
        $oServices = Parent::getInstance();


        try {

            $oClient = $oServices
                ->setMethod('GET')
                ->setUrl("workflow/" . $workflow_id)
                ->setBody(['workflow_id' => $workflow_id])
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function checkDocumentSign($aParams)
    {
        $oServices = Parent::getInstance();

        if (is_array($aParams)) {
            $nDocumentID = $aParams['id'];
            $nAccountID = $aParams['account_id'];
            $nSign = $aParams['sign'];
        } elseif (is_numeric($aParams)) {
            $nDocumentID = $aParams;
            $nAccountID = '';
        } else {
            return false;
        }
        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("documentSignee/checkDocumentSign")
                ->setBody(['account_id' => $nAccountID, 'document_id' => $nDocumentID, 'sign' => $nSign])
                ->request()
                ->getContent();

        } catch (\Exception $e) {

            return false;
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function getDocumentByOutboxNumber($aParams)
    {
        $oServices = Parent::getInstance();

        if (is_array($aParams)) {
            $sOutboxNumber = $aParams['sOutboxNumber'];
            $nAccountID = $aParams['account_id'];
        }

        try {

            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("documents/getDocumentByOutboxNumber")
                ->setBody(['account_id' => $nAccountID, 'sOutboxNumber' => $sOutboxNumber])
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function sendDocument($params)
    {
        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("document/")
                ->setBody($params)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];

    }

    public static function forwardDocument($params)
    {
        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("document/forward")
                ->setBody($params)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];

    }

    public static function Lock($aParams=[])
    {

        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl('document/' . $aParams['id'] . '/lock')
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];
            
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];

    }


    public static function insertNewDraft($aParams=[])
    {

        $oServices = Parent::getInstance();
        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl('document/draft')
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();
        } catch (\Exception $exception) {
            $response_array = [
                'data' => ['draft_id'=>false,'error' => $exception->getMessage()]
            ];

            return $response_array['data']['draft_id'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data']['draft_id'];

    }

    public function sign($params)
    {

        $oServices = Parent::getInstance();
        $params['id'] = currentAccountID();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("account/sign")
                ->setBody($params)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {

            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ];
            return $response_array['data'];
        }


        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public function sendDraft($aParams)
    {

        $oServices = Parent::getInstance();
        $aParams['draft'] = true;

        try {
            $oClient = ClientAPI::
                setMethod('POST')
                ->setUrl("document/")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ];

            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }


    public function deleteDraft($aParams)
    {

        $oServices = Parent::getInstance();

        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("document/draft/delete")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => []
            ];

            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public function setStatus($params)
    {

        $oServices = Parent::getInstance();
        $params['account_id'] = currentAccountID();
        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("documents/setStatus")
                ->setBody($params)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ];
            return $response_array['data'];
        }
        $response_array = json_decode($oClient, true);

        return $response_array['data'];
    }

    public static function getNotes($aParams){

        $oServices = Parent::getInstance();
        try {
            $oClient = $oServices
                ->setMethod('GET')
                ->setUrl("document/" . $aParams['document_id'] . '/notes')
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ];
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function getForwardNotes($aParams){

        $oServices = Parent::getInstance();
        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("document/forward_note")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ];
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function savePositionOfNote($aParams){

        $oServices = Parent::getInstance();
        try {
            $oClient = $oServices
                ->setMethod('POST')
                ->setUrl("document/notes/savePosition")
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

        } catch (\Exception $exception) {

            $response_array = [
                'data' =>  ['message' => $exception->getMessage()]
            ];
            return $response_array['data'];
        }

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }


    public static function getJson($aParams=[]) {

            $oClient = ClientAPI::
                setMethod('get')
                ->setUrl("user/" . nUserID() . '/inbox')
                ->setBody($aParams)
                ->withToken()
                ->request()
                ->getContent();

            $response_array = json_decode($oClient, true);
            return $response_array['data'];
        }

    public static function checkAccess($id){
            try{
                $oClient = ClientAPI::
                setMethod('get')
                    ->setUrl("document/access/" . $id )
                    ->withToken()
                    ->request()
                    ->getContent();

            }catch (\Exception $exception){
                 return false;
            }

            $response_array = json_decode($oClient, true);
            return $response_array['data'];
        }




    public static function getDocumentDetails($aParams=[]) {

        $oClient = ClientAPI::
        setMethod('post')
            ->setUrl("document/details")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent();

        $response_array = json_decode($oClient, true);
        return $response_array['data'];
    }

    public static function getDocumentAttachments($aParams=[]) {

        $oClient = ClientAPI::
        setMethod('post')
            ->setUrl("document/attachments")
            ->setBody($aParams)
            ->withToken()
            ->request()
            ->getContent();
        $response_array = json_decode($oClient, true);
        return $response_array['data']['aFiles'];
    }


    public static function import($aParams){

        try{
            $oClient = ClientAPI::
                setMethod('post')
                ->setUrl("document/import")
                ->withToken()
                ->setBody($aParams)
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['error' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

}