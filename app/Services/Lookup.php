<?php

namespace App\Services;

class Lookup extends \App\Services\Services
{



    public static function getCountries( $aParams = [] ) {

        $oServices = Parent::getInstance();
        $url   = 'lookup/countries' ;
        if(!isset($aParams['lang'])) {
            $aParams = ['lang'=>\App::getLocale()] ;
        }
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->setQuery($aParams)
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            return $exception->getMessage() ;
        }
        return json_decode($oClient,true);
    }

    public static function getBranchType( $aParams = [] ) {

        $oServices = Parent::getInstance();
        $url   = 'lookup/branch_types' ;
        if(!isset($aParams['lang'])) {
            $aParams = ['lang'=>\App::getLocale()] ;
        }
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl($url)
                ->setQuery($aParams)
                ->request()
                ->getContent() ;

        }catch (\Exception $exception){

            return $exception->getMessage() ;
        }
        return json_decode($oClient,true);
    }


}