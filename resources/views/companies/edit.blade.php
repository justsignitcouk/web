@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

   <div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.edit") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

   <div class="panel-body">

	   <ul class="breadcrumb">
		   <li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
		   <li><a href="{{route('admin.companies.show' , $company['id'])}}"><i class="position-left"></i>{{ $company['name'] }}</a></li>
		   <li class="active">{{ __("common.edit") }}</li>
	   </ul>

	    <div class="tabbable">

        @include('layouts/partials/tabs/companies_tabs')

        {!! Form::model($company , ['method' => 'POST', 'url' => route('admin.companies.update',$company['id']), 'class' => '', 'files' => true]) !!}

            @include('companies.form')
            
        {!! Form::close() !!}

        </div>
      </div>
    </div>
</div>

@endsection