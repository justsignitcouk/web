<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function (Blueprint $table) {
			$table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('user_name', 50);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('password');
            $table->boolean('activated')->default(0);
            $table->string('token')->nullable();
            $table->string('signup_ip_address', 45)->nullable();
            $table->string('signup_confirmation_ip_address', 45)->nullable();
            $table->string('signup_sm_ip_address', 45)->nullable();
            $table->string('admin_ip_address', 45)->nullable();
            $table->string('updated_ip_address', 45)->nullable();
            $table->string('deleted_ip_address', 45)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('profile_image')->nullable();
            //$table->primary(['id','email']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users');

    }

}
