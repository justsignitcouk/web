@extends('layouts.admin')

@section('content')

<div class="col-md-12">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">{{ __("common.details") }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>



			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">

				<ul class="breadcrumb">
					<li><a href="{{route('admin.companies')}}"><i class="icon-home2 position-left"></i>{{ __("common.companies") }}</a></li>
					<li><a href="{{route('admin.companies.show' , $company[0]->id)}}"><i class="position-left"></i>{{ $company[0]->name }}</a></li>
					<li><a href="{{route('admin.branches',$company[0]->id)}}"><i class="position-left"></i>{{ __("common.branches") }}</a></li>
					<li><a href="{{route('admin.combranches.show',$company[0]->id)}}"><i class="position-left"></i>{{ $branch->name }}</a></li>
					<li><a href="{{route('admin.departments',$branch->id )}}"><i class="position-left"></i>{{ __("common.departments") }}</a></li>
					<li><a href="{{route('admin.branchdeps.show' ,$oDepartment->id)}}"><i class="position-left"></i>{{ $oDepartment->name }}</a></li>
					<li class="active">{{ __("common.show") }}</li>
				</ul>

				<div class="tab-content">
					<div class="content-group">
						<div class="col-lg-12 col-md-12">
								@include('layouts/partials/tabs/department_tabes')
						    	<div class="panel panel-body 
						    	pull-right 
						    	col-md-12">

									<table class="table">

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.name") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												@if(!empty($oDepartment->name))
															{{$oDepartment->name}}
														@else
															--
														@endif
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.description") }}:</label>
											</td>
											<td>

												<span class="pull-right-sm">
												@if(!empty($oDepartment->description))
															{{$oDepartment->description}}
														@else
															--
														@endif
												</span>

											</td>

										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("departments.parent_department") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
												{{$aDepartment['parent_department_name']}}
														@if(!empty($aDepartment['parent_department_name']))

														@else
															--
														@endif

												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.created_at") }}:</label>
											</td>
											<td>
												<span class="pull-right-sm">
													<?php

														$date = new DateTime($aDepartment['created_at']) ;
													    echo $date->format('g:ia \o\n l jS F Y');
													?>
												</span>
											</td>
										</tr>

										<tr class="form-group mt-5">
											<td>
												<label class="text-semibold">{{ __("common.logo") }}:</label>
											</td>
											<td>
												<div class="pull-right-sm">
													<img src="{{cdn_url($sLogoName,$sLogoPath)}}" alt="" style="max-width: 115px;max-height: 90px;">
												</div>
											</td>
									    </tr>

									</table>

							  	</div>
		             	</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

@endsection
