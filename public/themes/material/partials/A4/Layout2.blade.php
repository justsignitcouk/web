<style>
    /*.header_and_footer {*/
    /*cursor: pointer;*/
    /*}*/

    .header_page {
        min-height: 0.5in;
        /*border-bottom: 1px solid #ccc;*/
    }

    .footer_page {
        position: absolute;
        bottom: 0px;
        min-height: 90px;
        border-top: 1px solid #ccc;
        z-index: 1;
        width: 90%;
    }

    .content_page {
        font-size: 16px;
        font-family: Arial;
    }

    .subject_page {
        font-size: 18px;

    }

    .action_container {
        box-shadow: 0 0 1px #ccc;
        height: 600px;
        padding: 5px 10px;
    }

    .subject_document_editor {

        display: inline-block;
        width: 649px;
        margin: 5px 0;
    }

    .to_document_editor {

        width: 400px;
        margin: 5px auto;
        text-align: center;
        font-size: 18px;
    }

    .cc_document_editor {
        margin: 5px 0;
        text-align: right;
        font-size: 18px;
    }

    .signer_document_editor {
        width: 100%;
        min-height: 100px;
        margin: 5px 0;
        text-align: left;
        font-size: 18px;
    }

    .editable {

        margin: 5px 0;
        min-height: 300px;

    }

    .sign_item {
        /*width:200px;*/
        display: inline-block;
        height: 80px;
        text-align: center;
        vertical-align: top;
        height: 100%;
        margin: 10 auto;

    }

    .editor .edit_dotted {
        border: 1px dotted dodgerblue;
        min-height: 35px;
    }

    .cc_document_editor {
        font-size: 12px;

    }

    .sidebar-default {
        border: none;
    }



    .mce-content-body {
        max-heigh: 450px;
    }

    .img-thumbnail {
        max-height: 90px;
    }

    .left_side {
        text-align: left;
        direction: ltr;
    }

    .left_side * {
        direction: ltr;
        text-align: left;
    }

    .right_side {
        text-align: right;
        direction: rtl;
    }

    .document_date {
        width: 430px;

    }

    .document_number {
        width: 420px;
        vertical-align: middle;

    }

    .document_number * {
        vertical-align: middle;
    }

    .to_page {
        text-align: center;
        vertical-align: middle;
    }

    .to_title {
        vertical-align: middle;
    }

    .to_document_editor {
        vertical-align: middle;
    }

    #document_number_text {
        display: inline-block;
        overflow: hidden;
        width: 280px;
        vertical-align: middle;
    }

    #document_date_text {
        display: inline-block;
        overflow: hidden;
        width: 280px;
        vertical-align: middle;
    }

    .page-container {
        width: 99%;
    }

    .header_top {
        position: absolute;
        top: 5px;
        left: 5px;
        text-align: center;
        color: #ccc;
    }

    .barcode {
        text-align: center;
    }

    .layout_box {
        padding: 10px 20px;
        border: 1px #ccc solid;
        margin: 10px 20px;
        box-shadow: 1px 1px 3px #ccc;
        direction: rtl;
        cursor: pointer;
    }

    .padd_20 {
        padding: 0 20px;
        position: relative
    }

    .signer_container{
        display:inline-block;
        position: relative;
        min-width: 250px;
        margin: 5px 10px;
        height:110px;
    }

    .signs_page{
        width:100%;
        height:110px;
    }

    .signer_sign_img{
        width: 240px;
        position: absolute;
        margin: 0 auto;
        top: 0px;
        left: 50%;
        margin-left: -130px;
    }

</style>
<?php
if(!isset($bContentEditable)) {
    $bContentEditable = false;
}
?>

<div class="paper A4 editor" id="document" data-id="{{isset($id) ? $id : '' }}" style="direction:ltr;padding: 0;zoom:1;margin: 0 auto;">
    <section class="sheet padding-10mm sticky-note">

        <div class="header_and_footer">
            <div class="header_and_footer header_page">
                <div class="header_container">
                    <div class="a4_header" style="height:150px;background: #F2F2F2;text-align: center;padding: 10px;margin-bottom: 0" >
                        <img src="{{ config()->get('app')['url'] }}/assets/images/mycompose_logo.png" width="200" style="display: inline-block;vertical-align:middle" />
                    </div>

                </div>
            </div>
        </div>

        @partial("A4.Content",['aDocument'=>$aDocument,'id'=>$id,'bContentEditable'=>$bContentEditable ])

        <div class="header_and_footer footer_page">
            <div class="footer_container">
                <div style=""></div>
            </div>
        </div>
    </section>
</div>



@foreach($aDocument['files'] as $file)

    <?php
    $ext = pathinfo($file['file']['name'], PATHINFO_EXTENSION);

    $type = $ext;
    if(in_array(mb_strtolower($ext),['png','jpg','jpeg','gif',''])){
        $type = 'image' ;
    }
    ?>

    <br/><br/>

    <div class="paper A4 editor" id="document" data-id="{{isset($id) ? $id : '' }}" style="direction:ltr;padding: 0;zoom:1;margin: 0 auto;">
        <section class="sheet padding-10mm sticky-note">
            <p style="text-align:center">{{$file['file']['name']}}</p>
            @if($type=='image')
                <img src="{{ $file['file']['url'] . '/files/' . $file['file']['path'] . '/' . $file['file']['hash_name']  }}" style="width:100%" />
            @endif
            <a href="{{ $file['file']['url'] . '/files/' . $file['file']['path'] . '/' . $file['file']['hash_name']  }}" target="blank">Click here to view</a>
        </section>
    </div>
@endforeach


<script>
    var viewAjax = '{{route('document.viewAjax',[$id])}}';
    {{--//var editor = '{{$editor}}' ;--}}
</script>

