<?php


namespace App\Services;

use App\Models\AccountPermission;
use DB ;


class AccountsService{


	function __construct(){


    }

    public function checkedAccountPermissions($account_id){

        return      AccountPermission::
                    select('permission_id')
                    ->where('account_id',$account_id)
                    ->get()
                    ->toArray(); 

    }

	
    public function Permissions($module_name){

        return  DB::table('modules')
                ->join('permissions','permissions.module_id','=','modules.id')
                ->select('modules.name','permissions.*')
                ->where('modules.name','=',$module_name)
                ->get();

    }



}