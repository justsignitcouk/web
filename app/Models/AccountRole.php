<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountRole
 */
class AccountRole extends Model
{
    protected $table = 'account_role';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'role_id',
        'active'
    ];

    protected $guarded = [];

        
}