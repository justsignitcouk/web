<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TranslatorLanguage
 */
class TranslatorLanguage extends Model
{
    protected $table = 'translator_languages';

    public $timestamps = true;

    protected $fillable = [
        'locale',
        'name',
        'direction'
    ];

    protected $guarded = [];

        
}