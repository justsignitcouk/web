<?php

return [

    /*
    |--------------------------------------------------------------------------
    | UsersController Language Lines
    |--------------------------------------------------------------------------
    */

    'permission' => 'Permission',
    'status'     => 'Status',
    'permissions'=> 'Permissions',

];