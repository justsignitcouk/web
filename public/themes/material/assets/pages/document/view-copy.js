/**
 * Created by Bayan on 11/16/2017.
 */
function drawAttachment(files) {

    var html = '<div>';
    $.each(files, function (key, file) {
        // html += '<div class="paper page A4">' ;

        var file_name = file.file.hash_name;
        var file_name_split = file_name.split(".");

        var ex = file_name_split[1];
        if( $.inArray(ex, ['png','jpg','gif','jpeg','pcx','pic','bmp','psp','pict']) != -1)
        {/*html += '<div class="paper page A4">'*/ ;
            html += '<div class="paper page A4">'+'<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>';
            html += '<span class="mail-attachments-preview">' +
                '<i class="icon-file-picture icon-2x"></i>' +
                '</span>' +
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li><a id="img_attached" target="blank"' +
                'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ view +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                // '</div>' +
                // '</div>' +
                '</div>';

        }
        /*else if( $.ex = 'txt'){
         html += '<div class="paper page A4">'+
         '<div class="mail-attachments-content">' +
         '<span class="text-semibold">' + file.file.name + '</span>' +
         '<ul class="list-inline list-inline-condensed no-margin">' +
         '<li class="text-muted">174 KB</li>' +
         '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-text2 icon-2x">'+'</i>' + ' ' +'.'+ ex +'</a>' +
         '</li>' +
         '<li><a href="#" class="hide">Download</a></li>' +
         '</ul>' +
         '</div>' +
         // '</div>' +
         // '</div>' +
         '</div>' ;


         }*/
        else if( $.ex = 'pdf'){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-pdf">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['doc','dot','rtf','wri','mcw','wpd']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-word-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['xl','xls','xlt','xla']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-excel-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['ppt','pps']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-powerpoint-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['mdb','adp','mda','mde','ade','db','dbf']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        /*else if( $.inArray(ex, ['mdb','adp','mda','mde','ade','db','dbf']) != -1){
         html += '<div class="paper page A4">';
         html +=
         '<div class="mail-attachments-content">' +
         '<span class="text-semibold">' + file.file.name + '</span>' +
         '<ul class="list-inline list-inline-condensed no-margin">' +
         '<li class="text-muted">174 KB</li>' +
         '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-video-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
         '</li>' +
         '<li><a href="#" class="hide">Download</a></li>' +
         '</ul>' +
         '</div>' +
         '</div>' ;
         }*//*inArray(, ['avi','mov','mpg','mpeg','mpa','asx','wm','wma','wmv','dat','ram','rm','avi','mp4']) != -1*/
        else if( $.ex='mp4'){
            html += '<div class="paper page A4">'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-video-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['wav','mid','mda','mp3','snd','au']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['ZIP','RAR','ARJ','LZH','ACE','TAR','GZIP','JAR','IOS','UUE','BZ2']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else{
            html += '<div class="paper page A4">'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +

                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }



    });
    // html += '<div>';
    $('.attachments').html(html);


}




function drawAttachment(files) {

    var html = '<div>';
    $.each(files, function (key, file) {
        html += '<div class="paper page A4">' ;
        var file_name = file.file.hash_name;
        var file_name_split = file_name.split(".");

        var ex = file_name_split[1];
        switch (ex){


         case['png','jpg','gif','jpeg']:

            html +='<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>';
            html += '<span class="mail-attachments-preview">' +
                '<i class="icon-file-picture icon-2x"></i>' +
                '</span>' +
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li><a id="img_attached" target="blank"' +
                'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ view +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>';
             break;

            case"exe":
            html += '<a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-pdf icon-2x">'+'</i>' + ' ' +'.'+ ex +'</a>' ;
                break;
        }



    });
    html += '<div>';
    $('.attachments').html(html);


}






switch (ex){

    case "Banana":
        text = "Banana is good!";
        break;
    case "Orange":
        text = "I am not a fan of orange.";
        break;
    case "Apple":
        text = "How you like them apples?";
        break;
    default:
        text = "I have never heard of that fruit...";
}



function drawAttachment(files) {

    var html = '<div>';
    $.each(files, function (key, file) {
        html += '<div class="paper page A4">' ;
        var file_name = file.file.hash_name;
        var file_name_split = file_name.split(".");

        var ex = file_name_split[1];
        if( $.inArray(ex, ['png','jpg','gif','jpeg']) != -1)
        {
            html +='<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>';
            html += '<span class="mail-attachments-preview">' +
                '<i class="icon-file-picture icon-2x"></i>' +
                '</span>' +
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li><a id="img_attached" target="blank"' +
                'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ view +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>';

        }else{
            html += '<a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-pdf icon-2x">'+'</i>' + ' ' +'.'+ ex +'</a>' ;
        }



    });
    html += '<div>';
    $('.attachments').html(html);


}




function drawAttachment(files) {

    var html = '<div>';
    $.each(files, function (key, file) {
        // html += '<div class="paper page A4">' ;

        var file_name = file.file.hash_name;
        var file_name_split = file_name.split(".");

        var ex = file_name_split[1];

        if( $.inArray(ex, ['png','jpg','gif','jpeg','pcx','pic','bmp','psp','pict']) != -1)
        {/*html += '<div class="paper page A4">'*/ ;
            html += '<div class="paper page A4">'+'<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>';
            html += '<span class="mail-attachments-preview">' +
                '<i class="icon-file-picture icon-2x"></i>' +
                '</span>' +
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li><a id="img_attached" target="blank"' +
                'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ view +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                // '</div>' +
                // '</div>' +
                '</div>';

        }
        else if( ex =='mp4'){
            html += '<div class="paper page A4">'+
                '<iframe class="vedio_attached" type="" controls="controls"  autoplay="autoplay" src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/  >'+'</iframe>'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-video-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;

        }
        else if( ex == 'txt'){
            html += '<div class="paper page A4">'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-text2 icon-2x">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                // '</div>' +
                // '</div>' +
                '</div>' ;


        }
        else if( ex =='mp4'){
            html += '<div class="paper page A4">'+
                '<video src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>'+'</video>'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-video-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;

        }
        else if( ex == 'pdf'){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-pdf">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['doc','dot','rtf','wri','mcw','wpd']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-word-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['xl','xls','xlt','xla']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-excel-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['ppt','pps']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-file-powerpoint-o">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else if( $.inArray(ex, ['mdb','adp','mda','mde','ade','db','dbf']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        /*inArray(, ['avi','mov','mpg','mpeg','mpa','asx','wm','wma','wmv','dat','ram','rm','avi','mp4']) != -1*/

        else if( $.inArray(ex, ['wav','mid','mda','mp3','snd','au']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }

        else if( $.inArray(ex, ['ZIP','RAR','ARJ','LZH','ACE','TAR','GZIP','JAR','IOS','UUE','BZ2']) != -1){
            html += '<div class="paper page A4">';
            html +=
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +
                '<li class="text-muted">174 KB</li>' +
                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="fa fa-database">'+'</i>' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }
        else{
            html += '<div class="paper page A4">'+
                '<div class="mail-attachments-content">' +
                '<span class="text-semibold">' + file.file.name + '</span>' +
                '<ul class="list-inline list-inline-condensed no-margin">' +

                '<li>'+'<a  target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">' + ' ' +'.'+ ex +'</a>' +
                '</li>' +
                '<li><a href="#" class="hide">Download</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>' ;
        }



    });
    html += '<div>';
    $('.attachments').html(html);


}





switch (ex){


    case['png','jpg','gif','jpeg']:

        html +='<div class="paper page A4">'+'<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>'+ '<span class="mail-attachments-preview">' +
            '<i class="icon-file-picture icon-2x"></i>' +
            '</span>' +
            '<div class="mail-attachments-content">' +
            '<span class="text-semibold">' + file.file.name + '</span>' +
            '<ul class="list-inline list-inline-condensed no-margin">' +
            '<li class="text-muted">174 KB</li>' +
            '<li><a id="img_attached" target="blank"' +
            'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ view +'</a>' +
            '</li>' +
            '<li><a href="#" class="hide">Download</a></li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        break;

    case"exe":
        html +='<div class="paper page A4">'+'<img src="'+ globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '" width="100"/>'+ '<span class="mail-attachments-preview">' +
            '<i class="icon-file-picture icon-2x"></i>' +
            '</span>' +
            '<div class="mail-attachments-content">' +
            '<span class="text-semibold">' + file.file.name + '</span>' +
            '<ul class="list-inline list-inline-condensed no-margin">' +
            '<li class="text-muted">174 KB</li>' +
            '<li><a id="img_attached" target="blank"' + 'href="' + globals.CDN_HOST + '/files/' + file.file.path + '/' + file.file.hash_name + '">'+ '<i class="icon-file-pdf icon-2x">'+'</i>' + ' ' +'.'+ ex +'</a>' +
            '</li>' +
            '<li><a href="#" class="hide">Download</a></li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        break;
}