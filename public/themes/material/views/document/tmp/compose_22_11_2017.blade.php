{!! Theme::asset()->themePath()->add([ ['select2', 'js/plugins/forms/selects/select2.min.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['form_select2', 'js/pages/form_select2.js',['select2']]]) !!}
{!! Theme::asset()->add([ ['tinymce', 'assets/components/tinymce/js/tinymce/tinymce.min.js',['form_select2']]]) !!}
{!! Theme::asset()->themePath()->add([ ['interactions', 'js/core/libraries/jquery_ui/interactions.min.js',['tinymce']]]) !!}
{{--
{!! Theme::asset()->themePath()->add([ ['mail_list_write', 'js/pages/mail_list_write.js',['interactions']]]) !!}
--}}
{!! Theme::asset()->themePath()->add([ ['plupload_full', 'js/plugins/uploaders/plupload/plupload.full.min.js',['tinymce']]]) !!}
{!! Theme::asset()->themePath()->add([ ['plupload_queue', 'js/plugins/uploaders/plupload/plupload.queue.min.js',['plupload_full']]]) !!}

{!! Theme::asset()->themePath()->add([ ['daterangepicker', 'js/plugins/pickers/daterangepicker.js',['bootstrap']]]) !!}
{!! Theme::asset()->themePath()->add([ ['compose', 'pages/document/compose.js',['daterangepicker']]]) !!}

{{--jquery.tinymce.min.js--}}

<link href="{{ asset('/assets/components/paper-css/paper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/components/paper-css/paper_tools.css') }}" rel="stylesheet" type="text/css">



<div class="content-wrapper">


    <!-- Content area -->
    <div class="content ">
        <div class="modal fade layout_modal" data-route="{{route('document.getLayouts')}}" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                </div>
            </div>
        </div>

        {{--                -moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;"
                        unselectable="on"
                        onselectstart="return false;"
                        onmousedown="return false;"--}}
        <div class="row">

            <div class="a4_paper_container" style="width: auto;width: 210mm;margin: 0 auto;">
                <div class="paper_container">

                    <div class="paper_tools" style="display:none">
                        <div class="paper_tools_elements">
                        </div>
                    </div>
@partial('document.compose.a4',['id'=> isset($id) ? $id : false ,'route'=> isset($route) ? $route : '' ])
</div>
</div>

</div>
</div>
<!-- /content area -->

</div>


@partial('compose_secondary_sidebar',['id'=> isset($id) ? $id : false ,'route'=> isset($route) ? $route : '' ])

<script>
    var globals_route;
    var globals_routeSendDocument = '{{ route('document.sendDoc') }}';
    var globals_routeSendDraft = '{{ route('document.sendDraft') }}';
    var globals_route_getLayout = '{{ route('document.getLayout',['id' => 1]) }}';
</script>