<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CompaniesController Language Lines
    |--------------------------------------------------------------------------
    */

    'shortname' => 'Companies Managment',
    'name' => 'Name',
    'number_of_branches' => 'Number of Branches',
    'number_of_departments' => 'Number of Departments',
    'number_of_accounts' => 'Number of Accounts',
    'add_company'   => 'Add Company',
    'manage_companies' => 'Manage Companies',
    'license_no' => 'License Number',
    'logo'=>'Logo',
    'companies'=>'Companies',
    'delete_company' => 'Are you sure you want delete this company',
    'cant_delete_company'=>'Sorry you can not delete company whic contains branches',
    'branches'=>'Branches',
    'departments'=>'Departments',


];
