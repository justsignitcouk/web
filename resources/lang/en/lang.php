<?php
return[

    "Size"=> "Size",
    "Format"=> "Format",
    "clear"=>"Clear",
    "save"=> "Save",
    "browse"=> "Browse",
    "upload"=> "Upload",
    "submit"=> "Submit",
    "contact_us"=> "Contact Us",
    "contacts"=> "Contacts",
    "block"=> "Block Buttons",
    "print"=> "Print",
    "msg"=>"Messages",
    "users"=> "Users",
    "close"=> "Close",
    "my account"=> "My Account",
    "my profile"=> "My Profile",
    "setting"=> "Setting",
    "logout"=>"Logout",
    "email_or_phone"=>"Email Or Phone Number",
    "password"=>"Password",
    "mobile"=>"Mobile",
    "search"=>"Search",
    "link_account"=>"link account",
    "Unlink"=> "Unlink",
    "Make_default"=> "Make Default",
    "email_add"=>"Email Address",
        "link"=>"link",
    "close"=> "Close",
    "activity_header"=>"Where You're Logged In",
    "Device"=>"Device",
    "Platform"=>"Platform",
    "Platform_Version"=>"Platform Version",
    "Browser"=>"Browser",
    "IP"=>"IP",
    "Last_Active"=>"Last Active",
    "title"=>"Title:",
    "doc_num"=>"Document number:",
    "doc_date"=>"Document Date:",
    "save_as_template"=>"Save As Template",
    "date"=>"Date:",
    "letter_num"=>"Letter Number",
    "to"=>"To",
    "cc_to"=>"Cc:",
    "Export_to_pdf"=>"Export to .pdf",
    "Export_to_csv"=>"Export to .csv",
    "Export_to_doc"=>"Export to .doc",
    "show"=>"Show",
"phone"=>"Phone",
    "inbox"=>"Inbox",
    "pening"=>"Pending",
    "sent"=>"Sent",
    "completed"=>"Completed",
    "draft"=>"Draft",
    "rejected"=>"Rejected",
    "Notifications"=>"Notifications",
    "view"=>"View"
];