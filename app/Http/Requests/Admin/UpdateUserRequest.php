<?php

namespace App\Http\Requests\Admin;

use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


    if (ctype_space( Request::get('password'))) {


       return [
                'username'=>'required|unique:users,username,'.$this->id,
                'first_name'=>'required|
                               regex:/^[^÷!@#$%^&*()~]+$/|
                               regex:/^\S*$/|
                               regex:/^([^0-9]*)$/',
                 'last_name'=>'required|
                               regex:/^[^÷!@#$%^&*()~]+$/|
                               regex:/^\S*$/|
                               regex:/^([^0-9]*)$/',
                'email'=>'required|email|unique:users,email,'.$this->id,
                'password'=>'required|confirmed|nullable|min:6|

                regex:/[a-z]/|
                regex:/[A-Z]/|
                regex:/[0-9]/|

                ',
                'password_confirmation' => '',
                'profile_image' => 'mimes:jpeg,jpg,png',
              ];

    }

    return [
              'username'=>'required|unique:users,username,'.$this->id,
              'first_name'=>'required|
                             regex:/^[^÷!@#$%^&*()~]+$/|
                             regex:/^\S*$/|
                             regex:/^([^0-9]*)$/',
               'last_name'=>'required|
                             regex:/^[^÷!@#$%^&*()~]+$/|
                             regex:/^\S*$/|
                             regex:/^([^0-9]*)$/',
              'email'=>'required|email|unique:users,email,'.$this->id,
              'password'=>'confirmed|nullable|min:6|

              regex:/[a-z]/|
              regex:/[A-Z]/|
              regex:/[0-9]/|

              ',
              'password_confirmation' => '',
              'profile_image' => 'mimes:jpeg,jpg,png',
              
            ];
        

    }


   
}
