
{!! Theme::asset()->themePath()->add([ ['steps', 'js/plugins/forms/wizards/steps.min.js',['jquery']]]) !!}
{!! Theme::asset()->themePath()->add([ ['cookie', 'js/plugins/extensions/cookie.js',['steps']]]) !!}
{!! Theme::asset()->themePath()->add([ ['wizard_static', 'partials/document/wizard/static.js',['steps']]]) !!}

<!-- Saving state -->
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title">Work Flow</h6>
        <div class="heading-elements">
            <ul class="icons-list">
             </ul>
        </div>
    </div>
    <form class="steps-state-saving" action="#">
        <h6>Write Document</h6>
        <fieldset>

        </fieldset>

        <h6>Review</h6>
        <fieldset>

        </fieldset>

        <h6>Waiting Signtures</h6>
        <fieldset>

        </fieldset>

        <h6>Receive Recipients</h6>
        <fieldset>



        </fieldset>
    </form>
</div>