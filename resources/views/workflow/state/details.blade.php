@extends('layouts.admin')

@push('scripts')

    {{--Select--}}
    <script type="text/javascript"
            src="{{ asset('/theme/default/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/theme/default/assets/js/pages/form_select2.js') }}"></script>


    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/components/jquery-ui/style.css')}}">
    {{--    <script src="{{asset('/assets/components/jquery-ui/external/jquery/jquery.js')}}"></script>--}}
    <script src="{{asset('/assets/components/jquery-ui/jquery-ui.js')}}"></script>
    <script>
        $(function () {
//                $( "#accordion" ).accordion();
            $("#accordion-styled").sortable({
                // cancel: ".ui-state-disabled",
                items: ".panel:not(:first-child,:last-child)",
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');

                    // POST to server using $.post or $.ajax
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: '/stage/sortable'
                    });
                }
            });
            $("#accordion-styled").disableSelection();


            $('#delete_state_submit').on('click', function () {
                $(this).prop('disabled', true);
                $.post("{{ route('admin.state.delete',$id) }}", {'id': '{{$id}}'}, function (data) {

                    if(data.status == true ){

                    window.location = "{{ route('admin.state.index') }}";
                    console.log(data);
                    }else{
                        console.log(data);
                    }
                },'json');
            });
            $('#delete_state_model').on('hide.bs.modal', function () {
                $('#delete_state_submit').prop('disabled', false);
            });
        });
    </script>
@endpush


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">{{ __('workflow.state.details') }}</h6>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_action">
                        <i class="icon-plus3 left"></i> {{ __('workflow.action.add_action') }}
                    </button>

                    <button type="button" class="btn  btn-danger" data-toggle="modal"
                            data-target="#delete_state_model" id="delete_workflow">
                        <i class="icon-plus3 left"></i> {{ __('workflow.state.delete_state') }}
                    </button>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <table id="workflow-state-details-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
                <thead>
                <tr>
                    <th>{{ __("workflow.action.name") .' '. __("common.name") }}</th>
                    <th>{{ __("workflow.action.description") .' '. __("common.description") }}</th>
                    <th>{{ __("common.created_at")}}</th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="modal fade scrollbox" id="add_action" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.actions.add_actions') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.workflow.stateActions.store'), 'method'=>'POST', 'id'=>'add_action_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <fieldset title="2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.action.stage') .' '. __('common.name') }}: <span
                                                    class="text-danger">*</span></label><br>
                                        {!! Form::select('actions', ['' => __('common.please_choose') ] + $oAction, '', ['class' => 'form-control m-bot15']) !!}
                                        <br/><font color="red" class="validation" id="stage_name_validation"></font>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{ __('workflow.action.permits') .' '. __('common.permits') }}: <span
                                                    class="text-danger">*</span></label><br>
                                        <select required="required"
                                                class="select-multiple-tags select2-validation"
                                                id="permits[]"
                                                name="permits[]" multiple="multiple">
                                            <optgroup label="{{ __('common.roles') }}">
                                                @foreach ($oRoles as $oRole)
                                                    <option value="role_{{ $oRole->id }}">
                                                        {{ $oRole->name }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="{{ __('common.users') }}">
                                                @foreach ($oAccounts as $oAccount)
                                                    <option value="account_{{ $oAccount->id }}">
                                                        {{ $oAccount->account_name }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                            {{--<optgroup label="Departments">--}}
                                            {{--@foreach ($aDepartments as $aDepartment)--}}
                                            {{--<option value="d_{{ $aDepartment['id'] }}">{{ $aDepartment['name'] }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--</optgroup>--}}
                                        </select>
                                        <br/><font color="red" class="validation" id="stage_name_validation"></font>
                                    </div>
                                </div>




                            </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="state_id" value="{{$id}}" />
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.close')}}</button>
                        <button type="button" id="save_state_action"
                                class="btn btn-primary">{{__('common.save')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        {{--{{ model to delete workflow }}--}}
        <div class="modal fade" id="delete_state_model" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('workflow.delete') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.state.delete',$id), 'method'=>'POST', 'id'=>'delete_workflow_state_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ __('workflow.do_you_want_delete_workflow_state') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">{{__('common.no')}}</button>
                        <button type="button" id="delete_state_submit"
                                class="btn btn-primary">{{__('common.yes')}}</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--{{ model to delete workflow }}--}}
        @endsection

        @push('scripts')
            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                // Add Stage
                $(document).on("click", "#save_stage", function (e) {
                    var data = $("#add_stage_form").serializeArray(),
                        url = $("#add_stage_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add Stage

                // Add State
                $(document).on("click", "#save_state", function (e) {
                    var data = $("#add_state_form").serializeArray(),
                        url = $("#add_state_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add State

                // Add State
                $(document).on("click", "#save_state_action", function (e) {
                    var data = $("#add_action_form").serializeArray(),
                        url = $("#add_action_form").attr('action');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            validation_messages(data);
                        } else {
                            window.location.reload();
                        }
                    });

                    e.preventDefault();
                });
                // End Add State


                $(document).on("click", "#add_state_button", function (e) {
                    $('#stage_id').val($(this).attr('data-stage-id'));
                });


                $(document).on("click", "#delete_stage_button", function (e) {
                    var delete_stage_button = $(this);
                    var id = delete_stage_button.attr('data-stage-id');
                    $.ajax({
                        url: "/admin/{{url('stage')}}/" + id,
                        type: 'DELETE',
                        datatype: 'json',
                    }).done(function (data) {
                        if (data.status == false) {
                            new PNotify({
                                title: 'Alert',
                                text: data.message,
                                icon: 'icon-warning',
                                addclass: 'bg-danger'
                            });
                        } else {
                            delete_stage_button.closest('.panel').remove();
                            new PNotify({
                                title: 'Success',
                                text: data.message,
                                icon: 'icon-checkmark3',
                                addclass: 'bg-success'
                            });
                        }
                    });
                    e.preventDefault();
                });


                $(document).on("click", "#actions_state_button", function (e) {
                    $('#state_id').val($(this).attr('data-state-id'));

                });

                function validation_messages(data) {
                    $('.validation').html('');
                    $.each(data.message, function (index, data2) {

                        var field_name = index + '_validation',
                            messages = data2;
                        var html = '';
                        $.each(messages, function (index3, data3) {
                            if (html != '') {
                                html = html + '<br/>';
                            }
                            html += data3;
                        });
                        $('#' + field_name).html(html);
                    });
                }


            </script>
    @endpush

        @push('scripts')
            <script>

                $(function() {

                    $.extend( $.fn.dataTable.defaults, {
                        autoWidth: true,
                        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                        language: {
                            "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                            "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                            "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                            "sInfo":         "{{ __('components/datatables.sInfo') }}",
                            "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                            "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                            "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                            "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                            "sUrl":          "{{ __('components/datatables.sUrl') }}",
                            "oPaginate": {
                                "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                                "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                                "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                                "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                            }
                        }
                    });

                    var table = $('#workflow-state-details-table').DataTable({

                        processing: true,
                        serverSide: true,
                        ajax: {
                            "url" : '{!! route('admin.state.details.data', $id ) !!}',
                            "type": "POST"
                        },
                        columns: [
                            { data: 'action.name', name: 'action.name' },
                            { data: 'state.name', name: 'state.name' },
                            { data: 'created_at', name: 'created_at' }
                        ]
                    });

                    // $('.datatable-ajax').on('click', 'tbody td', function() {
                    //
                    //     var data = table.row(this).data();
                    //     var id = data.id ;
                    //     window.location = '/state/' + id;
                    // });
                });
            </script>

    @endpush