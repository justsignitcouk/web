<?php

namespace App\Services;

use App\Http\Middleware\TranslationMiddleware;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Jenssegers\Agent\Facades\Agent;
use ClientAPI;
class SocialMedia extends \App\Services\Services
{


    public function checkMail($email){

        $oServices = Parent::getInstance();


        $url   = 'social_media/check_mail' ;
        $aBody = ['email' => $email] ;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;
            return false ;
        }

        $response_array = json_decode($oClient,true);


        return $response_array ;

    }


    public function RegistByFaceBook($userData){

        $oServices = Parent::getInstance();


        $url   = 'social_media/regist_by_facebook' ;
        $aBody = ClientAPI::getDeviceInfo();
        $aBody['user_data'] = $userData;

        try{
            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){
            $response_array = [
                'data' => ''
            ] ;

            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);


        return true ;

    }


    public function LogInByFaceBook($userData){


        $oServices = Parent::getInstance();



        $url   = 'social_media/log_in_by_facebook' ;
        $aBody = ClientAPI::getDeviceInfo();
        $aBody['user_data'] = $userData ;


        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;


            return $exception->getMessage() ;
        }


        $response_array = json_decode($oClient,true);
        $sToken = $response_array['data']['token'];

        $oUser = $response_array['data']['aUser'];

        $sLoginType = 'user';
        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
            'nQuotaSize' => $oUser['nQuotaSize'],
        ];


        Auth::setSessions($aParams);

        return ['status' =>'success' , 'data' =>$aParams] ;


    }

    public function RegistByGoogle($userData){


        $oServices = Parent::getInstance();


        $url   = 'social_media/regist_by_google' ;

        $aBody = ClientAPI::getDeviceInfo();
        $aBody['user_data'] = $userData;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;

            dd($exception->getMessage());
            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);


        return $response_array ;

    }

    public function LogInByGoogle($userData){


        $oServices = Parent::getInstance();

        $url   = 'social_media/log_in_by_google' ;
        $aBody = ['user_data' => $userData] ;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ''
            ] ;

            return $exception->getMessage() ;
        }


        $response_array = json_decode($oClient,true);

        $sToken = $response_array['data']['token'];

        $oUser = $response_array['data']['aUser'];

        $sLoginType = 'user';
        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
            'nQuotaSize' => $oUser['nQuotaSize'],
        ];


        Auth::setSessions($aParams);

        return ['status' =>'success'] ;

    }

    public function RegistByLinkedin($userData){

        $oServices = Parent::getInstance();


        $url   = 'social_media/regist_by_linkedin' ;
        $aBody = ['user_data' => $userData] ;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;


            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);

        return $response_array ;

    }

    public function LogInByLinkedin($userData){

        $oServices = Parent::getInstance();

        $url   = 'social_media/log_in_by_linkedin' ;
        $aBody = ['user_data' => $userData] ;

        try{

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl($url)
                ->setBody($aBody)
                ->request()
                ->getContent() ;


        }catch (\Exception $exception){

            $response_array = [
                'data' => ''
            ] ;


            return $exception->getMessage() ;
        }

        $response_array = json_decode($oClient,true);

        $sToken = $response_array['data']['token'];
        $oUser  = $response_array['data']['user'];
        $oDefaultAccount = $response_array['data']['default_account'];
        $oDefaultRoles = $response_array['data']['default_account']['role'];

        $aParams = ['sToken' => $sToken,
            'oUser' => $oUser,
            'oRoles' => $oUser['aRoles'],
            'oPermissions' => $oUser['aPermissions'],
            'aProfileRequirements' => $oUser['aProfileRequirements'],
            'nQuotaSize' => $oUser['nQuotaSize'],
            'nUserTotalFilesSize' => $oUser['nUserTotalFilesSize'],
        ];

        Auth::setSessions($aParams);


        return ['status' =>'success'] ;

    }




}