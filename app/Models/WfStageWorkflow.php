<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WfStageWorkflow
 */
class WfStageWorkflow extends Model
{
    protected $table = 'wf_stage_workflow';

    public $timestamps = false;

    protected $fillable = [
        'workflow_id',
        'stage_id'
    ];

    protected $guarded = [];

        
}