@extends('layouts.admin')

@section('content')
<!-- Ajax sourced data -->
<div class="panel panel-flat">

    @include('layouts/partials/message')

    <div class="panel-heading">
        <h5 class="panel-title">{{ __("languages.manage") }}</h5>
        <div class="heading-elements hidden">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addLanguage">{{ __("languages.add_language") }}</a>
        <!-- Modal -->
        <div class="modal fade" id="addLanguage" tabindex="-1" role="dialog" aria-labelledby="addLanguageLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('languages.add_language') }}</h4>
                    </div>
                    {!! Form::open(['url'=> route('admin.languages.store'), 'method'=>'POST', 'id'=>'add_language_form', 'enctype'=>'multipart/form-data']) !!}
                    <div class="modal-body">

                        <div class="form-group">
                                <label for="email">{{ __('common.name') }}</label>
                                <input type="text" class="form-control" id="language_name_modal" value="">
                            <span id="name_error"></span>
                        </div>
                            <div class="form-group">
                                <label for="pwd">{{ __('languages.direction') }}</label>
                                {{
                                    Form::select(
                                        'directions',
                                        array_merge(['' => 'Please Select'], ['rtl'=>'RTL','ltr'=>"LTR"]),'ltr',
                                        array(
                                            'class' => 'form-control',
                                            'id' => 'language_direction_modal'
                                        ))
                                    }}
                                <span id="direction_error"></span>
                            </div>


                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('common.close') }}</button>
                        <button type="button" class="btn btn-primary save_language">{{ __('common.save') }}</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <table id="languages-table" class="table datatable-ajax responsive  dt-responsive" style="width:100%">
            <thead>
            <tr>
                <th>{{ __("common.name") }}</th>
                <th>{{ __("common.created_at") }}</th>
            </tr>
            </thead>
        </table>
    </div>
    <!-- /ajax sourced data -->

    @endsection
    @push('scripts')
        <script>

            $(function() {

                $.extend( $.fn.dataTable.defaults, {
                    autoWidth: true,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        "sProcessing":   "{{ __('components/datatables.sProcessing') }}",
                        "sLengthMenu":   "{{ __('components/datatables.sLengthMenu') }}",
                        "sZeroRecords":  "{{ __('components/datatables.sZeroRecords') }}",
                        "sInfo":         "{{ __('components/datatables.sInfo') }}",
                        "sInfoEmpty":    "{{ __('components/datatables.sInfoEmpty') }}",
                        "sInfoFiltered": "{{ __('components/datatables.sInfoFiltered') }}",
                        "sInfoPostFix":  "{{ __('components/datatables.sInfoPostFix') }}",
                        "sSearch":       "{{ __('components/datatables.sSearch') }}:",
                        "sUrl":          "{{ __('components/datatables.sUrl') }}",
                        "oPaginate": {
                            "sFirst":    "{{ __('components/datatables.oPaginate.sFirst') }}",
                            "sPrevious": "{{ __('components/datatables.oPaginate.sPrevious') }}",
                            "sNext":     "{{ __('components/datatables.oPaginate.sNext') }}",
                            "sLast":     "{{ __('components/datatables.oPaginate.sLast') }}"
                        }
                    }
                });


                var table = $('#languages-table').DataTable({

                    processing: true,
                    serverSide: true,
                    ajax: '',
                    columns: [
                        { data: 'name', name: 'name' },
                        { data: 'created_at', name: 'created_at' },
                    ]
                });

        $(document).on('click','.save_language',function(){
            var data = $('#add_language_form').serialize();
            var url = $('#add_language_form').attr('action');
            $.post(url,data,function(data){
                    console.log(data);
            },'json').done(function(msg){  }).fail(function(xhr, status, error) {
                var errors = $.parseJSON(xhr.responseText);
                console.log(errors);
                $.each(errors.errors, function (key, val) {
                    $("#" + key + "_error").text(val[0]);
                });
            },'json') ;

        }) ;
            });
        </script>

@endpush

