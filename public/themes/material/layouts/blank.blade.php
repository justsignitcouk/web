@if(request()->ajax())
    @content()
    @else
<!DOCTYPE html>
    <html lang="en">

<head>
    {!! meta_init() !!}
    <meta name="keywords" content="@get('keywords')">
    <meta name="description" content="@get('description')">
    <meta name="author" content="@get('author')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@get('title')</title>

    @styles()

</head>

<body class="{{ $sSideBar }}">
@content()
@scripts()
@scripts('inline')
</body>
</html>
@endif

