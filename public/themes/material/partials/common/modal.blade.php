{!! Theme::asset()->themePath()->add([ ['common_modal', 'js/common/modal.js',['jquery']]]) !!}



@if(!isset($buttons))
    @php
    $buttons = ['main_button'=>['title' => 'Open dialog','id'=>'open_button' ,'class'=>[] ] ,
    'save_button'=>['title'=>'Save','id'=>'save_button' ,'class'=>[] ],
    'close_button'=>['title'=> 'Close' ,'id'=>'close_button' ,'class'=>[] ] ]
    @endphp
    @endif



<div class="@if(isset($class)) @foreach($class as $c) {{$c}} @endforeach @endif modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ isset($title) ? $title : ''  }}</h4>
            </div>
            <div class="modal-body">
                @if(isset($content))
                    @if(is_array($content))
                        @if($content['type'] == 'partial')
                            @partial($content['partial_name'])
                        @elseif($content['type']=='html')
                            {{ $content['html'] }}
                        @else
                            Unknown {{ $content['type'] }} type
                        @endif
                    @else
                        {{ $content }}
                    @endif
                    @endif
            </div>
            @if(!isset($disable_button))
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{isset($buttons['close_button']) ? $buttons['close_button']['title'] : '' }}</button>
                <button type="button" id="submit_{{ isset($modal_id) ? "id='" . $modal_id . "'"  : ''  }}"  class="btn btn-primary">{{ isset($save_button_title) ? $save_button_title : __('common.save') }}</button>
            </div>
                @endif
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var x='';
</script>