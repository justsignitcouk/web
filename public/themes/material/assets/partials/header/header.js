$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var _token = $('meta[name="csrf-token"]').attr('content') ;

    (function () {

        $.post(notificationsAjax,{ _token : _token },function(data){
            var html ='<div class="sign_item">' ;
            $('.notification_num').text(data.data.notifications.length);
            $.each(data.data.notifications, function (k, v) {

                if(v.read){
                    var li_class = 'p-3 mb-2  text-white unread_notification list_not' ;
                }else{
                    var li_class = 'p-3 mb-2  text-white read_notification list_not' ;
                }

                /*
                html +=
                    '<li class="read_notification media list_not'+li_class+'" id="read_notification" data-notification_id="'+v.notification_id+'" data-document_id="'+v.document_id+'">'+
                    '<a>'+'<div class="media-left img_not">'+
                    '<img src="' + (v.notification_id==1 ? '/assets/images/notify/complete-icon.png' : '/assets/images/notify/take-action.png') + '" class="img-circle img-sm" alt="">'+
                    '</div>'+
                    '<div class="media-body">' +
                    v.content
                '<div class="media-annotation"></div>'
                '</div>'+  '</a>'+

                '</li>'
                ;
                */

                html += '<li class="read_notification media list_not' + li_class + '" id="read_notification" data-notification_id="'+ v.notification_id + '" data-document_id="'+ v.document_id + '">\n' +
                    '<div class="media-left" style="color: black;margin: 12px 0px;">\n' +
                    (v.notification_id==1 ? '<i class="icon-file-check2"></i>\n' : '<i class="icon-pencil7"></i>\n') +
                    '</div>\n' +
                    '\n' +
                    '<div class="media-body">\n' +
                    '<a href="#">'+ v.content +'</a>\n' +
                    '<div class="media-annotation"></div>\n' +
                    '</div>\n' +
                    '</li>';

            });


            html += '</div>' ;
            $('.notification').html(html);

        },'json');


    })();

    $(document).on('click',".read_notification",function(e){

        var notification_id = $(this).data('notification_id');
        var document_id     = $(this).data('document_id');
        $.post(changeReadStatus,{'notification_id': notification_id,'document_id':document_id},function(data){
            window.location = '/document/'+document_id;
        },'json');


    });

    var url = document.location.toString();
    if (url.match('#')) {
        $('.navbar-nav a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    $('.navbar-nav a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    });

});



