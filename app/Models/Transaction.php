<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 */
class Transaction extends Model
{
    protected $table = 'transaction';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'content',
        'is_delivered',
        'is_read',
        'type',
        'type_id'
    ];

    protected $guarded = [];

        
}