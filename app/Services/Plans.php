<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class Plans extends \App\Services\Services {

    private $oServices;

    public function __construct()
    {
        parent::__construct();
        $this->oServices = Parent::getInstance();
    }

    public static function get($aParams=[]){
        $oServices = Parent::getInstance();
        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("plans")
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['errors' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return (object) $response_array['data'] ;
    }

    public static function getPlanById($plan_id){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("plans/" . $plan_id )
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['errors' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);
        return  $response_array['data'] ;
    }

    public static function getPlanByUserID($aParams=[]){
        $oServices = Parent::getInstance();

        try{
            $oClient =  $oServices
                ->setMethod('get')
                ->setUrl("plans/getFeatturesByUserId/" . nUserID() )
                ->request()
                ->getContent() ;
        }catch (\Exception $exception){
            $response_array = [
                'data' => ['errors' => $exception->getMessage()]
            ] ;
            return $response_array['data'] ;
        }

        $response_array = json_decode($oClient,true);
        return  $response_array['data'] ;
    }


}
