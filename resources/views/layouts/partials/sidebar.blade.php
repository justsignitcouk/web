
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-user">
        <div class="category-content">
            <div class="media">
                <a href="#" class="media-left"><img src="/theme/default/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                <div class="media-body">
                    <span class="media-heading text-semibold">Admin</span>
                    <div class="text-size-mini text-muted">
                        <i class="icon-pin text-size-small"></i>
                    </div>
                </div>

                <div class="media-right media-middle">
                    <ul class="icons-list">
                        <li>
                            <a href="#"><i class="icon-cog3"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
        <div class="category-content no-padding">
            <ul class="navigation navigation-main navigation-accordion">

                <!-- Main -->
                <li class="navigation-header"><span></span> <i class="icon-menu" title="Main pages"></i></li>

                {{--<li><a href="index.html"><i class="icon-home4"></i> <span>Dashboard</span></a></li>--}}
                {{--                            <li>
                                                <a href="">
                                                    <i class="icon-stack2">
                                                    </i> 
                                                    <span>Document Management</span>
                                                </a>

                                                <ul>
                                                    <li>
                                                    <a href="/documents">Documents</a></li>
                                                        <li><a href="/document_layout">Document layout</a></li>
                                                        <li><a href="/document_categories">Document Layout Categories</a>
                                                    </li>
                                                 </ul>

                                                </li>
                                                                            i>
                                                <a href=""><i class="icon-stack2"></i> <span>Work Flows</span></a>
                                                <ul>
                                                    <li><a href="/workflow/">Work Flow</a></li>
                                                    <li><a href="/stages/">Stages</a></li>
                                                </ul>
                                            </li>--}}

                {{--@can('allowed-permission', ['companies-index','companies-index'])--}}
                    <li>
                        <a href=""><i class="icon-stack2"></i><span>
                    {{ __("companies.shortname") }}
                    </span></a>
                        <ul>

                                <li><a href="/companies">{{ __("companies.shortname") }}</a></li>
                                <li><a href="/companies">{{ __("companies.branches") }}</a></li>
                                <li><a href="/companies">{{ __("companies.departments") }}</a></li>

                        </ul>
                    </li>

                {{--@endcan--}}

            {{--@if (Auth::user()->can('allowed-permission',   ['users-index','users-index']) ||--}}
            {{--Auth::user()->can('allowed-permission', ['roles-index','roles-index'])||--}}
            {{--Auth::user()->can('allowed-permission', ['permissions-index','permissions-index'])--}}
            {{--)--}}

                <li>
                    <a href=""><i class="icon-stack2"></i><span>
                    {{__("common.users_management")}}
                    </span></a>
                    <ul>
{{--                        @can('allowed-permission', ['users-index','users-index'])--}}
                        <li><a href="/admin/users/">Users</a></li>
                        <li><a href="/admin/users/">Accounts</a></li>
                        {{--@endcan--}}
{{--                        @can('allowed-permission', ['roles-index','roles-index'])--}}
                        <li><a href="/admin/roles/">Roles</a></li>
                        {{--@endcan--}}
{{--                        @can('allowed-permission', ['permissions-index','permissions-index'])--}}
                        <li><a href="/admin/permissions/">Permissions</a></li>
                        {{--@endcan--}}
                        {{--
                        @can('allowed-permission', ['accounts-all','accounts-all'])
                        <li><a href="/accounts/">Accounts</a></li>
                        @endcan
                        --}}
                    </ul>
                </li>

            {{--@endif--}}

            {{--@if (Auth::user()->can('allowed-permission', ['workflow-index','workflow-index']) ||--}}
            {{--Auth::user()->can('allowed-permission', ['workflow-stage','workflow-stage'])--}}
            {{--)--}}
                
                <li>
                    <a href=""><i class="icon-tree7"></i><span>
                    {{__("workflow.workflow.workflow_management")}}
                    </span></a>
                    <ul>
{{--                        @can('allowed-permission', ['workflow-index','workflow-index'])--}}
                        <li><a href="/admin/workflow">{{__("workflow.workflow.workflow")}}</a></li>
                        {{--@endcan--}}
{{--                        @can('allowed-permission', ['workflow-stage','workflow-stage'])--}}
                        <li><a href="/admin/state">{{__("workflow.state.index")}}</a></li>

                        {{--@endcan--}}
                    </ul>
                </li>

            {{--@endif--}}

                <li>
                    <a href=""><i class="icon-tree7"></i><span>
                    {{__("tickets.technical_support")}}
                    </span></a>
                    <ul>
                            <li><a href="http://mycompose.com/helpdesk/" target="blank">{{__("tickets.technical_support")}}</a></li>
                            <li><a href="http://mycompose.com/helpdesk/?v=staff" target="blank">{{__("tickets.submit_ticket")}}</a></li>
                    </ul>
                </li>



                    <li>
                        <a href=""><i class="icon-stack2"></i><span>
                    {{ __("document.document_management") }}
                    </span></a>
                        <ul>

                            <li><a href="#">{{ __("document.templates") }}</a></li>
                            <li><a href="#">{{ __("document.layouts") }}</a></li>
                            <li><a href="/companies">{{ __("companies.departments") }}</a></li>

                        </ul>
                    </li>

                <li>
                        <a href=""><i class="icon-stack2"></i><span>
                    {{ __("storage.storage_management") }}
                    </span></a>
                        <ul>

                            <li><a href="#">{{ __("storage.storages") }}</a></li>
                            <li><a href="#">{{ __("storage.upload_file") }}</a></li>
                            <li><a href="/companies">{{ __("storage.history") }}</a></li>

                        </ul>
                    </li>

                <li>
                    <a href=""><i class="icon-stack2"></i><span>
                    {{ __("user_manual.user_manual") }}
                    </span></a>
                    <ul>
                        <li><a href="#">{{ __("user_manual.user_manual") }}</a></li>
                    </ul>
                </li>

                <li>
                    <a href=""><i class="icon-stack2"></i><span>
                    {{ __("payment_gateway.payment_gateway") }}
                    </span></a>
                    <ul>

                        <li><a href="#">{{ __("payment_gateway.paypal") }}</a></li>
                        <li><a href="/companies">{{ __("payment_gateway.master_card") }}</a></li>
                        <li><a href="/companies">{{ __("payment_gateway.visa") }}</a></li>
                        <li><a href="/companies">{{ __("payment_gateway.visa") }}</a></li>

                    </ul>
                </li>
                <li>
                    <a href=""><i class="icon-stack2"></i><span>
                    {{ __("in_out_box.in_out_box") }}
                    </span></a>
                    <ul>

                        <li><a href="#">{{ __("in_out_box.in_out_box") }}</a></li>

                    </ul>
                </li>
             <li><a href="/languages"><i class="icon-stack2"></i> <span>{{ __("components/sidebar.languages") }}</span></a></li>
             <li><a href="/users"><i class="icon-user"></i> <span>{{ __("components/sidebar.users") }}</span></a></li>

             <li>
                 <a href=""><i class="icon-stack2"></i> <span>Settings</span></a>
                 <ul>
                     {{--<li><a href="/branchtype /">Branch Types </a></li>--}}
                     <li><a href="/admin/languages/">{{ __('sidebar.languages') }}</a></li>
                     {{--<li><a href="/countries/">Countries</a></li>--}}
                     {{--<li><a href="/logs/">Logs</a></li>--}}
                     </ul>
                 </li>
            <!-- /page kits -->

            </ul>
        </div>
    </div>
    <!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Page container -->
