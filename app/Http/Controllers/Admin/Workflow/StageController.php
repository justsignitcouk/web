<?php

namespace App\Http\Controllers\Admin\Workflow;

use App\Models\Workflow\StageState;
use App\Models\Workflow\State;
use App\Models\Workflow\Stage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class StageController extends Controller
{

    private $workflow_id;

    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workflow.stage.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $workflow_id = $request->get('workflow_id');
        if (isset($workflow_id) && $workflow_id) {
            $this->workflow_id = $workflow_id;
            return view('workflow.stage.create');
        }else{
            return redirect('workflow/'.$this->workflow_id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'stage_name'        => "required|string"
        ]);
        if($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ]);
        }

        $data = $request->all();

        $oStage = new Stage();
        $oStage->workflow_id = $data['workflow_id'];
        $oStage->name = $data['stage_name'];
        $oStage->action_type = 'In Progress';
        $oStage->description = $data['description'];
        $oStage->due_date_in_hours = '0';
        $oStage->sequence = stage::nextSequence($data['workflow_id']);
        $oStage->created_by = nUserID();
        if ($oStage->save()){
            return response()->json(['status' => true, 'message' => 'Added Successfully']);
        }

    }

    public function addState(Request $request){
        $validator = Validator::make($request->all(), [
            'state_id'        => "required|string",
            'next_stage'        => "required|string",
            'stage_id'        => "required|string",
        ]);
        if($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ]);
        }

        $oStageState = new StageState();
        $oStageState->stage_id = $request->get('stage_id');
        $oStageState->state_id = $request->get('state_id');
        $oStageState->next_stage_id = $request->get('next_stage');
        if(!$oStageState->save()){
            return response()->json(['status'=> false, 'message' => 'error: ' . $oStageState->getMessage()]);
        }
        return response()->json(['status'=> true, 'message' => 'save Successfully.']);


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0)
    {
        if ($id == 0){
            return response()->json(['status'=> false, 'message' => 'Invalid Deletion.']);
        }

        $oStage = stage::find($id);
        if ($oStage->hasRelation()){
            return response()->json(['status'=> false, 'message' => 'The Stage has relations with other stages.']);
        }
        if ($oStage->delete()){
            return response()->json(['status'=> true, 'message' => 'Deleted Successfully.']);
        }else{
            return response()->json(['status'=> false, 'message' => 'Something wrong!']);
        }
    }

    public function sortable(Request $request) {
        $stages = $request->get('stages');
        $count = sizeof($stages);
        foreach($stages as $k=>$stage) {
            $oStage = Stage::find($stage) ;
            if($oStage->action_type == 'START'){
                $oStage->sequence = 1 ;
            }elseif($oStage->action_type == 'END'){
                $oStage->sequence = 999999 ;
            }else{
                $oStage->sequence =  $k + 2 ;
            }

            $oStage->save();
        }

    }

    public function deleteStage (Request $request){
        $stage_id = $request->get('id');
        $oStage = Stage::find($stage_id);
        if(!$oStage){
            return \GuzzleHttp\json_encode(['status'=>0,'message'=>'Stage not found']);
        }
        if(!$oStage->delete()){
            return \GuzzleHttp\json_encode(['status'=>0,'message'=>"Stage can't deleted"]);
        }
        return \GuzzleHttp\json_encode(['status'=>1,'message'=>'Stage deleted!']);

    }

    public function deleteState(Request $request){
        $state_id = $request->get('id');
        $oState = StageState::find($state_id);
        if(!$oState){
            return \GuzzleHttp\json_encode(['status'=>0,'message'=>'statue not found']);
        }
        $oState->delete();
        return \GuzzleHttp\json_encode(['status'=>1,'message'=>'State deleted!']);

    }

}
