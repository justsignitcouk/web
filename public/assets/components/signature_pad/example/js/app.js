$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    savePNGButton = wrapper.querySelector("[data-action=save-png]"),
    saveSVGButton = wrapper.querySelector("[data-action=save-svg]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

    signaturePad = new SignaturePad(canvas,{   minWidth:2,
        maxWidth:3 });
    signaturePad.dotSize = 3 ;
clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

savePNGButton.addEventListener("click", function (event) {
    $('#save_draw_sign').attr("disabled", true);
    $('#save_draw_sign').prop("disabled", true);
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
        var switch_status = $('.switch').bootstrapSwitch('state') ;
        var sign_type ='';
        if(switch_status===true){
            sign_type ='Signature';
        }else{
            sign_type ='Initials';
        }

        $.post("/profile/saveSign",{'sign_data':signaturePad.toDataURL(),'sign_type':sign_type},function(data){
            $.fn.reload_signatures();
            $('#save_draw_sign').removeAttr("disabled");
            $('.modal').modal('hide');

        });
    }
});

// saveSVGButton.addEventListener("click", function (event) {
//     if (signaturePad.isEmpty()) {
//         alert("Please provide signature first.");
//     } else {
//         window.open(signaturePad.toDataURL('image/svg+xml'));
//     }
// });


    $('.modal').on('click','#signatures_draw_href',function(){

        resizeCanvas();

    }) ;

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab

        resizeCanvas();


    });

    $('.modal').on('shown.bs.modal', function() {
        resizeCanvas();

    }) ;
var color = '#000' ;
    $('.color_sign').on('click',function(){
        var color = $(this).data('value') ;
        signaturePad.penColor = color ;
    });


});