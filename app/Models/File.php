<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 */
class File extends Model
{
    protected $table = 'files';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'path',
        'hash_name',
        'hash_path',
        'is_private',
        'account_id',
        'size'
    ];

    protected $guarded = [];

        
}