$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {

                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({value: str});
                }
            });
            cb(matches);
        };
    };
    var references = ['DocumentID', 'UserID', 'Year', 'Month', 'Day',
        'Date', 'Sequence', '/', '-'
    ];

    // Attach typeahead
    $('#reference_input').tagsinput({
        allowDuplicates: true,
        freeInput: true,

    });
    var $elt = $('#reference_input').tagsinput('input');
    $elt.typeahead(
        {
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'references',
            displayKey: 'value',
            source: substringMatcher(references)
        }
    ).bind('typeahead:selected', $.proxy(function (obj, datum) {
        this.tagsinput('add', datum.value);
        this.tagsinput('input').typeahead('val', '');
    }, $('#reference_input')));


    $(".colorpicker-show-input").spectrum({
        showInput: true
    });
    // $(window).keyup(function(e){
    //     if(e.keyCode == 44){
    //         $("body").hide();
    //     }
    //
    // });
    //
    // function copyToClipboard() {
    //     // Create a "hidden" input
    //     var aux = document.createElement("input");
    //     // Assign it the value of the specified element
    //     aux.setAttribute("value", "Você não pode mais dar printscreen. Isto faz parte da nova medida de segurança do sistema.");
    //     // Append it to the body
    //     document.body.appendChild(aux);
    //     // Highlight its content
    //     aux.select();
    //     // Copy the highlighted text
    //     document.execCommand("copy");
    //     // Remove it from the body
    //     document.body.removeChild(aux);
    //     alert("Print screen desabilitado.");
    // }

    // $(window).keyup(function(e){
    //     if(e.keyCode == 44){
    //         copyToClipboard();
    //     }
    // });


    // function addLink(event) {
    //     event.preventDefault();
    //
    //     var pagelink = '<img src="/assets/images/dont-copy.png" />' ,
    //         copytext =   pagelink;
    //
    //     if (window.clipboardData) {
    //         window.clipboardData.setData('Text', copytext);
    //     }
    // }

    // document.addEventListener('copy', addLink);

    $("body").on("contextmenu", "img", function (e) {
        return false;
    });

    $(".switch").bootstrapSwitch();


    /*********************** uploader *****************************/
    var uploaderId = 'my_uploader_id';
    var $uploader = $('#uploader-' + uploaderId);

    if (!$uploader || !$uploader.length) {
        alert('Cannot find uploader');
    }

    var $filelist = $uploader.find('.filelist'),
        $uploaded = $uploader.find('.uploaded'),
        $uploadAction = $uploader.find('.upload-actions'),
        $uploadBtn = $('#' + $uploader.data('uploadbtn')) || false,
        options = $uploader.data('options') || {},
        autoStart = $uploader.data('autostart') || false,
        deleteUrl = $uploader.data('deleteurl') || false,
        deleteMethod = $uploader.data('deletemethod') || 'DELETE';


    defaultOptions = {
        init: {
            PostInit: function (up) {
                if (!autoStart && $uploadBtn) {
                    $uploadBtn.click(function () {
                        up.start();
                        return false;
                    });
                }
            },

            FilesAdded: function (up, files) {
                $.each(files, function (i, file) {
                    $filelist.append(
                        '<div id="' + file.id + '" class="alert alert-file">' +
                        '<div class="filename hide">' + file.name + ' (' + plupload.formatSize(file.size) + ')  <button type="button" class="close cancelUpload">&times;</button></div>' +
                        '<div class="progress progress-striped"><div class="progress-bar" style="width: 0;"></div></div></div>');

                    $filelist.on('click', '#' + file.id + ' button.cancelUpload', function () {
                        var $this = $(this),
                            $file = $('#' + file.id),
                            deleteUrl = $this.data('deleteurl') || false,
                            id = $this.data('id') || false;

                        if (deleteUrl) {
                            $.ajax({
                                dataType: 'json',
                                type: deleteMethod,
                                url: deleteUrl,
                                data: options.multipart_params,
                                success: function (result) {
                                    if (result.success) {
                                        up.removeFile(file);
                                        $file.remove();
                                        $('#' + file.id + '-hidden').remove();
                                        $uploadAction.show();
                                    }
                                    else {
                                        $('#' + file.id).append('<span class="text-danger">' + result.message + '</span>');
                                    }
                                }
                            });
                        }
                        else {
                            $uploadAction.show();
                            $file.remove();
                            $('#' + file.id + '-hidden').remove();
                            up.removeFile(file);
                        }
                    });
                });
                up.refresh(); // Reposition Flash/Silverlight
                if (autoStart) {
                    //$uploadAction.hide();
                    up.start();
                }
            },

            UploadProgress: function (up, file) {
                //$uploadAction.hide();

                $('#' + file.id + ' .progress').addClass('active');
                $('#' + file.id + ' button.cancelUpload').hide();
                $('#' + file.id + ' .progress .progress-bar').animate({width: file.percent + '%'}, 100, 'linear');
            },

            Error: function (up, err) {
                $filelist.append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    'Error: ' + err.code + ', Message: ' + err.message +
                    (err.file ? ', File: ' + err.file.name : '') +
                    "</div>"
                );
                up.refresh(); // Reposition Flash/Silverlight
            },

            FileUploaded: function (up, file, info) {
                var response = JSON.parse(info.response);

                $('#' + file.id + ' .progress .progress-bar').animate({width: '100%'}, 100, 'linear');
                $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').fadeOut();
                $('#' + file.id + ' .filename').removeClass('hide').show();
                $('#' + file.id + ' button.cancelUpload').show();

                if (response.result.id) {
                    $('#' + file.id + ' button.cancelUpload').attr('data-id', response.result.id);
                    $('<input type="hidden" name="' + uploaderId + '_files[]" value="' + response.result.id + '" id="' + file.id + '-hidden">').appendTo($uploader);
                }

                if (response.result.deleteUrl) {
                    $('#' + file.id + ' button.cancelUpload').attr('data-deleteurl', response.result.deleteUrl);
                }

                if (response.result.url) {
                    $('#' + file.id).append('<img src="' + response.result.url + '" class="img-responsive img-thumbnail" />');
                }
                $filelist.html('');
                up.splice();
                up.refresh();
                $.fn.reload_signatures();
                $('.modal').modal('hide');
            }
        }
    };

    $.extend(options, defaultOptions);

    var uploader = new plupload.Uploader(options);

    uploader.bind('BeforeUpload', function (up, file) {
        up.settings.multipart_params.sign_type = $('.switch').bootstrapSwitch('state') === true ? 'Signature' : 'Initials';
    });

    uploader.init();


    /*********************** uploader *****************************/

    /****************** change pic ******************************/

    // /*********************** uploader *****************************/
    // var uploaderId = 'change_pic' ;
    // var $uploader = $('#uploader-' + uploaderId);
    //
    // if (!$uploader || !$uploader.length) {
    //     alert('Cannot find uploader');
    // }
    //
    // var $filelist = $uploader.find('.filelist'),
    //     $uploaded = $uploader.find('.uploaded'),
    //     $uploadAction = $uploader.find('.upload-actions'),
    //     $uploadBtn = $('#' + $uploader.data('uploadbtn')) || false,
    //     options = $uploader.data('options') || {},
    //     autoStart = $uploader.data('autostart') || false,
    //     deleteUrl = $uploader.data('deleteurl') || false,
    //     deleteMethod = $uploader.data('deletemethod') || 'DELETE';
    //
    //
    //
    // defaultOptions = {
    //     init: {
    //         PostInit: function(up) {
    //             if (!autoStart && $uploadBtn) {
    //                 $uploadBtn.click(function() {
    //                     up.start();
    //                     return false;
    //                 });
    //             }
    //         },
    //
    //         FilesAdded: function(up, files) {
    //             $.each(files, function(i, file){
    //                 $filelist.append(
    //                     '<div id="' + file.id + '" class="alert alert-file">' +
    //                     '<div class="filename hide">' + file.name + ' (' + plupload.formatSize(file.size) + ')  <button type="button" class="close cancelUpload">&times;</button></div>' +
    //                     '<div class="progress progress-striped"><div class="progress-bar" style="width: 0;"></div></div></div>');
    //
    //                 $filelist.on('click', '#' + file.id + ' button.cancelUpload', function() {
    //                     var $this = $(this),
    //                         $file = $('#' + file.id),
    //                         deleteUrl = $this.data('deleteurl') || false,
    //                         id = $this.data('id') || false;
    //
    //                     if (deleteUrl) {
    //                         $.ajax({
    //                             dataType: 'json',
    //                             type: deleteMethod,
    //                             url: deleteUrl,
    //                             data: options.multipart_params,
    //                             success: function(result) {
    //                                 if (result.success) {
    //                                     up.removeFile(file);
    //                                     $file.remove();
    //                                     $('#' + file.id + '-hidden').remove();
    //                                     $uploadAction.show();
    //                                 }
    //                                 else {
    //                                     $('#' + file.id).append('<span class="text-danger">' + result.message + '</span>');
    //                                 }
    //                             }
    //                         });
    //                     }
    //                     else {
    //                         $uploadAction.show();
    //                         $file.remove();
    //                         $('#' + file.id + '-hidden').remove();
    //                         up.removeFile(file);
    //                     }
    //                 });
    //             });
    //             up.refresh(); // Reposition Flash/Silverlight
    //             if (autoStart) {
    //                 //$uploadAction.hide();
    //                 up.start();
    //             }
    //         },
    //
    //         UploadProgress: function(up, file) {
    //             //$uploadAction.hide();
    //
    //             $('#' + file.id + ' .progress').addClass('active');
    //             $('#' + file.id + ' button.cancelUpload').hide();
    //             $('#' + file.id + ' .progress .progress-bar').animate({width: file.percent + '%'}, 100, 'linear');
    //         },
    //
    //         Error: function(up, err) {
    //             $filelist.append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' +
    //                 'Error: ' + err.code + ', Message: ' + err.message +
    //                 (err.file ? ', File: ' + err.file.name : '') +
    //                 "</div>"
    //             );
    //             up.refresh(); // Reposition Flash/Silverlight
    //         },
    //
    //         FileUploaded: function(up, file, info) {
    //             var response = JSON.parse(info.response);
    //
    //             $('#' + file.id + ' .progress .progress-bar').animate({width: '100%'}, 100,  'linear');
    //             $('#' + file.id + ' .progress').removeClass('progress-striped').removeClass('active').fadeOut();
    //             $('#' + file.id + ' .filename').removeClass('hide').show();
    //             $('#' + file.id + ' button.cancelUpload').show();
    //
    //             if (response.result.id) {
    //                 $('#' + file.id + ' button.cancelUpload').attr('data-id', response.result.id);
    //                 $('<input type="hidden" name="' + uploaderId + '_files[]" value="' + response.result.id + '" id="' + file.id + '-hidden">').appendTo($uploader);
    //             }
    //
    //             if (response.result.deleteUrl) {
    //                 $('#' + file.id + ' button.cancelUpload').attr('data-deleteurl', response.result.deleteUrl);
    //             }
    //
    //             if (response.result.url) {
    //                 $('#' + file.id).append('<img src="' + response.result.url + '" class="img-responsive img-thumbnail" />');
    //             }
    //             $filelist.html('') ;
    //             up.splice();
    //             up.refresh();
    //         }
    //     }
    // };
    //
    // $.extend(options, defaultOptions);
    //
    // var uploader = new plupload.Uploader(options);
    //
    // uploader.bind('BeforeUpload', function(up, file) {
    //     up.settings.multipart_params.sign_type =  $('.switch').bootstrapSwitch('state')===true ? 'Signature' : 'Initials' ;
    // });
    //
    // uploader.init();
    //
    //
    // /*********************** uploader *****************************/
    //


    /****************** change pic ******************************/



    $.fn.block_panel('.panel-signatures');
    $.fn.block_panel('.panel-initials');


    $.fn.reload_signatures = function () {

        uploader.splice();
        uploader.refresh();


        $.fn.block_panel('.panel-signatures');
        $.fn.block_panel('.panel-initials');
        $.post("profile/signatures", {}, function (data) {

            $('.panel-signatures').html('');
            $('.panel-initials').html('');


            if (data.signs.length) {
                var count = data.signs.length;
                $.each(data.signs, function (index, element) {
                    $('.panel-signatures').append("<div  class='col-md-6'> <div class='thumbnail'><img class='img-rounded' style='height: 180px;width: auto;' src='" + element.sign_data_with_watermark + "' width=100% /><div class='caption'><a href=''><i class='glyphicon glyphicon-trash delete_sign' data-id='" + element.id + "'></i></a></div></div></div>");
                    if (!--count) {
                        $.fn.unblock_panel('.panel-signatures');
                    }
                });
            } else {
                $('.panel-signatures').append("<div  class='col-md-12'><p>" + profile_no_signature + "</p></div>");
                $.fn.unblock_panel('.panel-signatures');
            }
            if (data.initials.length) {
                var count2 = data.initials.length;
                $.each(data.initials, function (index, element) {
                    $('.panel-initials').append("<div  class='col-md-6'> <div class='thumbnail'><img class='img-rounded' style='height: 180px;width: auto;' src='" + element.sign_data_with_watermark + "' width=100% /><div class='caption'><a href=''><i class='glyphicon glyphicon-trash delete_sign' data-id='" + element.id + "'></i></a></div></div></div>");

                    if (!--count2) {
                        $.fn.unblock_panel('.panel-initials');
                    }
                });
            } else {
                $('.panel-initials').append("<div  class='col-md-12'><p>" + profile_no_initials + "</p></div>");
                $.fn.unblock_panel('.panel-initials');
            }
            console.log(data.signs);
        }, 'json');


    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if (target == '#signatures') {
            $.fn.reload_signatures();

        }
    });


    $(document).on('click', '.delete_sign', function (e) {
        $.fn.block_panel('.panel-signatures');
        $.fn.block_panel('.panel-initials');
        var sign_id = $(this).data('id');
        var c = confirm("Do you want delete this signature?");
        if (c) {
            $.post(profile_deleteSign, {'sign_id': sign_id}, function (data) {
                $.fn.reload_signatures();
            });
        }
        $.fn.reload_signatures();

        e.preventDefault();
    });


    $(document).on("click", ".make_default", function (e) {

        $(this).closest(".panel-accounts").find(".panel").removeClass("panel-success");
        $(this).closest(".panel-accounts").find(".panel").addClass("panel-primary");
        $(this).closest(".panel").removeClass("panel-primary");
        $(this).closest(".panel").addClass("panel-success");

        $(document).find(".make_default").removeClass('hidden');
        $(this).addClass('hidden');

        var account_id = $(this).closest(".panel").data("id");
        var dataPath = {
            id: account_id
        };
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
            }
        });
        $.ajax({
            url: account_make_default,
            type: "POST",
            data: dataPath
        }).done(function (data) {
            if (data.status == true) {
                $.post(account_make_default, {"id": account_id}, function () {

                });

            }
            location.reload();
        });
        new PNotify({
            title: "Success saved.",
            text: "Account made default.",
            icon: "icon-checkmark3",
            type: "success"
        });
        e.preventDefault();
    });


});
