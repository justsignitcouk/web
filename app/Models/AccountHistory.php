<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountHistory
 */
class AccountHistory extends Model
{
    protected $table = 'account_history';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'activation_date',
        'deactivation_date',
        'notes',
        'created_by'
    ];

    protected $guarded = [];

        
}