<div class="panel-toolbar panel-toolbar-inbox">
    <div class="navbar navbar-default">
        <ul class="nav navbar-nav visible-xs-block no-border">
            <li>
                <a class="text-center collapsed legitRipple" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                    <i class="icon-circle-down2"></i>
                </a>
            </li>
        </ul>

        <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
            <div class="btn-group navbar-btn">
                <button data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction" type="button" class="btn btn-default btn-icon btn-checkbox-all legitRipple">
                    <div class="checker"><span><input type="checkbox" class="styled"></span></div>
                </button>

                <button data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction" type="button" class="btn btn-default btn-icon dropdown-toggle legitRipple" data-toggle="dropdown">
                    <span class="caret"></span>
                </button>

                <ul class="dropdown-menu" data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction">
                    <li><a href="#">Select all</a></li>
                    <li><a href="#">Select read</a></li>
                    <li><a href="#">Select unread</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Clear selection</a></li>
                </ul>
            </div>

            <div class="btn-group navbar-btn">
                {{--<a href="{{route('document.compose')}}" class="btn btn-default legitRipple"><i class="icon-pencil7"></i> <span class="hidden-xs position-right">Compose</span></a>--}}
                <button type="button" class="btn btn-default legitRipple" data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction"><i class="icon-bin"></i> <span class="hidden-xs position-right"  data-i18n="toolbar.delete"></span></button>
                <button type="button" class="btn btn-default legitRipple" data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction"><i class="icon-spam"></i> <span class="hidden-xs position-right"  data-i18n="toolbar.spam"></span></button>
            </div>

{{--            <div class="navbar-right">
                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div>

             --}}{{--   <div class="btn-group navbar-btn" data-popup="popover-custom" data-trigger="hover" data-placement="top" title="" data-content="after 16/9/2017" data-original-title="under construction">
                    <button type="button" class="btn btn-default dropdown-toggle legitRipple" data-toggle="dropdown">
                        <i class="icon-cog3"></i>
                        <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">One more line</a></li>
                    </ul>
                </div>--}}{{--
            </div>--}}
        </div>
    </div>
</div>
