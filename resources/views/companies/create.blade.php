@extends('layouts.admin')

@section('content')

   <div class="col-md-12">

        {!! Form::open(['route'=>'companies.store','files'=>'true','enctype'=>'multipart/form-data']) !!}

            @include('companies.form')
           
        {!! Form::close() !!}
        
    </div>

@endsection
