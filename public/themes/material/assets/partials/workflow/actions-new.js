$(document).ready(function () {


    $(document).on('click', '.insert_sign', function (e) {

        var insert_sign = $(this);
        $.post('/' + globals.unique_name + '/default_signatures', {'json': true}, function (data) {
            if(data.sign.length==0){

                swal({
                    title: "Oops...",
                    text: i18n.t("Please define your signature in your profile page!"),
                    confirmButtonColor: "#EF5350",
                    type: "error"
                });
                return false;
            }

            $('.signer_user_item_' + globals.sIdentifiedS).find('.insert_sign').remove();
            $('.signer_user_item_' + globals.sIdentifiedS).append('<img src="' + data.sign.sign_data + '" class="signer_sign_img" />');




            $('.insert_sign').after("<a href=\"\" id=\"cancel\"\n" +
                "                               class=\"no-smoothState link-icon details-document details_button tooltip-tools\" data-popup=\"tooltip\"\n" +
                "                               title=\"Cancel\" data-placement=\"bottom\" data-original-title=\"Cancel\"><i\n" +
                "                                        class=\"fa fa-remove\"></i></a>") ;

            $('.insert_sign').data('original-title','Send');
            $('.insert_sign').removeClass('insert_sign').addClass('action_button').html('<i\n' +
                '                                        class="fa fa-send"></i>');

            $('.action_button').closest('form').append('<input type="hidden" name="sign_id" value="' + data.sign.id + '" />');
        }, 'json');


        e.preventDefault();

    });

$(document).on('click','#cancel',function () {
    location.reload();
});

    function editor() {
        window.location = '/document/' + document_id + '/edit';
    }

    function open_dialog_comments() {
        $('#global_modal').modal('show');
    }

    function open_dialog_forword() {

        //$('#global_modal').modal('show');
        $.fn.enable_forward_tools();

    }

    $.fn.enable_forward_tools = function () {
        $('body').addClass('add_note_cursor');
        var note_add = false;
        var recipt_add = true;

        if (recipt_add) {
            recipt_add = false;

            $('.action_button').closest('.sidebar-category-actions').before("<div class=\"sidebar-category-actions\">\n" +
                "    <div class=\"category-title\">\n" +
                "        <span data-i18n=\"sec_sidbar.recipient\" >recipient</span>\n" +
                "        <ul class=\"icons-list\">\n" +
                "            <li><a href=\"#\" data-action=\"collapse\">  </a></li>\n" +
                "        </ul>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"category-content\">\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-xs-12\">\n" +
                "\n" +
                "\n" +
                "                    <input type=\"text\" class=\"form-control typeahead\" name=\"recipient\" id=\"recipient_input\" value=\"\"\n" +
                "                           data-provide=\"typeahead\" placeholder=\"\"  data-i18n=\"[placeholder]sec_sidbar.recipient\" />\n" +
                "\n" +
                "\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"category-content\">\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-xs-12\">\n" +
                "                <ul class=\"media-list recipient-list\" id=\"recipient-list\">\n" +
                "\n" +
                "                </ul>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>");
            $('.action_button').after("<a href='' class='btn btn-default' style='margin-left: 10px;'>Cancel</a>") ;

            loadScript("/themes/material/assets/partials/document/recipient.js", function () {
                //initialization code
                $.fn.without_content = false;
            });
        }

        $('.A4').on('click', function (e) {
            $('body').removeClass('add_note_cursor');
            if (!note_add) {
                note_add = true;

                var elment = e.target.id;
                if (elment.indexOf("note_") >= 0 || $(e.target).parents(".note2").length) {
                } else {
                    var count = $(this).find('.note2').length;
                    count++;
                    $(this).append("<div class='editable_note note2' id='note_" + count + "' contentEditable='true' style='top: 0px;left: 0px;'></div>");

                    var x = (e.pageX - this.offsetLeft - 150) + 'px', y = (e.pageY - this.offsetTop - 50) + 'px';
                    $("#note_" + count).css({
                        "position": "absolute",
                        "top": y,
                        "left": x
                    });

                    $('#note_' + count).draggable({containment: "parent"}).on('dragstart',
                        function () {
                            $(this).zIndex(++noteZindex);
                        }).on('dragstop', function () {
                        var top = $(this).css('top');
                        var bottom = $(this).css('bottom');
                        var left = $(this).css('left');
                        var right = $(this).css('right');
                        var positions = 'top:' + top + ';bottom:' + bottom + ';left:' + left + ';right:' + right + ';';
                        var id = $(this).data('id');

                        // $.post('/document/notes/savePosition',{'positions':positions,'note_id':id},function(data){
                        //     console.log(data);
                        // },'json');

                    });


                }
            }

            $('.action_button').html('Send');

            $('.action_button').addClass('forward_action_button').removeClass('action_button');


        });
    }

    $(document).on('click','.forward_action_button',function(e){
        var user_forward = [] ;
        $(document).find('.recipient-user').each(function(){
            var id = $(this).data('id');
            var id_css = id.replace("@", "___");
            id_css = id_css.split('.').join('---');
            id_css = id_css.split(' ').join('');
            id_css = id_css.split('+').join('');

                user_forward.push(id_css);

        });

        if(user_forward.length==0){
            new PNotify({
                title: 'Please enter one recipient at least.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            return false;
        }

        //contact_forward
        var note_content = $('.editable_note').html();
        if(note_content==''){
            new PNotify({
                title: 'Please enter content.',
                text: '',
                icon: 'icon-checkmark3',
                type: 'warning'
            });

            return false;
        }
        var note_content_position = $('.editable_note').attr('style');


        $.post("/document/forward",{
            'note_content':note_content,
            'note_content_position':note_content_position,
            'document_id':document_id,
            'forward_id':forward_id,
            'user_forward': user_forward
        },function(data){
            console.log(data);
             window.location = '/document/forward/' + data.forward_id;
        },'json');

        e.preventDefault();


    }) ;


    function loadScript(url, callback) {

        var script = document.createElement("script")
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);

    }

    $.fn.printDocument = function () {
        printJS('/document/pdf/' + document_id);
    }

    $(document).on('click', '.action_button', function (e) {
        var data = $(this).closest('form').serializeArray();
        var action_id = $(this).data('action_id');
        var stage_id = $(this).data('stage_id');
        var state_id = $(this).data('state_id');
        data.push({name: 'action_id', value: action_id});
        data.push({name: 'stage_id', value: stage_id});
        data.push({name: 'state_id', value: state_id});
        data.push({name: 'document_id', value: document_id});

        $.post(workflow_save_action, data, function (data) {

            if (data.action == 'reload') {
                location.reload();
            }

            if (data.action == 'system') {
                if (data.action_type == 'to_next_stage' || data.action_type == 'sign') {
                    location.reload();
                    return true;
                }

                eval(data.javascript);
                $('#global_modal').find('#global_modal_form').html(data.frontend);
                if (data.modal !== undefined) {
                    $('#global_modal').find('.modal-title').html(data.modal.title);
                    $('#global_modal').find('.save-action').html(data.modal.buttons.send);
                    $('#global_modal').find('.save-action').data('action_id', action_id);
                    $('#global_modal').find('.save-action').data('state_id', state_id);
                    $('#global_modal').find('.save-action').data('stage_id', stage_id);
                }

            }

        }, 'json');

        e.preventDefault();
    });

    $(document).on('click', '.save-action', function (e) {
        var action_id = $(this).data('action_id');
        var stage_id = $(this).data('stage_id');
        var state_id = $(this).data('state_id');

        var data = $('#global_modal_form').serializeArray();
        data.push({name: 'action_id', value: action_id});
        data.push({name: 'state_id', value: state_id});
        data.push({name: 'stage_id', value: stage_id});
        data.push({name: 'submit', value: true});
        data.push({name: 'document_id', value: document_id});

        $.post(workflow_save_action, data, function (data) {
            console.log(data);
            if (data.action == 'reload') {
                location.reload();
            }

            if (data.action == 'system') {
                if (data.action_type == 'to_next_stage') {
                    location.reload();
                    return true;
                }
                eval(data.javascript);
                $('#global_modal').find('.modal-body').html(data.frontend);
                if (data.modal !== undefined) {
                    $('#global_modal').find('.modal-title').html(data.modal.title);
                    $('#global_modal').find('.save-action').html(data.modal.buttons.send);
                    $('#global_modal').find('.save-action').data('id', action_id);
                    $('#global_modal').find('.save-action').data('state_id', state_id);
                    $('#global_modal').find('.save-action').data('stage_id', stage_id);
                }

            }

        }, 'json');
        e.preventDefault();
    });



});