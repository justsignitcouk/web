<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Requests\setDefaultAccount;
use App\Services\Account;
use App\Services\Plans;
use App\Services\Services;
use Facuz\Theme\Facades\Theme;
use Illuminate\Http\Request;

class PlansController extends Controller
{

    public function ShowUpgradeForm(){
        $oPlans = Plans::get();
        return Theme::view('plans.upgrade', ['oPlans'=>$oPlans]);

    }

    public function ShowPaymentForm(){
        $oPlans = Plans::get();
        return Theme::view('plans.payment', ['oPlans'=>$oPlans]);

    }

    public function saveStipeCardToken(Request $request){

        return \GuzzleHttp\json_encode($request->all());
    }


}
