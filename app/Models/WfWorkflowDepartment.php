<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkflowDepartment
 */
class WorkflowDepartment extends Model
{
    protected $table = 'wf_workflow_department';

    public $timestamps = false;

    protected $fillable = [
        'workflow_id',
        'department_id'
    ];

    protected $guarded = [];

        
}