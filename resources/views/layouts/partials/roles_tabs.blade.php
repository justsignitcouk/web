<ul class="nav nav-tabs nav-justified">

        @can('show_role', App\Models\Role::class)
		<li class="@if (!empty($active_show) &&  $active_show == 1)) {{'active'}} @endif">
			<a href='{{url("roles/$role[id]")}}'>
			{{ __("common.show") }}
			</a>
		</li>
		@endcan

        @can('edit_role', App\Models\Role::class)
		<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
			<a href='{{url("roles/$role[id]/edit")}}' >
				{{ __("common.edit") }}
			</a>
		</li>
		@endcan

</ul>