<ul class="nav nav-tabs nav-justified">

     
        @can('allowed-permission', ['accounts-details','accounts-details'])
     
		<li class="@if (!empty($active_show) &&  $active_show == 1)) {{'active'}} @endif">
			<a href='{{url("accounts/$account->id")}}'>
			{{ __("common.details") }}
			</a>
		</li>

		@endcan


        @can('allowed-permission', ['accounts-edit','accounts-edit'])
		<li class="@if (!empty($active_edit) &&  $active_edit == 1)) {{'active'}} @endif">
			<a href='{{url("accounts/$account->id/edit")}}' >
				{{ __("common.edit") }}
			</a>
		</li>
		@endcan

	
	    @can('allowed-permission', ['accounts-roles','accounts-roles'])
		<li class="@if (!empty($active_roles) &&  $active_roles == 1)) {{'active'}} @endif">
			<a href='{{url("accounts/$account->id/roles")}}' >
				{{ __("common.roles") }}
			</a>
		</li>
		@endcan

        @can('allowed-permission', ['accounts-permissions','accounts-permissions'])
		<li class="@if (!empty($active_permissions) &&  $active_permissions == 1)) {{'active'}} @endif">
			<a href='{{url("accounts/$account->id/permissions")}}' >
				{{ __("common.permissions") }}
			</a>
		</li>
		@endcan
	
		

</ul>